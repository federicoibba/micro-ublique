import React, { Suspense } from 'react'
import ReactDOM from 'react-dom'
import './index.scss'
import App from './App'
import createStore from 'utils/hoc/createStore'
import { globalStore } from './services/state'
import Loading from '@ublique/components/Loading'
import ErrorBoundary from './components/ErrorBoundary'
import initI18n from './i18n'

initI18n()

const UbliqueContainer = createStore(App, {
  ...globalStore
})

ReactDOM.render(
  <React.StrictMode>
    <Suspense fallback={<Loading />}>
      <ErrorBoundary>
        <UbliqueContainer />
      </ErrorBoundary>
    </Suspense>
  </React.StrictMode>,
  document.getElementById('root')
)
