import PasswordManagement from 'components/PasswordManagement'
import DebugMsgModal from 'components/ErrorsHandler/DebugMsgModal'

export const isDevMode = process.env.NODE_ENV === 'development'

export const defaultThemes = [
  {
    id: 'light',
    label: 'Light'
  },
  {
    id: 'dark',
    label: 'Dark'
  }
]

export const platformModals = {
  password: PasswordManagement,
  // import: Import,
  // search: Search,
  // saveDocument: ModalSaveDocument,
  // confirmAction: ModalConfirmAction,
  // documentDeletionErrors: ModalDeletionError,
  // scenarioReset: ModalScenarioReset,
  debugMessage: DebugMsgModal
  // filterList: ModalFilterList,
  // filterSave: ModalSaveFilter
}
