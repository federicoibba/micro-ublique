import { LOCALSTORAGE_THEME_KEY, DEFAULT_THEME } from 'config'

export const getTheme = () => {
  return localStorage.getItem(LOCALSTORAGE_THEME_KEY)
}

export const getInitialTheme = () => {
  return getTheme() || DEFAULT_THEME
}

export const setLocalStorageTheme = (theme) => {
  return localStorage.setItem(LOCALSTORAGE_THEME_KEY, theme)
}

export const mapInstance = (instance) => ({
  ...instance,
  sidebar: true,
  sidebarTitle: instance.title,
  href: `/instances/${instance.id}/workingtables`
})

export const setConfigSidebar = (extRoutes, outputConfig, scenarioConfig) => {
  const routes = [...extRoutes]

  const { sidebar: outputSidebar = false } = outputConfig
  const { sidebar: scenarioSidebar = false } = scenarioConfig

  const output = routes.find((route) => route.id === 'output')
  const scenario = routes.find((route) => route.id === 'scenario')

  output.sidebar = outputSidebar
  output.routes[0].sidebar = outputSidebar

  scenario.sidebar = scenarioSidebar

  return routes
}
