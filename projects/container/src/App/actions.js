import { filterStore, instancesStore, scenarioStore, sidebarStore } from 'state'
import { getInstances } from 'api/instances'
import { canRead } from 'services/auth'
import { fetchConfig } from 'services/api/config'
import { mapInstance } from './utils'
import { fetchMyPermissions } from 'services/api/scenarios'

export async function getInstancesList() {
  const instances = await getInstances()

  if (!instances || !instances.body) {
    instancesStore.set([])
    return
  }

  setInstances(instances)
}

export const setInstances = (data) => {
  const {
    body: { items }
  } = data

  const instances = items
    .filter(({ permission }) => canRead(permission))
    .map(mapInstance)

  instancesStore.set(instances)
}

const updateFilterStore = (updaterFn) => {
  filterStore.update(updaterFn)
}

const resetFilterStore = () => {
  filterStore.set({})
}

const setFilterStore = (value) => {
  filterStore.set(value)
}

export const FiltersActions = {
  update: updateFilterStore,
  set: setFilterStore,
  reset: resetFilterStore
}

export const fetchAppConfig = () => {
  const configs = ['model', 'scenario', 'output']
  return Promise.all(configs.map(fetchConfig))
}

export const getMyScenarioPermissions = async () => {
  const permissions = await fetchMyPermissions()

  scenarioStore.update((prev) => ({ ...prev, permissions }))
}

const setSidebarStore = (value) => {
  sidebarStore.set(value)
}

export const resetSidebarStore = () => sidebarStore.set({ items: [] })

export const __unsafeSidebarActions = {
  set: setSidebarStore,
  reset: resetSidebarStore
}
