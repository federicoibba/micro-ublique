import { lazy } from 'react'

const Home = lazy(() => import('pages/Home/index.js'))
const Login = lazy(() => import('pages/Login/index.js'))
const ScenarioLazy = lazy(() => import('pages/microScenario.js'))
const PageNotFound = lazy(() => import('pages/PageNotFound/index.js'))
const Output = lazy(() => import('pages/MicroOutput.js'))
//const AnagraficaLazy = lazy(() => import('pages/MicroAnagrafica.js'))
import AnagraficaLazy from 'pages/MicroAnagrafica.js'

export const PATH_UBLIQUE_HEADER = {
  INSTANCES: 'instances',
  SCENARIO: 'scenario'
}

const defaultRoutes = [
  {
    title: 'Gestione scenari',
    id: PATH_UBLIQUE_HEADER.SCENARIO,
    path: '/scenario/:detailView',
    component: ScenarioLazy,
    href: '/scenario/management',
    extraProps: [
      'scenarioActionsConfig',
      'scenarioAllowedStatusForCloning',
      'scenarioWizardComponents'
    ],
    exact: true,
    sidebar: false,
    filterBar: true,
    showSidebarSearch: false,
    header: true,
    contentPadding: false
  },
  {
    title: 'Instances',
    id: PATH_UBLIQUE_HEADER.INSTANCES,
    path: '/instances/*',
    //href: '/instances/:instanceId',
    component: AnagraficaLazy,
    //exact: true,
    sidebar: false,
    filterBar: false,
    showSidebarSearch: false,
    header: true,
    contentPadding: false
    /*  routes: [
      {
        id: 'document',
        path: '/:workingTableId/:type/documents',
        sidebar: true,
        expandSidebar: true,
        component: Anagrafica,
        title: 'Documents',
        extraProps: ['defaultValueGetter'],
        breadcrumbsType: 'inputView' // TODO: da definire meglio
      }
    ] */
  },
  {
    title: 'Output',
    id: 'output',
    path: '/output/:scenarioId',
    component: Output,
    sidebar: false,
    header: false,
    expandSidebar: false,
    contentPadding: false,
    showSidebarSearch: false,
    filterBar: true,
    extraProps: ['outputConfigComponents'],
    excludeToHeader: true
  },
  /*
  {
    title: 'Translations',
    id: 'translations',
    path: '/translations/translationTables',
    sidebar: true,
    header: false,
    component: Translations,
    expandSidebar: true,
    routes: [
      {
        id: 'translationTable',
        path: '/:group/:type/documents',
        sidebar: true,
        expandSidebar: true,
        component: TranslationsTable,
        title: 'TranslationDocuments',
        breadcrumbsType: 'translationsView' // TODO: da definire meglio
      }
    ]
  },
  {
    title: 'Output',
    id: 'output',
    path: '/output/:scenarioId',
    component: Output,
    sidebar: false,
    header: false,
    expandSidebar: false,
    contentPadding: false,
    showSidebarSearch: false,
    filterBar: true,
    extraProps: ['outputConfigComponents'],
    routes: [
      {
        id: 'outputView',
        path: '/:view',
        sidebar: false,
        filterBar: true,
        expandSidebar: false,
        contentPadding: false,
        showSidebarSearch: false,
        component: OutputView,
        title: 'OutputView',
        extraProps: ['outputConfigComponents'],
        contentWrapperClassname: 'output-view-main-content'
      }
    ]
  },
  {
    id: 'admin',
    path: '/administration-permissions',
    sidebar: false,
    exact: true,
    header: false,
    expandSidebar: false,
    component: Administration
  },*/
  {
    id: 'login',
    path: '/login',
    public: true,
    component: Login,
    extraProps: ['languagesMenuConfig', 'fallbackRedirectPath'],
    shell: false,
    contentPadding: false,
    excludeToHeader: true
  },
  {
    id: 'homepage',
    path: '/',
    component: Home,
    title: 'Home',
    exact: true,
    extraProps: ['fallbackRedirectPath'],
    href: '/',
    header: true,
    sidebar: false,
    excludeToHeader: true
  },
  {
    id: '404',
    path: '*',
    public: true,
    component: PageNotFound,
    header: true,
    sidebar: false,
    excludeToHeader: true
  }
]

export default defaultRoutes
