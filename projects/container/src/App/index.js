import React, { Fragment, useCallback, useEffect, useMemo } from 'react'
import Theme from '@ublique/components/Theme'
import RouterX from 'components/Router'
import withStore from 'hoc/withStore'
import isUserAuthorized from 'services/auth/isUserAuthorized'
import { appConfigStore, appReadyStore, synapse, themeStore } from 'services/state'
import { setLocalStorageTheme } from './utils'
import userActionMenu from './panel-actions'
import { useTranslation } from 'react-i18next'
import { defaultThemes, platformModals } from './constants'
import Settings from '../components/Settings'
import defaultRoutes from './routes'
import { isEmpty } from 'lodash'
import ErrorsHandler from 'components/ErrorsHandler'
import NotificationsHandler from 'components/Notifications'
import { BASE_URL } from 'config'
import { fetchAppConfig, getInstancesList, getMyScenarioPermissions } from './actions'
import useStorageSession from '../services/auth/useStorageSession'

const languagesMenuConfig = {
  'it-IT': { full: 'Italiano', short: 'IT' },
  'en-GB': { full: 'English', short: 'EN' }
}

function App({ user, theme }) {
  const initialized = useStorageSession()
  const { t, i18n } = useTranslation('Sidebar')

  // useEffect(() => {
  //   initI18n()
  // }, [])

  const checkAuthorized = useCallback(
    (route) => isUserAuthorized(user, route),
    [user]
  )

  const allModals = useMemo(() => {
    // const languagesList = Object.keys(languagesMenuConfig)
    return {
      ...platformModals,
      /*newTranslation: (props) => (
        <NewTranslation {...props} allLanguages={languagesList} />
      ),*/
      settings: (props) => <Settings {...props} {...themeSelection} />
    }
  }, [themeSelection, languagesMenuConfig])

  const onThemeChange = useCallback(
    (theme) => {
      themeStore.set(theme.id)
      setLocalStorageTheme(theme.id)
    },
    [themeStore]
  )

  const themeSelection = useMemo(
    () => ({
      themes: defaultThemes,
      onThemeChange
    }),
    [onThemeChange]
  )

  const routerChildren = useMemo(
    () => (
      <Fragment>
        <ErrorsHandler />
        <NotificationsHandler />
      </Fragment>
    ),
    []
  )

  useEffect(() => {
    const closeStream = synapse.connect(`${BASE_URL}/stream`, (e) => {
      if (e.data === 'heartbeat' || e.data === 'connected') {
        return
      }
      synapse.publish(e.lastEventId, e)
    })

    return closeStream
  }, [])

  useEffect(async () => {
    if (initialized && !isEmpty(user)) {
      try {
        await getInstancesList(t)
        await getMyScenarioPermissions()

        const [models, scenarioWizardModel, outputConfigModel] =
          await fetchAppConfig()

        const outputConfigRoutes =
          // (isDevMode && clientOutputConfigRoutes) || outputConfigModel
          outputConfigModel

        appConfigStore.update((prevState) => ({
          ...prevState,
          // models: (isDevMode && clientModels) || models,
          models: models,
          // scenarioWizardModel:
          //   (isDevMode && clientScenarioWizardModel) || scenarioWizardModel,
          scenarioWizardModel: scenarioWizardModel,
          outputConfigRoutes
        }))

        // const appRoutes = setConfigSidebar(
        //   routes,
        //   outputConfigRoutes,
        //   scenarioWizardModel
        // )

        // setRoutes(appRoutes)
        appReadyStore.set(true)
      } catch (error) {
        console.warn(error)

        if (error.code !== 401) {
          appReadyStore.set(true)
        }
      }
    }
  }, [initialized, user])

  return (
    <Theme name={theme}>
      <RouterX
        routes={defaultRoutes}
        themeSelection={themeSelection}
        userActionMenu={userActionMenu}
        checkAuthorization={checkAuthorized}
        modals={allModals}
        languagesMenuConfig={languagesMenuConfig}
      >
        {routerChildren}
      </RouterX>
    </Theme>
  )
}

export default withStore(React.memo(App), ['user', 'theme'])
