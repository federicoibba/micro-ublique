import { appReadyStore, instancesStore, modalStore, synapse, userStore } from 'state'
import history from 'services/history'
import { openModal } from 'components/Modal/actions'
// import { resetSidebarStore } from './actions'

export default [
  {
    title: 'Settings',
    onClick: () => {
      const { activeModals } = modalStore.get()
      if (!activeModals.includes('settings')) {
        openModal('settings')
      }
    }
  },
  {
    title: 'Password',
    onClick: () => {
      const { activeModals } = modalStore.get()
      if (!activeModals.includes('password')) {
        openModal('password')
      }
    }
  },
  {
    title: 'Administration',
    id: 'admin',
    onClick: () => {
      return history.replace('/administration-permissions')
    }
  },
  {
    title: 'Translations',
    id: 'translations',
    onClick: () => {
      return history.replace('/translations/translationTables')
    }
  },
  {
    title: 'Logout',
    isDelete: true,
    onClick: async () => {
      await synapse.logout()

      const user = await synapse.user()
      userStore.set(user)
      // resetSidebarStore()
      instancesStore.set([])
      appReadyStore.set(false)

      return history.replace('/login')
    }
  }
]
