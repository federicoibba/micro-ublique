import { synapse } from 'state'
import { BASE_URL } from 'config'

export async function fetchDocuments(wtID, body) {
  const fetchParams = body ? [{ method: 'POST' }, body] : []

  const documents = await synapse
    .fetch(`${BASE_URL}/workingtables/${wtID}/documents`, ...fetchParams)
    .catch(console.error)

  if (!documents || !documents.body) {
    console.error(`Couldn't get documents for working table ${wtID}`)
    return { items: [], metadata: {} }
  }

  return documents.body
}

export const getDocuments = () => {} // TODO: delete
export const getPaginatedDocuments = fetchDocuments

export async function getDocumentOptions(wtID, type, body) {
  try {
    const options = await synapse.fetch(
      `${BASE_URL}/workingtables/${wtID}/documents/${type}`,
      {
        // method: 'POST'
        method: 'GET'
      }
      // body
    )

    return options
  } catch (err) {
    console.error(err)
    return { body: [] }
  }
}

export async function removePaginatedDocuments(wtID, type, body) {
  return synapse.fetch(
    `${BASE_URL}/workingtables/${wtID}/documents/${type}`,
    { method: 'DELETE' },
    body
  )
}
