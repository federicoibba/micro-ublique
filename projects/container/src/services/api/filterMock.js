export const defaultFilter = [
  {
    type: 'toggle',
    title: 'Grafico Rdp Fatturato',
    id: 'revenueRpdGraphFilter',
    elements: [{ label: 'revenueGraph', val: false }]
  },
  {
    type: 'checkbox',
    title: 'Segmento',
    id: 'segment',
    props: { withSelectAll: true },
    elements: [
      { val: 'OTHER', label: 'OTHER' },
      { val: 'LEISURE', label: 'LEISURE' },
      { val: 'CORPORATE', label: 'CORPORATE' }
    ]
  },
  {
    type: 'checkbox',
    title: 'Gruppo auto',
    id: 'cargroup',
    props: { withSelectAll: true },
    elements: [
      {
        id: 'vehicles',
        key: 'hid-vehicles',
        type: 'checkbox',
        expanded: true,
        elements: [
          { key: 'CAR', label: 'CAR' },
          { key: 'TRUCK', label: 'TRUCK' }
        ],
        visibility: [
          { hierarchy: ['targetFilter'], value: 'target' },
          { hierarchy: ['targetFilter'], value: 'targetMultiPurpose' }
        ]
      },
      {
        id: 'CAR',
        title: 'CAR',
        type: 'checkbox',
        elements: [
          { val: '4WD', label: '4WD' },
          { val: 'COMPACT', label: 'COMPACT' },
          { val: 'MPV', label: 'MPV' },
          { val: 'OW TRANSFER', label: 'OW TRANSFER' },
          { val: 'UTE', label: 'UTE' },
          { val: 'PRESTIGE', label: 'PRESTIGE' },
          { val: 'MID 2WD', label: 'MID 2WD' },
          { val: 'INTERMEDIATE', label: 'INTERMEDIATE' },
          { val: 'SELECTION', label: 'SELECTION' },
          { val: 'ELECTRIC', label: 'ELECTRIC' },
          { val: 'FULL SIZE', label: 'FULL SIZE' }
        ],
        visibility: [{ hierarchy: ['targetFilter'], value: 'none' }]
      },
      {
        id: 'TRUCK',
        title: 'TRUCK',
        type: 'checkbox',
        elements: [{ val: 'TRUCKS', label: 'TRUCKS' }],
        visibility: [{ hierarchy: ['targetFilter'], value: 'none' }]
      }
    ]
  },
  {
    title: 'Zona-Area',
    id: 'zone',
    props: { withSelectAll: true, search: true },
    elements: [
      {
        id: 'NZA02',
        title: 'NZA02',
        type: 'checkbox',
        elements: [
          { val: 'NZZ05', label: 'NZZ05' },
          { val: 'NZZ04', label: 'NZZ04' }
        ]
      },
      {
        id: 'NZA01',
        title: 'NZA01',
        type: 'checkbox',
        elements: [
          { val: 'FOREIGN', label: 'FOREIGN' },
          { val: 'NZZ03', label: 'NZZ03' },
          { val: 'NZZ02', label: 'NZZ02' },
          { val: 'NZZ01', label: 'NZZ01' }
        ]
      }
    ]
  },
  {
    id: 'timePeriod',
    title: 'Periodo',
    elements: [
      {
        id: 'timePeriodType',
        type: 'radio',
        expanded: true,
        elements: [
          { val: 'daily', label: 'Giornaliera' },
          { val: 'custom', label: 'Custom' }
        ]
      },
      {
        id: 'daysNumber',
        type: 'number',
        title: 'Numero di giorni',
        expanded: true,
        elements: [{ label: 'Giorni', val: 28 }],
        visibility: [{ hierarchy: ['timePeriod', 'timePeriodType'], value: 'daily' }]
      },
      {
        id: 'customPeriod',
        type: 'date',
        expanded: true,
        title: 'Periodo',
        elements: [
          { val: '2021-07-10', key: 'fromDate', label: 'Dal' },
          { val: '2021-12-25', key: 'toDate', label: 'Al' }
        ],
        visibility: [{ hierarchy: ['timePeriod', 'timePeriodType'], value: 'custom' }]
      }
    ]
  },
  {
    id: 'targetFilter',
    title: 'Target',
    type: 'radio',
    elements: [
      { val: 'none', label: 'Nessuno' },
      { val: 'target', label: 'Target' },
      { val: 'targetMultiPurpose', label: 'Target Multi-Uso' }
    ]
  },
  {
    id: 'forecastCorrection',
    title: 'Correzione previsione',
    type: 'checkbox',
    props: { withSelectAll: true },
    elements: [
      { val: 'userCorrections', label: 'userCorrections' },
      { val: 'freeUpgrade', label: 'freeUpgrade' },
      { val: 'newBusiness', label: 'newBusiness' },
      { val: 'upSells', label: 'upSells' }
    ]
  },
  {
    id: 'historicalFilter',
    title: 'Dati storici',
    type: 'checkbox',
    elements: [
      { key: 'lastYear', label: 'Ultimo anno', value: false },
      { key: 'currentMonth', label: 'Mese corrente', value: false }
    ]
  }
]

const carElite = [
  {
    type: 'toggle',
    title: 'Grafico Rdp Fatturato',
    id: 'revenueRpdGraphFilter',
    elements: [{ label: 'revenueGraph', val: false }]
  }
]

const caseSeries = [
  {
    id: 'targetFilter',
    title: 'Target',
    type: 'radio',
    elements: [
      { val: 'none', label: 'Nessuno' },
      { val: 'target', label: 'Target' },
      { val: 'targetMultiPurpose', label: 'Target Multi-Uso' }
    ]
  }
]

export const fakeFilterList = [
  {
    id: 'filter-0',
    title: 'None',
    favourite: false,
    sections: defaultFilter
  },
  {
    id: 'filter-1',
    title: 'Car Elite Zones',
    favourite: false,
    sections: carElite
  },
  {
    id: 'filter-2',
    title: 'Case Series Criteria',
    favourite: true,
    sections: caseSeries
  }
  // {
  //   id: 'filter-3',
  //   title: 'Active Case Series',
  //   favourite: false
  // },
  // {
  //   id: 'filter-4',
  //   title: 'Classification Criteria 2019',
  //   favourite: false
  // },
  // {
  //   id: 'filter-5',
  //   title: 'Future Report 2019',
  //   favourite: false
  // }
]
