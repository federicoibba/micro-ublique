import { synapse } from 'services/state'
import { BASE_URL } from 'config'

export const fetchPermissions = async () => {
  return await synapse.fetch(`${BASE_URL}/admin/scenarios/permissions`)
}

export const createPermission = async (body) => {
  return await synapse.fetch(
    `${BASE_URL}/admin/scenarios/permissions`,
    { method: 'POST' },
    body
  )
}

export const removePermission = async (rolename) => {
  return await synapse.fetch(`${BASE_URL}/admin/scenarios/permissions/${rolename}`, {
    method: 'DELETE'
  })
}
