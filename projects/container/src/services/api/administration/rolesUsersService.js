import { adminStore } from 'state'
import { _fetch, _fetchWithError, updateState } from 'api/administration/index'
import { BASE_URL } from 'config'

export const getAllRoles = async () => {
  const response = await fetchRolesList()
  const roles = response
    ? response.map((role) => {
        return {
          permission: role.permissions,
          roleName: role.roleName,
          id: role.id
        }
      })
    : []

  updateState(adminStore, {
    roles
  })
}

export const getAllUsers = async () => {
  const users = await fetchUsersList()

  updateState(adminStore, {
    users
  })
}

export const createRole = async (payload) => {
  await fetchRolesSave(payload)
  await getAllRoles()
}

export const updateRole = async (payload, id) => {
  await fetchRolesSave(payload, id)
  await getAllRoles()
}

export const createUser = async (payload) => {
  await fetchUserSave(payload)
  await getAllUsers()
}

export const updateUser = async (payload, id) => {
  await fetchUserSave(payload, id)
  await getAllUsers()
}

export const deleteRole = async (id) => {
  await fetchRoleDelete(id)
}

export const fetchRole = async () => {
  await getAllRoles()
}

export const deleteUser = async (id) => {
  await fetchUserDelete(id)
}

export const fetchUser = async () => {
  await getAllUsers()
}

const fetchRolesList = async () => {
  const url = `${BASE_URL}/admin/roles`

  return await _fetch(url)
}

const fetchRolesSave = async (payload, id) => {
  const params = {
    method: !id ? 'POST' : 'PUT',
    body: JSON.stringify(payload)
  }
  const url = !id ? `${BASE_URL}/admin/roles` : `${BASE_URL}/admin/roles/${id}`
  return await _fetchWithError(url, params)
}

const fetchRoleDelete = async (id) => {
  const params = {
    method: 'DELETE'
  }
  const url = `${BASE_URL}/admin/roles/${id}`
  return await _fetchWithError(url, params)
}

const fetchUsersList = async () => {
  const url = `${BASE_URL}/admin/users`

  return await _fetch(url)
}

const fetchUserSave = async (payload, id) => {
  const params = {
    method: !id ? 'POST' : 'PUT',
    body: JSON.stringify(payload)
  }
  const url = !id ? `${BASE_URL}/admin/users` : `${BASE_URL}/admin/users/${id}`
  return await _fetchWithError(url, params)
}

const fetchUserDelete = async (id) => {
  const params = {
    method: 'DELETE'
  }
  const url = `${BASE_URL}/admin/users/${id}`
  return await _fetch(url, params)
}
