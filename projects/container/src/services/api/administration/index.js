import { synapse } from 'state'
import { BASE_URL } from 'config'
import { v4 as uuidv4 } from 'uuid'

const SCENARIO = 'scenario'
const INSTANCE = 'instance'

export const INSTANCE_TYPE = {
  SCENARIO,
  INSTANCE
}

export const getFieldData = (dt, dtFields) => {
  const flatPermissions = getFlatPermission([dtFields])

  const flatPermissionForRole = flatPermissions.filter(
    (flatPermission) => flatPermission.roleName === dt.roleName
  )[0]
  if (flatPermissionForRole) {
    const app = { ...dtFields }
    app.children = []
    app.title = app.field
    app.workingTableTitle = dt.workingTableTitle
    app.instanceTitle = dt.instanceTitle
    delete app.field
    delete app.documentTypeId
    app.roleName = flatPermissionForRole.roleName
    app.permission = flatPermissionForRole.permission
    app.id = flatPermissionForRole.id
    app.ubliqueId = flatPermissionForRole.ubliqueId
    delete app.permissions
    return app
  }
}

export const getFlatPermission = (elements) => {
  const flat = []

  elements?.forEach((element) => {
    element.permissions.forEach((role) => {
      const row = {
        id: uuidv4(),
        ubliqueId: element.id,
        title: element.title,
        roleName: role.roleName,
        permission: {
          read: role.permission?.read || false,
          execute: role.permission?.execute || false,
          create: role.permission?.create || false,
          update: role.permission?.update || false,
          delete: role.permission?.delete || false
        }
      }
      flat.push(row)
    })
  })
  return flat
}

export const getPayloadDefault = (basePayload, flatPermissionForRole) => {
  const payloadDefault = { ...basePayload }
  payloadDefault.id = flatPermissionForRole.id
  payloadDefault.ubliqueId = flatPermissionForRole.ubliqueId
  payloadDefault.title = flatPermissionForRole.title
  payloadDefault.roleName = flatPermissionForRole.roleName
  payloadDefault.permission = flatPermissionForRole.permission
  delete payloadDefault.permissions

  return payloadDefault
}

export const pushBulkWT = (payload, wt, wtBulk) => {
  payload.workingTableId = wt.id
  wtBulk.push({ ...payload })
}

export const pushBulkDt = (payload, wt, dtBulk) => {
  delete payload.workingTableId
  payload.activityId = wt.id
  payload.documentType = wt.type
  dtBulk.push({ ...payload })
}

export const pushBulkDtFiled = (payload, fields, dtFieldsBulk) => {
  Object.keys(fields[payload.documentType]).forEach((curr) => {
    payload.field = curr
    payload.parent = null
    dtFieldsBulk.push({ ...payload })
    fields[payload.documentType][curr].forEach((currDet) => {
      payload.field = currDet
      payload.parent = curr
      dtFieldsBulk.push({ ...payload })
    })
  })
}

export const _fetchWithError = async (url, params) => {
  const response = params
    ? await synapse.fetch(url, params)
    : await synapse.fetch(url)
  return response?.body || null
}

export const _fetch = async (url, params) => {
  try {
    const response = params
      ? await synapse.fetch(url, params)
      : await synapse.fetch(url)
    return response?.body || null
  } catch (e) {
    return null
  }
}

export const updateState = (state, newData) =>
  state.update((prevState) => ({ ...prevState, ...newData }))

export const getInstancePath = (instanceType = INSTANCE) => {
  if (instanceType === SCENARIO) {
    return `${BASE_URL}/admin/scenarios/default-permissions`
  }

  return `${BASE_URL}/admin`
}

export const getPermissionBulkPath = (path, isPayloadArray) => {
  if (isPayloadArray) {
    return `${path}/permissionsBulk`
  }

  return `${path}/permissions`
}
