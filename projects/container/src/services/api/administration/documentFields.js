import { adminStore } from 'services/state'

import {
  _fetch,
  getFieldData,
  updateState,
  INSTANCE_TYPE,
  _fetchWithError
} from 'api/administration/index'

import { fetchDocumentFieldsByDTList, fetchUpdateFieldsPermissions } from './services'

const { INSTANCE } = INSTANCE_TYPE

export const createDocumentFieldsApi = (instanceType = INSTANCE) => {
  const getDocumentFieldsByDT = getDocumentFieldsByDocType(instanceType)
  const updateDTFieldsPermissions = updateDocTypeFieldsPermissions(instanceType)

  return {
    getDocumentFieldsByDT,
    updateDTFieldsPermissions
  }
}

const getDocumentFieldsByDocType =
  (instanceType = INSTANCE) =>
  async (dt) => {
    const documentFields = dt
      ? await fetchDocumentFieldsByDTList(dt, instanceType)
      : null
    const dtFieldsFlat = []

    documentFields?.forEach((dtFields) => {
      const app = getFieldData(dt, dtFields)
      if (app) {
        if (dtFields.children?.length > 0) {
          dtFields.children?.forEach((child, index) => {
            child.id = child.id + index + 1
            const childApp = getFieldData(dt, child)
            if (childApp) {
              childApp.parent = app.title
              app.children.push(childApp)
            }
          })
        }
        dtFieldsFlat.push(app)
      }
    })
    updateState(adminStore, {
      documentFields: dtFieldsFlat
    })
  }

const updateDocTypeFieldsPermissions =
  (instanceType = INSTANCE) =>
  async (payload) => {
    await fetchUpdateFieldsPermissions(payload, instanceType)
  }
