import { isEmpty } from 'lodash'
import { adminStore, instancesStore } from 'services/state'

import {
  _fetch,
  getFlatPermission,
  pushBulkDt,
  pushBulkDtFiled,
  pushBulkWT,
  updateState,
  INSTANCE_TYPE,
  _fetchWithError
} from 'api/administration/index'

import {
  fetchAllFields,
  fetchInstancesWithRoleList,
  fetchUpdateDTPermissions,
  fetchUpdateFieldsPermissions,
  fetchUpdateInstancePermissions,
  fetchUpdateWTPermissions,
  fetchWorkingTableByInstanceList,
  fetchCloningInstanceRecord,
  fetchInstanceDelete
} from './services'

const { INSTANCE } = INSTANCE_TYPE

export const createInstanceApi = (instanceType = INSTANCE) => {
  const getAllInstancesWithRole = getAllInstances(instanceType)
  const updateInstancePermissions = updateInstancePerm(instanceType)
  const updateInstancePermissionsBulk = updateInstancePermBulk(instanceType)
  const cloningInstanceRecord = cloneRecord(instanceType)

  return {
    getAllInstancesWithRole,
    updateInstancePermissions,
    updateInstancePermissionsBulk,
    cloningInstanceRecord
  }
}

const getAllInstances = (instanceType) => async () => {
  const instances = await fetchInstancesWithRoleList(instanceType)
  const initInstances = instancesStore.get()

  const allInstances = initInstances.filter(
    (item) => item.config === undefined || item.config === null
  )

  const state = {
    allInstances,
    instances: []
  }

  if (!isEmpty(instances)) {
    state.instances = getFlatPermission(
      instances.filter((item) => item.config === undefined || item.config === null)
    )
  }

  updateState(adminStore, state)
}

const updateInstancePermBulk = (instanceType) => async (payload) => {
  const isCreating = true
  const wtBulk = []
  const dtBulk = []
  const dtFieldsBulk = []

  await fetchUpdateInstancePermissions(payload, instanceType)

  const wts = await fetchWorkingTableByInstanceList(
    payload.instanceId,
    instanceType,
    isCreating
  )
  const fields = await fetchAllFields(instanceType)
  delete payload.instanceId

  wts.forEach((wt) => {
    pushBulkWT(payload, wt, wtBulk)
    pushBulkDt(payload, wt, dtBulk)
    pushBulkDtFiled(payload, fields, dtFieldsBulk)
  })

  await fetchUpdateWTPermissions(wtBulk, instanceType)
  await fetchUpdateDTPermissions(dtBulk, instanceType)
  await fetchUpdateFieldsPermissions(dtFieldsBulk, instanceType)
  await getAllInstances(instanceType)()
}

const updateInstancePerm = (instanceType) => async (payload) => {
  await fetchUpdateInstancePermissions(payload, instanceType)
  await getAllInstances(instanceType)()
}

const cloneRecord = (instanceType) => async (payload) => {
  await fetchCloningInstanceRecord(payload, instanceType)
  await getAllInstances(instanceType)()
}

export const deleteInstance = async (payload, instanceType) => {
  await fetchInstanceDelete(payload, instanceType)
}
