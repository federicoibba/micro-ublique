import { BASE_URL } from 'config'
import { isArray } from 'lodash'

import {
  _fetch,
  INSTANCE_TYPE,
  _fetchWithError,
  getInstancePath,
  getPermissionBulkPath
} from 'api/administration/index'

const { SCENARIO, INSTANCE } = INSTANCE_TYPE

export const fetchInstancesWithRoleList = async (instanceType = INSTANCE) => {
  const url = `${getInstancePath(instanceType)}/instances`

  return await _fetch(url)
}

export const fetchUpdateInstancePermissions = async (
  payload,
  instanceType = INSTANCE
) => {
  const params = {
    method: 'POST',
    body: JSON.stringify(payload)
  }

  let url = getInstancePath(instanceType)

  url +=
    instanceType === SCENARIO ? '/instance/permissions' : '/instances/permissions'

  return await _fetchWithError(url, params)
}

export const fetchWorkingTableByInstanceList = async (
  id,
  instanceType = INSTANCE,
  isCreating = false
) => {
  let path = getInstancePath(instanceType)

  if (instanceType === INSTANCE || isCreating) {
    path = `${getInstancePath(INSTANCE)}/instances`
  }

  const url = `${path}/${id}/workingtables`

  return await _fetch(url)
}

export const fetchAllFields = async (instanceType = INSTANCE) => {
  let url = `${BASE_URL}/admin/documentTypeField`

  if (instanceType === SCENARIO) {
    url = `${BASE_URL}/admin/scenarios/allFields`
  }

  return await _fetch(url)
}

export const fetchUpdateWTPermissions = async (payload, instanceType = INSTANCE) => {
  const params = {
    method: 'POST',
    body: JSON.stringify(payload)
  }

  let path = getInstancePath(instanceType)

  // the path is /workingTables for instance, /workingTable for scenario
  path += instanceType === INSTANCE ? '/workingTables' : '/workingTable'

  const url = getPermissionBulkPath(path, isArray(payload))

  return await _fetchWithError(url, params)
}

export const fetchUpdateDTPermissions = async (payload, instanceType = INSTANCE) => {
  const params = {
    method: 'POST',
    body: JSON.stringify(payload)
  }

  const path = `${getInstancePath(instanceType)}/documentType`
  const url = getPermissionBulkPath(path, isArray(payload))

  return await _fetchWithError(url, params)
}

export const fetchUpdateFieldsPermissions = async (
  payload,
  instanceType = INSTANCE
) => {
  const params = {
    method: 'POST',
    body: JSON.stringify(payload)
  }

  const path = `${getInstancePath(instanceType)}/documentTypeField`
  const url = getPermissionBulkPath(path, isArray(payload))

  return await _fetchWithError(url, params)
}

export const fetchDocumentTypeByWTList = async (wt, instanceType = INSTANCE) => {
  // instance `${BASE_URL}/admin/workingTables/${wt.ubliqueId}/documentType`
  // scenario `${BASE_URL}/admin/scenarios/default-permissions/${wt.ubliqueId}/documentType
  const path = `${getInstancePath(instanceType)}${
    instanceType === INSTANCE ? '/workingTables' : ''
  }`

  const url = `${path}/${wt.ubliqueId}/documentType`

  return await _fetch(url)
}

export const fetchDocumentFieldsByDTList = async (dt, instanceType) => {
  const path = `${getInstancePath(instanceType)}${
    instanceType === INSTANCE ? '/documentTypeField' : ''
  }`

  const url = `${path}/${dt.workingTableId}/${dt.title}`

  return await _fetch(url)
}

export const fetchCloningInstanceRecord = async (payload, instanceType) => {
  const endpoint =
    instanceType === 'instance'
      ? 'admin/instances/cloneInstancePermission'
      : 'admin/scenarios/cloneScenarioPermission'
  const params = {
    method: 'POST',
    body: JSON.stringify(payload)
  }

  const url = `${BASE_URL}/${endpoint}`

  return await _fetchWithError(url, params)
}

export const fetchInstanceDelete = async (payload, instanceType) => {
  const endpoint =
    instanceType === INSTANCE
      ? 'admin/instances/deleteInstancePermission'
      : 'admin/scenarios/deleteScenarioPermission'

  if (instanceType === SCENARIO) {
    payload.id = payload.instance
    delete payload.instance
  }
  const params = {
    method: 'POST',
    body: JSON.stringify(payload)
  }
  const url = `${BASE_URL}/${endpoint}`

  return await _fetchWithError(url, params)
}
