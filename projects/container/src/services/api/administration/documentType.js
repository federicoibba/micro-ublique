import { v4 as uuidv4 } from 'uuid'
import { adminStore } from 'services/state'

import {
  _fetch,
  getFlatPermission,
  pushBulkDtFiled,
  updateState,
  INSTANCE_TYPE,
  _fetchWithError
} from 'api/administration/index'

import {
  fetchAllFields,
  fetchUpdateDTPermissions,
  fetchUpdateFieldsPermissions,
  fetchDocumentTypeByWTList
} from './services'

const { INSTANCE } = INSTANCE_TYPE

export const createDocumentTypeApi = (instanceType = INSTANCE) => {
  const getDocumentTypeByWT = getDocumentTypeByWorkingTable(instanceType)
  const updateDTPermissions = updateDocTypePermissions(instanceType)
  const updateDTPermissionsBulk = updateDocTypePermissionsBulk(instanceType)

  return {
    getDocumentTypeByWT,
    updateDTPermissions,
    updateDTPermissionsBulk
  }
}

const getDocumentTypeByWorkingTable = (instanceType) => async (wt) => {
  const documentType =
    wt && wt.length > 0 ? await fetchDocumentTypeByWTList(wt[0], instanceType) : null

  const dtFlat = []
  // TODO refactor
  documentType?.forEach((dt) => {
    const flatPermissions = getFlatPermission([dt])
    const flatPermissionForRole = flatPermissions.filter(
      (flatPermission) => flatPermission.roleName === wt[0].roleName
    )[0]
    if (flatPermissionForRole) {
      const app = { ...dt }
      app.id = uuidv4()
      app.ubliqueId = app.documentTypeId
      app.title = app.documentType
      delete app.documentType
      delete app.documentTypeId
      app.roleName = flatPermissionForRole.roleName
      app.permission = flatPermissionForRole.permission
      delete app.permissions
      dtFlat.push(app)
    }
  })
  updateState(adminStore, {
    documentType: dtFlat
  })
}

const updateDocTypePermissions = (instanceType) => async (payload) => {
  await fetchUpdateDTPermissions(payload, instanceType)
}

const updateDocTypePermissionsBulk = (instanceType) => async (payload) => {
  await fetchUpdateDTPermissions(payload, instanceType)

  const dtFieldsBulk = []
  const fields = await fetchAllFields(instanceType)

  pushBulkDtFiled(payload, fields, dtFieldsBulk)

  await fetchUpdateFieldsPermissions(dtFieldsBulk, instanceType)
}
