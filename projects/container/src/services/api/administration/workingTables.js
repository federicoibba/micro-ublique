import { isEmpty } from 'lodash'
import { adminStore } from 'services/state'

import {
  _fetch,
  getFlatPermission,
  pushBulkDtFiled,
  updateState,
  getPayloadDefault,
  INSTANCE_TYPE,
  _fetchWithError
} from 'api/administration/index'

import {
  fetchAllFields,
  fetchUpdateDTPermissions,
  fetchUpdateFieldsPermissions,
  fetchUpdateWTPermissions,
  fetchWorkingTableByInstanceList
} from './services'

const { INSTANCE } = INSTANCE_TYPE

export const createWorkingTablesApi = (instanceType = INSTANCE) => {
  const getWorkingTableByInstanceForRole = getWTByInstanceForRole(instanceType)
  const updateWTPermissions = updateWTPerm(instanceType)
  const updateWTPermissionsBulk = updateWTPermBulk(instanceType)

  return {
    getWorkingTableByInstanceForRole,
    updateWTPermissions,
    updateWTPermissionsBulk
  }
}

const getWTByInstanceForRole = (instanceType) => async (instance) => {
  const workingTables = instance
    ? await fetchWorkingTableByInstanceList(instance.ubliqueId, instanceType)
    : []
  const workingTablesFlat = []

  if (!isEmpty(workingTables)) {
    workingTables.forEach((wt) => {
      const flatPermissions = getFlatPermission([wt])
      const flatPermissionForRole = flatPermissions.filter(
        (flatPermission) => flatPermission.roleName === instance.roleName
      )[0]

      if (flatPermissionForRole) {
        const app = getPayloadDefault(wt, flatPermissionForRole)
        workingTablesFlat.push(app)
      }
    })

    updateState(adminStore, {
      workingTables: workingTablesFlat
    })
  }
}

const updateWTPerm = (instanceType) => async (payload) => {
  await fetchUpdateWTPermissions(payload, instanceType)
}

const updateWTPermBulk = (instanceType) => async (payload) => {
  await fetchUpdateWTPermissions(payload, instanceType)

  const dtFieldsBulk = []
  const fields = await fetchAllFields(instanceType)

  payload.activityId = payload.workingTableId
  payload.documentType = payload.type
  const dtBulk = { ...payload }

  pushBulkDtFiled(payload, fields, dtFieldsBulk)

  await fetchUpdateDTPermissions(dtBulk, instanceType)
  await fetchUpdateFieldsPermissions(dtFieldsBulk, instanceType)
}
