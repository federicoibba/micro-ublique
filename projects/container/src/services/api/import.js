import { trim } from 'lodash'
import { BASE_URL, IMPORT_SERVICE_BASE_PATH } from 'config'
import { synapse } from 'state'

const createImportApiInstance = () => {
  const baseParams = ['product', 'customer', 'lng']
  const _private = {}

  return {
    init({
      product,
      customer,
      host = BASE_URL,
      basePath = IMPORT_SERVICE_BASE_PATH
    }) {
      _private.host = host
      _private.product = product
      _private.customer = customer
      _private.basePath = trim(basePath, '/')
    },
    setLanguage(lang) {
      _private.lng = lang
    },
    createFormDataWithBaseParams(file) {
      const formData = new FormData()
      baseParams.forEach((param) => {
        formData.set(param, _private[param])
      })
      formData.set('file', file)
      return formData
    },
    async importInstance(id, file, truncate) {
      const formData = this.createFormDataWithBaseParams(file)
      if (truncate) formData.set('truncate', true)

      return synapse.fetch(
        `${_private.host}/${_private.basePath}/instance/${id}`,
        { method: 'POST' },
        formData
      )
    },
    async importWorkingTableGroup(instanceId, wtGroup, file) {
      const formData = this.createFormDataWithBaseParams(file)
      formData.set('group', wtGroup)

      return synapse.fetch(
        `${_private.host}/${_private.basePath}/instance/${instanceId}`,
        { method: 'POST' },
        formData
      )
    },
    async importWorkingTable(id, file) {
      const formData = this.createFormDataWithBaseParams(file)

      return synapse.fetch(
        `${_private.host}/${_private.basePath}/workingtable/${id}`,
        { method: 'POST' },
        formData
      )
    },
    async importDocumentsByType(wtID, type, file) {
      const formData = this.createFormDataWithBaseParams(file)

      return synapse.fetch(
        `${_private.host}/${_private.basePath}/workingtable/${wtID}/${type}`,
        { method: 'POST' },
        formData
      )
    },
    async importDocument(wtType, activityId, id, field, file) {
      const formData = this.createFormDataWithBaseParams(file)
      if (field) formData.set('item', field)

      return synapse.fetch(
        `${_private.host}/${_private.basePath}/documents/${wtType}/${activityId}/${id}`,
        { method: 'POST' },
        formData
      )
    },
    async importTranslation(file) {
      const formData = new FormData()

      if (file) {
        formData.append('file', file)
      }

      return synapse.fetch(
        `${BASE_URL}/admin/translations/import`,
        { method: 'POST' },
        formData
      )
    }
  }
}

export const ImportApi = createImportApiInstance()
