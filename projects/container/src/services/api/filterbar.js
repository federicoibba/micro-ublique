import { BASE_URL } from 'config'
import { synapse } from 'services/state'

export const fetchFilter = async (id, outputName) => {
  const queryParams = new URLSearchParams({
    outputName
  })
  const { body } = await synapse.fetch(
    `${BASE_URL}/output-filters/${id}?${queryParams}`
  )

  return body
}

export const fetchDefaultFilter = async (instanceId, outputName) => {
  const queryParams = new URLSearchParams({
    instanceId,
    outputName
  })

  const { body } = await synapse.fetch(
    `${BASE_URL}/output-filters/default?${queryParams}`
  )

  return body
}

export const fetchFilterList = async (outputName, instanceId, defaultFilter) => {
  const queryParams = new URLSearchParams({
    outputName
  })

  if (defaultFilter) {
    queryParams.append('default', instanceId)
  }

  const { body } = await synapse.fetch(`${BASE_URL}/output-filters/?${queryParams}`)

  return body
}

export const insertFilter = async (body, outputName) => {
  const queryParams = new URLSearchParams({
    outputName
  })
  const { body: resBody } = await synapse.fetch(
    `${BASE_URL}/output-filters?${queryParams}`,
    {
      method: 'POST'
    },
    body
  )

  return resBody
}

export const deleteFilter = async (id, outputName) => {
  const queryParams = new URLSearchParams({
    outputName
  })

  const { body } = await synapse.fetch(
    `${BASE_URL}/output-filters/${id}?${queryParams}`,
    {
      method: 'DELETE'
    }
  )

  return body
}

export const updateFilter = async (id, body, outputName) => {
  const queryParams = new URLSearchParams({
    outputName
  })

  const { body: resBody } = await synapse.fetch(
    `${BASE_URL}/output-filters/${id}?${queryParams}`,
    {
      method: 'PUT'
    },
    body
  )

  return resBody
}
