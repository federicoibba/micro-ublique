import { synapse } from 'state'
import { BASE_URL } from 'config'

const replacer = (_, value) => {
  if (value instanceof RegExp) {
    return value.toString()
  }

  return value
}

export const fetchConfig = async (resource) => {
  const url = `${BASE_URL}/dataconfig/${resource}`
  const { body } = await synapse.fetch(url)
  return body
}

export const updateConfig = async (data, resource, section) => {
  const url = `${BASE_URL}/dataconfig/${resource}${section ? `/${section}` : ''}`
  const { body } = await synapse.fetch(
    url,
    { method: section ? 'PUT' : 'POST', stringifyReplacer: replacer },
    data
  )
  return body
}
