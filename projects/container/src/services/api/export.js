import { compact, trim } from 'lodash'
import { BASE_URL } from 'config'
import { synapse } from 'state'

const MAX_JAVA_INTEGER = 2147483647

const bodyFilter = {
  filter: {},
  length: MAX_JAVA_INTEGER,
  sort: [],
  start: 0
}

const processFileName = (name) => trim(name, '"')

const handleExport = async (effect) => {
  const { body, fileName } = await effect()
  return { body, fileName: processFileName(fileName) }
}

const createExportApiInstance = () => {
  const _private = {
    language: null
  }
  return {
    setLanguage(lang) {
      _private.language = lang
    },
    exportInstance(id) {
      return handleExport(() =>
        synapse.fetch(`${BASE_URL}/instances/${id}/export?lng=${_private.language}`)
      )
    },
    exportWorkingTableGroup(instanceId, wtGroup) {
      return handleExport(() =>
        synapse.fetch(
          `${BASE_URL}/instances/${instanceId}/export?group=${wtGroup}&lng=${_private.language}`
        )
      )
    },
    exportWorkingTable(id) {
      return handleExport(() =>
        synapse.fetch(
          `${BASE_URL}/workingtables/${id}/export?lng=${_private.language}`
        )
      )
    },
    exportDocumentsByType(wtID, type, filters) {
      return handleExport(() =>
        synapse.fetch(
          `${BASE_URL}/workingtables/${wtID}/${type}/export?lng=${_private.language}`,
          { method: 'POST' },
          filters
        )
      )
    },
    // Export the entire document or a specific document field
    exportDocument(wtType, activityId, id, field, filters = bodyFilter) {
      const params = compact([field && `item=${field}`, `lng=${_private.language}`])

      return handleExport(() =>
        synapse.fetch(
          `${BASE_URL}/${wtType}/${activityId}/${id}/export?${params.join('&')}`,
          { method: 'POST' },
          filters
        )
      )
    }
  }
}

export const ExportApi = createExportApiInstance()
