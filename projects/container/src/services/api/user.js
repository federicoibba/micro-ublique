import { BASE_URL } from 'config'
import { synapse } from 'services/state'

export const updatePassword = async ({
  currentPassword,
  newPassword,
  repeatPassword
}) => {
  const url = `${BASE_URL}/profile/update_password`
  const body = {
    currentPassword: currentPassword,
    newPassword: newPassword,
    repeatNewPassword: repeatPassword
  }
  return await synapse.fetch(url, { method: 'POST' }, body)
}
