import { synapse } from 'state'
import { BASE_URL } from 'config'
import prop from 'lodash/fp/prop'

const getBody = prop('body')

export const getPermissionsUrl = (wtId, documentType) =>
  `${BASE_URL}/workingtables/${wtId}/${documentType}/permissions`

export const getDocumentUrl = (documentType, activityId, id) => {
  const url = `${BASE_URL}/${documentType}`
  return activityId && id ? `${url}/activityId=${activityId}&id=${id}` : url
}

export const deleteDocumentById = (type, id) => {
  const documentURL = getDocumentUrl(type, id.activityId, id.id)
  return synapse.fetch(documentURL, { method: 'DELETE' }).then(getBody)
}

export const updateDocument = (documentType, activityId, id, data) =>
  synapse
    .fetch(getDocumentUrl(documentType, activityId, id), { method: 'PUT' }, data)
    .then(getBody)

export const addDocument = (documentType, data) =>
  synapse.fetch(`${BASE_URL}/${documentType}`, { method: 'POST' }, data).then(getBody)

export const getPermissionsForDocumentType = (wtId, documentType) => {
  return synapse.fetch(getPermissionsUrl(wtId, documentType)).then(getBody)
}
