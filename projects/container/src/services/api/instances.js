import { synapse } from 'state'
import { BASE_URL } from 'config'

export async function getInstances() {
  try {
    const instances = await synapse.fetch(`${BASE_URL}/instances/`)
    return instances
  } catch (error) {
    console.error(error)
    return null
  }
}

export async function getWorkingTables(instanceID) {
  try {
    const workingTables = await synapse.fetch(
      `${BASE_URL}/instances/${instanceID}/workingtables`
    )

    return workingTables
  } catch (error) {
    console.error(error)
    return null
  }
}
export async function getTranslations() {
  try {
    const translations = await synapse.fetch(`${BASE_URL}/admin/translations/sidebar`)

    return translations // TODO: rimuovere body
  } catch (error) {
    console.error(error)
    return null
  }
}

export async function getInstancesByUserId(userId) {
  try {
    const instances = await synapse.fetch(`${BASE_URL}/instances/`)
    return instances
  } catch (error) {
    console.error(error)
    return null
  }
}

export async function getInstanceByTitle(title) {
  try {
    const instance = await synapse.fetch(`${BASE_URL}/instances/?title=${title}`)
    return instance
  } catch (error) {
    console.error(error)
    return null
  }
}

export async function getDocumentCount(wtIDs) {
  try {
    const counts = await synapse.fetch(
      `${BASE_URL}/workingtables/documentcount`,
      {
        method: 'POST'
      },
      wtIDs
    )
    return counts.body
  } catch (error) {
    console.error(error)
    return {}
  }
}

export default {
  getInstances,
  getInstancesByUserId,
  getWorkingTables
}
