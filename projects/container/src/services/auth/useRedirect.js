import { useEffect, useState } from 'react'
import { isEmpty, isNil } from 'lodash'
import find from 'lodash/fp/find'
import useStoreSelector from 'utils/hooks/useStoreSelector'

const getFirstMainInstance = find(({ config }) => isNil(config))

const useRedirect = ({ fallbackPath = '/home' }) => {
  // const [path, setPath] = useState(null)

  // const user = useStoreSelector('user')
  // // const appReady = useStoreSelector('appReady')
  // // const mainInstance = useStoreSelector('instances', getFirstMainInstance)
  // // const { permissions } = useStoreSelector('scenario')
  // const permissions = { owner: false, others: false }
  // const mainInstance = false
  // const appReady = true

  // useEffect(() => {
  //   if (appReady && !isEmpty(user)) {
  //     const { owner, others } = permissions

  //     if (owner || others) {
  //       setPath('/scenario/management')
  //       return
  //     }

  //     if (mainInstance) {
  //       setPath(`/instances/${mainInstance.id}/workingTables`)
  //       return
  //     }

  //     setPath(fallbackPath)
  //   }
  // }, [appReady, user, mainInstance, permissions])

  return fallbackPath
}

export default useRedirect
