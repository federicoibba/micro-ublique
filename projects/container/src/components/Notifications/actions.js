import { reject, uniqueId } from 'lodash'
import { notificationsStore } from 'state'

export const addNotification = (notification) => {
  notificationsStore.update((state) => ({
    ...state,
    items: [
      ...(state.items || []),
      { ...notification, id: notification.id || uniqueId('notification') }
    ]
  }))
}

export const removeNotification = (id) => {
  notificationsStore.update((state) => ({
    ...state,
    items: reject(state.items, ['id', id])
  }))
}
