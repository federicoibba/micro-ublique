import { matchPath } from 'react-router-dom'
import { compact, first, groupBy, isEmpty, isUndefined, uniqBy } from 'lodash'
import { saveAs } from 'file-saver'
import memoize from 'moize'
import { addNotification } from 'components/Notifications/actions'
import { openModal } from 'components/Modal/actions'
import { ExportApi } from 'api/export'
import { ImportApi } from 'api/import'
import {
  ImportTranslationsApi,
  ExportTranslationsApi,
  ResultTranslationApi
} from 'api/translations'
import { canCreate } from 'services/auth'
import { modalStore } from 'services/state'
import { PATH_UBLIQUE_HEADER } from '../../App/routes'
// import { createFilterbarApiActions } from 'components/Filterbar/actions'

export function handleInstanceItems(headerItems, instances) {
  // Instance handling
  const instancesSubItems = instances.filter(
    (i) => i.config !== PATH_UBLIQUE_HEADER.SCENARIO
  )

  const instancesItem = isEmpty(instancesSubItems)
    ? null // Hide instances menu when it has no children
    : { ...headerItems, items: instancesSubItems }

  return instancesItem
}

export function handleScenarioItems(headerItems, scenario) {
  // Scenario manager button handling
  const {
    permissions: { owner, others }
  } = scenario

  const scenarioItem = !owner && !others ? null : headerItems

  return scenarioItem
}

export const mergePaths = (...paths) => {
  return '/'.concat(
    paths
      .reduce((acc, inc) => [...acc, ...(inc || '').split('/')], [])
      .filter(Boolean)
      .join('/')
  )
}

const getDefaultRoute = (routes) => {
  return routes.find((route) => !route.path && route.public)
}

export const getCurrentRoute = (pathname, params) => {
  const { flatRoutes, routes } = params
  const currentRoute = flatRoutes.find((route) => {
    if (!route || !route.path) return false
    return matchPath(pathname, {
      path: route.path,
      exact: true
    })
  })
  return currentRoute || getDefaultRoute(routes)
}

export const inheritParentRouteProps = memoize((routes, parent) => {
  return routes.reduce((acc, inc) => {
    const routePath = inc.path && mergePaths(parent && parent.path, inc.path)

    const proxyParent = parent || {}
    const inheritedProps = {
      shell: proxyParent.shell,
      roles: proxyParent.roles,
      public: proxyParent.public
    }

    const route = { ...inheritedProps, ...inc, path: routePath, parent }
    const routeRoutes = inc.routes && inheritParentRouteProps(inc.routes, route)

    return [
      ...acc,
      {
        ...route,
        routes: routeRoutes
      }
    ]
  }, [])
})

export const flattenRoutes = (routes) =>
  (routes || []).reduce((acc, inc) => {
    return [...acc, inc, ...flattenRoutes(inc.routes)]
  }, [])

export const matchPathRecursive = memoize((pathname, route) => {
  if (!route) {
    return false
  }

  if (matchPath(pathname, { path: route.path, exact: true })) {
    return true
  }

  if (route.routes) {
    return route.routes.some((childRoute) => matchPathRecursive(pathname, childRoute))
  }

  return false
})

export const mapRoutesToHeaders = (routes, t) => {
  return routes
    .filter((route) => !route.excludeToHeader)
    .map(({ routes, ...rest }) => {
      // const items = routes ? mapRoutesToHeaders(routes, t) : []
      return {
        ...rest,
        title: t(rest.title)
        // items: items.length > 0 ? items : undefined
      }
    })
}

const getSidebarTranslationsActions = (groupName, t) => [
  {
    title: t('Translations:Import'),
    onClick: () => {
      openModal('import', {
        onSubmit: (file) => ImportTranslationsApi.importTranslation(file)
      })
    }
  },
  {
    title: t('Export'),
    onClick: async () => {
      addNotification({ message: t('Shell:ExportStarted') })
      const fileName = `${groupName}.xlsx`
      const blob = await ExportTranslationsApi.exportTranslation(groupName)
      saveAs(blob, fileName)
    }
  },
  {
    title: t('Translations:NewTranslation'),
    onClick: () => {
      openModal('newTranslation', {
        type: 'SUBGROUP',
        group: groupName
      })
    }
  }
]

const getSidebarWtActions = (idInstance, groupName, canImport, t) =>
  compact([
    canImport && {
      title: t('Import'),
      onClick: () => {
        openModal('import', {
          onSubmit: (file) =>
            ImportApi.importWorkingTableGroup(idInstance, groupName, file)
        })
      }
    },
    {
      title: t('Export'),
      onClick: async () => {
        addNotification({ message: t('Shell:ExportStarted') })
        const { body, fileName } = await ExportApi.exportWorkingTableGroup(
          idInstance,
          groupName
        )
        saveAs(body, fileName)
      }
    }
  ])

export const getSidebarListTables = memoize((items, { type }, t) => {
  if (items?.some((item) => item.loading)) return items
  const isTranslations = type === 'translations'
  const isWorkingTables = type === 'workingTables'

  if (isTranslations || isWorkingTables) {
    return Object.values(groupBy(items, 'groups')).map((group) => {
      const translatedItems = group.map((item) => ({
        ...item,
        id: isTranslations ? `${item.id}-${item.type}` : item.id,
        title: t(`Sidebar:${item.title || item.type.toUpperCase()}`)
      }))

      const { groups: groupName, groupTitle, id, idInstance } = first(group) || {}
      const canImport = group.some(({ permission }) => canCreate(permission)) // At least 1 wt with write permission

      return {
        items: translatedItems,
        id: `${id}-${groupTitle}`,
        name: groupName,
        title: t(`Sidebar:${groupTitle.toUpperCase()}`),
        actions: isTranslations
          ? getSidebarTranslationsActions(groupName, t)
          : getSidebarWtActions(idInstance, groupName, canImport, t)
      }
    })
  }

  if (['output', 'custom'].includes(type)) {
    const sideNavGroup = items.map((item) => {
      if (isUndefined(item.groups)) {
        return {
          href: item.href,
          title: item.title,
          icon: item.icon
        }
      } else {
        const groups = items.filter((el) => el.groupTitle === item.groupTitle)
        const { groupTitle, icon } = first(groups) || {}

        const outputItems = groups.map((item) => ({
          href: item.href,
          icon: 'none',
          id: item.id,
          title: t(`Sidebar:${item.title || item.type.toUpperCase()}`)
        }))
        return {
          items: outputItems,
          icon,
          id: `${groupTitle}`,
          name: groupTitle,
          title: t(`Sidebar:${groupTitle}`)
        }
      }
    })
    return uniqBy(sideNavGroup, 'title')
  }

  return []
})

export const getSidebarTitleProp = () => ''
// export const getSidebarTitleProp = memoize(
//   ({ items, type, title }, instances, instanceId, scenarioId, t) => {
//     if (!type) {
//       return ''
//     }

//     if (type === 'workingTables' || type === 'output') {
//       if (!items?.length) return undefined

//       const sidebarTitleMap = instances.reduce(
//         (acc, { id, title }) => ({ ...acc, [id]: title }),
//         {}
//       )

//       const id = type === 'output' ? scenarioId : instanceId

//       return sidebarTitleMap[id]
//     }
//     if (type === 'translations') return t('Translations:Title')

//     return title
//   }
// )

export const getExtraProps = (props, extraProps = []) =>
  extraProps.reduce((acc, propName) => {
    const propValue = props[propName]
    return propValue ? { ...acc, [propName]: propValue } : acc
  }, {})

export const getInstanceActions = (instanceId, permission, isSuperuser, t) => {
  if (!instanceId) return

  const create = canCreate(permission)

  return compact([
    create &&
      isSuperuser && {
        title: t('InitInstance'),
        onClick: () => {
          openModal('import', {
            onSubmit: (file) => ImportApi.importInstance(instanceId, file, true)
          })
        }
      },
    create && {
      title: t('UpdateInstance'),
      onClick: () => {
        openModal('import', {
          onSubmit: (file) => ImportApi.importInstance(instanceId, file)
        })
      }
    },
    {
      title: t('Export'),
      onClick: async () => {
        addNotification({ message: t('Shell:ExportStarted') })
        const { body, fileName } = await ExportApi.exportInstance(instanceId)
        saveAs(body, fileName)
      }
    }
  ])
}

export const getTranslationsActions = (t) => {
  return [
    {
      title: t('Translations:Import'),
      onClick: () => {
        openModal('import', {
          onSubmit: (file) => ImportTranslationsApi.importTranslation(file)
        })
      }
    },
    {
      title: t('Export'),
      onClick: async () => {
        addNotification({ message: t('Shell:ExportStarted') })
        const blob = await ExportTranslationsApi.exportAllTranslation()
        saveAs(blob, `Translations-export.xlsx`)
      }
    },
    {
      title: t('Translations:NewTranslation'),
      onClick: () => {
        openModal('newTranslation', {
          type: 'GROUP'
        })
      }
    },
    {
      title: t('Translations:Search'),
      onClick: () => {
        openModal('search', {
          onSubmit: (value) => ResultTranslationApi.resultTranslation(value)
        })
      }
    }
  ]
}

// Memoized fetchDefault function
// It changes iff the outputName and the instanceId changes
// With maxSize = 1, it updates every time the page changes
/* const createFetchDefault = memoize(
  (outputName, instanceId) => {
    const { getFilterList } = createFilterbarApiActions(outputName, instanceId)

    return async () => {
      const { name, sections } = first(await getFilterList(true))

      return { name, sections }
    }
  },
  { maxSize: 1 }
)

// In this point, the platform checks if the filterbar contains the props for the backend
// If it is the case, it transforms the props, passing the functions to be used in order to
// open the modals for the list and the save
export const getFilterProps = ({ servicesConfig, ...props }) => {
  if (isEmpty(servicesConfig)) {
    return props
  }

  const {
    outputName,
    modals = {},
    instanceId,
    translationNamespace = ''
  } = servicesConfig

  const { list = 'filterList', save = 'filterSave' } = modals

  const saveFilter = (data) => {
    const { activeModals } = modalStore.get()

    if (!activeModals.includes(save)) {
      openModal(save, { data, outputName })
    }
  }

  const filterList = (filterNameRendered) => {
    const { activeModals } = modalStore.get()

    if (!activeModals.includes(list)) {
      openModal(list, { outputName, instanceId, filterNameRendered })
    }
  }

  return {
    ...props,
    servicesConfig: {
      saveFilter,
      filterList,
      translationNamespace,
      fetchDefault: createFetchDefault(outputName, instanceId)
    }
  }
}
*/
