import React from 'react'
import { withRouter } from 'react-router-dom'

const Breadcrumbs = ({ t, currentRoute, type, instances, items, match }) => {
  const { params } = match

  const currentInstance = instances.find((instance) => {
    return params?.scenarioId
      ? instance.id === Number(params?.scenarioId)
      : instance.id === Number(params?.instanceId)
  })

  if (type === 'inputView' || currentRoute?.path === '/home') {
    const currentWT = items.find((WT) => {
      return WT.id === Number(params?.workingTableId)
    })

    return (
      <div className="ublique-platform-breadcrumbs">
        {currentInstance && <span>{currentInstance.title} / </span>}
        {currentWT && <span>{t(`Sidebar:${currentWT.groups}`)} / </span>}
        {currentWT && <span>{t(`Sidebar:${currentWT.title}`)}</span>}
      </div>
    )
  }

  if (type === 'translationsView' || currentRoute?.path === '/home') {
    const currentTR = items?.find((tr) => {
      return (
        tr.groups?.toUpperCase() === params.group?.toUpperCase() &&
        tr.type === params.type
      )
    })

    return (
      <div className="ublique-platform-breadcrumbs">
        {currentTR && <span>{t(`Sidebar:TRANSLATIONS`)} / </span>}
        {currentTR && (
          <span>{t(`Sidebar:${currentTR.groups.toUpperCase()}`)} / </span>
        )}
        {currentTR && <span>{t(`Sidebar:${currentTR.type.toUpperCase()}`)}</span>}
      </div>
    )
  }

  return null
}

export default withRouter(React.memo(Breadcrumbs))
