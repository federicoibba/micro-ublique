import React, { PureComponent, Suspense } from 'react'
import { Route, Router as ReactRouter, Switch } from 'react-router-dom'
import { withTranslation } from 'react-i18next'
import moize from 'moize'
import memoize from 'moize'
import history from 'services/history'
import ModalHandler from 'components/Modal'
import Shell from '@ublique/components/Shell'
import Loading from '@ublique/components/Loading'
import { getI18nControls } from 'utils/hooks/useI18nControls'
import { compact } from 'lodash'
import compose from 'lodash/fp/compose'
import { withStoreCurried } from '../../utils/hoc/withStore'

import { handleInstanceItems, handleScenarioItems, mapRoutesToHeaders } from './utils'
import { canReadResource } from '../../services/auth'
import { PATH_UBLIQUE_HEADER } from '../../App/routes'

// Use the current language (first argument) as a key, since the entire i18n object does not change
const memoizedGetI18nControls = moize((_, ...rest) => getI18nControls(...rest), {
  maxArgs: 1
})

const getHeaderList = memoize((routes, instances, scenario, t) => {
  const headerItems = mapRoutesToHeaders(routes, t)
  let scenarioItem,
    instancesItem = null

  // instances section for Ublique
  const instancesItemIndex = headerItems.findIndex(
    (item) => item.id === PATH_UBLIQUE_HEADER.INSTANCES
  )

  if (instancesItemIndex !== -1) {
    instancesItem = handleInstanceItems(headerItems[instancesItemIndex], instances)
    headerItems.splice(instancesItemIndex, 1)
  }

  // scenario section for Ublique
  const scenarioItemIndex = headerItems.findIndex(
    (item) => item.id === PATH_UBLIQUE_HEADER.SCENARIO
  )

  if (scenarioItemIndex !== -1) {
    scenarioItem = handleScenarioItems(headerItems[scenarioItemIndex], scenario)
    headerItems.splice(scenarioItemIndex, 1)
  }

  return compact([instancesItem, scenarioItem, ...headerItems]).filter((route) =>
    canReadResource(route.id)
  )
})

class Router extends PureComponent {
  get i18nControls() {
    const { i18n, languagesMenuConfig } = this.props
    return memoizedGetI18nControls(i18n.language, i18n, languagesMenuConfig)
  }

  getShellProps = ({
    expandSidebar,
    contentWrapperClassname,
    themeSelection,
    shell = true,
    sidebar = true,
    filterBar = false,
    contentPadding = true,
    showSidebarSearch = true
  }) => {
    const { logo, userActionMenu, instances, scenario, user, routes, t } = this.props
    return {
      showHeader: shell,
      showSidebar: shell && sidebar,
      showFilterbar: shell && filterBar,
      userActionList: userActionMenu,
      contentClassname: contentWrapperClassname,
      i18nControls: this.i18nControls,
      headerList: getHeaderList(routes, instances, scenario, t),
      showSidebarSearch,
      themeSelection,
      contentPadding,
      expandSidebar,
      // filterProps,
      productLogo: logo,
      userName: user?.sub
    }
  }

  renderRoute = (route, index) => {
    const { path, routes, component: Component, exact = false } = route

    /*if (!Component && routes) {
      return routes.map(this.renderRoute)
    }*/

    return (
      <Route key={route.id || route.path || index} path={path} exact={exact}>
        <Shell {...this.getShellProps(route)}>
          <Component />
        </Shell>
      </Route>
    )
  }

  render() {
    const { routes, modals, children } = this.props
    return (
      <Suspense fallback={<Loading />}>
        <ReactRouter history={history}>
          <Switch>{routes.map(this.renderRoute)}</Switch>
          {children}
          <ModalHandler modals={modals} />
        </ReactRouter>
      </Suspense>
    )
  }
}

const subscriptions = ['instances', 'filter', 'sidebarList', 'user', 'scenario']
const i18nNamespaces = ['Shell', 'Sidebar', 'Common']

export default compose(
  React.memo,
  withTranslation(i18nNamespaces),
  withStoreCurried(subscriptions)
)(Router)
