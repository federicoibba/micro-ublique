import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'

import Modal from '@ublique/components/Modal'
import TextInput from '@ublique/components/TextInput'

import { Information20 } from '@carbon/icons-react'

import { addNotification } from 'components/Notifications/actions'

import {
  isPasswordRepeated,
  isPasswordValid,
  checkEmptyPasswords,
  aesCbcEncrypt,
  KEY,
  IV
} from './helpers'
import './index.scss'

import { updatePassword } from 'services/api/user'

const PasswordManagement = ({ open, close }) => {
  const { t } = useTranslation('Common')

  const [currentPassword, setCurrentPassword] = useState('')
  const [newPassword, setNewPassword] = useState('')
  const [repeatPassword, setRepeatPassword] = useState('')
  const [currentPasswordIsValid, setCurrentPasswordIsValid] = useState(true)
  const [newPasswordIsValid, setNewPasswordIsValid] = useState(false)
  const [newPasswordIsRepeated, setNewPasswordIsRepeated] = useState(false)
  const [disableSubmit, setDisableSubmit] = useState(true)
  const [currentError, setCurrentError] = useState('')
  const [newError, setNewError] = useState('')
  const [repeatError, setRepeatError] = useState('')

  // manage disable submit button.
  useEffect(() => {
    setDisableSubmit(
      !(currentPasswordIsValid && newPasswordIsValid && newPasswordIsRepeated) ||
        checkEmptyPasswords(currentPassword, newPassword, repeatPassword)
    )
  }, [
    currentPasswordIsValid,
    newPasswordIsValid,
    newPasswordIsRepeated,
    currentPassword,
    newPassword,
    repeatPassword
  ])

  // manage new password validation
  useEffect(() => {
    const newPasswordValidation = isPasswordValid(newPassword)
    setNewPasswordIsValid(newPasswordValidation.valid)
    setNewError(newPasswordValidation.message)
  }, [newPassword])

  // manage repeated password validation
  useEffect(() => {
    const repeatValidation = isPasswordRepeated(newPassword, repeatPassword)
    setNewPasswordIsRepeated(repeatValidation.valid)
    setRepeatError(repeatValidation.message)
  }, [newPassword, repeatPassword])

  const manageSubmit = async () => {
    const encryptedCurrent = aesCbcEncrypt(currentPassword, KEY, IV)
    const encryptedNew = aesCbcEncrypt(newPassword, KEY, IV)
    const encryptedRepeat = aesCbcEncrypt(repeatPassword, KEY, IV)

    try {
      await updatePassword({
        currentPassword: encryptedCurrent,
        newPassword: encryptedNew,
        repeatPassword: encryptedRepeat
      })
      addNotification({
        id: 'password-updated-successfully',
        kind: 'success',
        type: 'inline',
        title: t('PASSWORD_UPDATED_SUCCESSFULLY_TITLE'),
        message: t('PASSWORD_UPDATED_SUCCESSFULLY_CAPTION')
      })
      close()
    } catch (error) {
      const { message } = error
      const formattedMessage = message.replace(' ', '_')
      if (message.includes('current') && !message.includes('different')) {
        setCurrentError(formattedMessage)
        setCurrentPasswordIsValid(false)
      }
      if (message.includes('different')) {
        setNewError(formattedMessage)
        setNewPasswordIsValid(false)
      }
    }
  }

  return (
    <Modal
      open={open}
      onRequestClose={close}
      onRequestSubmit={manageSubmit}
      preventCloseOnClickOutside
      modalHeading={t('PASSWORD_MANAGEMENT')}
      primaryButtonText={t('Save')}
      primaryButtonDisabled={disableSubmit}
      secondaryButtonText={t('Cancel')}
      className="modal-password-management"
    >
      <div className="modal-password-management-inner-box">
        <div className="password-management-input-container">
          <TextInput
            className=""
            value={currentPassword}
            id="current-password"
            name="current-password"
            invalid={!currentPasswordIsValid}
            invalidText={t(currentError)}
            labelText={t('CURRENT_PASSWORD')}
            autoComplete=""
            light
            onChange={({ target: { value } }) => setCurrentPassword(value)}
            placeholder={t('CURRENT_PASSWORD')}
            type="password"
            required
          />
        </div>
        <div className="password-management-input-container">
          <TextInput
            className=""
            value={newPassword}
            id="new-password"
            name="new-password"
            invalid={!newPasswordIsValid}
            invalidText={t(newError)}
            labelText={t('NEW_PASSWORD')}
            autoComplete=""
            light
            onChange={({ target: { value } }) => setNewPassword(value)}
            placeholder={t('NEW_PASSWORD')}
            type="password"
            required
          />
        </div>
        <div className="password-management-input-container">
          <TextInput
            className=""
            value={repeatPassword}
            id="repeat-new-password"
            name="repeat-new-password"
            invalid={!newPasswordIsRepeated}
            invalidText={t(repeatError)}
            labelText={t('REPEAT_NEW_PASSWORD')}
            autoComplete=""
            light
            onChange={({ target: { value } }) => setRepeatPassword(value)}
            placeholder={t('REPEAT_NEW_PASSWORD')}
            type="password"
            required
          />
        </div>
      </div>
      <div className="update-password-guideline">
        <Information20 className="update-password-info-icon" />
        <span>{t('UPDATE_PASSWORD_GUIDELINE')}</span>
      </div>
    </Modal>
  )
}

export default PasswordManagement
