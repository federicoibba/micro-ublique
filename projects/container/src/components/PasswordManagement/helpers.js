import CryptoJS from 'crypto-js'

const regex =
  /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[\]:;<>,.?/~_=|+-]).{8,20}$/

export const KEY = '1234567890123456'
export const IV = '1234567890123456'

export const isPasswordRepeated = (newPassword, repeatedPassword) => {
  return newPassword === repeatedPassword
    ? { valid: true, message: null }
    : { valid: false, message: 'REPEAT_PASSWORD_ERROR' }
}

export const isPasswordValid = (password) => {
  if (password.length > 0) {
    return regex.test(password)
      ? { valid: true, message: null }
      : { valid: false, message: 'NEW_PASSWORD_ERROR' }
  }
  return { valid: true, message: null }
}

export const checkEmptyPasswords = (
  currentPassword,
  newPassword,
  repeatedPassword
) => {
  return currentPassword === '' || newPassword === '' || repeatedPassword === ''
}

export const aesCbcEncrypt = (message, key, iv) => {
  const keyHex = CryptoJS.enc.Utf8.parse(key)
  const ivHex = CryptoJS.enc.Utf8.parse(iv)
  const messageHex = CryptoJS.enc.Utf8.parse(message)
  const encrypted = CryptoJS.AES.encrypt(messageHex, keyHex, {
    iv: ivHex,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7
  })
  return encrypted.toString()
}
