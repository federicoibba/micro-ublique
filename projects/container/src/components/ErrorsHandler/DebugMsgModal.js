import React from 'react'
import { useTranslation } from 'react-i18next'
import Modal from '@ublique/components/Modal'

function DebugMsgModal({ open, close, data }) {
  const { error } = data
  const { t } = useTranslation('Common')

  return (
    <Modal
      size="md"
      open={open}
      onRequestClose={close}
      modalHeading={t('Errors.ERROR')}
      className="debug-msg-modal"
      passiveModal
    >
      <p>{error.debugMessage}</p>
    </Modal>
  )
}

export default DebugMsgModal
