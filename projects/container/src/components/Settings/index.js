import React, { useState } from 'react'
// import { toPairs } from 'lodash'
import prop from 'lodash/fp/prop'
import { useTranslation } from 'react-i18next'
import Modal from '@ublique/components/Modal'
import RadioButton from '@ublique/components/RadioButton'
import useStoreSelector from 'utils/hooks/useStoreSelector'
// import useI18nControls from 'utils/hooks/useI18nControls'
import './index.scss'

const selectVersion = prop('appVersion')

const Settings = ({ open, close, themes, onThemeChange }) => {
  const { t } = useTranslation('Common')

  /* const {
    languages,
    onLanguageChange,
    selectedLanguage: currentLanguage
  } = useI18nControls() */

  const currentTheme = useStoreSelector('theme')
  const appVersion = useStoreSelector('appConfig', selectVersion)

  const [initialState] = useState({
    // language: currentLanguage,
    theme: currentTheme
  })

  const manageThemeChange = (id) => {
    onThemeChange({ id })
  }

  const handleClose = () => {
    onThemeChange({ id: initialState.theme })
    // onLanguageChange(initialState.language)
    return close()
  }

  return (
    <Modal
      open={open}
      onRequestClose={handleClose}
      onRequestSubmit={close}
      modalHeading={t('Settings')}
      primaryButtonText={t('Save')}
      secondaryButtonText={t('Cancel')}
      className="modal-settings"
    >
      {/* <div className='modal-settings-inner-box'>
        <h6>{t('Language')}</h6>
        <RadioButton.ButtonGroup
          legend='Language'
          labelPosition='right'
          orientation='vertical'
          name='radio-button-group-language'
          onChange={onLanguageChange}
          valueSelected={currentLanguage}
        >
          {toPairs(languages).map(([langCode, lang]) => (
            <RadioButton
              id={`radio-${lang.short}`}
              key={lang.short}
              value={langCode}
              labelText={lang.full}
            />
          ))}
        </RadioButton.ButtonGroup>
      </div> */}

      <div className="modal-settings-inner-box">
        <h6>{t('Theme')}</h6>
        <RadioButton.ButtonGroup
          legend="Theme"
          labelPosition="right"
          orientation="vertical"
          name="radio-button-group-theme"
          onChange={manageThemeChange}
          valueSelected={currentTheme}
        >
          {themes.map((theme) => (
            <RadioButton
              id={`radio-${theme.id}`}
              key={theme.id}
              value={theme.id}
              labelText={t(theme.label)}
            />
          ))}
        </RadioButton.ButtonGroup>
      </div>

      <div className="platform-info">
        <span>Ublique v{appVersion}</span>
      </div>
    </Modal>
  )
}

export default Settings
