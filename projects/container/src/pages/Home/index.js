/**
 * Created by fgallucci on 17/06/2020.
 */
import React from 'react'
import { isNull } from 'lodash'
import { useHistory } from 'react-router-dom'
import useRedirect from 'services/auth/useRedirect'
import Button from '@ublique/components/Button'

function Home() {
  const history = useHistory()
  // const redirect = useRedirect({ fallbackPath: fallbackRedirectPath })

  // if (!isNull(redirect) && redirect !== '/home') {
  //   return <Redirect to={redirect} />
  // }

  return (
    <div>
      <h1>Home</h1>
      <Button
        onClick={() => {
          console.log('push')
          history.push('/scenario')
        }}
      >
        Go to scenario
      </Button>
    </div>
  )
}

export default Home
