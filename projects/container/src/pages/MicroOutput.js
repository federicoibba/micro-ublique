import React, { useRef, useEffect, useState } from 'react'
import Loading from '@ublique/components/Loading'
import { useHistory } from 'react-router-dom'

import { mount } from 'microoutput/App'

export default function MicroOutput() {
  const [loading, setLoading] = useState(false)
  const ref = useRef(null)
  const history = useHistory()

  useEffect(() => {
    // Tentativo di risolvere lo schermo bianco quando l'import del microfrontend non è lazy
    // React.lazy e Suspense non funzionano quando l'export non è un componente React
    // Funziona ancora male
    // setLoading(true)
    // const { mount } = await import('microoutput/App')
    // setLoading(false)

    const { onParentNavigate, unmount } = mount(ref.current, {
      initialPath: history.location.pathname,
      onNavigate: ({ pathname: nextPathname }) => {
        const { pathname } = history.location
        if (pathname !== nextPathname) {
          history.push(nextPathname)
        }
      }
    })

    history.listen(onParentNavigate)

    return unmount
  }, [])

  return (
    <>
      {loading && <Loading />}
      <div ref={ref} />
    </>
  )
}
