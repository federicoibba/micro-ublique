import { mount } from 'microanagrafica/App'
import React, { useRef, useEffect } from 'react'
import { useHistory } from 'react-router-dom'

export default function MicroAnagrafica() {
  const ref = useRef(null)
  const history = useHistory()

  useEffect(() => {
    const { onParentNavigate, unmount } = mount(ref.current, {
      initialPath: history.location.pathname,
      onNavigate: ({ pathname: nextPathname }) => {
        const { pathname } = history.location

        if (pathname !== nextPathname) {
          history.push(nextPathname)
        }
      }
    })

    history.listen(onParentNavigate)

    return unmount
  }, [])

  return <div ref={ref} />
}
