import { mount } from 'microscenario/App'
import React, { useRef, useEffect } from 'react'
import { useHistory } from 'react-router-dom'

export default function Microscenario() {
  const ref = useRef(null)
  const history = useHistory()

  useEffect(() => {
    // console.log(history.location.pathname)
    const { onParentNavigate, unmount } = mount(ref.current, {
      initialPath: history.location.pathname,
      onNavigate: ({ pathname: nextPathname }) => {
        const { pathname } = history.location

        if (pathname !== nextPathname) {
          history.push(nextPathname)
        }
      }
    })

    history.listen(onParentNavigate)

    return unmount
  }, [])

  return <div ref={ref} />
}
