import { BASE_URL } from 'config'
import { appReadyStore, synapse, userStore } from 'state'

export async function login(username, password) {
  await synapse.login(`${BASE_URL}/authentication/signin`, {
    username,
    password
  })

  const user = await synapse.user()
  userStore.set(user)
}

export async function logout() {
  await synapse.logout()

  const user = await synapse.user()
  userStore.set(user)
  appReadyStore.set(false)
}

export async function refreshToken() {
  try {
    if (localStorage.getItem('token')) {
      const response = await synapse.fetch(
        `${BASE_URL}/authentication/refreshtoken`,
        { method: 'POST', retry: 0 }
      )

      localStorage.setItem('token', response?.body?.token)

      return true
    }

    return false
  } catch (e) {
    return false
  }
}
