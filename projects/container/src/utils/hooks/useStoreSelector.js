import { useEffect, useState, useRef, useContext } from 'react'
import { identity } from 'lodash'
import StoreInstancesContext from 'context/StoreInstancesContext'

const useStoreSelector = (storeKey, selector = identity) => {
  const globalStore = useContext(StoreInstancesContext) || {}

  const selectedStore = globalStore[storeKey]
  const initialStoreState = selectedStore?.get(selector)

  const [state, setState] = useState(initialStoreState)
  const lastSelectedState = useRef(initialStoreState)

  useEffect(() => {
    if (!selectedStore) {
      return console.warn(
        `[useStoreSelector hook]: ${storeKey} is not a valid store key`
      )
    }

    const unsubscribe = selectedStore.subscribe((value) => {
      const selectedState = selector(value)

      if (selectedState !== lastSelectedState.current) {
        setState(selectedState)
        lastSelectedState.current = selectedState
      }
    })

    return unsubscribe
  }, [])

  return state
}

export default useStoreSelector
