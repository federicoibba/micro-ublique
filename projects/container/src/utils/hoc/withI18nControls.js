import React from 'react'
import useI18nControls from 'hooks/useI18nControls'

export const withI18nControls = (WrappedComponent) =>
  React.memo(
    React.forwardRef((props, ref) => {
      const i18nControls = useI18nControls()
      return <WrappedComponent ref={ref} {...props} i18nControls={i18nControls} />
    })
  )

export default withI18nControls
