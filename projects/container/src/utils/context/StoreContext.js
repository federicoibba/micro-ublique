/**
 * Created by fgallucci on 27/04/2020.
 */

import React from 'react'

// Context for Synapse stores value
const StoreContext = React.createContext()
StoreContext.displayName = 'Store'

export default StoreContext
