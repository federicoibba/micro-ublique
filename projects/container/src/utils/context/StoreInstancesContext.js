import React from 'react'

// Context for Synapse stores instances `{ set, get, update, subscribe }`
// Used in 'useStoreSelector' hook
const StoreInstancesContext = React.createContext()
StoreInstancesContext.displayName = 'StoreInstances'

export default StoreInstancesContext
