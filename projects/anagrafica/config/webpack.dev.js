const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin')
const { merge } = require('webpack-merge')
const commonConfig = require('./webpack.common')

const deps = require('../package.json').dependencies

const PORT = 8083

const devConfig = {
  mode: 'development',
  output: {
    publicPath: `http://localhost:${PORT}/`
  },
  devServer: {
    port: PORT,
    historyApiFallback: true
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(scss|sass)$/,
        use: ['style-loader', 'css-loader', 'fast-sass-loader']
      }
    ]
  },
  plugins: [
    new ModuleFederationPlugin({
      name: 'microanagrafica',
      filename: 'remoteEntry.js',
      exposes: {
        './App': './src/bootstrap'
      },
      shared: {
        ...deps,
        react: {
          singleton: true,
          requiredVersion: deps.react
        },
        'react-dom': {
          singleton: true,
          requiredVersion: deps['react-dom']
        },
        'react-router': {
          singleton: true,
          requiredVersion: deps['react-router']
        },
        'react-router-dom': {
          singleton: true,
          requiredVersion: deps['react-router-dom']
        }
      }
    })
  ],
  devtool: 'inline-source-map'
}

module.exports = merge(commonConfig, devConfig)
