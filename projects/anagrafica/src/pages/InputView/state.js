export const initialDocumentState = {
  documentsById: {},
  editedDocuments: [],
  latestEdit: null,
  selection: {
    selectAll: false,
    total: 0,
    count: 0,
    excluded: {},
    included: {}
  }
}

export const initialLinkedDocumentsState = {
  _uriKeys_: [],
  documentsByUri: {}
}
