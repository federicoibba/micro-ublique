const { updateColumnsPosition } = require('../helpers/column-sorting/helpers')

describe('Test column sorting helpers', () => {
  const items = [
    { id: 'A1', position: 3 },
    { id: 'A2', position: 2 },
    { id: 'A3', position: 4 },
    { id: 'B1', position: 1 },
    { id: 'B2', position: 5 },
    { id: 'B3', position: 8 },
    { id: 'C1', position: 7 },
    { id: 'C2', position: 6 }
  ]

  it('Updates items position correctly, moving to the right - 1', () => {
    const expected = [
      { id: 'A1', position: 3 },
      { id: 'A2', position: 2 },
      { id: 'A3', position: 5 },
      { id: 'B1', position: 1 },
      { id: 'B2', position: 4 },
      { id: 'B3', position: 8 },
      { id: 'C1', position: 7 },
      { id: 'C2', position: 6 }
    ]
    expect(updateColumnsPosition(items, ['A3', 4, 5])).toEqual(expected)
  })

  it('Updates items position correctly, moving to the right - 2', () => {
    const expected = [
      { id: 'A1', position: 3 },
      { id: 'A2', position: 2 },
      { id: 'A3', position: 7 },
      { id: 'B1', position: 1 },
      { id: 'B2', position: 4 },
      { id: 'B3', position: 8 },
      { id: 'C1', position: 6 },
      { id: 'C2', position: 5 }
    ]
    expect(updateColumnsPosition(items, ['A3', 4, 7])).toEqual(expected)
  })

  it('Updates items position correctly, moving to the left', () => {
    const expected = [
      { id: 'A1', position: 4 },
      { id: 'A2', position: 3 },
      { id: 'A3', position: 5 },
      { id: 'B1', position: 1 },
      { id: 'B2', position: 6 },
      { id: 'B3', position: 2 },
      { id: 'C1', position: 8 },
      { id: 'C2', position: 7 }
    ]
    expect(updateColumnsPosition(items, ['B3', 8, 2])).toEqual(expected)
  })

  // B1, A3, A1, B2, A2, B3, C1, C2
  // B1, A1, B2, A2, B3, A3, C1, C2
  it('Updates items position with missing positions, moving to the left - 1', () => {
    const items = [
      { id: 'A1', position: 3 },
      { id: 'A2' },
      { id: 'A3', position: 2 },
      { id: 'B1', position: 1 },
      { id: 'B2', position: 4 },
      { id: 'B3' },
      { id: 'C1' },
      { id: 'C2' }
    ]
    const expected = [
      { id: 'A1', position: 2 },
      { id: 'A2', position: 4 },
      { id: 'A3', position: 6 },
      { id: 'B1', position: 1 },
      { id: 'B2', position: 3 },
      { id: 'B3', position: 5 },
      { id: 'C1', position: 7 },
      { id: 'C2', position: 8 }
    ]
    expect(updateColumnsPosition(items, ['A3', 2, 6])).toEqual(expected)
  })

  // B1, A3, A1, B2, A2, B3, C1, C2
  // B1, A2, A3, A1, B2, B3, C1, C2
  it('Updates items position with missing positions, moving to the left - 2', () => {
    const items = [
      { id: 'A1', position: 3 },
      { id: 'A2' },
      { id: 'A3', position: 2 },
      { id: 'B1', position: 1 },
      { id: 'B2', position: 4 },
      { id: 'B3' },
      { id: 'C1' },
      { id: 'C2' }
    ]
    const expected = [
      { id: 'A1', position: 4 },
      { id: 'A2', position: 2 },
      { id: 'A3', position: 3 },
      { id: 'B1', position: 1 },
      { id: 'B2', position: 5 },
      { id: 'B3', position: 6 },
      { id: 'C1', position: 7 },
      { id: 'C2', position: 8 }
    ]
    expect(updateColumnsPosition(items, ['A2', 5, 2])).toEqual(expected)
  })

  // B1, A3, A1, B2, A2, B3, C1, C2
  // B1, A3, B2, A2, B3, C1, C2, A1
  it('Updates items position with missing positions, moving to the right', () => {
    const items = [
      { id: 'A1', position: 3 },
      { id: 'A2' },
      { id: 'A3', position: 2 },
      { id: 'B1', position: 1 },
      { id: 'B2', position: 4 },
      { id: 'B3' },
      { id: 'C1' },
      { id: 'C2' }
    ]
    const expected = [
      { id: 'A1', position: 8 },
      { id: 'A2', position: 4 },
      { id: 'A3', position: 2 },
      { id: 'B1', position: 1 },
      { id: 'B2', position: 3 },
      { id: 'B3', position: 5 },
      { id: 'C1', position: 6 },
      { id: 'C2', position: 7 }
    ]
    expect(updateColumnsPosition(items, ['A1', 3, 8])).toEqual(expected)
  })

  it('Updates items position with no positions, moving to the right', () => {
    const items = [
      { id: 'A1' },
      { id: 'A2' },
      { id: 'A3' },
      { id: 'B1' },
      { id: 'B2' },
      { id: 'B3' },
      { id: 'C1' },
      { id: 'C2' }
    ]
    const expected = [
      { id: 'A1', position: 0 },
      { id: 'A2', position: 7 },
      { id: 'A3', position: 1 },
      { id: 'B1', position: 2 },
      { id: 'B2', position: 3 },
      { id: 'B3', position: 4 },
      { id: 'C1', position: 5 },
      { id: 'C2', position: 6 }
    ]
    expect(updateColumnsPosition(items, ['A2', 1, 7])).toEqual(expected)
  })

  // A1, B3, A2, A3, B1, B2, C1, C2
  it('Updates items position with no positions, moving to the left', () => {
    const items = [
      { id: 'A1' },
      { id: 'A2' },
      { id: 'A3' },
      { id: 'B1' },
      { id: 'B2' },
      { id: 'B3' },
      { id: 'C1' },
      { id: 'C2' }
    ]
    const expected = [
      { id: 'A1', position: 0 },
      { id: 'A2', position: 2 },
      { id: 'A3', position: 3 },
      { id: 'B1', position: 4 },
      { id: 'B2', position: 5 },
      { id: 'B3', position: 1 },
      { id: 'C1', position: 6 },
      { id: 'C2', position: 7 }
    ]
    expect(updateColumnsPosition(items, ['B3', 5, 1])).toEqual(expected)
  })
})
