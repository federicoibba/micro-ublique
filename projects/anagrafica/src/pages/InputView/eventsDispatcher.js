import { synapse } from 'services/state'
import { documentEvents } from './constants'

export default {
  refreshEvent(data) {
    synapse.publish(documentEvents.DATA_REFRESH_REQUESTED, data)
  },
  crudEvent(data) {
    synapse.publish(documentEvents.DOCUMENT_CRUD, data)
  },
  unsavedDocumentEvent(data) {
    synapse.publish(documentEvents.DOCUMENT_UNSAVED, data)
  },
  unsavedDocumentsEvent(data) {
    synapse.publish(documentEvents.MULTIPLE_DOCUMENTS_UNSAVED, data)
  },
  saveDocumentEvent(data) {
    synapse.publish(documentEvents.DOCUMENT_SAVE_REQUESTED, data)
  },
  emptyDocumentCreated(data) {
    synapse.publish(documentEvents.EMPTY_DOCUMENT_CREATED, data)
  },
  documentRefreshed(data) {
    synapse.publish(documentEvents.DOCUMENT_REFRESHED, data)
  },
  documentsMultisaveEvent() {
    synapse.publish(documentEvents.MULTISAVE_REQUESTED)
  },
  documentTabClosed(data) {
    synapse.publish(documentEvents.DOCUMENT_TAB_CLOSED, data)
  }
}
