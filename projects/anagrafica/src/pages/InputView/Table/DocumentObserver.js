import { useEffect } from 'react'
import { noop } from 'lodash'
import { synapse } from 'services/state'
import { openModal } from 'components/Modal/actions'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { extraKeys } from 'utils/documents/DocumentMapper'
import { documentEvents } from '../constants'
import { selectLatestEdit } from '../selectors'
import dispatcher from '../eventsDispatcher'

const { DOCUMENT_ID_KEY } = extraKeys

const DocumentObserver = ({ gridApi }) => {
  const document = useStoreSelector('documents', selectLatestEdit)

  useEffect(() => {
    if (document && gridApi) {
      const documentId = document[DOCUMENT_ID_KEY]
      const node = gridApi.api.getRowNode(documentId)
      node?.setData(document)
    }
  }, [gridApi, document])

  useEffect(() => {
    const unsubscribers = [
      synapse.subscribe(
        documentEvents.DOCUMENT_UNSAVED,
        ({ title, isNewDocument, id, callback = noop }) => {
          openModal('saveDocument', {
            onDropEdits: !isNewDocument
              ? () => {
                  dispatcher.documentRefreshed(id)
                  callback()
                }
              : callback,
            onSaveDocument: () => {
              dispatcher.saveDocumentEvent({ id, callback })
            },
            title: { key: 'confirmSaveSingle', params: { title } },
            message: { key: 'saveBeforeContinue' }
          })
        }
      ),
      synapse.subscribe(
        documentEvents.MULTIPLE_DOCUMENTS_UNSAVED,
        ({ documentType }) => {
          openModal('saveDocument', {
            onDropEdits: () => dispatcher.refreshEvent({ overwrite: true }),
            onSaveDocument: dispatcher.documentsMultisaveEvent,
            title: { key: 'confirmSaveMultiple' },
            message: {
              key: 'saveBeforeContinueMultiple',
              params: { documentType }
            }
          })
        }
      )
    ]

    return () => {
      unsubscribers.forEach((fn) => fn())
    }
  }, [])

  return null
}

export default DocumentObserver
