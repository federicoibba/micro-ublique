import React from 'react'
import { get } from 'lodash'
import TabsOpenerLink from '@ublique/components/TabsOpenerLink'
import { extraKeys } from 'utils/documents/DocumentMapper'
import useStoreSelector from 'utils/hooks/useStoreSelector'

const { IS_BEING_EDITED } = extraKeys

const CellLinkRenderer = ({ data, value, node }) => {
  const isBeingEdited = useStoreSelector('documents', ({ documentsById }) =>
    get(documentsById, [node.id, IS_BEING_EDITED])
  )

  const id = `${data.type}:${data.id.activityId}:${data.id.id}`
  const tabsOpenerData = {
    type: data.type,
    id: data.id
  }

  return (
    <div className="ublique-cell-link-renderer">
      <TabsOpenerLink title={value} data={tabsOpenerData} id={id}>
        {value}
      </TabsOpenerLink>
      {isBeingEdited && <span className="row-unsaved-edits" />}
    </div>
  )
}
export default CellLinkRenderer
