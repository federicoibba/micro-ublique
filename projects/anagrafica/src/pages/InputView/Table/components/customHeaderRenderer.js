import React, { useCallback, useEffect, useMemo, useState, useRef } from 'react'
import { size, keys } from 'lodash'
import { documentsStore } from 'services/state'
import useStoreSelector from 'utils/hooks/useStoreSelector'

const selectSelectionStore = (store) => store.selection

export default ({
  api,
  column,
  setSort,
  displayName,
  enableSorting,
  showColumnMenu
}) => {
  const [noSort, setNoSort] = useState('inactive')
  const [ascSort, setAscSort] = useState('inactive')
  const [descSort, setDescSort] = useState('inactive')
  const menuRef = useRef()

  const { selectAll, excluded, included } = useStoreSelector(
    'documents',
    selectSelectionStore
  )

  const onSortChanged = () => {
    setAscSort(column.isSortAscending() ? 'active' : 'inactive')
    setDescSort(column.isSortDescending() ? 'active' : 'inactive')
    setNoSort(
      !column.isSortAscending() && !column.isSortDescending() ? 'active' : 'inactive'
    )
  }

  useEffect(() => {
    column.addEventListener('sortChanged', onSortChanged)
    onSortChanged()
  }, [])

  const onPaginationChanged = useCallback(
    ({ keepRenderedRows }) => {
      if (keepRenderedRows && selectAll) {
        api.forEachNode((node) => node.setSelected(true))
      }
    },
    [selectAll]
  )

  useEffect(() => {
    api.addEventListener('paginationChanged', onPaginationChanged)

    return () => api.removeEventListener('paginationChanged', onPaginationChanged)
  }, [onPaginationChanged])

  // TODO handle if a click in indeterminated with selectAll=true is performed,
  // then it has to select everything
  const onChangeSelectAll = () => {
    api.forEachNode((node) => node.setSelected(!selectAll))

    documentsStore.update((prevState) => ({
      ...prevState,
      selection: {
        ...prevState.selection,
        selectAll: !prevState.selection.selectAll,
        count: selectAll ? 0 : prevState.selection.total
      }
    }))
  }

  const onSortRequested = (order, event) => {
    setSort(order, event.shiftKey)
  }

  const onMenuClicked = () => {
    showColumnMenu(menuRef.current)
  }

  const getRequest = () => {
    if (noSort === 'active') {
      return 'desc'
    }

    if (descSort === 'active') {
      return 'asc'
    }

    return ''
  }

  const getSelectionClasses = () => {
    const classes = 'ag-wrapper ag-input-wrapper ag-checkbox-input-wrapper '

    if (size(keys(excluded)) > 0 || size(keys(included)) > 0) {
      return `${classes} ag-indeterminate`
    }

    if (!selectAll) {
      return classes
    }

    return `${classes} ag-checked`
  }

  const renderSorting = useMemo(
    () => (
      <div style={{ display: 'inline-block' }}>
        {descSort === 'active' && (
          <div className={`customSortDownLabel ${ascSort}`}>
            <span className="ag-icon ag-icon-desc" />
          </div>
        )}
        {ascSort === 'active' && (
          <div className={`customSortUpLabel ${descSort}`}>
            <span className="ag-icon ag-icon-asc" />
          </div>
        )}
      </div>
    ),
    [enableSorting, ascSort, descSort]
  )

  return (
    <div style={{ display: 'flex', alignItems: 'center' }}>
      <div className="ag-selection-checkbox" onClick={onChangeSelectAll}>
        <div className={getSelectionClasses()}>
          <input
            id="custom-select-all"
            type="checkbox"
            aria-label="Toggle Row Selection"
            className="ag-input-field-input ag-checkbox-input"
          />
        </div>
      </div>
      <div
        style={{ display: 'flex', flex: 1 }}
        className="customHeaderLabel"
        onClick={(event) => onSortRequested(getRequest(), event)}
        onTouchEnd={(event) => onSortRequested(getRequest(), event)}
      >
        {displayName}
        {renderSorting}
      </div>
      <span
        ref={menuRef}
        onClick={onMenuClicked}
        className="ag-header-icon ag-header-cell-menu-button"
        // aria-hidden='true'
        style={{
          opacity: 1,
          transition: 'opacity 0.2s ease 0s, border 0.2s ease 0s'
        }}
      >
        <span className="ag-icon ag-icon-menu" unselectable="on" />
      </span>
    </div>
  )
}
