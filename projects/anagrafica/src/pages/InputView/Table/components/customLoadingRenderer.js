import React from 'react'
import Skeleton from '@ublique/components/Skeleton'

const CustomLoadingRenderer = () => (
  <div className="ag-custom-loading-cell">
    <Skeleton />
  </div>
)

export default CustomLoadingRenderer
