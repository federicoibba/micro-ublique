import React from 'react'
import useDocumentTranslation from 'utils/hooks/useDocumentTranslation'
import './index.scss'

const Option = ({ currentSelection, workingTableType, labelKey, valueKey }) => {
  const { t } = useDocumentTranslation(workingTableType)
  return (
    <span>
      {[]
        .concat(currentSelection)
        .filter(Boolean)
        .map((item, index, array) => (
          <span key={item[valueKey]} className="grid-option-renderer">
            {t(`Options:${item[labelKey]}`)}
            {index !== array.length - 1 ? ', ' : ''}
          </span>
        ))}
    </span>
  )
}

export default Option
