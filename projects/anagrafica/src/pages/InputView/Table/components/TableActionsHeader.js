import React from 'react'
import { saveAs } from 'file-saver'
import { useTranslation } from 'react-i18next'
import Button from '@ublique/components/Button'
import { addNotification } from 'components/Notifications/actions'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { tableDataMode } from 'pages/InputView/constants'
import { hasEditedDocumentsOfType, selectSelection } from 'pages/InputView/selectors'
import { openModal } from 'components/Modal/actions'
import { ImportApi } from 'services/api/import'
import { ExportApi } from 'services/api/export'
import { getRequestFullBody, refreshDocuments } from 'pages/InputView/actions'

const { CLIENT_SIDE } = tableDataMode

function TableActionsHeader({
  gridApi,
  columnApi,
  tableMode,
  onAddNew,
  onDelete,
  onRefresh,
  onSave,
  workingTableId,
  workingTableType,
  didColumnsMove,
  onColumnsMoveUndo,
  onColumnsPositionSave,
  canCreate,
  canImport,
  canDelete,
  canExport = true
}) {
  const { t } = useTranslation('Document')
  const { count: selectedRows } = useStoreSelector('documents', selectSelection)
  const hasEditedDocuments = useStoreSelector(
    'documents',
    hasEditedDocumentsOfType(workingTableType)
  )

  const toggleMasterRows = () => {
    const rowsPerPage = gridApi.paginationGetPageSize()
    const currentPage = gridApi.paginationGetCurrentPage()
    const firstIndex = rowsPerPage * currentPage
    const lastIndex = firstIndex + rowsPerPage - 1

    gridApi[
      tableMode === CLIENT_SIDE ? 'forEachNodeAfterFilterAndSort' : 'forEachNode'
    ]((node, index) => {
      if (node.master && index >= firstIndex && index <= lastIndex) {
        node.setExpanded(false)
      }
    })
  }

  const onImportHandler = () =>
    openModal('import', {
      onSubmit: (file) =>
        ImportApi.importDocumentsByType(workingTableId, workingTableType, file).then(
          refreshDocuments
        )
    })

  const onExportHandler = async () => {
    addNotification({ message: t('Shell:ExportStarted') })
    const payload = getRequestFullBody({ api: gridApi, columnApi })
    const { body, fileName } = await ExportApi.exportDocumentsByType(
      workingTableId,
      workingTableType,
      payload
    )
    saveAs(body, fileName)
  }

  const onDeleteHandler = () =>
    openModal('confirmAction', {
      message: 'confirmDeletionQuestion',
      itemsCount: selectedRows,
      onConfirm: onDelete
    })

  const buttons = [
    {
      title: 'UndoColumnsMove',
      onClick: onColumnsMoveUndo,
      visible: didColumnsMove
    },
    {
      title: 'SaveColumnsPosition',
      onClick: onColumnsPositionSave,
      visible: didColumnsMove
    },
    {
      title: 'ManualButton',
      onClick: (_) =>
        onAddNew({ type: workingTableType, activityId: workingTableId }),
      visible: selectedRows === 0 && canCreate
    },
    {
      title: 'DeleteButton',
      onClick: onDeleteHandler,
      visible: selectedRows > 0 && canDelete
    },
    {
      title: 'SaveEdits',
      onClick: onSave,
      visible: hasEditedDocuments
    },
    {
      title: 'ImportButton',
      onClick: onImportHandler,
      visible: selectedRows === 0 && canImport
    },
    {
      title: 'ExportButton',
      onClick: onExportHandler,
      visible: selectedRows > 0 && canExport
    },
    {
      title: 'UpdateButton',
      onClick: onRefresh,
      visible: true
    },
    {
      title: 'collapseMasterRows',
      onClick: toggleMasterRows,
      visible: true
    }
  ]

  const renderButtons = () =>
    buttons.reduce(
      (acc, { title, onClick, visible, disabled, kind = 'primary' }, i) => {
        if (!visible) return acc
        return [
          ...acc,
          <Button
            key={`${title}-${i}`}
            onClick={onClick}
            disabled={disabled}
            kind={kind}
          >
            {t(title)}
          </Button>
        ]
      },
      []
    )

  return (
    <div style={{ display: 'flex', width: '100%' }}>
      {selectedRows > 0 && (
        <div className="selected-row-count active-selection">
          {t('MainTable.SelectedRowCount', {
            count: selectedRows
          })}
        </div>
      )}
      <div className="document-actions-row">{renderButtons()}</div>
    </div>
  )
}

export default React.memo(TableActionsHeader)
