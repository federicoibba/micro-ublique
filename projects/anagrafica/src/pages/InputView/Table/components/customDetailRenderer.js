import React, { Component } from 'react'
import DetailItemsTable from 'components/DetailItemsTable'

// Add here other custom details
const detailComponentsMap = {
  grid: DetailItemsTable
}

const UnsupportedComponent = ({ type }) => (
  <p>Component of type "{type}" is not supported</p>
)

/*
 * The CustomDetailRenderer will iterate through all the fields of a section (section = expandable group)
 * and will render the detail component specified with 'type', passing down the entire document
 */
class CustomDetailRenderer extends Component {
  render() {
    const { data, api } = this.props

    // Each section can potentially have more than one field, like a grid and a map
    return (
      <div>
        {data.fields.map((field, i) => {
          const DetailComponent =
            detailComponentsMap[field.type] || UnsupportedComponent
          return (
            <DetailComponent
              key={`${i}-${field.id}`}
              masterGridApi={api}
              {...field}
              {...this.props}
            />
          )
        })}
      </div>
    )
  }
}

export default CustomDetailRenderer
