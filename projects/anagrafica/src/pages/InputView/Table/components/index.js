import withRemoteOptions from 'utils/documents/withRemoteOptions'
import withGridCellEditor from 'utils/documents/withGridCellEditor'
import formComponents from 'components/DocumentFormComponents'
import CustomDateComponent from 'components/AgGridCustomFilters/CustomDateComponent'
import CellLinkRenderer from './cellLinkRenderer'
import CustomHeaderRenderer from './customHeaderRenderer'
import CustomLoadingRenderer from './customLoadingRenderer'
import SingleCellLinkRenderer from './singleCellLinkRenderer'
import Link from './Link'
import Option from './Option'

const {
  text,
  number,
  date,
  checkbox,
  time,
  datetime,
  select,
  autocomplete,
  autocompletelink,
  multiselect,
  multiselectlink
} = formComponents

export const frameworkComponents = {
  cellLinkRenderer: CellLinkRenderer,
  singleCellLinkRenderer: SingleCellLinkRenderer,
  loadingCellRenderer: CustomLoadingRenderer,
  customHeader: CustomHeaderRenderer,
  agDateInput: CustomDateComponent,

  'cellRenderer:option': withRemoteOptions(Option),
  'cellRenderer:link': withRemoteOptions(Link),

  'cellEditor:text': withGridCellEditor(text),
  'cellEditor:number': withGridCellEditor(number),
  'cellEditor:date': withGridCellEditor(date),
  'cellEditor:checkbox': withGridCellEditor(checkbox),
  'cellEditor:time': withGridCellEditor(time),
  'cellEditor:datetime': withGridCellEditor(datetime),
  'cellEditor:select': withGridCellEditor(select, {
    stopEditingOnChange: true
  }),
  'cellEditor:autocomplete': withGridCellEditor(autocomplete, {
    stopEditingOnChange: true
  }),
  'cellEditor:autocompletelink': withGridCellEditor(autocompletelink, {
    stopEditingOnChange: true
  }),
  'cellEditor:multiselect': withGridCellEditor(multiselect, {
    stopEditingOnChange: true
  }),
  'cellEditor:multiselectlink': withGridCellEditor(multiselectlink, {
    stopEditingOnChange: true
  })
}

export const cellRenderers = {
  select: 'cellRenderer:option',
  autocomplete: 'cellRenderer:option',
  autocompletelink: 'cellRenderer:link',
  multiselect: 'cellRenderer:option',
  multiselectlink: 'cellRenderer:link'
}

export const cellEditors = Object.keys(formComponents).reduce(
  (acc, name) => ({ ...acc, [name]: `cellEditor:${name}` }),
  {}
)
