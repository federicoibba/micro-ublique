import React, { Component } from 'react'
import TabsOpenerLink from '@ublique/components/TabsOpenerLink'

export default class CellLinkRenderer extends Component {
  render() {
    if (!this.props.value) return null
    const data = this.props.value
    if (!data.id) return this.props.value
    const id = `${data.type}:${data.id.activityId}:${data.id.id}`
    const tabData = {
      type: data.type,
      id: data.id
    }

    const value = data.title

    return (
      <TabsOpenerLink title={value} data={tabData} id={id}>
        {value}
      </TabsOpenerLink>
    )
  }
}
