import React from 'react'
import TabsOpenerLink from '@ublique/components/TabsOpenerLink'
import './index.scss'

const Link = ({ currentSelection, workingTableType, labelKey, valueKey }) => {
  return (
    <span>
      {[]
        .concat(currentSelection)
        .filter(Boolean)
        .map((item, index, array) => (
          <span key={item[valueKey]} className="grid-link-renderer">
            <TabsOpenerLink
              title={item[labelKey]}
              data={item}
              id={item.code} // Something unique to avoid reopening other tabs with the same document
            >
              {item[labelKey]}
            </TabsOpenerLink>
            {index !== array.length - 1 ? ', ' : ''}
          </span>
        ))}
    </span>
  )
}

export default Link
