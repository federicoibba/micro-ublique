import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { isEmpty, uniqueId } from 'lodash'
import cn from 'classnames'
import DataTable from '@ublique/components/DataTable'

import DocumentMapper, { extraKeys } from 'utils/documents/DocumentMapper'
import { buildRowId } from 'utils/documents'
import { useAgGridTranslation } from 'utils/hooks/useDocumentTranslation'
import makeConditionalFormatter from 'components/ConditionalFormatter'
import { synapse } from 'services/state'
import { useIsSuperuser } from 'services/auth/useUserRoles'

import {
  getFieldsFlatMap,
  getDetailItemIds,
  getTranslatedColumnDefinitions,
  groupSections
} from '../helpers/table-config'
import {
  tableDataMode,
  defaultColumnDefs,
  defaultGridOptions,
  documentEvents,
  GENERAL_CONFIG
} from '../constants'
import { useTableData } from '../helpers/data/useTableData'
import useColumnVisibility from '../helpers/useColumnVisibility'
import useColumnSorting from '../helpers/column-sorting/useColumnSorting'
import { getRowHeight, refreshAllColumnFilters } from '../helpers/misc'
import useDebouncedQuickFilterListeners from '../helpers/useDebouncedQuickFilterListeners'
import {
  closeTabsAndDispatchUniqueCrudEvents,
  deleteCurrentlySelectedDocuments,
  startMultipleDocumentsSavingProcess,
  updateDocuments
} from '../actions'
import dispatcher from '../eventsDispatcher'

import DocumentObserver from './DocumentObserver'
import CustomDetailRenderer from './components/customDetailRenderer'
import TableActionsHeader from './components/TableActionsHeader'
import { frameworkComponents } from './components'

const { DOCUMENT_ID_KEY } = extraKeys

const ROW_FORMATTER_STYLE = 'borderLeft'

const getRowNodeId = (data) => data[DOCUMENT_ID_KEY]

const InputViewTable = ({
  t,
  models,
  modelTitle,
  instanceId,
  workingTableType,
  workingTableId,
  permissionsManager,
  openTab
}) => {
  const { showHeaderInDetailTables } = models[GENERAL_CONFIG] || {}
  const model = models[workingTableType]
  const { rules, sections, clientMode, formatting, readOnly } = model

  const [gridApi, setGridApi] = useState()

  const tableMode = clientMode ? tableDataMode.CLIENT_SIDE : tableDataMode.SERVER_SIDE

  const { t: agGridT, destroyed } = useAgGridTranslation()

  const fieldsMap = useMemo(() => getFieldsFlatMap(sections), [sections])

  const { mainSection, detailSections } = useMemo(
    () => groupSections(sections),
    [sections]
  )

  const detailItemIds = useMemo(
    () => getDetailItemIds(detailSections),
    [detailSections]
  )

  const mapper = useMemo(
    () => new DocumentMapper({ instanceId, fieldsMap, detailItemIds }),
    [detailItemIds, fieldsMap, instanceId]
  )

  const isSuperuser = useIsSuperuser()

  const tableDataModeProps = useTableData(tableMode, {
    wtId: workingTableId,
    wtType: workingTableType,
    gridApi: gridApi?.api,
    language: destroyed,
    fieldsMap,
    mapper
  })

  const columnVisibility = useColumnVisibility({ modelType: workingTableType })

  const columnSorting = useColumnSorting({
    modelType: workingTableType,
    isSuperuser
  })

  useDebouncedQuickFilterListeners()

  // Ugly workaround to pass updated instances to 'detailCellRendererParams'
  // AG Grid keeps using the instances of the first initialization
  const mutableInstancesRef = useRef({
    detailSections,
    columnSorting
  })

  const conditionalFormatter = useMemo(
    () =>
      formatting
        ? makeConditionalFormatter({
            modelType: workingTableType,
            style: ROW_FORMATTER_STYLE
          })
        : undefined,
    [workingTableType]
  )

  const columnDefs = useMemo(
    () =>
      getTranslatedColumnDefinitions(mainSection.fields, {
        t,
        permissionsManager,
        tableMode,
        rules,
        fieldsMap, // Not included in dependencies on purpose, add if necessary
        workingTableType,
        workingTableId,
        isSuperuser,
        isColumnHidden: columnVisibility.isColumnHidden,
        disableEditing: readOnly,
        firstColumnType: 'masterRow'
      }),
    [
      t,
      mainSection,
      isSuperuser,
      permissionsManager,
      workingTableType,
      workingTableId,
      rules,
      readOnly
    ]
  )

  const deleteSelectedDocuments = useCallback(
    () =>
      deleteCurrentlySelectedDocuments({
        tableMode,
        gridApi,
        workingTableType,
        workingTableId,
        instanceId
      }),
    [gridApi, workingTableId, workingTableType, instanceId, tableMode]
  )

  const isRowMaster = useCallback(
    (document) =>
      detailItemIds.some(
        (key) => permissionsManager.canRead(key) && document[key]?.length > 0
      ),
    [detailItemIds, permissionsManager]
  )

  const openTabWithNewDocument = useCallback(
    ({ type, activityId } = {}) => {
      // Only send permissions if the new document is of the same type of
      // the documents in the table, so they won't be refetched needlessly
      const permissions =
        type == workingTableType && activityId == workingTableId // eslint-disable-line eqeqeq
          ? permissionsManager.rawPermissions
          : undefined
      const temporaryId = uniqueId('new_')

      openTab(
        t(`Models:DocumentModel-${type}.${models[type].title}`),
        `${type}:${activityId}:${temporaryId}`,
        { type, id: { activityId, temporaryId }, permissions }
      )
    },
    [workingTableId, workingTableType, permissionsManager, openTab]
  )

  const dispatchSaveMultipleEvent = useCallback(
    () => dispatcher.unsavedDocumentsEvent({ documentType: modelTitle }),
    [modelTitle]
  )

  const refreshAndCancelEdits = useCallback(
    () => dispatcher.refreshEvent({ overwrite: true }),
    []
  )

  // --------------------------------- Event handlers --------------------------------- //

  const onGridReady = useCallback((params) => {
    setGridApi(params)
    if (columnSorting.enabled) {
      columnSorting.addGridApi({ documentId: 'main' }, params)
    }
  }, [])

  const onCellValueChangedHandler = useCallback(
    ({ value, data, column }) => {
      // Exclude details rules, they are handled by the corresponding detail component
      const mainSectionRules = Object.entries(rules).reduce(
        (acc, [field, rules]) => (rules.$each ? acc : { ...acc, [field]: rules }),
        {}
      )
      updateDocuments({
        documentId: buildRowId(data.id),
        field: column.colId,
        rules: mainSectionRules,
        mapper,
        value
      })
    },
    [rules, mapper]
  )

  // --------------------------------- Effects --------------------------------- //

  useEffect(() => {
    mutableInstancesRef.current = {
      columnSorting,
      detailSections
    }
  }, [columnSorting, detailSections])

  useEffect(() => {
    return synapse.subscribe(documentEvents.MULTISAVE_REQUESTED, async () => {
      const savedDocuments = await startMultipleDocumentsSavingProcess({
        documentType: workingTableType,
        sections,
        mapper,
        rules,
        t
      })
      if (savedDocuments) {
        closeTabsAndDispatchUniqueCrudEvents(savedDocuments, instanceId)
        dispatcher.refreshEvent({ overwrite: true })
      }
    })
  }, [rules, sections, t, mapper, workingTableType, instanceId])

  useEffect(() => {
    return synapse.subscribe(
      documentEvents.EMPTY_DOCUMENT_CREATED,
      openTabWithNewDocument
    )
  }, [openTabWithNewDocument])

  useEffect(() => {
    if (!gridApi) return

    return synapse.subscribe(
      documentEvents.DOCUMENT_CRUD,
      ({ type: crudType, instanceId: crudInstanceId }) => {
        if (crudType === workingTableType && crudInstanceId === instanceId) {
          refreshAllColumnFilters(gridApi)
        }
      }
    )
  }, [gridApi])

  // --------------------------------- Grid master/detail config --------------------------------- //

  const detailCellRendererParams = ({ data: masterRowData }) => ({
    detailGridOptions: {
      columnDefs: [
        {
          field: 'name',
          cellRenderer: 'agGroupCellRenderer',
          flex: 1
        }
      ],
      headerHeight: 0,
      masterDetail: true,
      detailCellRenderer: 'customDetail',
      suppressCsvExport: true,
      suppressExcelExport: true,
      getRowHeight,
      detailCellRendererParams() {
        return {
          documentId: masterRowData[DOCUMENT_ID_KEY],
          documentObjectId: masterRowData.id,
          showHeader: showHeaderInDetailTables, // Enable add/delete/import/export
          columnSorting: mutableInstancesRef.current.columnSorting,
          initialPageSize: 4,
          fieldsMap,
          workingTableType,
          workingTableId,
          permissionsManager,
          columnVisibility,
          readOnly,
          mapper,
          rules
        }
      },
      frameworkComponents: {
        customDetail: CustomDetailRenderer
      }
    },
    autoHeight: true,
    getDetailRowData({ data, successCallback }) {
      const rowData = []

      for (const section of mutableInstancesRef.current.detailSections) {
        // A detail section could have more than one field (for instance a table and a map)
        // This is future-proof, as currently every section always includes one field, another table
        for (const { id } of section.fields) {
          // Only include readable detail sections that have items inside
          if (permissionsManager.canRead(id) && !isEmpty(data[id])) {
            rowData.push({ id, name: t(section.title), fields: section.fields })
          }
        }
      }

      successCallback(rowData)
    }
  })

  const className = cn('inputview-grid', {
    'conditional-formatting': !!conditionalFormatter,
    'conditional-formatting-border-left':
      !!conditionalFormatter && ROW_FORMATTER_STYLE === 'borderLeft'
  })

  const canCreate = permissionsManager.canCreate()
  const canDelete = permissionsManager.canDelete()

  if (destroyed) return null

  return (
    <>
      <DocumentObserver gridApi={gridApi} />
      <DataTable
        {...defaultGridOptions}
        {...tableDataModeProps}
        onGridReady={onGridReady}
        getRowNodeId={getRowNodeId}
        canSearch
        columnDefs={columnDefs}
        defaultColDef={defaultColumnDefs}
        minHeight={300}
        className={className}
        rowSelection="multiple"
        isRowMaster={isRowMaster}
        masterDetail
        detailCellRendererParams={detailCellRendererParams}
        paginationPageSize={10}
        localeTextFunc={agGridT}
        getRowStyle={conditionalFormatter}
        loadingCellRenderer="loadingCellRenderer"
        frameworkComponents={frameworkComponents}
        onCellValueChanged={onCellValueChangedHandler}
        onColumnVisible={columnVisibility.columnVisibilityHandler}
        onColumnMoved={columnSorting.onColumnMoved}
        onDragStopped={columnSorting.onDragStopped}
        extraHeaderComponentContainerClassName="extraComponent"
        blockLoadDebounceMillis={300}
        rowClass="master-row"
        popupParent={document.querySelector('.ublique-datatable')}
        suppressCsvExport
        suppressExcelExport
        extraHeaderComponent={
          <TableActionsHeader
            workingTableId={workingTableId}
            workingTableType={workingTableType}
            canCreate={canCreate}
            canDelete={canDelete}
            canImport={canCreate}
            onAddNew={openTabWithNewDocument}
            onDelete={deleteSelectedDocuments}
            onSave={dispatchSaveMultipleEvent}
            onRefresh={refreshAndCancelEdits}
            didColumnsMove={columnSorting.didColumnsMove}
            onColumnsMoveUndo={columnSorting.onColumnsMoveUndo}
            onColumnsPositionSave={columnSorting.onColumnsPositionSave}
          />
        }
      />
    </>
  )
}

export default React.memo(InputViewTable)
