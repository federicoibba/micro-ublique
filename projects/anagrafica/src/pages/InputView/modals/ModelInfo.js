import React from 'react'
import { useTranslation } from 'react-i18next'
import DataTable from '@ublique/components/DataTable'
import Modal from '@ublique/components/LoadingModal'
import Tabs from '@ublique/components/Tabs'
import Tab from '@ublique/components/Tabs/Tab'

import {
  fieldsSectionColDef,
  formatSectionData
} from '../helpers/column-definitions/model-Info'

import '../index.scss'
import {
  formatConditionalFormattingData,
  formatRulesData,
  getLabelMap
} from '../helpers/rules-parser/rulesRenderer'

const ModelInfo = ({ data, open, close }) => {
  const { modelDefinition, modelTitle } = data

  const { t: modelInfo } = useTranslation(['ModelInfo', 'Options', 'Sidebar'])
  const { t } = useTranslation('Models')
  const documentT = (val) => {
    const toTranslate = `DocumentModel-${modelDefinition.title
      .toLowerCase()
      .replaceAll('_', '')}.${val}`
    const label = t(toTranslate)
    return label === toTranslate ? val : label
  }
  const labelMap = getLabelMap(modelDefinition.sections, documentT)
  const tablesTab = [
    {
      id: 1,
      tabTitle: modelInfo('SECTIONS'),
      tables: [
        {
          id: 2,
          columnDefs: fieldsSectionColDef(modelInfo),
          rowData: formatSectionData(modelDefinition, documentT, modelInfo, labelMap),
          masterDetail: false,
          pagination: true,
          paginationPageSize: 10
        }
      ]
    },
    {
      id: 3,
      tabTitle: modelInfo('RULES'),
      content: formatRulesData(modelDefinition, documentT, modelInfo, labelMap)
    }
  ]

  if (modelDefinition.formatting !== undefined) {
    tablesTab.push({
      id: 4,
      tabTitle: modelInfo('COND_FORMAT'),
      content: formatConditionalFormattingData(modelDefinition, documentT, modelInfo)
    })
  }

  return (
    <Modal
      open={open}
      size="lg"
      className="model-info-modal"
      hasScrollingContent={false}
      iconDescription={t('close')}
      modalHeading={`${modelTitle} - ${modelInfo('MODEL_DEF')}`}
      onRequestClose={close}
      passiveModal
    >
      <Tabs>
        {modelDefinition &&
          tablesTab.map(({ tabTitle, tables, id, content }) => (
            <Tab title={tabTitle} key={id}>
              <div className="tab-wrapper">
                {tables ? (
                  tables.map(
                    ({
                      id,
                      rowData,
                      columnDefs,
                      headerHeight,
                      masterDetail,
                      pagination,
                      paginationPageSize,
                      detailCellRendererParams
                    }) => (
                      <DataTable
                        key={id}
                        headerHeight={headerHeight}
                        canSearch={false}
                        rowData={rowData}
                        columnDefs={columnDefs}
                        sortable
                        filter
                        resizable
                        pagination={pagination}
                        paginationPageSize={paginationPageSize}
                        masterDetail={masterDetail}
                        detailCellRendererParams={
                          detailCellRendererParams && detailCellRendererParams
                        }
                      />
                    )
                  )
                ) : (
                  <div className="model-info-content">{content}</div>
                )}
              </div>
            </Tab>
          ))}
      </Tabs>
    </Modal>
  )
}

export default ModelInfo
