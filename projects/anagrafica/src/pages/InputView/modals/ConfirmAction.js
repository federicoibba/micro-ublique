import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { noop } from 'lodash'
import Modal from '@ublique/components/LoadingModal'

const ModalConfirmAction = ({ open, close, data }) => {
  const { t } = useTranslation('Document')
  const [loading, setLoading] = useState(false)

  const { message, itemsCount, onConfirm, onCancel } = data

  const onConfirmHandler = () => {
    setLoading(true)
    // The modal is not closed if onConfirm throws an error
    // to properly open another modal with the error details
    Promise.resolve(onConfirm()).then(close).catch(noop)
  }

  return (
    <Modal
      open={open}
      confirmLoading={loading}
      className="modal-confirm"
      hasScrollingContent={false}
      iconDescription={t('close')}
      modalHeading={t('warning')}
      onRequestClose={close}
      onRequestSubmit={onConfirmHandler}
      onSecondarySubmit={onCancel}
      primaryButtonText={t('confirm')}
      confirmLoadingMessage={t('confirm')}
      secondaryButtonText={t('discard')}
      selectorPrimaryFocus="[data-modal-primary-focus]"
    >
      <p className="bx--modal-content__text">{t(message, { count: itemsCount })}</p>
    </Modal>
  )
}

export default ModalConfirmAction
