import React from 'react'
import { useTranslation } from 'react-i18next'
import Modal from '@ublique/components/Modal'
import Button from '@ublique/components/Button'

const { Composed, Header, Body, Footer } = Modal

const ModalSaveDocument = ({ open, close, data }) => {
  const { t } = useTranslation('Document')
  const { title, message, onDropEdits, onSaveDocument } = data

  const onDropEditsHandler = () => {
    Promise.resolve(onDropEdits?.()).then(close)
  }

  const onSaveDocumentHandler = () => {
    Promise.resolve(onSaveDocument?.()).then(close)
  }

  return (
    <Composed
      id="modal-document-save"
      open={open}
      onClose={close}
      preventCloseOnClickOutside
    >
      <Header buttonOnClick={close}>
        <h3 className="bx--modal-header__heading">{`${t(
          title.key,
          title.params
        )}`}</h3>
      </Header>
      <Body>{t(message.key, message.params)}</Body>
      <Footer>
        <Button kind="secondary" onClick={close} data-modal-primary-focus>
          {t('continue')}
        </Button>
        <Button kind="danger" onClick={onDropEditsHandler}>
          {t('discard')}
        </Button>
        <Button onClick={onSaveDocumentHandler}>{t('saveDocument')}</Button>
      </Footer>
    </Composed>
  )
}

export default ModalSaveDocument
