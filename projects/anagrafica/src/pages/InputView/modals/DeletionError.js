import React, { useCallback } from 'react'
import { useTranslation } from 'react-i18next'

import moment from 'moment'
import { saveAs } from 'file-saver'
import { keys, groupBy } from 'lodash'

import Modal from '@ublique/components/Modal'
import Button from '@ublique/components/Button'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { selectModels } from '../selectors'

const MAX_ROWS = 5

const ModalDeletionError = ({ close, open, data }) => {
  const { deletionErrors } = data
  const grouped = groupBy(deletionErrors, 'type')

  const models = useStoreSelector('appConfig', selectModels)
  const { t } = useTranslation('Document')

  const translateDocumentTitle = useCallback(
    (document) => {
      return t(`DocumentModel-${document}:${models[document]?.title}`)
    },
    [t, document, models]
  )

  const downloadLogs = useCallback(() => {
    const reduceDocument = (acc, { name }) => `${acc}${name}\n`

    // Create a text listed of various products
    const text = keys(grouped).reduce(
      (acc, document) =>
        `${acc}\n${translateDocumentTitle(document)} \n${grouped[document].reduce(
          reduceDocument,
          ''
        )}`,
      ''
    )

    const blobText = new Blob([text], { type: 'text/plain' })
    const fileName = `Deletion_error_${moment().format('YYYY-MM-DD_HH-mm')}.txt`

    saveAs(blobText, fileName)
  }, [grouped, translateDocumentTitle])

  return (
    <Modal.Composed id="modal-deletion-error" open={open} onClose={close}>
      <Modal.Header buttonOnClick={close}>
        <h3 className="bx--modal-header__heading">{t('CannotDeleteTitle')}</h3>
      </Modal.Header>
      <Modal.Body>
        <h5>{t('CannotDeleteMessage')}</h5>
        <div className="modal-body-response">
          {keys(grouped).map((document) => (
            <div key={document}>
              <p>{translateDocumentTitle(document)}</p>
              {grouped[document].slice(0, MAX_ROWS).map(({ name }) => (
                <p className="name-error" key={name}>
                  {name}
                </p>
              ))}
              {grouped[document].length > MAX_ROWS && (
                <p className="name-error">
                  (
                  {t('OtherEntries', {
                    count: grouped[document].length - MAX_ROWS
                  })}
                  )
                </p>
              )}
            </div>
          ))}
        </div>
      </Modal.Body>
      <Modal.Footer>
        {deletionErrors.length > MAX_ROWS && (
          <Button kind="secondary" onClick={downloadLogs}>
            {t('DownloadErrorLogs')}
          </Button>
        )}
        <Button onClick={close}>{t('close')}</Button>
      </Modal.Footer>
    </Modal.Composed>
  )
}

export default ModalDeletionError
