import { isEmpty } from 'lodash'
import compose from 'lodash/fp/compose'
import { updateInstanceWorkingTableCount } from 'App/actions'
import { documentsStore, linkedDocumentsStore, synapse } from 'services/state'
import { getDocumentUrl } from 'services/api/documents'
import {
  fetchDocuments,
  getDocumentOptions,
  removePaginatedDocuments
} from 'services/api/workingTables'
import { extraKeys } from 'utils/documents/DocumentMapper'
import { buildRowId, nullFieldsByState } from 'utils/documents'
import { getDefaultValues } from 'utils/documents/default-values'
import { validateFields } from 'utils/validation'
import { closeAllModals, openModal } from 'components/Modal/actions'
import { tableDataMode } from './constants'
import dispatcher from './eventsDispatcher'
import { buildBodyWithSelection } from './helpers/data'
import {
  saveDocument,
  processDocument,
  getEitherDocumentOrValidationErrors,
  displayValidationErrorsIfAny,
  getEitherDocumentsOrAllValidationErrors,
  displayAllValidationErrorsIfAny,
  processAllDocuments,
  saveAllDocuments
} from './helpers/saving'
import { initialDocumentState, initialLinkedDocumentsState } from './state'
import { selectAllEditedDocuments } from './selectors'

const { DOCUMENT_ID_KEY, IS_BEING_EDITED } = extraKeys

export const getDocuments = fetchDocuments

export const resetSelectionStore = () => {
  documentsStore.update((prevState) => ({
    ...prevState,
    selection: initialDocumentState.selection
  }))
}

export const resetDocumentsStore = () => {
  documentsStore.set(initialDocumentState)
}

export const resetLinkedDocumentsStore = () => {
  linkedDocumentsStore.set(initialLinkedDocumentsState)
}

export const refreshDocuments = () => {
  dispatcher.refreshEvent({ overwrite: true })
  updateInstanceWorkingTableCount()
}

export const purgeWorkingTableDocuments = (documentType) => {
  documentsStore.update((prevState) => {
    const documentsById = {}
    const editedDocuments = []

    for (const [key, document] of Object.entries(prevState.documentsById)) {
      // Only keep data on documents that are of a different type
      if (document.type !== documentType) {
        documentsById[key] = document
        editedDocuments.push(key)
      }
    }

    return { ...prevState, documentsById, editedDocuments }
  })
}

export const updateDocuments = ({ documentId, field, value, rules, mapper }) => {
  const { documentsById, latestEdit, editedDocuments, ...prevState } =
    documentsStore.get()

  const updatedDocument = {
    ...documentsById[documentId],
    [IS_BEING_EDITED]: true,
    [field]: value
  }

  const isNew = !updatedDocument.id.id

  const fieldStates = validateFields(updatedDocument, rules, {
    showValidationResult: false
  })

  mapper.frontend
    // At every update, virtual fields (readonly, calculated, ...) are calculated again
    .addVirtualFields(nullFieldsByState(updatedDocument, fieldStates))
    .then((document) => {
      documentsStore.set({
        ...prevState,
        latestEdit: isNew ? latestEdit : documentId,
        editedDocuments:
          isNew || editedDocuments.includes(documentId)
            ? editedDocuments
            : [...editedDocuments, documentId],
        documentsById: { ...documentsById, [documentId]: document }
      })
    })
}

export const loadDocumentIntoStore = async (type, id, mapper) => {
  const documentURL = getDocumentUrl(type, id.activityId, id.id)
  const {
    body: { data: document }
  } = await synapse.fetch(documentURL)

  const mappedDocument = await mapper.frontend.transformDocument(document)
  const documentId = buildRowId(document.id)

  documentsStore.update((prevState) => {
    return {
      ...prevState,
      editedDocuments: prevState.editedDocuments.filter((id) => id !== documentId), // A newly fetched document should not be considered edited
      documentsById: {
        ...prevState.documentsById,
        [documentId]: mappedDocument
      }
    }
  })
}

export const createNewEmptyDocument = async (
  temporaryId,
  {
    mainSection,
    detailItemIds,
    workingTableId,
    permissions,
    defaultValueGetter,
    scenario,
    instanceId,
    mapper
  }
) => {
  const defaultValues = await getDefaultValues(mainSection.fields, {
    defaultValueGetter,
    scenario,
    instanceId
  })
  const newDocument = {
    ...defaultValues,
    ...detailItemIds.reduce((acc, key) => ({ ...acc, [key]: [] }), {}), // Initialize details with an empty list
    ...permissions,
    id: { activityId: workingTableId },
    [DOCUMENT_ID_KEY]: temporaryId
  }

  // We also need to get virtual fields (readonly, calculated, ...) that may be dependent on default values
  const finalNewDocument = !isEmpty(defaultValues)
    ? await mapper.frontend.addVirtualFields(newDocument)
    : newDocument

  documentsStore.update((prevState) => ({
    ...prevState,
    documentsById: {
      ...prevState.documentsById,
      [temporaryId]: finalNewDocument
    }
  }))
}

const composeWithParams = (composition, params) =>
  compose(...composition.map((fn) => fn(params)))

/**
 * Check document validity, displaying a notification if there are errors; if not, process the document and save it.
 * @param {string} documentId
 * @param {Object} params
 * @returns {Promise} The saved document | null
 */
export const startDocumentSavingProcess = (documentId, params) => {
  const document = documentsStore.get(
    ({ documentsById }) => documentsById[documentId]
  )

  const composition = composeWithParams(
    [
      saveDocument,
      processDocument,
      displayValidationErrorsIfAny,
      getEitherDocumentOrValidationErrors
    ],
    params
  )

  return composition(document)
}

/**
 * Save multiple documents, only if all of them are valid; documents must be of the same type.
 * @param {Object} params
 * @returns {Promise} The saved documents | null
 */
export const startMultipleDocumentsSavingProcess = (params) => {
  const editedDocuments = documentsStore.get(selectAllEditedDocuments)

  const sameTypeDocuments = editedDocuments.filter(
    ({ type }) => type === params.documentType
  )

  const composition = composeWithParams(
    [
      saveAllDocuments,
      processAllDocuments,
      displayAllValidationErrorsIfAny,
      getEitherDocumentsOrAllValidationErrors
    ],
    params
  )

  return composition(sameTypeDocuments)
}

export const getRequestFullBody = ({ api, columnApi }) => {
  const selection = documentsStore.get((store) => store.selection)

  const filterModel = api.getFilterModel()
  const sortModel = api.getSortModel()
  const start = api.paginationGetCurrentPage() * 10

  const { body } = buildBodyWithSelection({
    selection,
    filterModel,
    sortModel,
    start,
    columnApi
  })
  return body
}

export const handleDeletionError = ({ responseBody }) => {
  const { documents } = JSON.parse(responseBody.debugMessage)
  closeAllModals()
  openModal('documentDeletionErrors', { deletionErrors: documents })

  throw new Error(responseBody) // Rethrow to properly work with modals
}

export const deleteCurrentlySelectedDocuments = async ({
  tableMode,
  gridApi,
  workingTableId,
  workingTableType,
  instanceId
}) => {
  const body = getRequestFullBody(gridApi)

  if (tableMode === tableDataMode.CLIENT_SIDE) {
    body.filter = {}
  }

  try {
    const { body: deleted } = await removePaginatedDocuments(
      workingTableId,
      workingTableType,
      body
    )

    closeTabsAndDispatchUniqueCrudEvents(deleted, instanceId)
    gridApi.api.deselectAll()
    resetSelectionStore()
    updateInstanceWorkingTableCount()
    dispatcher.refreshEvent({ overwrite: false })
  } catch (error) {
    handleDeletionError(error)
  }
}

export async function getOptionsFromWTDocument(wtID, type) {
  const { filterBody } = documentsStore.get()
  const { body } = await getDocumentOptions(wtID, type, filterBody)
  return body
}

export const closeTabsAndDispatchUniqueCrudEvents = (documents, instanceId) => {
  documents.forEach(({ id }) => {
    dispatcher.documentTabClosed(id)
  })

  const uniqueDocumentTypes = documents.reduce(
    (acc, { type }) => (acc.includes(type) ? acc : [...acc, type]),
    []
  )

  // Since it only interacts with the options cache, it is better to dispatch a single CRUD event for document type
  uniqueDocumentTypes.forEach((type) => {
    dispatcher.crudEvent({ type, instanceId })
  })
}
