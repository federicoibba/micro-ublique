export const tableDataMode = {
  CLIENT_SIDE: 'clientSideData',
  SERVER_SIDE: 'serverSideData'
}

export const documentEvents = {
  CELL_CHANGED: 'Document:cellChanged',
  DOCUMENT_UNSAVED: 'Document:documentUnsaved',
  MULTIPLE_DOCUMENTS_UNSAVED: 'Document:multipleDocumentsUnsaved',
  DOCUMENT_READY: 'Document:documentReady',
  EMPTY_DOCUMENT_CREATED: 'Document:emptyDocumentCreated',
  DOCUMENT_CRUD: 'Document:documentCrud',
  DATA_REFRESH_REQUESTED: 'Document:dataRefreshRequested',
  DOCUMENT_SAVE_REQUESTED: 'Document:documentSaveRequested',
  DOCUMENT_REFRESHED: 'Document:documentRefreshed',
  MULTISAVE_REQUESTED: 'Document:multisaveRequested',
  DOCUMENT_TAB_CLOSED: 'Document:documentTabClosed'
}

export const MAIN_SECTION_ID = 'DEFINITION'

export const FIELD_POSITION_KEY = 'position'

export const defaultColumnDefs = {
  sortable: true,
  resizable: true,
  lockPinned: true,
  suppressMovable: true,
  floatingFilter: true
}

export const defaultGridOptions = {
  pagination: true,
  suppressRowClickSelection: true,
  stopEditingWhenGridLosesFocus: true,
  suppressColumnVirtualisation: true,
  suppressDragLeaveHidesColumns: true
}

export const QUICK_FILTER_INPUT =
  '.ublique-tabs-content .header-search input.bx--search-input'
export const QUICK_FILTER_DELETE =
  '.ublique-tabs-content .header-search button.bx--search-close'

export const STORAGE_KEY = 'hiddenColumns'

export const GENERAL_CONFIG = '__config'
