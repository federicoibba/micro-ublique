import { MAIN_SECTION_ID } from '../constants'

export const autosizeOrFitColumnsToWidth = ({ columnApi, api }) => {
  const colIds = columnApi.getAllColumns().map(({ colId }) => colId)
  columnApi.autoSizeColumns(colIds, false)

  const { eBodyViewport, columnController } = api.gridPanel
  const availableWidth = eBodyViewport.clientWidth
  const columns = columnController.getAllDisplayedColumns()
  const usedWidth = columnController.getWidthOfColsInList(columns)

  if (usedWidth < availableWidth) {
    api.sizeColumnsToFit()
  }
}

const getDetailGridRowHeight = ({ api, data }) => {
  const detailGrid = api?.getDetailGridInfo(data.id)
  // The detail grid is registered with a bit of delay
  if (detailGrid) {
    return detailGrid.domElement.offsetHeight
  }
  return 0
}

const rowHeightByDetailType = {
  grid: getDetailGridRowHeight,
  map: () => 400 // Just an example
}

export const getRowHeight = (params) => {
  // Ignore rows that are not details
  if (!params.node.detail) return undefined

  return params.data.fields.reduce((acc, field) => {
    const heightCalculator = rowHeightByDetailType[field.type]
    return acc + heightCalculator(params)
  }, 0)
}

export const isMainSection = (section) => section.title === MAIN_SECTION_ID

export const refreshAllColumnFilters = ({ api, columnApi }) => {
  const allColumns = columnApi.getAllColumns()
  const filters = allColumns.map(({ colId }) => api.getFilterInstance(colId))
  filters.forEach((filter) => {
    // Refresh only set filters
    if (filter.setFilterParams) {
      filter.refreshFilterValues()
    }
  })
}
