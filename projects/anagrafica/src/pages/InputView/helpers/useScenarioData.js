import { useEffect, useMemo } from 'react'
import prop from 'lodash/fp/prop'
import { first } from 'lodash'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { documentsStore } from 'services/state'
import { fetchDocuments } from 'services/api/workingTables'
import { selectItems, isScenarioInstance } from '../selectors'

export default function useScenarioData({ instanceId, fieldsMap, isNew }) {
  const scenario = useStoreSelector('documents', prop('scenario'))
  const workingTables = useStoreSelector('sidebarList', selectItems)
  const isScenario = useStoreSelector('instances', isScenarioInstance(instanceId))

  const hasScenarioDep = useMemo(() => {
    if (!isScenario || !isNew) return false
    return Object.values(fieldsMap).some(({ value }) => value?.type === 'scenarioDep')
  }, [fieldsMap, isScenario, isNew])

  useEffect(() => {
    if (!hasScenarioDep || scenario) return

    const scenarioWt = workingTables.find(
      ({ type, idInstance }) =>
        type === 'scenario' && idInstance === parseInt(instanceId)
    )
    fetchDocuments(scenarioWt.id).then(({ items }) => {
      documentsStore.update((store) => ({
        ...store,
        scenario: first(items)
      }))
    })
  }, [hasScenarioDep, scenario])

  return { scenario, hasScenarioDep }
}
