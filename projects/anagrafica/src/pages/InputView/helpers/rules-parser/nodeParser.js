import { first, isString } from 'lodash'

const statesActions = {
  invalid: 'IS_INVALID_WHEN',
  hidden: 'IS_HIDDEN_WHEN',
  disabled: 'IS_DISABLED_WHEN'
}
export const HEADER = 'HEADER'
export const COLOUR_ACTION = 'HAS_COLOUR'

export const parseRules = ({ rules = {} }) => {
  const fieldBySection = getFieldBySection(rules)
  const verbalizedRulesObj = {}
  Object.keys(fieldBySection).forEach((section) => {
    verbalizedRulesObj[section] = parseSection(fieldBySection[section])
  })
  return verbalizedRulesObj
}

const getFieldBySection = (rules) => {
  const fieldBySection = {}
  Object.keys(rules).forEach((field) => {
    if (rules[field]?.$each) {
      fieldBySection[field] = rules[field]?.$each
    } else {
      fieldBySection[HEADER] === undefined
        ? (fieldBySection[HEADER] = { [field]: rules[field] })
        : (fieldBySection[HEADER][field] = rules[field])
    }
  })
  return fieldBySection
}

export const parseSection = (sectionFields) => {
  var verbalizedSection = []
  Object.keys(sectionFields).forEach((mainField) => {
    verbalizedSection = verbalizedSection.concat(
      parseStates(mainField, sectionFields[mainField].states)
    )
  })
  return verbalizedSection
}

export const parseStates = (mainField, states, mainState) => {
  var verbalizedStates = []
  Object.keys(states).forEach((state) => {
    if (states[state].states) {
      // case invalid -> states -> {invalidRule, invalidLength}
      verbalizedStates = verbalizedStates.concat(
        parseStates(mainField, states[state].states, state)
      )
    } else {
      const verbalizedState = {}
      verbalizedState.mainField = mainField
      verbalizedState.statesType = mainState || state
      verbalizedState.action = statesActions[mainState || state] || COLOUR_ACTION
      verbalizedState.when = parseOperations(mainField, states[state].when)
      verbalizedStates.push(verbalizedState)
    }
  })
  return verbalizedStates
}

export const parseOperations = (mainField, opSubjects) => {
  const verbalizedSubjects = []
  Object.keys(opSubjects).forEach((subject) => {
    if (subject.toUpperCase() === '$OR') {
      // case "$or": {field1:{___}, field2:{____}}
      const verbalizedInnerSubjects = []
      Object.keys(opSubjects[subject]).forEach((innerSbj) => {
        // innerSbj = field1   opSubjects[subject][innerSbj] = {___}
        const innerConcreteField = getConcreteValue(innerSbj, mainField)
        verbalizedInnerSubjects.push(
          parseMultiOpPerField(innerConcreteField, opSubjects[subject][innerSbj])
        )
      })
      verbalizedSubjects.push({ $OR: verbalizedInnerSubjects })
    } else {
      const concreteField = getConcreteValue(subject, mainField)
      verbalizedSubjects.push(
        parseMultiOpPerField(concreteField, opSubjects[subject])
      )
    }
  })
  if (verbalizedSubjects.length > 1) {
    return [{ $AND: verbalizedSubjects }]
  } else {
    return verbalizedSubjects
  }
}

const parseMultiOpPerField = (field, operations) => {
  // "field": { "ex": true, "gt": 10 },
  const verbalizedOperations = []
  Object.keys(operations).forEach((op) => {
    if (op.toUpperCase() === '$OR') {
      // case "$or": [{ "in": ["a", "b"] }, { "re": "[0-9]" }]
      const verbalizedInnerSubjects = []
      operations[op].forEach((innerOp) => {
        // innerOp = { "in": ["a", "b"] }
        const concreteOp = Object.keys(innerOp)[0]
        const concreteValue = getConcreteValue(innerOp[concreteOp], field)
        verbalizedInnerSubjects.push(
          parseLeafOperation(field, concreteOp, concreteValue)
        )
      })
      verbalizedOperations.push({ $OR: verbalizedInnerSubjects })
    } else {
      const concreteValue = getConcreteValue(operations[op], field)
      verbalizedOperations.push(parseLeafOperation(field, op, concreteValue))
    }
  })

  if (verbalizedOperations.length > 1) {
    return { $AND: verbalizedOperations }
  } else {
    return first(verbalizedOperations)
  }
}

const getConcreteValue = (original, mainField) => {
  if (!isString(original) || original === '$or') return original

  if (original.startsWith('$')) return original.replace('$', mainField)

  if (original.startsWith('%')) return original.replace('%', '')

  return original
}

export const parseLeafOperation = (field, op, value) => {
  switch (op.toLowerCase()) {
    case 'gt':
    case 'lgt':
      return {
        field: field,
        op: 'GREATER_TO',
        value: value
      }
    case 'gte':
    case 'lgte':
      return {
        field: field,
        op: 'GREATER_OR_EQUAL_TO',
        value: value
      }
    case 'lt':
    case 'dlt':
      return {
        field: field,
        op: 'LESS_THAN',
        value: value
      }
    case 'lte':
    case 'dlte':
      return {
        field: field,
        op: 'LESS_OR_EQUAL_TO',
        value: value
      }
    case 'eq':
      return {
        field: field,
        op: 'EQUALS_TO',
        value: value
      }
    case 'neq':
      return {
        field: field,
        op: 'NOT_EQUALS_TO',
        value: value
      }
    case 'ex':
      return {
        field: field,
        op: value ? 'EXISTS' : 'NOT_EXISTS'
      }
    case 'between':
      return {
        field: field,
        op: 'IS_BETWEEN',
        value: value
      }
    case 'betweene':
      return {
        field: field,
        op: 'IS_BETWEEN_OR_EQUAL',
        value: value
      }
    case 'betweenle':
      return {
        field: field,
        op: 'IS_BETWEEN_LE',
        value: value
      }
    case 'betweenre':
      return {
        field: field,
        op: 'IS_BETWEEN_RE',
        value: value
      }
    case 'in':
      return {
        field: field,
        op: 'IS_CONTAINED_IN',
        value: value
      }
    case 'nin':
      return {
        field: field,
        op: 'IS_NOT_CONTAINED_IN',
        value: value
      }
    case 're':
      return {
        field: field,
        op: 'SATISFY_RE',
        value: value
      }
    case 'nre':
      return {
        field: field,
        op: 'NOT_SATISFY_RE',
        value: value
      }
  }
}
