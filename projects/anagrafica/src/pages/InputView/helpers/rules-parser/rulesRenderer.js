import React from 'react'
import { parseRules, parseStates, HEADER, COLOUR_ACTION } from './nodeParser'
import Accordion, { AccordionItem } from '@ublique/components/Accordion'

import { first } from 'lodash'

export const formatRulesData = (modelDef, documentT, otherT, labelMap) => {
  const rulesSimpleObj = parseRules(modelDef)
  const headerRendered = renderTable(
    HEADER,
    rulesSimpleObj[HEADER],
    documentT,
    otherT,
    labelMap
  )
  delete rulesSimpleObj[HEADER]
  const renderedSections = Object.keys(rulesSimpleObj).map((section) =>
    renderTable(section, rulesSimpleObj[section], documentT, otherT, labelMap)
  )

  return (
    <Accordion>
      {headerRendered}
      {renderedSections}
    </Accordion>
  )
}

const renderTable = (sectionName, section, documentT, otherT, labelMap) => {
  return (
    <AccordionItem
      key={sectionName}
      title={labelMap[sectionName] || otherT(sectionName)}
    >
      {renderSection(sectionName, section, documentT, otherT, labelMap)}
    </AccordionItem>
  )
}

const renderSection = (section, rules, documentT, otherT, labelMap) => {
  const renderedSection = rules.map(({ mainField, action, when, statesType }) => {
    return (
      <div className="model-info-rule-single-rule" key={mainField}>
        <span className="model-info-rule-main-field">
          {labelMap[mainField] || otherT(mainField)}
        </span>{' '}
        {<span className="model-info-main-action">{otherT(action)}</span>}
        {action === COLOUR_ACTION ? (
          <>
            {' '}
            <span style={{ color: statesType }}>{statesType}</span>
            {' ' + otherT('IF') + ' '}
          </>
        ) : (
          ''
        )}
        {verbalizeWhen(first(when), documentT, otherT, labelMap)}
      </div>
    )
  })
  return <>{renderedSection}</>
}

const verbalizeWhen = (when, documentT, otherT, labelMap) => {
  if (when.$OR === undefined && when.$AND === undefined) {
    const { field, op, value } = when
    return (
      <>
        {' '}
        <span className="rule-leaf-field">
          {getFieldLabel(field, documentT, otherT, labelMap)}
        </span>{' '}
        <span className="rule-leaf-op">{otherT(op)}</span>{' '}
        {value && (
          <span className="rule-leaf-value">
            {getValueLabel(value, otherT, labelMap)}
          </span>
        )}
      </>
    )
  } else if (when.$OR !== undefined || when.$AND !== undefined) {
    const conj = first(Object.keys(when))
    const innerVerbalization = when[conj].reduce((acc, curr, idx) => {
      return (
        <>
          {acc}
          {idx !== 0 && (
            <span className="rule-conjunction">{` ${otherT(conj)}`}</span>
          )}
          {verbalizeWhen(curr, documentT, otherT, labelMap)}
        </>
      )
    }, <></>)

    return (
      <>
        {' ('} {innerVerbalization}
        {' )'}
      </>
    )
  }
}

export const getLabelMap = (sections, t) => {
  const fields = []
  sections.forEach((section) => {
    if (section.layout) {
      fields.push(...section.fields)
    } else {
      fields.push({
        id: section.fields[0].id,
        labelText: section.title
      })
      section.fields.forEach((field) => {
        fields.push(
          ...field.columnDefs.map((el) => {
            return {
              field: el.field,
              headerName: el.headerName || el.field
            }
          })
        )
      })
    }
  })
  return fields.reduce(
    (acc, curr) => ({
      ...acc,
      [curr.field || curr.id]: t(
        curr.labelText || curr.headerName || curr.field || curr.id
      )
    }),
    {}
  )
}

const getValueLabel = (value, t, labelMap) => {
  // is an array
  if (Array.isArray(value)) {
    const innerValues = value.reduce((acc, curr, i) => {
      return acc + (i !== 0 ? ',' : '') + getValueLabel(curr)
    }, '')
    return '[' + innerValues + ']'
  }

  // is a boolean
  if (!!value === value) {
    return t(value)
  }

  // is a number
  if (!isNaN(value)) {
    return value
  }

  // is a field
  if (labelMap[value]) {
    return <span className="rule-leaf-field">{labelMap[value]}</span>
  }

  // is an option
  if (t(`Options:${value}`) !== value) {
    return '"' + t(`Options:${value}`).toUpperCase() + '"'
  }

  return value
}

const getFieldLabel = (field, documentT, otherT, labelMap) => {
  // is a simple field
  if (labelMap[field]) {
    return labelMap[field]
  }

  // is like car.wheel
  if (field.includes('.')) {
    const splitted = field.split('.', 2)
    if (splitted.length === 2) {
      if (labelMap[first(splitted)] !== undefined) {
        return labelMap[first(splitted)] + ' ' + otherT(splitted[1])
      }
      if (first(splitted) === 'formatting') {
        return documentT(splitted[1])
      }
    }
  }

  return field
}

export const formatConditionalFormattingData = (modelDef, documentT, otherT) => {
  const formattingSimpleObj = parseStates('ROW', modelDef.formatting?.row?.states)
  const labelMap = getLabelMap(modelDef.sections, documentT)
  return <>{renderSection('ROW', formattingSimpleObj, documentT, otherT, labelMap)}</>
}
