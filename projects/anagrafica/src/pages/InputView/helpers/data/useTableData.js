import { useCallback, useEffect, useState, useMemo } from 'react'
import { getDocuments, purgeWorkingTableDocuments } from 'pages/InputView/actions'
import { documentsStore, synapse } from 'services/state'
import { extraKeys } from 'utils/documents/DocumentMapper'
import { documentEvents, tableDataMode } from '.././../constants'
import { selectionHandler } from '../selection'
import { makeCreateGridDataSource } from './server-side'
import { groupDocumentsById } from '.'

const { CLIENT_SIDE, SERVER_SIDE } = tableDataMode
const { DOCUMENT_ID_KEY } = extraKeys

export const useTableData = (tableData, params) => {
  const { wtId, wtType, mapper, gridApi, language } = params
  const isClientSide = tableData === CLIENT_SIDE

  const [clientSideRowData, setClientSideRowData] = useState()
  const createGridDataSource = useMemo(makeCreateGridDataSource, [language])

  const loadDocuments = useCallback(
    async (body) => {
      try {
        const { items, metadata } = await getDocuments(wtId, body)
        const { total } = metadata.pagination

        const mappedDocuments = await mapper.frontend.transformDocuments(items)

        const { documentsById, selection, ...store } = documentsStore.get()

        // Never overwrite existing documents, the user might be editing them.
        // If they should be replaced, empty the store first.
        const newDocuments = mappedDocuments.filter(isNewItem(documentsById))

        documentsStore.set({
          ...store,
          documentsById: {
            ...documentsById,
            ...groupDocumentsById(newDocuments)
          },
          selection: { ...selection, total }
        })

        return {
          items: mappedDocuments.map(
            (d) => documentsById[d[DOCUMENT_ID_KEY]] || d // Keep previous documents, if any, to preserve edits
          ),
          total
        }
      } catch (e) {
        console.error(e)
      }
    },
    [mapper, wtId]
  )

  useEffect(() => {
    if (isClientSide) {
      loadDocuments().then(({ items }) => setClientSideRowData(items))
    }
  }, [])

  useEffect(() => {
    if (!gridApi) return

    return synapse.subscribe(
      documentEvents.DATA_REFRESH_REQUESTED,
      ({ overwrite }) => {
        if (overwrite) {
          purgeWorkingTableDocuments(wtType)
        }
        if (isClientSide) {
          gridApi.showLoadingOverlay()
          loadDocuments().then(({ items }) => {
            setClientSideRowData(items)
            gridApi.hideOverlay()
          })
        } else {
          gridApi.purgeServerSideCache()
        }
      }
    )
  }, [gridApi, loadDocuments, wtType])

  const rowSelectionHandler = selectionHandler[tableData]

  switch (tableData) {
    case SERVER_SIDE:
      return {
        rowModelType: 'serverSide',
        serverSideStoreType: 'partial',
        cacheBlockSize: 10,
        onRowSelected: rowSelectionHandler,
        serverSideDatasource: createGridDataSource({ ...params, loadDocuments }) // TODO: memoize?
      }
    default:
      // Client-side mode
      return {
        rowData: clientSideRowData,
        onSelectionChanged: rowSelectionHandler,
        autoSizeColumnsOnFirstRender: true
      }
  }
}

const isNewItem = (itemsById) => (item) => {
  return !itemsById[item[DOCUMENT_ID_KEY]]
}
