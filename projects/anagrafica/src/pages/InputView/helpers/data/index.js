import { keys, values } from 'lodash'
import { QUICK_FILTER_INPUT } from 'pages/InputView/constants'
import { buildRowId } from 'utils/documents'

const cellRenderersForDocuments = ['singleCellLinkRenderer', 'cellRenderer:link']

const formatSortModel = (sortModel, columnApi) =>
  sortModel.map(({ colId, sort }) => {
    const { colDef } = columnApi.getColumn(colId) || {}
    // const { innerRenderer } = colDef.cellRendererParams || {} // Controllare l'innerRenderer per la prima colonna?
    return {
      name: colId,
      direction: sort.toUpperCase(),
      isDocument: cellRenderersForDocuments.includes(colDef.cellRenderer)
    }
  })

const getQuickFilter = () => {
  const { value } = document.querySelector(QUICK_FILTER_INPUT)
  return value === '' ? null : value
}

export const groupDocumentsById = (documents) =>
  documents.reduce(
    (acc, document) => ({ ...acc, [buildRowId(document.id)]: document }),
    {}
  )

export const buildPaginatedBody = ({
  filterModel,
  sortModel,
  columnApi,
  start = 0,
  length = 10
}) => ({
  start,
  length,
  sort: formatSortModel(sortModel, columnApi),
  filter: sanitizeFilterModel(filterModel),
  fullTextSearch: getQuickFilter()
})

const mapSelectionValues = (selectedRows) => {
  if (keys(selectedRows).length > 0) {
    return values(selectedRows).map((node) => node?.data || node)
  }

  return []
}

export const buildBodyWithSelection = ({
  selection,
  filterModel,
  sortModel,
  start,
  columnApi
}) => {
  const { included, excluded, selectAll: all } = selection
  const rows = [...mapSelectionValues(included), ...mapSelectionValues(excluded)]

  const ids = rows.map(({ id }) => id)
  const filter = buildPaginatedBody({
    filterModel,
    sortModel,
    start,
    columnApi
  })

  return {
    body: {
      ...filter,
      selection: { all, rows: ids }
    }
  }
}

const sanitizeTextFilter = ({ filter, ...rest }) => ({
  ...rest,
  filter: filter.replace(/'|"/g, '')
})

const sanitizeFilterModel = (filterModel) => {
  return Object.entries(filterModel).reduce((acc, [id, filterBody]) => {
    if (filterBody.filterType === 'text') {
      const { filter, condition1, condition2, ...rest } = filterBody
      // Different structure between single and multiple conditions (AND/OR)
      return filter
        ? { ...acc, [id]: sanitizeTextFilter(filterBody) }
        : {
            ...acc,
            [id]: {
              ...rest,
              condition1: sanitizeTextFilter(condition1),
              condition2: sanitizeTextFilter(condition2)
            }
          }
    }
    return acc
  }, filterModel)
}
