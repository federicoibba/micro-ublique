import { buildPaginatedBody } from '.'
import { autosizeOrFitColumnsToWidth } from '../misc'

export const makeCreateGridDataSource = () => {
  const state = {
    columnAutosized: false
  }

  return function createGridDataSource(params) {
    const { loadDocuments } = params

    return {
      getRows: ({ request, successCallback, failCallback, columnApi, api }) => {
        const { startRow: start, sortModel, filterModel } = request

        const body = buildPaginatedBody({
          filterModel,
          sortModel,
          start,
          columnApi
        })

        loadDocuments(body)
          .then(({ items, total }) => {
            successCallback(items, total)
            if (!state.columnAutosized) {
              autosizeOrFitColumnsToWidth({ columnApi, api })
              state.columnAutosized = true
            }
          })
          .catch((err) => {
            console.error(err)
            failCallback()
          })
      }
    }
  }
}
