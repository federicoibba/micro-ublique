import has from 'lodash/fp/has'
import { addDocument, updateDocument } from 'services/api/documents'
import { validateFields } from 'utils/validation'
import {
  buildFieldsMapWithSection,
  getValidationErrorMessages,
  getValidationTree
} from 'components/Document/utils'
import { addNotification } from 'components/Notifications/actions'

/*
 * Single save utils
 *
 * Retun type in the chain: Either
 * | { document: x }
 * | { errors: y, documentTitle: z }
 */
export const getEitherDocumentOrValidationErrors =
  ({ rules, sections, t }) =>
  (document) => {
    const validationResult = validateFields(document, rules, {
      showValidationResult: false
    })

    const invalidFields = getValidationTree(validationResult)

    if (invalidFields.length > 0) {
      const validationErrors = getValidationErrorMessages(
        invalidFields,
        buildFieldsMapWithSection(sections),
        t
      )
      return { errors: validationErrors, documentTitle: document.title } // left
    }

    return { document } // right
  }

export const displayValidationErrorsIfAny =
  ({ t }) =>
  ({ document, errors }) => {
    if (!errors) return { document }

    addNotification({
      kind: 'warning',
      wrap: true,
      duration: 0,
      message: [t('Document:VALIDATION_WARNING'), errors].join('\n')
    })

    return { errors }
  }

export const processDocument =
  ({ mapper }) =>
  ({ document, errors }) => {
    if (errors) return { errors }

    return { document: mapper.backend.transformDocument(document) }
  }

export const saveDocument =
  ({ documentType }) =>
  ({ document, errors }) => {
    if (errors) return Promise.resolve(null)

    const { id, activityId } = document.id || {}

    const effect = id
      ? () => updateDocument(documentType, activityId, id, document)
      : () => addDocument(documentType, document)

    return effect()
  }

/*
 * Multiple saves utils
 *
 * Retun type in the chain: Either
 * | { documents: [{ document: x1 }, { document: x2 }] }
 * | { errors: [{ errors: y1, documentTitle: z1 }, { errors: y2, documentTitle: z2 }] }
 */
export const getEitherDocumentsOrAllValidationErrors = (params) => (documents) => {
  const maybeDocuments = documents.map(getEitherDocumentOrValidationErrors(params))
  const errors = maybeDocuments.filter(has('errors'))
  return errors.length > 0 ? { errors } : { documents: maybeDocuments }
}

export const displayAllValidationErrorsIfAny =
  ({ t }) =>
  ({ errors, documents }) => {
    if (!errors) return { documents }

    const compositeErrorsString = errors.reduce(
      (acc, { errors, documentTitle }) =>
        `${acc}\n\u2014 ${documentTitle} \u2014\n${errors}`,
      ''
    )

    addNotification({
      kind: 'warning',
      wrap: true,
      duration: 0,
      message: [t('Document:VALIDATION_WARNING'), compositeErrorsString].join('\n')
    })

    return { errors }
  }

export const processAllDocuments =
  (params) =>
  ({ documents, errors }) => {
    if (errors) return { errors }

    return { documents: documents.map(processDocument(params)) }
  }

export const saveAllDocuments =
  (params) =>
  ({ documents, errors }) => {
    if (errors) return Promise.resolve(null)

    return Promise.all(documents.map(saveDocument(params)))
  }
