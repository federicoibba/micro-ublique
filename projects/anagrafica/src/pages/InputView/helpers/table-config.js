import { compact, negate, sortBy } from 'lodash'
import { adapters } from 'utils/documents'
import { FIELD_POSITION_KEY } from '../constants'
import {
  makeGetColumnDefs,
  makeGetEditingColumnDefs,
  makeGetSuperuserColumnDefs
} from './column-definitions'
import { isMainSection } from './misc'

// Get a flat map of all fields, including those who are nested inside sections
export const getFieldsFlatMap = (sections, parent = 'main') => {
  return sections.reduce((acc, section) => {
    const fields = adapters.getFields(section)
    if (fields) {
      return { ...acc, ...getFieldsFlatMap(fields, section.id) }
    }
    return { ...acc, [adapters.getId(section)]: { ...section, parent } }
  }, {})
}

// Get a list of detail item ids, assuming detail items are of 'grid' type
export const getDetailItemIds = (sections) => {
  const isGrid = ({ type }) => type === 'grid'
  const getId = ({ id }) => id
  return sections.reduce((acc, section) => {
    const fields = adapters.getFields(section)
    return [...acc, ...fields.filter(isGrid).map(getId)]
  }, [])
}

const makeIsVisible =
  ({ permissionsManager }) =>
  (field) => {
    if (field.hidden) return false
    return permissionsManager.canRead(adapters.getId(field))
  }

export const getTranslatedColumnDefinitions = (fields, params) => {
  const { disableEditing, permissionsManager, isSuperuser } = params
  const isVisible = makeIsVisible({ permissionsManager })

  const mappers = compact([
    !disableEditing && makeGetEditingColumnDefs(params),
    isSuperuser && makeGetSuperuserColumnDefs(params),
    makeGetColumnDefs(params)
  ])

  const mapFieldToColumnDefs = (field, i) =>
    mappers.reduce((acc, mapper) => ({ ...acc, ...mapper(field, i) }), {})

  const sortedFields = sortBy(fields, FIELD_POSITION_KEY)

  return sortedFields.filter(isVisible).map(mapFieldToColumnDefs)
}

export const groupSections = (sections) => ({
  mainSection: sections.find(isMainSection),
  detailSections: sections.filter(negate(isMainSection))
})
