import { MAIN_SECTION_ID } from 'pages/InputView/constants'
import { isMainSection } from '../misc'

const getHighestPosition = (items) =>
  items.reduce((acc, { position }) => (position ? Math.max(acc, position) : acc), -1)

export const updateColumnsPosition = (
  columns,
  [columnId, oldPosition, newPosition],
  idKey = 'id'
) => {
  let highestPosition = getHighestPosition(columns) // Items without position go after the ones with position

  return columns.map(({ position, ...column }) => {
    const pos = position ?? ++highestPosition

    if (column[idKey] === columnId) {
      return { ...column, position: newPosition }
    }

    // Update intermediates items moving to the right
    if (pos >= oldPosition && pos <= newPosition) {
      return { ...column, position: pos - 1 }
    }

    // Update intermediate items moving to the left
    if (pos >= newPosition && pos <= oldPosition) {
      return { ...column, position: pos + 1 }
    }
    return { ...column, position: pos }
  })
}

export const getFields = (model, sectionId) => {
  if (sectionId === MAIN_SECTION_ID) {
    return model.sections.find(isMainSection)?.fields
  }
  // Detail section
  const detailNode = model.sections.find(({ id }) => id === `${sectionId}_section`)
  return detailNode?.fields.find(({ id }) => id === sectionId)?.columnDefs
}
