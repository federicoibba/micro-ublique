import { replace, trimEnd } from 'lodash'
import { useState, useMemo, useRef } from 'react'
import { updateConfig } from 'services/api/config'
import { appConfigStore } from 'services/state'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { MAIN_SECTION_ID } from '../../constants'
import { selectModelByType } from '../../selectors'
import { autosizeOrFitColumnsToWidth, isMainSection } from '../misc'
import { getFields, updateColumnsPosition } from './helpers'

const buildKey = ({ fieldId, documentId }) =>
  fieldId ? `${fieldId}:${documentId}` : documentId

class AgGridMovableColumnsManager {
  constructor({ notifyColumnMove, model, modelType, gridApis }) {
    this.notifyColumnMove = notifyColumnMove
    this.model = model
    this.modelType = modelType
    this.gridApis = gridApis
    this.startedMoving = false
    this.movedColumn = null
    this.currentDocumentId = null
    this.temporarySections = {}
  }

  addGridApi = (ids, gridApi) => {
    if (this.currentDocumentId && this.currentDocumentId !== ids.documentId) {
      this.suppressMovableColumns(gridApi.api, true)
    }
    this.gridApis[buildKey(ids)] = gridApi
  }

  clearDetailGridData = (ids) => {
    this.currentDocumentId = null
    delete this.temporarySections[ids.fieldId]
    delete this.gridApis[buildKey(ids)]
    this.suppressDetailsMovableColumns(false)
  }

  onColumnMoved = (event) => {
    if (event.column) {
      this.movedColumn = event // Save moved column for later
    }
  }

  onDragStopped = ({ documentId, sectionId = MAIN_SECTION_ID }) => {
    if (!this.movedColumn) return

    const { toIndex, columns } = this.movedColumn
    const [{ colId }] = columns
    // AG Grid appends '_1' when column definitions are changed after initialization ???
    const trimmedColId = trimEnd(colId, '_1')

    const mainSection = sectionId === MAIN_SECTION_ID
    const dynamicId = mainSection ? 'id' : 'field'

    const sections =
      this.temporarySections[sectionId] || getFields(this.model, sectionId)

    const colModelIndex = sections.findIndex(
      ({ [dynamicId]: id }) => id === trimmedColId
    )

    const oldPosition = sections[colModelIndex]?.position ?? colModelIndex

    const updatedModel = updateColumnsPosition(
      sections,
      [trimmedColId, oldPosition, toIndex],
      dynamicId
    )

    if (!this.currentDocumentId && documentId) {
      this.suppressDetailsMovableColumns(true, {
        activeDetailKey: buildKey({ fieldId: sectionId, documentId })
      })
      this.currentDocumentId = documentId
    }
    this.temporarySections[sectionId] = updatedModel
    this.movedColumn = null

    if (!this.startedMoving) {
      this.startedMoving = true
      this.notifyColumnMove(true)
    }
  }

  onColumnsPositionSave = () => {
    const mainFields = this.temporarySections[MAIN_SECTION_ID]

    const updatedModel = {
      ...this.model,
      sections: this.model.sections.map((section) => {
        // Main section
        if (isMainSection(section) && mainFields) {
          return { ...section, fields: mainFields }
        }
        // Detail section
        if (this.temporarySections[replace(section.id, '_section', '')]) {
          return {
            ...section,
            fields: section.fields.map((field) =>
              field.type === 'grid'
                ? { ...field, columnDefs: this.temporarySections[field.id] }
                : field
            )
          }
        }
        return section
      })
    }

    appConfigStore.update((store) => ({
      ...store,
      models: { ...store.models, [this.modelType]: updatedModel }
    }))

    // API call to the backend to save models
    updateConfig(updatedModel, 'model', this.modelType)

    this.movedColumn = null
    this.currentDocumentId = null
    this.startedMoving = false
    this.temporarySections = {}
    this.notifyColumnMove(false)
    // Needed to properly update detail grids after model sections change, due to AG Grid limitations
    this.closeAllMasterRows()
    setTimeout(() => {
      autosizeOrFitColumnsToWidth(this.gridApis.main)
    }, 100)
  }

  onColumnsMoveUndo = () => {
    this.suppressDetailsMovableColumns(false)

    Object.values(this.gridApis).forEach((api) => {
      api.columnApi.resetColumnState()
      autosizeOrFitColumnsToWidth(api)
    })

    this.movedColumn = null
    this.currentDocumentId = null
    this.startedMoving = false
    this.temporarySections = {}
    this.notifyColumnMove(false)
  }

  closeAllMasterRows() {
    const mainGridApi = this.gridApis.main?.api
    if (mainGridApi) {
      mainGridApi.forEachNode((node) => {
        mainGridApi.setRowNodeExpanded(node, false)
      })
    }
  }

  // Lock all detail grids, but the active one
  suppressDetailsMovableColumns(enabled, { activeDetailKey } = {}) {
    Object.entries(this.gridApis).forEach(([key, { api }]) => {
      if (key !== 'main' && key !== activeDetailKey) {
        this.suppressMovableColumns(api, enabled)
      }
    })
  }

  suppressMovableColumns(api, enabled) {
    api.gridOptionsWrapper.setProperty('suppressMovableColumns', enabled)
  }
}

export default function useColumnSorting({ modelType, isSuperuser }) {
  const model = useStoreSelector('appConfig', selectModelByType(modelType))

  const [didColumnsMove, setDidColumnsMove] = useState(false)
  const gridApisRef = useRef({})

  const manager = useMemo(
    () =>
      isSuperuser
        ? new AgGridMovableColumnsManager({
            gridApis: gridApisRef.current, // Ugly workaround not to lose gridApis when model changes
            notifyColumnMove: setDidColumnsMove,
            modelType,
            model
          })
        : null,
    [isSuperuser, model, modelType]
  )

  // Disable all column sorting features for regular users
  if (!manager) return { enabled: false }

  return {
    enabled: true,
    didColumnsMove,
    onColumnMoved: manager.onColumnMoved,
    onDragStopped: manager.onDragStopped,
    onColumnsPositionSave: manager.onColumnsPositionSave,
    onColumnsMoveUndo: manager.onColumnsMoveUndo,
    addGridApi: manager.addGridApi,
    clearDetailGridData: manager.clearDetailGridData
  }
}
