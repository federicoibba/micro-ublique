import { isEmpty, set } from 'lodash'
import { useMemo } from 'react'
import { adapters } from 'utils/documents'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { STORAGE_KEY } from '../constants'
import { selectUserId } from '../selectors'

const getStorage = () => JSON.parse(localStorage.getItem(STORAGE_KEY)) || {}

const makeColumnVisibilityHandler =
  ({ modelType, userId }) =>
  ({ visible, columns }) => {
    const hiddenColumnsStorage = getStorage()

    const colIds = columns.map(({ colId }) => colId)
    const userHiddenColumns = hiddenColumnsStorage[userId]?.[modelType] || []

    const hiddenColumns = visible
      ? userHiddenColumns.filter((col) => !colIds.includes(col))
      : [...userHiddenColumns, ...colIds]

    if (hiddenColumns.length > 0) {
      set(hiddenColumnsStorage, [userId, modelType], hiddenColumns)
    } else {
      delete hiddenColumnsStorage[userId][modelType]
      if (isEmpty(hiddenColumnsStorage[userId])) {
        delete hiddenColumnsStorage[userId]
      }
    }

    localStorage.setItem(STORAGE_KEY, JSON.stringify(hiddenColumnsStorage))
  }

const makeIsColumnHidden =
  ({ modelType, userId }) =>
  (colDef) => {
    const hiddenColumnsStorage = getStorage()
    const colId = adapters.getId(colDef)
    return hiddenColumnsStorage[userId]?.[modelType]?.includes(colId)
  }

export default function useColumnVisibility({ modelType }) {
  const userId = useStoreSelector('user', selectUserId)

  const methods = useMemo(() => {
    const params = { modelType, userId }
    return {
      columnVisibilityHandler: makeColumnVisibilityHandler(params),
      isColumnHidden: makeIsColumnHidden(params)
    }
  }, [modelType, userId])

  return methods
}
