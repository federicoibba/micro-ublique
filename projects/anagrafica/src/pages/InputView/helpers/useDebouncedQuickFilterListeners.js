import { useEffect } from 'react'
import { debounce } from 'lodash'
import { QUICK_FILTER_DELETE, QUICK_FILTER_INPUT } from '../constants'
import dispatcher from '../eventsDispatcher'

// It seems that AG Grid does not support quick filter when in server side mode
const useDebouncedQuickFilterListeners = () => {
  useEffect(() => {
    const handler = debounce(dispatcher.refreshEvent, 500)

    const input = document.querySelector(QUICK_FILTER_INPUT)
    const clearButton = document.querySelector(QUICK_FILTER_DELETE)

    input?.addEventListener('keyup', handler)
    clearButton?.addEventListener('click', handler)

    return function cleanUp() {
      input?.removeEventListener('keyup', handler)
      clearButton?.removeEventListener('click', handler)
    }
  }, [])
}

export default useDebouncedQuickFilterListeners
