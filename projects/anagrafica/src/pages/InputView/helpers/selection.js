import { documentsStore } from 'services/state'
import { tableDataMode } from '../constants'
import { groupDocumentsById } from './data'

const makeObjectWithSelectAll = (node, selection) => {
  const { id, selected } = node
  const { total, count: prevCount, excluded: prevExcluded } = selection

  // With selectAll = true, included remains empty
  const result = {
    included: {}
  }

  // If the item is selected
  // The item is removed if is present in the excluded object, the count is incremented
  if (selected) {
    const { [id]: remove, ...excluded } = prevExcluded

    result.excluded = excluded
    result.count = Object.keys(excluded).length === 0 ? total : prevCount + 1
  } else {
    // The item is added to the excluded list, the count is decremented
    result.excluded = {
      ...prevExcluded,
      [id]: node
    }
    result.count = prevCount - 1
  }

  return result
}

const makeObjectWithoutSelectAll = (node, selection) => {
  const { id, selected } = node
  const { count: prevCount, included: prevIncluded, total } = selection

  // With selectAll = false, excluded remains empty
  const result = {
    excluded: {}
  }

  // When all items are selected one by one, convert the selection into a select all
  if (selected && prevCount + 1 === total) {
    result.count = total
    result.included = {}
    result.selectAll = true
  } else if (selected) {
    // When the item is selected it is added to the included list, the count is incremented
    result.count = prevCount + 1
    result.included = {
      ...prevIncluded,
      [id]: node
    }
  } else {
    // The element is removed from the included object, the count is decremented
    const { [id]: remove, ...included } = prevIncluded

    result.count = prevCount > 0 ? prevCount - 1 : prevCount
    result.included = included
  }

  return result
}

const updateOnRowSelected = ({ node }) => {
  documentsStore.update((prevState) => {
    const { selection } = prevState
    const { selectAll } = selection

    const newSelection = selectAll
      ? makeObjectWithSelectAll(node, selection)
      : makeObjectWithoutSelectAll(node, selection)

    return {
      ...prevState,
      selection: {
        ...selection,
        ...newSelection
      }
    }
  })
}

const updateOnSelectionChanged = (nodes) => {
  documentsStore.update((prevState) => {
    return {
      ...prevState,
      selection: {
        ...prevState.selection,
        included: groupDocumentsById(nodes),
        count: nodes.length
      }
    }
  })
}

export const selectionHandler = {
  [tableDataMode.CLIENT_SIDE]: updateOnSelectionChanged,
  [tableDataMode.SERVER_SIDE]: updateOnRowSelected
}
