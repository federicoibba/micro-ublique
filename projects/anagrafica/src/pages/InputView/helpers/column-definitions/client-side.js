import { uriFormatter, valueFormatters } from './common'

const collator = new Intl.Collator(undefined, {
  numeric: true,
  sensitivity: 'base'
})

export const getClientSideColDefsByFieldType = (field, params) => {
  const { type } = field
  const { t } = params

  switch (type) {
    case 'autocompletelink':
      return {
        comparator: (a, b) => {
          const titleA = uriFormatter({ value: a })
          const titleB = uriFormatter({ value: b })
          return collator.compare(titleA, titleB)
        },
        filterParams: {
          valueFormatter: valueFormatters.autocompletelink
        }
      }
    case 'select':
      return {
        valueFormatter: valueFormatters.withOptionsTranslation(t),
        filter: 'agSetColumnFilter',
        filterParams: {
          valueFormatter: valueFormatters.withOptionsTranslation(t)
        }
      }
  }
}
