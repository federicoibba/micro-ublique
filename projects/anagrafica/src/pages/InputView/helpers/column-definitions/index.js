import { adapters } from 'utils/documents'
import { validateField } from 'utils/validation'
import { cellEditors, cellRenderers } from '../../Table/components'
import { tableDataMode } from '../../constants'
import {
  getCommonColDefsByFieldType,
  getMasterRowFirstColumnDef,
  valueFormatters
} from './common'
import { getClientSideColDefsByFieldType } from './client-side'
import { getServerSideColDefsByFieldType } from './server-side'

const firstColumnDefByType = {
  masterRow: getMasterRowFirstColumnDef
}

const uneditableStates = ['disabled', 'readOnly', 'hidden']

const getValidationResultForField = (field, data, rules) => {
  if (!rules) return { state: undefined } // Rules are not mandatory

  const [validationResult] = validateField(
    data,
    { id: field, ...rules[field] },
    { showValidationResult: false }
  )
  return validationResult
}

const highlightUneditableCells = (params) => {
  const {
    colDef: { editable }
  } = params
  const isEditable = editable && editable(params)
  return !isEditable
}

/* const highlightInvalidCells = ({
  data,
  colDef: { field, cellEditorParams }
}) => {
  if (!cellEditorParams.rules) return false

  const validationResult = getValidationResultForField(
    field,
    data,
    cellEditorParams.rules
  )
  return validationResult.state === 'invalid'
} */

const makeIsEditable =
  ({ permissionsManager, disabled }) =>
  ({ data, colDef: { field, cellEditorParams } }) => {
    // If we are here, the column can be read
    if (disabled || !permissionsManager.canEdit(field)) {
      return false
    }

    const validationResult = getValidationResultForField(
      field,
      data,
      cellEditorParams.rules
    )
    return !uneditableStates.includes(validationResult.state)
  }

const getCellParams = (field, extra = {}) => ({
  ...adapters.getCellParams(field),
  ...extra
})

// Return a mapper that creates column definitions, taking into account the table mode
export const makeGetColumnDefs = (params) => {
  const {
    t,
    tableMode,
    firstColumnType,
    workingTableType,
    detailField,
    isColumnHidden
  } = params

  const getTableModeColDefsByFieldType =
    tableMode === tableDataMode.CLIENT_SIDE
      ? getClientSideColDefsByFieldType
      : getServerSideColDefsByFieldType

  const getFirstColumnDef = firstColumnDefByType[firstColumnType]

  return (field, index) => ({
    minWidth: 200,
    hide: isColumnHidden ? isColumnHidden(field) : false,
    headerName: t(adapters.getLabel(field)),
    field: adapters.getId(field),
    cellRenderer: cellRenderers[field.type],
    cellRendererParams: getCellParams(field, { workingTableType, detailField }),
    valueFormatter: valueFormatters[field.type],
    ...(getFirstColumnDef && index === 0 && getFirstColumnDef(params)),
    ...getCommonColDefsByFieldType(field, params),
    ...getTableModeColDefsByFieldType(field, params)
  })
}

// Return a mapper that adds column definitions specific to inline editing
export const makeGetEditingColumnDefs = (params) => {
  const { workingTableType, rules, permissionsManager, detailField, fieldsMap } =
    params

  return (field) => ({
    editable: makeIsEditable({
      permissionsManager,
      disabled: field.disabled
    }),
    cellEditor: cellEditors[field.type],
    cellEditorParams: getCellParams(field, {
      allowEmpty: field.allowEmpty,
      workingTableType,
      detailField,
      fieldsMap,
      rules
    }),
    cellClassRules: {
      'uneditable-cell': highlightUneditableCells
      // 'invalid-cell': highlightInvalidCells
    }
  })
}

export const makeGetSuperuserColumnDefs = (params) => {
  return (field) => ({
    suppressMovable: false
  })
}
