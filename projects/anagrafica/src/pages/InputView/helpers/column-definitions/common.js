import {
  documentDateFilterParams,
  timeFilterParams
} from 'components/AgGridCustomFilters/helpers'
import { adapters } from 'utils/documents'
import moment from 'moment'
import moize from 'moize'
import { linkedDocumentsStore } from 'services/state'
import { tableDataMode } from '../../constants'

export const getCommonColDefsByFieldType = (field) => {
  const { type } = field

  switch (type) {
    case 'autocompletelink':
      return {
        filter: 'agSetColumnFilter'
      }
    case 'date':
    case 'datetime':
      return {
        filter: 'agDateColumnFilter',
        filterParams: documentDateFilterParams
      }
    case 'time':
      return {
        filter: 'agTextColumnFilter',
        filterParams: timeFilterParams
      }
    case 'calculated': {
      const { operation } = adapters.getCellParams(field)
      const [op] = Array.isArray(operation) ? operation : [operation]

      return {
        filter: op === 'concat' ? 'agTextColumnFilter' : 'agNumberColumnFilter',
        editable: false
      }
    }
    case 'readonly':
      return {
        editable: false,
        filter: 'agTextColumnFilter'
      }
    /*  case 'number':
      return {
        filter: 'agNumberColumnFilter'
      } */
    default:
      return {
        filter: 'agTextColumnFilter'
      }
  }
}

export const getMasterRowFirstColumnDef = ({ tableMode }) => ({
  cellRenderer: 'agGroupCellRenderer',
  cellRendererParams: { innerRenderer: 'cellLinkRenderer' },
  pinned: 'left',
  checkboxSelection: true,
  lockVisible: true,
  suppressSizeToFit: true,
  ...(tableMode === tableDataMode.CLIENT_SIDE && {
    headerCheckboxSelection: true
  }),
  ...(tableMode === tableDataMode.SERVER_SIDE && {
    headerComponent: 'customHeader'
  })
})

const formatMoment = (value, format) => value && moment.utc(value).format(format)

export const uriFormatter = ({ value, colDef }) => {
  const fullDocument = linkedDocumentsStore.get(
    ({ documentsByUri }) => documentsByUri[value]
  )
  const { key, mapKey } = colDef?.cellRendererParams?.optionsKey || {}

  // Special case of a document nested in details of another document
  if (mapKey) {
    return getNestedDocumentTitleOrUri(value, { key, mapKey })
  }

  // Workaround due to autocompletelink inconsistencies
  // Sometimes they are saved using the uri, sometimes the name
  return fullDocument ? fullDocument.title : value
}

export const valueFormatters = {
  autocompletelink: uriFormatter,
  date: ({ value }) => formatMoment(value, 'YYYY/MM/DD'),
  time: ({ value }) => formatMoment(value, 'HH:mm'),
  datetime: ({ value }) => formatMoment(value, 'YYYY/MM/DD HH:mm'),
  withOptionsTranslation:
    (t) =>
    ({ value }) =>
      t(`Options:${value}`)
}

const getNestedDocumentTitleOrUri = moize(
  (uri, { key, mapKey }) => {
    const linkedDocs = linkedDocumentsStore.get((store) => store.documentsByUri)
    for (const doc of Object.values(linkedDocs)) {
      const result = doc[key]?.find((detail) => detail[mapKey]?.uri === uri)
      if (result) return result[mapKey].title
    }
    return uri
  },
  {
    maxArgs: 1,
    maxAge: 5 * 60 * 1000 // 5 minutes
  }
)
