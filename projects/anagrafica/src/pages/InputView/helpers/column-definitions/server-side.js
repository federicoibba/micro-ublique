import { first } from 'lodash'
import prop from 'lodash/fp/prop'
import { getOptionsFromWTDocument } from 'pages/InputView/actions'
import { valueFormatters } from './common'

export const getServerSideColDefsByFieldType = (field, params) => {
  const { type } = field
  const { workingTableId, t } = params

  switch (type) {
    case 'autocompletelink':
      return {
        filterParams: {
          textFormatter: (value) => valueFormatters.autocompletelink({ value }),
          valueFormatter: valueFormatters.autocompletelink,
          values: async ({ success }) => {
            const values =
              (await getOptionsFromWTDocument(workingTableId, field.id)) || []
            // Workaround due to autocompletelink inconsistencies
            // Ideally they should always be uris, to be consistent with client-side mode
            const hasUri = !!first(values)?.uri
            const options = values.map(prop(hasUri ? 'uri' : 'name'))
            success(options)
          }
        }
      }
    case 'select':
      return {
        valueFormatter: valueFormatters.withOptionsTranslation(t),
        filter: 'agSetColumnFilter',
        filterParams: {
          values: async ({ success }) => {
            const values =
              (await getOptionsFromWTDocument(workingTableId, field.id)) || []
            const options = values.map(prop('name'))
            success(options)
          },
          valueFormatter: valueFormatters.withOptionsTranslation(t)
        }
      }
  }
}
