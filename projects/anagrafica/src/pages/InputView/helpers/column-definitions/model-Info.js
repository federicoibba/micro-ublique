// MODEL DEFINITION - COLUMN DEFS

import { first } from 'lodash'

export const fieldsSectionColDef = (t) => [
  {
    headerName: t('FIELD_NAME'),
    field: 'labelText',
    flex: 1
  },
  {
    headerName: t('LABEL'),
    field: 'label',
    flex: 1
  },
  {
    headerName: t('PARENT_TABLE'),
    field: 'parentTable',
    flex: 1
  },

  {
    headerName: t('TYPE'),
    field: 'type',
    flex: 0.5
  },
  {
    headerName: t('TYPE_EXT'),
    field: 'typeExtended',
    flex: 1,
    wrapText: true,
    autoHeight: true
  },
  {
    headerName: t('HIDDEN'),
    field: 'hidden',
    flex: 0.5
  }
]

export const formatSectionData = ({ sections = [] }, t, modelInfoT, labelMap) => {
  const fields = flattenSections(sections)

  return fields.map((f) => {
    const { labelText, headerName, field, id, parentTable, position, hidden } = f
    return {
      ...f,
      labelText: t(labelText || headerName || field || id),
      label: labelText || headerName,
      id: field || id,
      parentTable: parentTable
        ? labelMap[parentTable] || parentTable
        : `${modelInfoT('HEADER')} ${
            position ? '- ' + modelInfoT('position') + ' ' + position : ''
          }`,
      hidden: hidden || false,
      typeExtended: getExtendedType(f, t, modelInfoT, labelMap)
    }
  })
}

const flattenSections = (sections) => {
  const fields = []
  sections.forEach((section) => {
    if (section.layout) {
      fields.push(...section.fields)
    } else {
      section.fields.forEach((field) => {
        fields.push(
          ...field.columnDefs.map((el) => ({
            ...el,
            parentTable: field.id
          }))
        )
      })
    }
  })
  return fields
}

const getExtendedType = (field, t, modelInfoT, labelMap) => {
  const renderInfo = field.cellEditorParams || field
  switch (field.type?.toLowerCase()) {
    case 'autocompletelink':
    case 'multiselectlink': {
      const type = renderInfo.documentType
      return type && modelInfoT('Sidebar:DOCUMENTMODEL-' + type.toUpperCase())
    }
    case 'select':
    case 'multiselect': {
      const options = renderInfo.options
      if (options)
        return options.map((o) => modelInfoT('Options:' + o.label)).join(' | ')

      const remoteOptions = renderInfo.documentType
      const key = renderInfo.filterKey
      if (remoteOptions)
        return (
          modelInfoT('Sidebar:DOCUMENTMODEL-' + remoteOptions.toUpperCase()) +
          (key ? ' ' + modelInfoT('LINKED_TO') + ' ' + (labelMap[key] || key) : '')
        )

      return ''
    }
    case 'readonly':
      return field.labelKey
    case 'calculated':
      return getCalculatedVerbalization(field, t, modelInfoT, labelMap)
    case undefined:
      return field.value
  }
}

const getCalculatedVerbalization = (field, t, modelInfoT, labelMap) => {
  const operation = field.operation || field.cellEditorParams.operation
  const operands = field.operands || field.cellEditorParams.operands
  const op = Array.isArray(operation) ? first(operation) : operation

  const values = !Array.isArray(operands)
    ? labelMap[operands] || operands
    : operands.map((o) => {
        var tmp = Array.isArray(o) ? first(o) : o
        if (tmp.includes('.')) {
          tmp = tmp.split('.')[1]
        }
        return labelMap[tmp] || tmp
      })
  return modelInfoT(op) + ' ' + values.join(', ')
}
