import React, { useEffect, useMemo, useState } from 'react'
import { useParams } from 'react-router-dom'
import { InformationFilled24 } from '@carbon/icons-react'

import Loading from '@ublique/components/Loading'
import TabsOpener from '@ublique/components/TabsOpener'
import Forbidden from '@ublique/components/PageForbidden'

import { updateInstanceWorkingTableCount } from 'App/actions'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import useDocumentTranslation from 'utils/hooks/useDocumentTranslation'
import PermissionsManager from 'utils/documents/PermissionsManager'
import { getPermissionsForDocumentType } from 'services/api/documents'
import Document from 'components/Document'
import EditingIndicator from 'components/Document/EditingIndicator'
import { documentsStore } from 'services/state'
import { buildRowId } from 'utils/documents'
import { extraKeys } from 'utils/documents/DocumentMapper'

import dispatcher from './eventsDispatcher'
import { selectItems, selectModels } from './selectors'
import {
  resetDocumentsStore,
  resetLinkedDocumentsStore,
  resetSelectionStore
} from './actions'
import InputViewTable from './Table'

import './index.scss'
import { openModal } from 'components/Modal/actions'

const { IS_BEING_EDITED } = extraKeys

const InputView = ({
  // read: readResource,
  // create: createResource,
  // delete: deleteResource,
  defaultValueGetter
}) => {
  const { instanceId, workingTableId, type: workingTableType } = useParams()

  const [permissions, setPermissions] = useState()

  const { t } = useDocumentTranslation(workingTableType)

  const models = useStoreSelector('appConfig', selectModels)
  const workingTables = useStoreSelector('sidebarList', selectItems)

  const modelDefinition = models[workingTableType]

  const {
    readOnly,
    title: modelTitle,
    permissions: modelPermissions
  } = modelDefinition || {}

  const workingTable = useMemo(
    () => workingTables.find(({ id }) => id?.toString() === workingTableId),
    [workingTables, workingTableId]
  )

  const permissionsManager = useMemo(
    () =>
      permissions
        ? new PermissionsManager({ ...permissions, readOnly, modelPermissions })
        : null,
    [permissions, modelPermissions, readOnly]
  )

  useEffect(() => {
    getPermissionsForDocumentType(workingTableId, workingTableType).then(
      setPermissions
    )
    return function onInputViewUnmount() {
      resetDocumentsStore()
      resetLinkedDocumentsStore()
    }
  }, [])

  // Need all this data to continue
  if (!(workingTable && permissionsManager && modelDefinition)) {
    return <Loading />
  }

  if (!permissionsManager.canRead()) {
    return <Forbidden canGoBack={false} text={t('Document:Forbidden')} />
  }

  const renderMainContent = (openTab) => (
    <InputViewTable
      t={t}
      models={models}
      modelTitle={t(modelTitle)}
      instanceId={instanceId}
      workingTableType={workingTableType}
      workingTableId={workingTableId}
      permissionsManager={permissionsManager}
      openTab={openTab}
    />
  )

  const renderTab = (
    title,
    tabId,
    { type, id, permissions },
    closeTab,
    setTabData
  ) => {
    const onDelete = ({ id: deletedId }) => {
      closeTab()
      resetSelectionStore()
      updateInstanceWorkingTableCount()
      // eslint-disable-next-line eqeqeq
      if (deletedId.activityId == workingTableId) {
        dispatcher.refreshEvent({ overwrite: false })
      }
    }

    const onUpdate = ({ id: updatedId, refresh }, { title }) => {
      // eslint-disable-next-line eqeqeq
      if (updatedId.activityId == workingTableId) {
        if (refresh) dispatcher.refreshEvent({ overwrite: false })
        setTabData(title)
      }
    }

    const onCreate = ({ id: createdId }, { title }) => {
      const { activityId, id } = createdId
      setTabData(title, `${type}:${activityId}:${id}`, {
        id: createdId,
        type
      })
      updateInstanceWorkingTableCount()
      // eslint-disable-next-line eqeqeq
      if (activityId == workingTableId) {
        dispatcher.refreshEvent({ overwrite: false })
      }
    }
    return (
      <TabsOpener.Tab
        id={tabId}
        title={`${!id.id ? `${t('Document:NEW')} ` : ''}${title}`}
        rightComponent={() => <EditingIndicator documentId={id} />}
      >
        <Document
          key={`${type}:${id.activityId}:${id.id}`} // TODO: check components to investigate the tab closing bug
          id={id}
          modelDefinition={models[type]}
          instanceId={instanceId}
          workingTableType={type}
          permissions={permissions}
          onDelete={onDelete}
          onCreate={onCreate}
          onUpdate={onUpdate}
          closeTab={closeTab}
          defaultValueGetter={defaultValueGetter}
        />
      </TabsOpener.Tab>
    )
  }

  const onTabClose = (title, _, { id }, close) => {
    const { temporaryId, activityId } = id
    const documentId = buildRowId(temporaryId ? { id: temporaryId, activityId } : id)
    const document = documentsStore.get(
      ({ documentsById }) => documentsById[documentId]
    )

    if (document?.[IS_BEING_EDITED]) {
      dispatcher.unsavedDocumentEvent({
        callback: close,
        isNewDocument: !!temporaryId,
        id,
        title
      })
    } else {
      close()
    }
  }

  const getTitle = (modelTitle, modelDefinition, t) => (
    <span className="model-title">
      {t(modelTitle)}
      <span
        className="model-info-icon"
        onClick={() =>
          openModal('modelInfo', {
            modelDefinition,
            modelTitle: t(modelTitle)
          })
        }
      >
        <InformationFilled24 />
      </span>
    </span>
  )

  return (
    <div className="input-view">
      <div className="document-header-row">
        <h1 className="header-title">{t('Document:Header')}</h1>
      </div>

      <TabsOpener
        title={modelTitle && getTitle(modelTitle, modelDefinition, t)}
        renderMainContent={renderMainContent}
        renderTab={renderTab}
        onTabClose={onTabClose}
      />
    </div>
  )
}

export default React.memo(InputView)
