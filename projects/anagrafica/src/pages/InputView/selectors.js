import compose from 'lodash/fp/compose'
import prop from 'lodash/fp/prop'
import { extraKeys } from 'utils/documents/DocumentMapper'

export const selectItems = prop('items')

export const selectModels = prop('models')

export const selectSelection = prop('selection')

export const selectLatestEdit = ({ documentsById, latestEdit }) =>
  documentsById[latestEdit]

export const selectDocumentsByUri = prop('documentsByUri')

export const selectUserId = prop('userId')

export const selectModelByType = (type) => compose(prop(type), prop('models'))

export const hasEditedDocumentsOfType =
  (documentType) =>
  ({ editedDocuments, documentsById }) =>
    editedDocuments.some((id) => documentsById[id]?.type === documentType)

export const selectAllEditedDocuments = ({ documentsById, editedDocuments }) =>
  editedDocuments.map((id) => documentsById[id])

export const isDocumentBeingEdited =
  (id) =>
  ({ documentsById }) =>
    !!documentsById[id]?.[extraKeys.IS_BEING_EDITED]

export const isScenarioInstance = (instanceId) => (store) => {
  const inst = store.find((el) => el.id === parseInt(instanceId))
  return inst?.config === 'scenario'
}
