/**
 * Created by fgallucci on 17/06/2020.
 */

import React, { useCallback } from 'react'
import { isNull } from 'lodash'
import { useTranslation } from 'react-i18next'
import { Redirect } from 'react-router-dom'

import LoginComponent from '@ublique/components/Login'

import withStore from 'hoc/withStore'
import useRedirect from 'services/auth/useRedirect'
import useI18nControls from 'hooks/useI18nControls'

import { login } from './actions'

function Login({ user, languagesMenuConfig, fallbackRedirectPath }) {
  const { t } = useTranslation(['Login', 'Common'])

  const i18nControls = useI18nControls(languagesMenuConfig)

  const redirect = useRedirect({ fallbackPath: fallbackRedirectPath })

  const mapErrorToString = useCallback(
    (errorCode) => (errorCode ? t(`Errors.${errorCode}`) : undefined),
    [t]
  )

  if (user) {
    return !isNull(redirect) ? <Redirect to={redirect} /> : null
  }

  return (
    <LoginComponent
      submit={login}
      title={t('Title')}
      submitText={t('Submit')}
      usernameLabel={t('UsernameLabel')}
      passwordLabel={t('PasswordLabel')}
      errorTitle={t('Login:WrongAuthentication')}
      mapErrorToString={mapErrorToString}
      i18nControls={i18nControls}
    />
  )
}

export default withStore(Login, ['user'])
