/**
 * Created by: ahmedjumaah
 * Creation Date: 18/06/2020
 */
import React from 'react'
import { useTranslation } from 'react-i18next'

import NotFound from '@ublique/components/PageNotFound'

export default function PageNotFound() {
  const { t } = useTranslation('Common')
  return <NotFound goBackText={t('GoBack')} text={t('PageNotFound')} />
}
