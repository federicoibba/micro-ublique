/**
 * Created by fgallucci on 25/06/2020.
 */

export const BASE_URL =
  process.env.REACT_APP_BASE_URL ||
  process.env.BASE_URL ||
  `${window.location.origin}${window.location.pathname}api`

export const STORAGE_PREFIX = 'ublique:'

export const AUTH_ACCESS = {
  GRANTED: true,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403
}

export const LOCALSTORAGE_THEME_KEY = `${STORAGE_PREFIX}theme`

export const I18N_REMOTE = true

export const I18N_DEFAULT = 'it-IT'

export const I18N_SUPPORTED_LANGUAGES = ['it-IT', 'en-GB', 'es-ES', 'ru-RU']

export const DEFAULT_NOTIFICATION_DURATION = 3500

export const DEFAULT_THEME = 'dark'

export const IMPORT_SERVICE_BASE_PATH = '/ublique/toolkit/import'
