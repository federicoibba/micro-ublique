import React, { Component } from 'react'
import createStore from 'hoc/createStore'
import { globalStore } from 'state'
import { ImportApi } from 'services/api/import'
import UbliqueApp from '../App'
import defaultRoutes from '../App/routes'
import { getTheme } from '../App/utils'
import initI18n from '../i18n'

let i18nInitialized = false

if (module?.hot) {
  module.hot.dispose((data) => {
    data.skipI18nCheck =
      window.i18nInitialized || (window.i18nInitialized = i18nInitialized)
  })
}

const checkExternalStoreKeys = (store) => {
  for (const key of Object.keys(store)) {
    if (globalStore[key]) {
      throw new Error(
        `Reserved key "${key}" found in the external store. Reserved keys are used internally by the platform store and cannot be overwritten.`
      )
    }
  }
}

const checkI18nInitialization = () => {
  if (module?.hot?.data?.skipI18nCheck) return
  if (!i18nInitialized) {
    throw new Error(
      'The i18n module was not initialized. Be sure to call Ublique.initI18n() before rendering <Ublique />.'
    )
  }
}

// Library entry point
class Ublique extends Component {
  static initI18n = (languages, initialLanguage) => {
    if (!i18nInitialized) {
      initI18n(languages, initialLanguage)
      i18nInitialized = true
    }
  }

  constructor(props) {
    super(props)

    const { initialTheme, importServiceConfig, routes = [], store = {} } = props

    checkI18nInitialization()
    checkExternalStoreKeys(store)

    if (importServiceConfig) {
      ImportApi.init(importServiceConfig)
    }

    if (initialTheme && !getTheme()) {
      globalStore.theme.set(initialTheme)
    }

    this.rootComponent = createStore(UbliqueApp, {
      ...globalStore,
      ...store
    })

    this.state = {
      routes: [...(Array.isArray(routes) ? routes : [routes]), ...defaultRoutes]
    }
  }

  setRoutes = (newRoutes) => {
    this.setState({
      routes: newRoutes
    })
  }

  render() {
    const { initialTheme, routes, store, ...rest } = this.props

    const UbliqueRootComponent = this.rootComponent

    return (
      <UbliqueRootComponent
        routes={this.state.routes}
        setRoutes={this.setRoutes}
        {...rest}
      />
    )
  }
}

export default Ublique
