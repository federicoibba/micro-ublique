import withRemoteOptions from 'utils/documents/withRemoteOptions'
import Number from './Number'
import Text from './Text'
import Section from './Section'
import Select from './Select'
import Time from './Time'
import Date from './Date'
import DateTime from './DateTime'
import Multiselect from './Multiselect'
import MultiselectLink from './MultiselectLink'
import Checkbox from './Checkbox'
import Autocomplete from './Autocomplete'
import AutocompleteLink from './AutocompleteLink'
import withTranslation from './withTranslation'
import Readonly from './Readonly'
import './index.scss'

export default {
  number: Number,
  text: Text,
  section: Section,
  time: withTranslation(Time),
  date: withTranslation(Date),
  datetime: DateTime, // DateTime uses Date and Time internally, there are issues with components double wrapped in 'withTranslation' HOC
  checkbox: Checkbox,
  select: withRemoteOptions(Select),
  multiselect: withRemoteOptions(Multiselect),
  multiselectlink: withRemoteOptions(MultiselectLink),
  autocomplete: withRemoteOptions(Autocomplete),
  autocompletelink: withRemoteOptions(AutocompleteLink),
  readonly: Readonly,
  calculated: Readonly
}
