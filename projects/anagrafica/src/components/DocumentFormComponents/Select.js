import React from 'react'
import Select from 'components/Select'
import withTranslation from './withTranslation'

const Component = React.forwardRef(({ t, documentType, ...props }, ref) => {
  const optionT = (elementKey) => t(`Options:${elementKey}`)
  return <Select ref={ref} {...props} getLabel={optionT} />
})

export default withTranslation(Component)
