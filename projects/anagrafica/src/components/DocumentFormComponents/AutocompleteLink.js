import React, { useCallback } from 'react'
import AutocompleteLink from '@ublique/components/AutocompleteLink'
import { identity } from 'lodash'
import withTranslation from './withTranslation'

const Component = React.forwardRef(
  ({ documentType, t, currentSelection, allowEmpty, ...props }, ref) => {
    const getLinkData = useCallback(
      (value) => {
        return {
          id: value.id,
          type: documentType
        }
      },
      [documentType]
    )

    const getLinkId = useCallback(
      (value) => {
        return `${documentType}:${value.id.activityId}:${value.id.id}`
      },
      [documentType]
    )

    return (
      <AutocompleteLink
        ref={ref}
        {...props}
        canClear={allowEmpty}
        getLabel={identity}
        getLinkId={getLinkId}
        getLinkData={getLinkData}
      />
    )
  }
)

export default withTranslation(Component)
