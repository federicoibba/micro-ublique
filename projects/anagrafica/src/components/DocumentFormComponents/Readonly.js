import React from 'react'
import TextInput from '@ublique/components/TextInput'
import withTranslation from './withTranslation'

const Component = React.forwardRef(({ value = '', labelText, id }, ref) => {
  return (
    <TextInput
      id={id}
      ref={ref}
      autoComplete="off"
      disabled
      labelText={labelText}
      value={value}
    />
  )
})

export default withTranslation(Component)
