import React, { useCallback, useEffect, useMemo, useState, forwardRef } from 'react'
import { debounce, trimStart } from 'lodash'
import TextInput from '@ublique/components/TextInput'
import withTranslation from './withTranslation'

const DebouncedTextInput = forwardRef(
  ({ onChange, value, debounceMs, ...props }, ref) => {
    const [internalValue, setInternalValue] = useState(value)

    const debouncedOnChange = useMemo(
      () => debounce(onChange, debounceMs),
      [onChange, debounceMs]
    )

    // Keep internal value in sync
    useEffect(() => {
      setInternalValue(value)
    }, [value])

    useEffect(() => {
      return function onUnmount() {
        debouncedOnChange.cancel()
      }
    }, [debouncedOnChange])

    const handleOnChange = useCallback(
      ({ target: { value } }) => {
        const trimmedValue = trimStart(value)
        setInternalValue(trimmedValue)
        debouncedOnChange(trimmedValue)
      },
      [debouncedOnChange]
    )

    return (
      <TextInput
        ref={ref}
        autoComplete="off"
        {...props}
        onChange={handleOnChange}
        value={internalValue}
      />
    )
  }
)

const BaseTextInput = forwardRef(({ onChange, ...props }, ref) => {
  const handleOnChange = useCallback(
    ({ target: { value } }) => {
      onChange(trimStart(value))
    },
    [onChange]
  )

  return (
    <TextInput ref={ref} autoComplete="off" {...props} onChange={handleOnChange} />
  )
})

export default withTranslation(
  forwardRef(({ t, value, labelText = '', ...props }, ref) => {
    const Component = props.debounceMs ? DebouncedTextInput : BaseTextInput
    return (
      <Component ref={ref} {...props} value={value ?? ''} labelText={labelText} />
    )
  })
)
