import React, { useCallback } from 'react'
import moment from 'moment'
import InputDate from '@ublique/components/DatePicker'

const Component = React.forwardRef(
  (
    { onChange, value, t, currentSelection, documentType, helperText, ...props },
    ref
  ) => {
    const handleOnChange = useCallback(
      ([value]) => {
        onChange(moment(value).utcOffset(0, true).format())
      },
      [onChange]
    )

    const dateValue = value
      ? moment.utc(value).set({ hour: 0, minutes: 0 }).format()
      : undefined

    return (
      <InputDate
        datePickerType="single"
        value={dateValue}
        onChange={handleOnChange}
        dateFormat="Y/m/d"
      >
        <InputDate.Input ref={ref} autoComplete="off" labelText="" {...props} />
      </InputDate>
    )
  }
)

export default Component
