import React, { useCallback } from 'react'
import TabsOpenerLink from '@ublique/components/TabsOpenerLink'
import MultiSelect from './Multiselect'
import withTranslation from './withTranslation'

const Component = React.forwardRef(({ t, ...props }, ref) => {
  const renderLink = useCallback(
    (value, { labelKey }) => {
      return (
        <TabsOpenerLink title={t(value[labelKey])} data={value}>
          {t(value[labelKey])}
        </TabsOpenerLink>
      )
    },
    [t]
  )
  return <MultiSelect ref={ref} {...props} renderValueItem={renderLink} />
})

export default withTranslation(Component)
