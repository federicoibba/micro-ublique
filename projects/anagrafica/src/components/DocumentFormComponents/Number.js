import React from 'react'
import NumberInput from 'components/NumberInput'
import withTranslation from './withTranslation'

const Component = React.forwardRef(({ t, labelText = '', ...props }, ref) => (
  <NumberInput ref={ref} {...props} labelText={labelText} />
))

export default withTranslation(Component)
