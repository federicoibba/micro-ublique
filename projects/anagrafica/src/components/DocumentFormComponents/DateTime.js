import React from 'react'
import cx from 'classnames'
import CustomInput from '@ublique/components/CustomInput'
import { utc } from 'moment'
import InputDate from './Date'
import TimePicker from './Time'
import withTranslation from './withTranslation'

const Component = ({ onChange, value, className, disabled, t, ...props }) => {
  const handleOnChangeDate = (date) => {
    if (!date) {
      return onChange()
    }
    const momentValue = utc(value)
    const momentDate = utc(date)
    const hour = momentValue.hour()
    const minute = momentValue.minute()
    onChange(momentDate.set({ hour, minutes: minute }).format())
  }

  const handleOnChangeTime = (time) => {
    const momentValue = utc(value)
    const momentTime = utc(time)
    const day = momentValue.date()
    const month = momentValue.month()
    const year = momentValue.year()
    onChange(momentTime.set({ date: day, month, year }).format())
  }

  return (
    <CustomInput
      disabled={disabled}
      {...props}
      className={cx('ublique-datetime', { disabled }, className)}
    >
      <InputDate
        id={`date_${props.id}`}
        hideLabel
        datePickerType="single"
        value={value}
        disabled={disabled}
        className={cx('ublique-datetime--date', { disabled })}
        onChange={handleOnChangeDate}
      />
      <TimePicker
        id={`time${props.id}`}
        hideLabel
        value={value}
        disabled={disabled}
        onChange={handleOnChangeTime}
      />
    </CustomInput>
  )
}

export default withTranslation(Component)
