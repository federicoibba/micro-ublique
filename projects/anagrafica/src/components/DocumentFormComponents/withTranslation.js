import React, { forwardRef, memo } from 'react'
import useDocumentTranslation from 'utils/hooks/useDocumentTranslation'

export const withTranslation = (WrappedComponent) =>
  memo(
    forwardRef(
      ({ workingTableType, rules, data, detailField, fieldsMap, ...props }, ref) => {
        const { t } = useDocumentTranslation(workingTableType)

        const { helperText, invalidText, labelText, hideLabel } = props

        const getText = (text) => {
          if (hideLabel) return
          if (text) return t(text)
          return '\u00a0'
        }

        return (
          <WrappedComponent
            ref={ref}
            t={t}
            {...props}
            labelText={getText(labelText)}
            helperText={getText(helperText)}
            invalidText={invalidText ? t(invalidText) : undefined}
          />
        )
      }
    )
  )

export default withTranslation
