import React, { useCallback } from 'react'
import { utc } from 'moment'
import TimePicker from '@ublique/components/TimePicker'

const Component = React.forwardRef(
  ({ onChange, value, t, helperText, ...props }, ref) => {
    const dateValue = value ? utc(value) : undefined

    const [hours, minutes] = dateValue
      ? [
          dateValue.hour().toString().padStart(2, '0'),
          dateValue.minute().toString().padStart(2, '0')
        ]
      : []

    const handleOnChange = useCallback(
      ({ target: { value: nextValue } }) => {
        if (!nextValue) {
          return onChange(undefined)
        }
        const [hours, minutes] = nextValue.split(':')
        onChange(utc(0).set({ hour: hours, minutes }).format())
      },
      [onChange]
    )

    return (
      <TimePicker
        ref={ref}
        {...props}
        type="time"
        value={dateValue ? [hours, minutes].join(':') : undefined}
        onChange={handleOnChange}
      />
    )
  }
)

export default Component
