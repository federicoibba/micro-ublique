import React from 'react'
import Autocomplete from '@ublique/components/Autocomplete'
import withTranslation from './withTranslation'
import { identity } from 'lodash'

const Component = React.forwardRef(
  ({ t, documentType, currentSelection, ...props }, ref) => {
    return <Autocomplete ref={ref} {...props} getLabel={identity} />
  }
)

export default withTranslation(Component)
