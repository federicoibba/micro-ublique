import React from 'react'
import InputCheckbox from '@ublique/components/Checkbox'
import withTranslation from './withTranslation'

const Component = React.forwardRef(({ value, t, ...props }, ref) => {
  return <InputCheckbox ref={ref} {...props} defaultChecked={value} />
})

export default withTranslation(Component)
