import React, { useCallback, useState, useEffect } from 'react'
import MultiSelect from '@ublique/components/MultiSelect'
import withTranslation from './withTranslation'

const styles = {
  container: {
    position: 'relative'
  },
  displayContainer: {
    position: 'absolute',
    top: 0,
    left: '4rem',
    right: '3rem',
    bottom: 0,
    overflowX: 'hidden',
    display: 'flex',
    alignItems: 'center',
    zIndex: 1,
    pointerEvents: 'none'
  },
  displayContainerPadding: {
    // paddingTop: '2rem'
  },
  displayItem: {
    marginRight: '0.25em'
  }
}

const Component = React.forwardRef(
  (
    {
      id,
      t,
      options = [],
      onChange,
      currentSelection,
      valueKey,
      labelKey,
      labelText,
      hideLabel,
      renderValueItem,
      ...props
    },
    ref
  ) => {
    const [key, setKey] = useState()
    const optionT = (elementKey) => t(`Options:${elementKey}`)
    // Force rerender of MultiSelect when options (and currentSelection) change
    useEffect(() => {
      setKey(`${id}-${options.length}`)
    }, [options, id, setKey])

    const handleOnChange = useCallback(
      ({ selectedItems }) => onChange(selectedItems.map((item) => item[valueKey])),
      [onChange]
    )
    const currentSelectionArray = [].concat(currentSelection || []).filter(Boolean)

    const itemToString = useCallback(
      (item) => (item ? optionT(item[labelKey]) : ''),
      [optionT, labelKey]
    )

    return (
      <div style={styles.container}>
        <div
          style={{
            ...styles.displayContainer,
            ...(!hideLabel && styles.displayContainerPadding)
          }}
        >
          {currentSelectionArray.map((item, index, array) => (
            <span key={item[valueKey]} style={styles.displayItem}>
              {renderValueItem
                ? renderValueItem(item, { valueKey, labelKey })
                : optionT(item[labelKey])}
              {index !== array.length - 1 ? ', ' : ''}
            </span>
          ))}
        </div>
        <MultiSelect
          id={id}
          ref={ref}
          key={key}
          {...props}
          labelText=""
          sortItems={(items, sorting) => {
            return items
          }}
          initialSelectedItems={currentSelectionArray}
          titleText={!hideLabel && labelText}
          onChange={handleOnChange}
          items={options}
          itemToString={itemToString}
        />
      </div>
    )
  }
)

export default withTranslation(Component)
