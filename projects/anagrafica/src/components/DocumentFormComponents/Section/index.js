import React from 'react'
import cx from 'classnames'
import './index.scss'

const getGroups = (layout, length) => {
  let total = 0
  return layout.split(',').map((row) => {
    let [number, columns /* , align = 'start' */] = row.split('-')
    const filler = number % columns !== 0
    if (columns === undefined) {
      columns = number
      number = length
    } else if (columns === 'start' || columns === 'center' || columns === 'right') {
      // align = columns
      columns = number
      number = length
    }
    const group = {
      start: total,
      end: total + +number,
      className: cx(
        'ublique-form-section-group',
        `layout-${columns}`,
        // `align-${align}`,
        { filler }
      )
    }
    total += +number
    return group
  })
}

export default function FormSection({ title, children, layout = '1' }) {
  const items = React.Children.toArray(children)
  const groups = getGroups(layout, items.length)

  return (
    <div className="ublique-form-section">
      <h6>{title}</h6>
      {groups.map(({ start, end, className }, index) => (
        <div className={className} key={index}>
          {items.slice(start, end)}
        </div>
      ))}
    </div>
  )
}
