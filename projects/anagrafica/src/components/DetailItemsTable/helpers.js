import { extraKeys } from 'utils/documents/DocumentMapper'

export const getRowNodeId = (data) => data[extraKeys.DETAIL_ID_KEY]

export const getIndexColumnDef = (t) => ({
  pinned: 'left',
  filter: false,
  resizable: false,
  headerName: t('Document:ROW_INDEX'),
  suppressMenu: true,
  suppressSizeToFit: true,
  suppressMovable: true,
  checkboxSelection: true,
  lockVisible: true,
  headerCheckboxSelection: true,
  headerCheckboxSelectionFilteredOnly: true,
  valueGetter: ({ node }) => node.rowIndex + 1
})
