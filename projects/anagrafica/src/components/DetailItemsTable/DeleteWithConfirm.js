import React, { useEffect, useRef, useState } from 'react'
import { noop } from 'lodash'
import { useTranslation } from 'react-i18next'
import Button from '@ublique/components/Button'
import Tooltip from '@ublique/components/Tooltip'

const DeleteWithConfirm = ({ children, onClick, duration = 3000 }) => {
  const { t } = useTranslation('Document')
  const [tooltipVisible, setTooltipVisible] = useState(false)
  const timebarRef = useRef()

  useEffect(() => {
    if (tooltipVisible) {
      timebarRef.current?.classList.toggle('tooltip-time-bar--animated')

      const to = setTimeout(() => {
        setTooltipVisible(false)
      }, duration)

      return () => {
        clearTimeout(to)
      }
    }
  }, [tooltipVisible])

  const onFirstClick = () => setTooltipVisible(true)

  return (
    <Button onClick={tooltipVisible ? onClick : onFirstClick}>
      {children}
      <Tooltip
        className="detail-grid-delete-tooltip"
        direction="top"
        showIcon={false}
        menuOffset={{ top: 30 }}
        open={tooltipVisible}
        onChange={noop}
      >
        <div ref={timebarRef} className="tooltip-time-bar" />
        <p>{t('ClickAgainToDelete')}</p>
      </Tooltip>
    </Button>
  )
}

export default DeleteWithConfirm
