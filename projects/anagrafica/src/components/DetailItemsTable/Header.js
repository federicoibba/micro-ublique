import React from 'react'
import { useTranslation } from 'react-i18next'
import Button from '@ublique/components/Button'
import { Select, SelectItem } from '@ublique/components/Select'
import DeleteWithConfirm from './DeleteWithConfirm'

export const paginationOptions = [
  {
    id: 'option-1',
    text: '5',
    value: 5
  },
  {
    id: 'option-2',
    text: '10',
    value: 10
  },
  {
    id: 'option-3',
    text: '25',
    value: 25
  },
  {
    id: 'option-4',
    text: '50',
    value: 50
  },
  {
    id: 'option-5',
    text: '100',
    value: 100
  }
]

function Header({
  selectedRows,
  onAdd,
  onDelete,
  canAddRows,
  onExport,
  onImport,
  canImport,
  canExport,
  canDeleteRows,
  gridApi
}) {
  const { t } = useTranslation('Document')

  const buttons = [
    {
      title: 'ImportButton',
      onClick: onImport,
      visible: selectedRows === 0 && canImport
    },
    {
      title: 'ExportButton',
      onClick: onExport,
      visible: selectedRows === 0 && canExport
    },
    {
      title: 'AddButton',
      onClick: onAdd,
      visible: selectedRows === 0 && canAddRows
    },
    {
      title: 'DeleteButton',
      onClick: onDelete,
      visible: selectedRows > 0 && canDeleteRows,
      component: DeleteWithConfirm
    }
  ]

  const onPageSizeChanged = ({ target }) => {
    gridApi.paginationSetPageSize(Number(target.value))
  }

  const paginationList = () => {
    return paginationOptions.map((size, index) => (
      <SelectItem
        key={index}
        text={`${t('Rows')}: ${size.text}`}
        value={size.value}
      />
    ))
  }

  const renderButtons = () =>
    buttons.reduce((acc, { title, onClick, visible, component }, i) => {
      if (!visible) return acc
      const Component = component || Button
      return [
        ...acc,
        <Component key={`${title}-${i}`} onClick={onClick}>
          {t(title)}
        </Component>
      ]
    }, [])

  return (
    <div className="ublique-grid--actions-header">
      {selectedRows > 0 ? (
        <div className="selected-row-count active-selection">
          {t('MainTable.SelectedRowCount', {
            count: selectedRows
          })}
        </div>
      ) : (
        <div className="selected-row-count" />
      )}

      <div className="document-actions-row">
        {renderButtons()}
        <Select
          defaultValue={5}
          id="select-1"
          noLabel
          inline
          light
          size="xl"
          onChange={onPageSizeChanged}
          children={paginationList()}
        />
      </div>
    </div>
  )
}

export default Header
