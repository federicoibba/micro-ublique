import React, { useCallback, useMemo, useState, useEffect, useRef } from 'react'
import { saveAs } from 'file-saver'
import { noop } from 'lodash'
import DataTable from '@ublique/components/DataTable'
import { getTranslatedColumnDefinitions } from 'pages/InputView/helpers/table-config'
import { frameworkComponents } from 'pages/InputView/Table/components'
import {
  defaultColumnDefs,
  defaultGridOptions,
  tableDataMode
} from 'pages/InputView/constants'
import { updateDocuments } from 'pages/InputView/actions'
import { openModal } from 'components/Modal/actions'
import { ImportApi } from 'services/api/import'
import { ExportApi } from 'services/api/export'
import { nullFieldsByState } from 'utils/documents'
import { getDefaultValues } from 'utils/documents/default-values'
import { validateFields } from 'utils/validation'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import useDocumentTranslation, {
  useAgGridTranslation
} from 'utils/hooks/useDocumentTranslation'
import { extraKeys } from 'utils/documents/DocumentMapper'
import { generateUniqueId } from 'utils/documents/DocumentMapper/frontend'
import { getRowNodeId, getIndexColumnDef } from './helpers'
import Header from './Header'
import './index.scss'

const { DETAIL_ID_KEY, DOCUMENT_ID_KEY } = extraKeys

/**
 * The detail table works in client-side mode, and it's directly connected to the global store (the only source of truth).
 * It follows the immutable data pattern to be kept in sync with the store (https://www.ag-grid.com/react-grid/immutable-data/).
 */
const DetailItemsTable = ({
  isNew,
  masterGridApi,
  columnDefs,
  workingTableType,
  workingTableId,
  permissionsManager,
  rules,
  fieldsMap,
  mapper,
  readOnly,
  documentId,
  instanceId,
  documentObjectId,
  id: fieldId,
  refreshDocument = noop,
  initialPageSize = 5,
  columnVisibility = {},
  columnSorting = {},
  rowSelection = 'multiple',
  showHeader = true,
  defaultValueGetter,
  scenario
}) => {
  const { t } = useDocumentTranslation(workingTableType)
  const { t: agGridT, destroyed } = useAgGridTranslation()

  const [selectedRows, setSelectedRows] = useState([])
  const [gridApi, setGridApi] = useState()
  const tableRef = useRef()

  const rowRules = rules[fieldId]?.$each

  const rowData = useStoreSelector(
    'documents',
    ({ documentsById }) => documentsById[documentId]?.[fieldId]
  )

  const colDefs = useMemo(() => {
    const translatedColumnDefs = getTranslatedColumnDefinitions(columnDefs, {
      isSuperuser: columnSorting.enabled, // Safe if columnSorting is enabled for super users only
      rules: rowRules,
      disableEditing: readOnly,
      detailField: fieldId,
      tableMode: tableDataMode.CLIENT_SIDE,
      isColumnHidden: columnVisibility.isColumnHidden,
      fieldsMap, // Not included in dependencies on purpose, add if necessary
      permissionsManager,
      workingTableType,
      t
    })

    return showHeader && translatedColumnDefs.length > 0
      ? [getIndexColumnDef(t), ...translatedColumnDefs]
      : translatedColumnDefs
  }, [t, permissionsManager, workingTableType, rowRules, readOnly, fieldId])

  const popupParent = useMemo(
    () =>
      masterGridApi
        ? document.querySelector(`[row-id="detail_${documentId}"]`)
        : undefined,
    []
  )

  // --------------------------------- Effects --------------------------------- //

  useEffect(() => {
    return () => {
      masterGridApi?.removeDetailGridInfo(fieldId)
      if (columnSorting.enabled) {
        columnSorting.clearDetailGridData({ fieldId, documentId })
      }
    }
  }, [masterGridApi, columnSorting])

  // --------------------------------- Event handlers --------------------------------- //

  const onGridReady = useCallback((params) => {
    // Register the detail grid with the master, if it has one
    if (masterGridApi) {
      const gridInfo = {
        id: fieldId,
        api: params.api,
        columnApi: params.columnApi,
        domElement: tableRef.current
      }
      masterGridApi.addDetailGridInfo(fieldId, gridInfo)
      masterGridApi.resetRowHeights()
    }
    if (columnSorting.enabled) {
      columnSorting.addGridApi({ fieldId, documentId }, params)
    }
    setGridApi(params)
  }, [])

  const onPaginationChanged = useCallback(() => {
    if (masterGridApi) {
      masterGridApi.resetRowHeights()
    }
  }, [masterGridApi])

  const onSelectionChangedHanlder = useCallback((rows) => {
    setSelectedRows(rows.map(({ [DETAIL_ID_KEY]: id }) => id))
  }, [])

  const onColumnDragStopped = useCallback(() => {
    if (columnSorting.enabled) {
      columnSorting.onDragStopped({ sectionId: fieldId, documentId })
    }
  }, [fieldId, documentId, columnSorting])

  // --------------------------------- CRUD operations --------------------------------- //

  const updateDetailValue = useCallback(
    (value) => updateDocuments({ documentId, field: fieldId, value, mapper }),
    [documentId, fieldId, mapper]
  )

  const onRowEdit = useCallback(
    ({ value, data, column }) => {
      const processChangedRow = () => {
        const newRowData = { ...data, [column.colId]: value }
        const fieldStates = validateFields(newRowData, rowRules, {
          showValidationResult: false
        })
        return nullFieldsByState(newRowData, fieldStates)
      }

      const newValue = rowData.map((row) =>
        row[DETAIL_ID_KEY] === data[DETAIL_ID_KEY] ? processChangedRow() : row
      )

      updateDetailValue(newValue)
    },
    [updateDetailValue, rowData, rowRules]
  )

  const onRowAdd = useCallback(async () => {
    const defaultValues = await getDefaultValues(columnDefs, {
      defaultValueGetter,
      scenario,
      instanceId
    })

    const newRow = {
      [DOCUMENT_ID_KEY]: documentId,
      activityId: workingTableId,
      ...defaultValues
    }
    updateDetailValue([...(rowData || []), generateUniqueId(newRow)])

    setTimeout(() => {
      gridApi.api.paginationGoToLastPage()
    }, 50)
  }, [gridApi, updateDetailValue, columnDefs, rowData, documentId])

  const onRowDelete = useCallback(() => {
    const idsToDelete = {}
    const selectedIdsMap = selectedRows.reduce(
      (acc, id) => ({ ...acc, [id]: true }),
      {}
    )

    gridApi.api.forEachNodeAfterFilter(({ data }) => {
      const id = data[DETAIL_ID_KEY]
      if (selectedIdsMap[id]) idsToDelete[id] = true
    })

    updateDetailValue(rowData.filter((row) => !idsToDelete[row[DETAIL_ID_KEY]]))

    // Remove filters and deselect - optional
    // gridApi.api.setFilterModel(null)
    gridApi.api.deselectAll()
  }, [gridApi, updateDetailValue, selectedRows])

  // --------------------------------- Import/export --------------------------------- //

  const exportField = async () => {
    const { body, fileName } = await ExportApi.exportDocument(
      workingTableType,
      workingTableId,
      documentObjectId.id,
      fieldId
    )
    saveAs(body, fileName)
  }

  const importField = () => {
    openModal('import', {
      onSubmit: (file) =>
        ImportApi.importDocument(
          workingTableType,
          workingTableId,
          documentObjectId.id,
          fieldId,
          file
        ).then(refreshDocument)
    })
  }

  const canCreate = permissionsManager.canCreate(fieldId)
  const canDelete = permissionsManager.canDelete(fieldId)

  if (destroyed) return null

  return (
    <div className="ublique-detail-items-grid" ref={tableRef}>
      <DataTable
        {...defaultGridOptions}
        immutableData
        localeTextFunc={agGridT}
        onGridReady={onGridReady}
        getRowNodeId={getRowNodeId}
        rowData={rowData}
        columnDefs={colDefs}
        defaultColDef={defaultColumnDefs}
        canSearch={false}
        paginationPageSize={initialPageSize}
        rowSelection={rowSelection}
        onCellValueChanged={onRowEdit}
        onSelectionChanged={onSelectionChangedHanlder}
        frameworkComponents={frameworkComponents}
        headerCheckboxSelectionFilteredOnly
        autoSizeColumnsOnFirstRender
        onPaginationChanged={onPaginationChanged}
        onColumnVisible={columnVisibility.columnVisibilityHandler}
        onColumnMoved={columnSorting?.onColumnMoved}
        onDragStopped={onColumnDragStopped}
        popupParent={popupParent}
        alwaysShowVerticalScroll
        suppressCsvExport
        suppressExcelExport
        extraHeaderComponent={
          showHeader && (
            <Header
              t={t}
              selectedRows={selectedRows.length}
              onAdd={onRowAdd}
              onDelete={onRowDelete}
              onImport={importField}
              onExport={exportField}
              canAddRows={canCreate}
              canDeleteRows={canDelete}
              canImport={!isNew && canCreate}
              canExport={!isNew}
            />
          )
        }
      />
    </div>
  )
}

export default React.memo(DetailItemsTable)
