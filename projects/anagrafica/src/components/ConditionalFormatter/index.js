import { isString, last } from 'lodash'
import { appConfigStore, linkedDocumentsStore } from 'state'
import { validateFields } from 'utils/validation'

const validationOptions = {
  showValidationResult: false
}

const getStyle = (style, color) =>
  ({
    borderFull: { border: color ? `1px solid ${color}` : 'unset' },
    borderLeft: {
      borderLeft: color ? `6px solid ${color}` : '6px solid transparent'
    },
    background: { background: color || 'unset' }
  }[style])

const makeConditionalFormatter =
  (config) =>
  ({ data }) => {
    if (!data) return

    const { modelType, style = 'background' } = config

    const model = appConfigStore.get((store) => store.models[modelType])
    const linkedDocs = linkedDocumentsStore.get((store) => store.documentsByUri)

    const populatedRow = Object.assign({}, data)
    for (const key in populatedRow) {
      const value = populatedRow[key]
      if (key !== 'uri' && isString(value) && value.startsWith('ev://')) {
        populatedRow[key] = linkedDocs[value]
      }
    }

    const formatState = validateFields(
      populatedRow,
      model.formatting,
      validationOptions
    )

    return getStyle(style, last(formatState.row)?.state)
  }

export default makeConditionalFormatter

/* {
  row: {
    states: {
      red: {
        when: {
          totalLoadUnitEquivalent_calc: {
            gt: '%orderPosEndMaxUnitLoadEquivalent'
          }
        }
      }
    }
  }
} */
