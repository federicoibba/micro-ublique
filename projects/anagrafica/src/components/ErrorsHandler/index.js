import React, { useEffect, useMemo } from 'react'
import { useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import { debounce } from 'lodash'
import { Help32 } from '@carbon/icons-react'
import Notification from '@ublique/components/Notification'
import Button from '@ublique/components/Button'
import { openModal } from 'components/Modal/actions'
import { logout } from 'pages/Login/actions'

import useStoreSelector from 'utils/hooks/useStoreSelector'
import { errorsStore } from 'state'

import './index.scss'

function ErrorsHandler() {
  const errors = useStoreSelector('errors')
  const history = useHistory()
  const { t } = useTranslation('Common')

  const deleteError = (id) => {
    errorsStore.set(errors.filter((error) => error.id !== id))
  }

  const logoutAndRedirect = useMemo(
    () =>
      debounce(async () => {
        await logout()
        history.push('/')
      }),
    [history]
  )

  useEffect(() => {
    if (errors.some(({ status }) => status === 401)) {
      logoutAndRedirect()
    }

    if (errors.some(({ code }) => code === 401)) {
      logoutAndRedirect()
    }
  }, [errors])

  const getDebugMsg = (error) => (
    <div className="caption-btn">
      <Button
        className="btn"
        kind="danger--ghost"
        renderIcon={Help32}
        onClick={() => openModal('debugMessage', { error })}
      />
    </div>
  )

  const getSubtitle = (responseBody) => (
    <p className="subtitle">
      {t(`Errors.${responseBody?.message}`, responseBody?.placeholders || {})}
    </p>
  )

  return (
    <div className="errors-handler-modal">
      {errors.map(({ code, id, responseBody }) => {
        return (
          <Notification.Toast
            key={id}
            caption={responseBody.debugMessage ? getDebugMsg(responseBody) : ''}
            kind="error"
            timeout={5000}
            title={`${responseBody?.code || code} ${
              responseBody?.status ? `- ${responseBody?.status}` : ''
            }`}
            subtitle={getSubtitle(responseBody)}
            onCloseButtonClick={() => deleteError(id)}
          />
        )
      })}
    </div>
  )
}

export default React.memo(ErrorsHandler)
