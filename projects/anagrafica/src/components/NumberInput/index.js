import React, { Fragment, useCallback } from 'react'
import cx from 'classnames'

import CustomInput from '@ublique/components/CustomInput'
import { CaretDownGlyph, CaretUpGlyph } from '@carbon/icons-react'

import './index.scss'

export default React.forwardRef(
  (
    {
      value,
      onChange,
      min,
      max,
      step = 1,
      integer,
      allowNegative = false,
      className,
      onBlur,
      onKeyPress,
      type,
      disabled,
      ...props
    },
    ref
  ) => {
    if (!allowNegative && typeof min === 'undefined') min = 0

    const cap = (value) => {
      const minValue = allowNegative ? min : Math.max(0, min)
      const maxValue = max
      const numberValue = parseFloat(value)

      if (!isFinite(numberValue)) return value
      if (isFinite(minValue) && numberValue < minValue) return minValue
      if (isFinite(maxValue) && numberValue > maxValue) return maxValue

      return value
    }

    const handleOnChange = useCallback(
      ({ target: { value } }) => {
        if (value === '-') {
          onChange('-')
        } else if (value === '.' || value === ',') {
          onChange(cap('0.'))
        } else if (value === '-.' || value === '-,') {
          onChange(cap('-0.'))
        } else if (/^0[1-9]/.test(value.toString())) {
          onChange(value.toString().replace(/^0/, ''))
        } else {
          onChange(
            cap(
              value
                .toString()
                .replace(/^00/, '0')
                .replace(/^-00/, '-0')
                .replace(',', '.')
            )
          )
        }
      },
      [onChange]
    )

    const handleArrowUp = useCallback(() => {
      onChange(cap((+value || 0) + step))
    }, [value, step])
    const handleArrowDown = useCallback(() => {
      onChange(cap((+value || 0) - step))
    }, [value, step])

    const handleBlur = useCallback(
      (event) => {
        if (onBlur) onBlur(event)
        const {
          target: { value }
        } = event
        if (value.toString().length && isFinite(value)) {
          onChange(parseFloat(cap(value)))
        } else {
          onChange()
        }
      },
      [onChange]
    )

    const handleKeyPress = useCallback((event) => {
      if (onKeyPress) onKeyPress(event)

      const {
        key,
        target: { value }
      } = event
      if (
        !/[0-9,.-]/.test(key) ||
        (value === '0' && key === '0') ||
        (value === '-' && key === '-') ||
        (!allowNegative && key === '-') ||
        (integer && (key === ',' || key === '.')) ||
        (/[,.]/.test(key) && (value || '').includes('.'))
      ) {
        return event.preventDefault()
      }
    }, [])

    return (
      <CustomInput
        {...props}
        disabled={disabled}
        className={cx('ublique-number', className)}
      >
        {(inputProps) => (
          <Fragment>
            <input
              {...inputProps}
              ref={ref}
              value={typeof value === 'undefined' || value === null ? '' : value}
              onChange={handleOnChange}
              onKeyPress={handleKeyPress}
              onBlur={handleBlur}
            />
            <div className="bx--number__controls">
              <button
                disabled={disabled}
                type="button"
                className="bx--number__control-btn up-icon"
                onClick={handleArrowUp}
              >
                <CaretUpGlyph className="up-icon" />
              </button>
              <button
                disabled={disabled}
                type="button"
                className="bx--number__control-btn down-icon"
                onClick={handleArrowDown}
              >
                <CaretDownGlyph className="down-icon" />
              </button>
            </div>
          </Fragment>
        )}
      </CustomInput>
    )
  }
)
