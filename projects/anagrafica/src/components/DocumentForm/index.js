import React, { PureComponent } from 'react'
import moize from 'moize'
import Tabs from '@ublique/components/Tabs'
import { sortBy } from 'lodash'
import { groupSections } from 'pages/InputView/helpers/table-config'
import { FIELD_POSITION_KEY, MAIN_SECTION_ID } from 'pages/InputView/constants'
import { extraKeys } from 'utils/documents/DocumentMapper'
import { adapters, getPropsFromFieldState } from 'utils/documents'
import { validateFields } from 'utils/validation'
import DetailItemsTable from '../DetailItemsTable'
import formComponents from '../DocumentFormComponents'

const { DOCUMENT_ID_KEY } = extraKeys

const Unsupported = () => <span>Unsupported field</span>

const specialFields = {
  grid: DetailItemsTable
}

const fieldSpecificPropsMap = {
  text: { debounceMs: 350 }
}

class DocumentForm extends PureComponent {
  state = this.getInitialState()

  getInitialState() {
    return {
      touchedFields: {},
      fieldsState: this.getValidatedFields(this.props.document)
    }
  }

  resetState() {
    this.setState(this.getInitialState())
  }

  getSections = moize(groupSections, { maxSize: 1 })

  makeOnChange = moize((fieldId) => {
    return (value) => {
      const { document, onChange } = this.props
      const fieldsState = this.getValidatedFields({
        ...document,
        [fieldId]: value
      })
      this.setState({ fieldsState }, () => {
        onChange(fieldId, value)
      })
    }
  })

  makeOnBlur = moize((fieldId) => {
    return () => {
      this.setState((state) => ({
        touchedFields: state.touchedFields[fieldId]
          ? state.touchedFields
          : { ...state.touchedFields, [fieldId]: true },
        fieldsState: this.getValidatedFields(this.props.document)
      }))
    }
  })

  getSortedFields = moize((fields) => sortBy(fields, FIELD_POSITION_KEY))

  // Need to use the same structure that AG Grid passes to cell renderers, to use the same wrappers
  // Using moize.react only for shallow comparison of arguments
  getFieldData = moize.react(({ documentId }) => ({
    [DOCUMENT_ID_KEY]: documentId
  }))

  getValidatedFields(document) {
    return validateFields(document, this.props.rules, {
      showValidationResult: false
    })
  }

  getFieldStateProps(fieldId) {
    const { permissionsManager, isNew, submitted, readOnly } = this.props

    if (readOnly || !permissionsManager.canEdit(fieldId, isNew)) {
      return { disabled: true }
    }

    // States set by rules, after checking states set by permissions
    const { touchedFields, fieldsState } = this.state

    const fieldStates = getPropsFromFieldState(fieldsState[fieldId])

    return submitted || touchedFields[fieldId] || !fieldStates.invalid // States that are not invalid should always be considered
      ? fieldStates
      : {}
  }

  isHidden(field) {
    const { fieldsState } = this.state
    const { permissionsManager } = this.props
    const fieldId = adapters.getId(field)?.replace('_section', '') // Match sections with their children permission (not 100% reliable!)

    return (
      field.hidden ||
      !permissionsManager.canRead(fieldId) ||
      getPropsFromFieldState(fieldsState[fieldId])?.hidden ||
      false
    )
  }

  renderField = (field) => {
    const { document, workingTableType, rules } = this.props
    const Field = formComponents[field.type] || Unsupported

    const fieldId = adapters.getId(field)
    const onChange = this.makeOnChange(fieldId)
    const onBlur = this.makeOnBlur(fieldId)
    const data = this.getFieldData({ documentId: document[DOCUMENT_ID_KEY] })

    const fieldSpecificProps = fieldSpecificPropsMap[field.type] || {}

    if (this.isHidden(field)) return null

    return (
      <Field
        key={fieldId}
        {...field}
        {...fieldSpecificProps}
        {...this.getFieldStateProps(fieldId)}
        workingTableType={workingTableType}
        rules={rules}
        data={data}
        value={document[fieldId]}
        onChange={onChange}
        onBlur={onBlur}
      />
    )
  }

  renderSpecialField = (field) => {
    const { document, ...rest } = this.props
    const SpecialField = specialFields[field.type]
    const fieldId = adapters.getId(field)

    if (this.isHidden(field)) return null

    return (
      <SpecialField
        key={fieldId}
        {...rest}
        {...field}
        {...this.getFieldStateProps(fieldId)}
        // TODO: use data like in renderField?
        documentId={document[DOCUMENT_ID_KEY]}
        documentObjectId={document.id}
      />
    )
  }

  renderSection = ({ fields, title, ...sectionProps }) => {
    const { t } = this.props
    const Section = formComponents.section

    if (this.isHidden(sectionProps)) return null // TODO: should all children be reset inside a hidden section?

    const sectionFields =
      title === MAIN_SECTION_ID ? this.getSortedFields(fields) : fields

    return (
      <Section key={sectionProps.id} {...sectionProps} title={t(title)}>
        {sectionFields.map((field) =>
          specialFields[field.type]
            ? this.renderSpecialField(field)
            : this.renderField(field)
        )}
      </Section>
    )
  }

  render() {
    const { sections, detailsAsTabs } = this.props

    if (detailsAsTabs) {
      const { mainSection, detailSections } = this.getSections(sections)
      return (
        <div className="document-detail-tabs-mode">
          {this.renderSection(mainSection)}
          <Tabs>{detailSections.map(this.renderSection).filter(Boolean)}</Tabs>
        </div>
      )
    }

    return <div>{sections.map(this.renderSection)}</div>
  }
}

export default DocumentForm
