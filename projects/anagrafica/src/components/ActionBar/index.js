import React from 'react'
import cx from 'classnames'

import Button from '@ublique/components/Button'

import Icon from 'components/Icon'

import './index.scss'

const defaultIsActionHidden = () => false
const defaultIsActionDisabled = () => false

const ActionBar = ({
  actions,
  className,
  solid = false,
  isActionHidden = defaultIsActionHidden,
  isActionDisabled = defaultIsActionDisabled
}) => {
  return (
    <div className={cx('ublique-button-section', className)}>
      {actions.map((item, index) => {
        return (
          !isActionHidden(item) && (
            <Button
              hasIconOnly
              disabled={isActionDisabled(item)}
              tooltipAlignment="end"
              tooltipPosition="bottom"
              kind={solid ? 'primary' : 'ghost'}
              iconDescription={item.title}
              key={item.id || index}
              onClick={item.onClick}
              renderIcon={() => <Icon name={item.icon} />}
            />
          )
        )
      })}
    </div>
  )
}

export default React.memo(ActionBar)
