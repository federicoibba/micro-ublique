import moment from 'moment'

const documentDateComparator = (date1, date2) => {
  const date1Number = moment(date1).startOf('day').valueOf()
  const date2Number = moment(date2).startOf('day').valueOf()
  return date2Number - date1Number
}

export const scenarioDateComparator = (DATE_FORMAT) => (date1, date2) => {
  const date1Number = date1
    ? moment(date1, DATE_FORMAT).utc(true).local().valueOf()
    : 0
  const date2Number = date2
    ? moment(date2, DATE_FORMAT).utc(true).local().valueOf()
    : 0

  return date1Number - date2Number
}

export const scenarioDateFilterParams = (DATE_FORMAT) => {
  return {
    comparator: (filterValue, cellValue) => {
      const filterDate = moment(filterValue)
      const cellDate = moment(cellValue, DATE_FORMAT).utc(true).local().startOf('day')

      return filterDate.valueOf() - cellDate.valueOf()
    }
  }
}

export const scenarioSummaryFilterParams = {
  textFormatter: (elements) => {
    if (Array.isArray(elements)) {
      return elements
        .map(({ value }) => value)
        .join(' ')
        .toLowerCase()
    }

    return elements.toLowerCase()
  }
}

export const documentDateFilterParams = {
  comparator: documentDateComparator
}

export const timeFilterParams = {
  textFormatter: (value) => moment.utc(value).format('HH:mm')
}
