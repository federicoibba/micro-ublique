import React, { Fragment, useCallback, useState } from 'react'

import { useTranslation } from 'react-i18next'
import { isEmpty, isUndefined, chunk, concat } from 'lodash'

import Modal from '@ublique/components/Modal'
import SearchItems from '@ublique/components/SearchItems'
import { InlineLoading } from '@ublique/components/Loading'
import Button from '@ublique/components/Button'
import {
  StructuredListWrapper,
  StructuredListHead,
  StructuredListRow,
  StructuredListCell,
  StructuredListBody
} from '@ublique/components/StructuredList'

import { Link } from 'react-router-dom'

import './index.scss'

const SearchModal = ({ open, close, data: { onSubmit } }) => {
  const { t } = useTranslation('Translations')

  const [value, setValue] = useState()
  const [listIndex, setListIndex] = useState(1)
  const [loading, setLoading] = useState(false)
  const [resultData, setResultData] = useState()
  const [chunkData, setChunkData] = useState([])
  const [lengthData, setLengthData] = useState(0)

  const onChange = (e) => setValue(e.target.value)

  const onSearch = async () => {
    setListIndex(1)
    setChunkData([])
    setLoading(true)
    if (isEmpty(value)) {
      setLoading(false)
      return setResultData()
    }
    const res = await onSubmit(value)
    const chunkedData = chunk(res.body.results, 100)
    setChunkData(chunkedData)
    setLengthData(res.body.results.length)
    setResultData(chunkedData[0] || [])
    setLoading(false)
  }

  const customHeaderModal = () => (
    <div className="modal-header">
      <span>{t('Translations:Search')}</span>
      <div className="modal-header-results">
        {loading || !isEmpty(resultData) ? `${t('Translations:Results')}: ` : null}
        {loading ? (
          <InlineLoading className="header-loader" />
        ) : isEmpty(resultData) ? null : (
          lengthData
        )}
      </div>
    </div>
  )

  const showMoreFunc = () => {
    setListIndex(listIndex + 1)
    setResultData(concat(resultData, chunkData[listIndex]))
  }

  const renderResult = useCallback(() => {
    return resultData.map(({ group, label, namespace, value, lang }, index) => {
      return (
        <Fragment key={`${namespace}-${label}-${lang}`}>
          <StructuredListRow key={index}>
            <StructuredListCell className="cell-value">
              <span>{value}</span>
            </StructuredListCell>
            <StructuredListCell>
              <Link
                onClick={close}
                className="cell-link"
                replace
                to={{
                  pathname: `/translations/translationTables/${namespace}/${
                    isEmpty(group) ? 'MAIN' : group
                  }/documents`,
                  state: label.split('.')
                }}
              >
                <span>
                  {!isEmpty(group)
                    ? [namespace, group, label].join('.')
                    : [namespace, label].join('.')}
                </span>
              </Link>
            </StructuredListCell>
          </StructuredListRow>
        </Fragment>
      )
    })
  }, [resultData])

  return (
    <Modal
      size="md"
      open={open}
      onRequestClose={close}
      onRequestSubmit={onSearch}
      secondaryButtonText={t('Translations:Cancel')}
      primaryButtonText={t('Translations:Search')}
      modalHeading={customHeaderModal()}
      className="modal-translation-search"
    >
      <SearchItems
        value={value}
        onChange={onChange}
        placeholder={t('Translations:Search')}
        className="search-bar"
      />

      <div className="modal-body-container">
        {loading ? (
          <InlineLoading className="custom-inline-loader" />
        ) : isUndefined(resultData) ? (
          <div className="no-data" />
        ) : isEmpty(resultData) ? (
          <h3 className="no-data">{t('Translations:NoData')}</h3>
        ) : (
          <StructuredListWrapper>
            <StructuredListHead>
              <StructuredListRow head>
                <StructuredListCell head>
                  {t('Translations:LabelName')}
                </StructuredListCell>
                <StructuredListCell head>{t('Translations:Path')}</StructuredListCell>
              </StructuredListRow>
            </StructuredListHead>
            <StructuredListBody>{renderResult()}</StructuredListBody>
          </StructuredListWrapper>
        )}
        {!isEmpty(chunkData) && chunkData.length !== listIndex && (
          <div className="show-more-button">
            <Button kind="tertiary" onClick={showMoreFunc}>
              {t('Translations:ShowMore')}
            </Button>
          </div>
        )}
      </div>
    </Modal>
  )
}

export default SearchModal
