import React from 'react'
import { kebabCase } from 'lodash'
import Accordion, { AccordionItem } from '@ublique/components/Accordion'

const MAX_NUMBER_OF_ERRORS = 10

const ErrorRow = ({ fieldName, rowNumber, reason, t }) => (
  <p>
    <span className="row">
      {t('Shell:ImportRow')} {rowNumber}:
    </span>
    <span>
      <span>{fieldName} - </span>
      <span className="reason">{t(reason)}</span>
    </span>
  </p>
)

const ErrorsAccordion = ({ errorsBySheetList, t }) => (
  <Accordion>
    {errorsBySheetList.map(([sheetName, errors]) => {
      const slicedErrors = errors.slice(0, MAX_NUMBER_OF_ERRORS)
      const remainingErrors = errors.length - slicedErrors.length
      const key = kebabCase(sheetName)

      return (
        <AccordionItem
          key={key}
          title={sheetName}
          open={errorsBySheetList.length === 1}
        >
          <div className="errors-list">
            {slicedErrors.map((error, i) => (
              <ErrorRow key={`${key}-${i}`} t={t} {...error} />
            ))}
            {remainingErrors > 0 && (
              <p>
                {t('Shell:ImportMoreError', {
                  count: remainingErrors
                })}
              </p>
            )}
          </div>
        </AccordionItem>
      )
    })}
  </Accordion>
)

export default ErrorsAccordion
