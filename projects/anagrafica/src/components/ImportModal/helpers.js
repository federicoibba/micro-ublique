import { groupBy, repeat } from 'lodash'
import moment from 'moment'

const line = `\n${repeat('-', 40)}\n`

export const formatFileSize = (size) => {
  const oneMB = Math.pow(1024, 2)
  return size < oneMB
    ? `${(size / 1024).toFixed(2)} KB`
    : `${(size / oneMB).toFixed(2)} MB`
}

export const groupBySheetName = (data) => Object.entries(groupBy(data, 'sheetName'))

export const createTextErrorsLog = (t, fileName, errors) => {
  const errorsBySheetList = groupBySheetName(errors)

  const logTitle = `LOG ${fileName} - ${moment().format('DD/MM/YYYY HH:mm')}\n\n`

  return errorsBySheetList.reduce(
    (acc, [sheetName, errors]) =>
      `${acc}\n# ${sheetName}${line}${errors.reduce(
        (acc, { reason, fieldName, rowNumber }) =>
          `${acc}${t('Shell:ImportRow')} ${rowNumber}: ${fieldName} - ${t(reason)}\n`,
        ''
      )}`,
    logTitle
  )
}
