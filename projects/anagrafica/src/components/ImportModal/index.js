import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { saveAs } from 'file-saver'
import Modal from '@ublique/components/LoadingModal'
import { WarningAlt24 } from '@carbon/icons-react'
import { FileUploaderDropContainer } from '@ublique/components/FileUploader'
import { addNotification } from 'components/Notifications/actions'
import ErrorsAccordion from './ErrorsAccordion'
import { groupBySheetName, formatFileSize, createTextErrorsLog } from './helpers'
import './index.scss'

const ImportModal = ({ open, close, data: { onSubmit } }) => {
  const [loading, setLoading] = useState(false)
  const [file, setFile] = useState()
  const [error, setError] = useState()
  const { t } = useTranslation(['Common', 'Shell'])

  const onAddFile = (_, { addedFiles }) => {
    const [addedFile] = addedFiles
    setFile(addedFile)
  }

  const onImport = async () => {
    try {
      setLoading(true)
      await onSubmit(file)
      addNotification({
        message: t('Shell:ImportSuccess', { filename: file.name })
      })
      close()
    } catch (error) {
      setError({
        type: error.code === 400 ? 'validation' : 'generic',
        data: error.responseBody
      })
      setLoading(false)
    }
  }

  const onRetry = () => {
    setFile()
    setError()
  }

  const onDownloadLog = () => {
    const text = createTextErrorsLog(t, file.name, error.data.errors)
    const blob = new Blob([text], { type: 'text/plain;charset=utf-8' })
    saveAs(blob, `IMPORT_LOG-${file.name}.txt`)
  }

  const hasValidationErrors = error?.type === 'validation'

  const renderMainContent = () => (
    <>
      {file && (
        <div className="file-recap">
          <p>
            <span>{t('Shell:SelectedFile')}: </span>
            <span>
              {file.name} ({formatFileSize(file.size)})
            </span>
          </p>
        </div>
      )}
      {error && (
        <div className="file-import-error">
          <p>{t('Shell:ImportError')}</p>
        </div>
      )}
      <div className="uploader-wrapper">
        <FileUploaderDropContainer
          accept={['.xlsx', '.xls']}
          labelText={t('Shell:ChooseFile')}
          onAddFiles={onAddFile}
        />
      </div>
    </>
  )

  const renderValidationErrors = () => {
    const errorsBySheetList = groupBySheetName(error.data.errors)

    return (
      <div className="import-validation-errors">
        <div className="header">
          <WarningAlt24 />
          <h3>{t('Shell:ImportValidationError')}</h3>
        </div>
        {errorsBySheetList.length === 0 ? (
          <p>{error.data.errorMessage}</p>
        ) : (
          <>
            <p>{t('Shell:ImportSheetWithErrors')}</p>
            <div className="sheets-accordion">
              <ErrorsAccordion t={t} errorsBySheetList={errorsBySheetList} />
            </div>
          </>
        )}
      </div>
    )
  }

  const modalPropsMain = {
    onRequestSubmit: onImport,
    primaryButtonText: t('Shell:Import'),
    primaryButtonDisabled: !file,
    secondaryButtonText: t('Common:Cancel')
  }

  const modalPropsErrors = {
    onRequestSubmit: onRetry,
    primaryButtonText: t('Common:Retry'),
    secondaryButtonText: t('Shell:ImportDownloadLog'),
    onSecondarySubmit: onDownloadLog
  }

  return (
    <Modal
      open={open}
      onRequestClose={close}
      modalHeading={t('Shell:ImportTitle')}
      className="import-service-modal"
      confirmLoading={loading}
      confirmLoadingMessage={t('Common:Submitting')}
      {...(hasValidationErrors ? modalPropsErrors : modalPropsMain)}
    >
      {hasValidationErrors ? renderValidationErrors() : renderMainContent()}
    </Modal>
  )
}

export default ImportModal
