import React, {
  useRef,
  useEffect,
  useMemo,
  useCallback,
  useState,
  useImperativeHandle
} from 'react'
import PropTypes from 'prop-types'

import { get, isEqual, isString } from 'lodash'
import cx from 'classnames'
import { FixedSizeList as List } from 'react-window'

import CustomInput from '@ublique/components/CustomInput'

import { ChevronDown16, WarningFilled16 } from '@carbon/icons-react'

import './index.scss'

const useOnClickOutside = (ref, handler) => {
  useEffect(() => {
    const listener = (event) => {
      if (!ref.current || ref.current.contains(event.target)) {
        return
      }
      handler(event)
    }
    document.addEventListener('mousedown', listener)
    document.addEventListener('keyup', listener)
    document.addEventListener('keypress', listener)
    document.addEventListener('touchstart', listener)

    return () => {
      document.removeEventListener('mousedown', listener)
      document.removeEventListener('keydown', listener)
      document.removeEventListener('keyup', listener)
      document.removeEventListener('keypress', listener)
      document.removeEventListener('touchstart', listener)
    }
  }, [ref, handler])
}

const SelectOption = React.memo(({ index, style, data }) => {
  const { items, currentFocusIndex, renderItem, onClick } = data
  const value = items[index]
  const handleOnClick = useCallback(() => {
    onClick(value, index)
  }, [onClick, value])

  return (
    <div
      style={style}
      onClick={handleOnClick}
      className={cx('option', 'bx--list-box__menu-item', {
        current: currentFocusIndex === index
      })}
    >
      <div className="bx--list-box__menu-item__option">{renderItem(value)}</div>
    </div>
  )
})

const Select = React.forwardRef(
  (
    {
      options,
      items = options,
      labelKey = 'label',
      valueKey = 'value',
      renderItem,
      onChange,
      onChangeValue,
      onBlur,
      onFocus,
      onKeyDown,
      className,
      value,
      getLabel,
      invalid,
      disabled,
      currentSelection,
      placeholderLabel,
      ...props
    },
    ref
  ) => {
    const selectRef = useRef(null)
    const virtualListRef = useRef(null)
    const optionsContainerRef = useRef(null)

    const [display, setDisplay] = useState(false)
    const [currentFocusIndex, setCurrentFocusIndex] = useState(-1)
    const [currentValueObject, setCurrentValueObject] = useState(currentSelection)

    useEffect(() => {
      if (value && items) {
        const itemIndex =
          typeof value !== 'undefined'
            ? items.findIndex((item) =>
                isEqual(valueKey ? get(item, valueKey) : item, value)
              )
            : -1
        setValue(items[itemIndex], true)
        setCurrentFocusIndex(itemIndex)
      }
    }, [items, value])

    useEffect(() => {
      if (
        display &&
        optionsContainerRef.current &&
        optionsContainerRef.current.scrollIntoViewIfNeeded
      ) {
        optionsContainerRef.current.scrollIntoView({
          behavior: 'smooth',
          block: 'nearest'
        })
      }
    }, [display])

    useEffect(() => {
      if (
        items &&
        display &&
        virtualListRef.current &&
        currentFocusIndex > -1 &&
        currentFocusIndex < items.length
      ) {
        virtualListRef.current.scrollToItem(currentFocusIndex)
      }
    }, [display, currentFocusIndex, items, virtualListRef.current])

    const defaultRenderItem = useCallback(
      (item) => <span>{getLabel(item[labelKey])}</span>,
      [labelKey]
    )

    const showOptions = useCallback(() => {
      setDisplay(true)
    }, [])

    const hideOptions = useCallback(() => {
      if (value && items) {
        const itemIndex =
          typeof value !== 'undefined'
            ? items.findIndex((item) =>
                isEqual(valueKey ? get(item, valueKey) : item, value)
              )
            : -1
        setCurrentFocusIndex(itemIndex)
      } else {
        setCurrentFocusIndex(-1)
      }
      setDisplay(false)
    }, [value, items])

    const setValue = useCallback(
      (details, auto) => {
        setDisplay(false)
        setCurrentValueObject(details)
        const nextValue = details
          ? valueKey
            ? get(details, valueKey)
            : details
          : null
        if (onChangeValue) onChangeValue(details)
        if (onChange && (!auto || value !== nextValue)) onChange(nextValue)
      },
      [
        labelKey,
        valueKey,
        setDisplay,
        setCurrentValueObject,
        onChangeValue,
        onChange,
        value
      ]
    )

    const handleBlur = useCallback(
      (event) => {
        if (onBlur) onBlur(event)
        hideOptions()
      },
      [onBlur]
    )

    const handleFocus = useCallback(
      (event) => {
        if (!disabled) {
          onFocus?.(event)
          showOptions()
        }
      },
      [showOptions, onFocus, disabled]
    )

    const handleKeyDown = useCallback(
      (event) => {
        if (disabled) return

        onKeyDown?.(event)

        if (event.key === ' ') {
          event.preventDefault()
          // Space
          if (!display) {
            return showOptions()
          } else {
            return hideOptions()
          }
        } else if (event.key === 'ArrowUp') {
          event.preventDefault()

          if (!display) {
            return showOptions()
          }
          setCurrentFocusIndex(
            Math.abs((currentFocusIndex - 1 + items.length) % items.length)
          )
        } else if (event.key === 'ArrowDown') {
          event.preventDefault()

          if (!display) {
            return showOptions()
          }
          setCurrentFocusIndex(
            Math.abs((currentFocusIndex + 1 + items.length) % items.length)
          )
        } else if (event.key === 'Enter' && items[currentFocusIndex]) {
          event.preventDefault()
          setValue(items[currentFocusIndex])
        } else if (event.key === 'Escape') {
          event.preventDefault()
          hideOptions()
        }
      },
      [
        display,
        currentFocusIndex,
        items,
        showOptions,
        hideOptions,
        setValue,
        disabled
      ]
    )

    const onItemClick = useCallback(
      (value, index) => {
        setValue(value)
        setCurrentFocusIndex(index)
      },
      [setValue]
    )

    const itemData = useMemo(
      () => ({
        currentFocusIndex,
        items: items || [],
        renderItem: renderItem || defaultRenderItem,
        onClick: onItemClick
      }),
      [items, renderItem, defaultRenderItem, currentFocusIndex]
    )
    // Call hook passing in the ref and a function to call on outside click
    useOnClickOutside(selectRef, hideOptions)

    useImperativeHandle(ref, () => ({
      focus: () => {
        if (selectRef.current) {
          selectRef.current.focus()
        }
      }
    }))

    const getLabelToDisplay = () => {
      if (currentValueObject) return getLabel(currentValueObject[labelKey])
      if (isString(currentSelection)) return getLabel(currentSelection)
      return placeholderLabel
    }

    return (
      <CustomInput
        {...props}
        tabIndex={disabled ? undefined : 0}
        ref={selectRef}
        invalid={invalid}
        disabled={disabled}
        onBlur={handleBlur}
        onFocus={handleFocus}
        onKeyDown={handleKeyDown}
        onClick={!display ? handleFocus : undefined}
        className={cx('ublique-select', { display, disabled }, className)}
      >
        <span className="bx--select-value">{getLabelToDisplay()}</span>
        <div
          className={cx('bx--select-icon', {
            'bx--select-icon-active': display
          })}
          onClick={!display ? handleFocus : undefined}
        >
          <ChevronDown16 />
        </div>
        {invalid && (
          <div className="bx--select-danger-icon">
            <WarningFilled16 />
          </div>
        )}
        {display ? (
          <div className="bx--list-box__menu items" ref={optionsContainerRef}>
            <List
              ref={virtualListRef}
              itemSize={40}
              itemData={itemData}
              itemCount={(items || []).length}
              height={Math.min((items || []).length * 40, 300)}
            >
              {SelectOption}
            </List>
          </div>
        ) : null}
      </CustomInput>
    )
  }
)

Select.defaultProps = {
  valueKey: 'value',
  labelKey: 'label',
  labelText: '',
  placeholderLabel: '',
  getLabel: (label) => label
}

Select.propTypes = {
  /** The TextInput id */
  id: PropTypes.string,
  /** The TextInput label text */
  labelText: PropTypes.string,
  /** The Placeholder label text */
  placeholderLabel: PropTypes.string,
  /** The list of data items that will be used to search and select the item for autocompletion */
  options: PropTypes.array,
  items: PropTypes.array,
  /** Item key that will be displayed in the input and for the options (unless renderItem is specified). */
  labelKey: PropTypes.string,
  /** Item key that will be used to match the `value` prop with the provided options */
  valueKey: PropTypes.string,
  /** Function for rendering a custom option. (item) => React.node */
  renderItem: PropTypes.func,
  /** Function called on value change (with the valueKey property of the selected item as parameter) */
  onChange: PropTypes.func,
  /** Function called on value change (with the selected item object as parameter) */
  onChangeValue: PropTypes.func,
  /**  */
  getLabel: PropTypes.func
}

export default Select
