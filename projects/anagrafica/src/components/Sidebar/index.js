import React, { useMemo, useState } from 'react'
import MicroSidebar from '@ublique/components/MicroSidebar'
import { getSidebarItems } from './utils'
import useStoreSelector from '../../utils/hooks/useStoreSelector'

const Sidebar = () => {
  const [isSideNavExpanded, setIsSideNavExpanded] = useState(true)
  const sidebarList = useStoreSelector('sidebarList', (state) => state.items)

  const wtList = useMemo(() => getSidebarItems(sidebarList), [sidebarList])

  return (
    <MicroSidebar
      showSidebar
      showSidebarSearch
      searchLabel="Search"
      title="Working tables"
      sidebarList={wtList}
      isSideNavExpanded={isSideNavExpanded}
      onClickSideNavExpand={() => setIsSideNavExpanded((val) => !val)}
    />
  )
}

export default Sidebar
