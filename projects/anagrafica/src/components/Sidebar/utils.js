import { identity, groupBy, first, compact } from 'lodash'
import { canCreate } from '../../services/auth'

export const getSidebarItems = (items, t = identity) => {
  if (items?.some((item) => item.loading)) return items

  return Object.values(groupBy(items, 'groups')).map((group) => {
    const translatedItems = group.map((item) => ({
      ...item,
      title: t(`Sidebar:${item.title || item.type.toUpperCase()}`)
    }))

    const { groups: groupName, groupTitle, id, idInstance } = first(group) || {}
    const canImport = group.some(({ permission }) => canCreate(permission)) // At least 1 wt with write permission

    return {
      items: translatedItems,
      id: `${id}-${groupTitle}`,
      name: groupName,
      title: t(`Sidebar:${groupTitle.toUpperCase()}`),
      actions: getSidebarWtActions(idInstance, groupName, canImport, t)
    }
  })
}

const getSidebarWtActions = (idInstance, groupName, canImport, t) =>
  compact([
    canImport && {
      title: t('Import'),
      onClick: () => {
        // openModal('import', {
        //   onSubmit: (file) =>
        //     ImportApi.importWorkingTableGroup(idInstance, groupName, file)
        // })
      }
    },
    {
      title: t('Export'),
      onClick: async () => {
        // addNotification({ message: t('Shell:ExportStarted') })
        // const { body, fileName } = await ExportApi.exportWorkingTableGroup(
        //   idInstance,
        //   groupName
        // )
        // saveAs(body, fileName)
      }
    }
  ])
