import React, { useState, useEffect } from 'react'
import {
  Save20,
  TrashCan20,
  DocumentExport20,
  DocumentImport20,
  Renew20
} from '@carbon/icons-react'

// This component is only used in the document ActionBar, to show a few icons
// Only those icons are imported here to avoid including the entire '@carbon/icons-react' in the package

const iconsMap = {
  save: Save20,
  'trash-can': TrashCan20,
  import: DocumentImport20,
  export: DocumentExport20,
  renew: Renew20
}

const Icon = ({ name, size = 20, ...props }) => {
  const [IconComponent, setIconComponent] = useState(null)

  useEffect(() => {
    if (name) {
      setIconComponent(iconsMap[name] || null)
    } else {
      setIconComponent(null)
    }
  }, [name])

  return IconComponent ? <IconComponent {...props} /> : null
}

export default React.memo(Icon)
