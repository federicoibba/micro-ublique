import React, { PureComponent } from 'react'
import './index.scss'

class ErrorBoundary extends PureComponent {
  constructor(props) {
    super(props)
    this.state = { hasError: false, error: null }
  }

  static getDerivedStateFromError(error) {
    return { hasError: true, error }
  }

  componentDidCatch(error, errorInfo) {
    console.error(error)
    console.info(errorInfo)
  }

  render() {
    if (this.state.hasError) {
      return <h1 className="error-message">Something went wrong.</h1>
    }
    return this.props.children
  }
}

export default ErrorBoundary
