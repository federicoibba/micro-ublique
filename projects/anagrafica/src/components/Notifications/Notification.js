import React, { useEffect, useState } from 'react'
import cx from 'classnames'
import {
  InlineNotification,
  ToastNotification,
  NotificationActionButton
} from '@ublique/components/Notification'
import { delay } from 'utils'
import { DEFAULT_NOTIFICATION_DURATION } from 'config'
import { removeNotification } from './actions'
import './index.scss'

const NotificationComponent = ({
  id,
  action,
  message,
  type = 'inline',
  kind = 'info',
  title = '',
  wrap = false,
  duration = DEFAULT_NOTIFICATION_DURATION,
  ...props
}) => {
  const [visible, setVisible] = useState(false)

  useEffect(() => {
    setTimeout(() => {
      setVisible(true)
    }, 10)
  }, [])

  useEffect(() => {
    if (duration === 0) return

    const timeout = setTimeout(async () => {
      setVisible(false)
      await delay(200) // Wait for animation to finish
      removeNotification(id)
    }, duration)
    return () => {
      clearTimeout(timeout)
    }
  }, [])

  const getAction = () => {
    const onActionClick = () => {
      action.onClick()
      removeNotification(id)
    }
    return (
      <NotificationActionButton onClick={onActionClick}>
        {action.title}
      </NotificationActionButton>
    )
  }

  const NotificationComponent =
    type === 'toast' ? ToastNotification : InlineNotification

  return (
    <NotificationComponent
      {...props}
      kind={kind}
      title={title}
      subtitle={message ? (title ? ` - ${message}` : message) : ''}
      actions={action ? getAction() : undefined}
      className={cx({ visible, wrap })}
      onCloseButtonClick={() => removeNotification(id)}
    />
  )
}

export const Notification = React.memo(NotificationComponent)
