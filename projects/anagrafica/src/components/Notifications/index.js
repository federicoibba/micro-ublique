import React from 'react'
import { map } from 'lodash'
import withStore from 'hoc/withStore'
import { Notification } from './Notification'
import './index.scss'

const sortNotifications = (notifications) =>
  notifications.reduce(
    ([toast, inline], notification) => {
      return notification.type === 'toast'
        ? [[...toast, notification], inline]
        : [toast, [...inline, notification]]
    },
    [[], []]
  )

const getNotificationsBlock = (notifications, type) => (
  <div className={`${type}-notifications__container`}>
    {map(notifications, (notificationData) => (
      <Notification key={notificationData.id} {...notificationData} />
    ))}
  </div>
)

const Notifications = ({ notifications }) => {
  const { items } = notifications

  if (!items || items.length === 0) return null

  const [toastNotifications, inlineNotifications] = sortNotifications(items)

  return (
    <div className="notifications">
      {getNotificationsBlock(inlineNotifications, 'inline')}
      {getNotificationsBlock(toastNotifications, 'toast')}
    </div>
  )
}

export default withStore(Notifications, ['notifications'])
