import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { getWorkingTablesFromIstanceID, resetSidebarStore } from 'App/actions'
import { mapWorkingTableWithT } from 'App/utils'
import { sidebarStore } from 'services/state'
import Sidebar from '../../components/Sidebar'
import './index.scss'

function withWorkingTablesSidebar(WrappedComponent, basename) {
  return function InnerComponent({ match, history }) {
    const {
      params: { instanceId, workingTableId, type }
    } = match

    const isLandingPage =
      match.path === `${basename}/:instanceId/workingtables` && match.isExact

    const [wtData, setWtData] = useState()

    const { t } = useTranslation('Shell')

    // Do not refetch needlessly when changing language
    useEffect(() => {
      getWorkingTablesFromIstanceID(instanceId).then(setWtData)
    }, [instanceId])

    useEffect(() => {
      if (!wtData) return

      const { wts, wtCounts } = wtData

      const mapWorkingTable = mapWorkingTableWithT(t, history, basename)
      const WTs = wts.map((wt) => mapWorkingTable(wt, wtCounts, instanceId))
      sidebarStore.set({ type: 'workingTables', items: WTs })

      // If url and location.pathname are the same
      // we are not showing a working table detail yet.
      if (isLandingPage && WTs.length) {
        // Open the first available WT
        history.replace(WTs[0].href)
      }
    }, [wtData, t])

    useEffect(() => {
      return resetSidebarStore
    }, [])

    return (
      <div className="shell-wrapper">
        <Sidebar />
        {!isLandingPage && (
          <main className="bx--content ublique-shell-content-container">
            <WrappedComponent key={`${instanceId}-${workingTableId}-${type}`} />
          </main>
        )}
      </div>
    )
  }
}
export default withWorkingTablesSidebar
