import React, { useState, useEffect, useCallback } from 'react'
import { map } from 'lodash'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { closeModal as closeModalAction } from './actions'

// Preserves opening and closing animation
const InnerComponent = React.memo(({ modal: Modal, data }) => {
  const [open, setOpen] = useState(false)

  useEffect(() => {
    setTimeout(() => setOpen(true), 10)
  }, [])

  const closeModal = useCallback(() => {
    setOpen(false)
    const timeout = setTimeout(closeModalAction, 200)
    return () => {
      clearTimeout(timeout)
    }
  }, [])

  return <Modal open={open} close={closeModal} data={data} />
})

const ModalContainer = ({ modals: componentsMap }) => {
  const { activeModals, modalsData } = useStoreSelector('modal')

  return map(activeModals, (modalId, i) => {
    const ModalComponent = componentsMap[modalId]

    if (!ModalComponent) return null

    return (
      <InnerComponent
        key={`${modalId}-${i}`}
        modal={ModalComponent}
        data={modalsData[modalId]}
      />
    )
  })
}

export default ModalContainer
