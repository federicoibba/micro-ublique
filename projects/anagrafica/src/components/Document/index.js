import React, { useCallback, useEffect, useMemo, useState, useRef } from 'react'
import { saveAs } from 'file-saver'
import { isUndefined } from 'lodash'
import { ExportApi } from 'services/api/export'
import { ImportApi } from 'services/api/import'
import ActionBar from 'components/ActionBar'
import Forbidden from '@ublique/components/PageForbidden'
import DocumentForm from 'components/DocumentForm'
import {
  getDetailItemIds,
  getFieldsFlatMap,
  groupSections
} from 'pages/InputView/helpers/table-config'
import { addNotification } from 'components/Notifications/actions'
import { openModal } from 'components/Modal/actions'
import {
  deleteDocumentById,
  getPermissionsForDocumentType
} from 'services/api/documents'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import useDocumentTranslation from 'utils/hooks/useDocumentTranslation'
import { buildRowId } from 'utils/documents'
import DocumentMapper, { extraKeys } from 'utils/documents/DocumentMapper'
import PermissionsManager from 'utils/documents/PermissionsManager'
import { documentsStore, synapse } from 'services/state'
import { documentEvents } from 'pages/InputView/constants'
import {
  createNewEmptyDocument,
  loadDocumentIntoStore,
  handleDeletionError,
  updateDocuments,
  startDocumentSavingProcess
} from 'pages/InputView/actions'
import dispatcher from 'pages/InputView/eventsDispatcher'
import useScenarioData from 'pages/InputView/helpers/useScenarioData'
import LoadingSkeleton from './LoadingSkeleton'
import './index.scss'

// When a new tab is opened with an empty document, `id` only contains the activityId
// A temporary id is added when opening the tab to properly work with the store
const createId = ({ temporaryId, ...id }) => {
  if (temporaryId) {
    return buildRowId({ ...id, id: temporaryId })
  }
  return buildRowId(id)
}

const Document = ({
  id,
  modelDefinition,
  instanceId,
  workingTableType,
  onDelete,
  onCreate,
  onUpdate,
  closeTab,
  permissions: permissionsFromParent,
  defaultValueGetter
}) => {
  const {
    title,
    sections,
    readOnly,
    rules,
    permissions: modelPermissions
  } = modelDefinition
  const isNew = !id.id
  const workingTableId = id.activityId
  const documentId = useMemo(() => createId(id), [isNew])

  const { t } = useDocumentTranslation(workingTableType)

  // Always get the document from the store, without fetching it again.
  // Currently it's not possible to open a document without passing from the table.
  // (where all documents are loaded into the store), so this is safe.
  const document = useStoreSelector(
    'documents',
    ({ documentsById }) => documentsById[documentId]
  )

  const { permission, fieldPermission } = document || {}
  const [loading, setLoading] = useState(!document)
  const [buttonsDisabled, setButtonsDisabled] = useState(false)
  const [submitted, setSubmitted] = useState(false)
  const formRef = useRef()

  const fieldsMap = useMemo(() => getFieldsFlatMap(sections), [sections])

  const { scenario, hasScenarioDep } = useScenarioData({
    instanceId,
    fieldsMap,
    isNew
  })

  const { mainSection, detailSections } = useMemo(
    () => groupSections(sections),
    [sections]
  )

  const detailItemIds = useMemo(
    () => getDetailItemIds(detailSections),
    [detailSections]
  )

  const mapper = useMemo(
    () => new DocumentMapper({ detailItemIds, fieldsMap, instanceId }),
    [detailItemIds, fieldsMap, instanceId]
  )

  const permissionsManager = useMemo(
    () =>
      !isUndefined(permission)
        ? new PermissionsManager({
            permission,
            fieldPermission,
            modelPermissions,
            readOnly,
            isNewDocument: isNew
          })
        : null,
    [permission, fieldPermission, isNew, modelPermissions, readOnly]
  )

  const isActionDisabled = useCallback((_) => buttonsDisabled, [buttonsDisabled])

  // --------------------------------- CRUD operations --------------------------------- //

  const onDocumentChanged = useCallback(
    (field, value) => {
      updateDocuments({ documentId, field, value, rules, mapper })
    },
    [documentId, mapper]
  )

  const saveDocument = useCallback(async () => {
    setSubmitted(true)

    try {
      setButtonsDisabled(true)
      const result = await startDocumentSavingProcess(documentId, {
        documentType: workingTableType,
        sections,
        mapper,
        rules,
        t
      })

      // Validation failed
      if (result === null) {
        setButtonsDisabled(false)
        return
      }

      addNotification({
        kind: 'success',
        message: t('Document:SAVE_SUCCESS')
      })

      const mappedDocument = await mapper.frontend.transformDocument(result)
      const finalId = mappedDocument[extraKeys.DOCUMENT_ID_KEY]

      documentsStore.update(({ documentsById, ...prevState }) => ({
        ...prevState,
        documentsById: { ...documentsById, [finalId]: mappedDocument },
        ...(!isNew && {
          latestEdit: null,
          editedDocuments: prevState.editedDocuments.filter((id) => id !== finalId)
        })
      }))

      dispatcher.crudEvent({
        type: workingTableType,
        id: result.id,
        instanceId
      })

      setButtonsDisabled(false)

      // To correctly update the master row when adding/removing details, all data must be refreshed
      const refresh = detailSections.length > 0
      const parentCallback = isNew ? onCreate : onUpdate
      parentCallback({ id: result.id, refresh }, result)
    } catch (error) {
      console.error(error)
      setButtonsDisabled(false)
      addNotification({
        kind: 'error',
        message: t(
          error.code === 403 ? 'Document:PRIMARY_KEY_ERROR' : 'Document:SAVE_ERROR'
        )
      })
    }
  }, [documentId, mapper, rules, t, sections, isNew, workingTableType])

  const deleteDocument = useCallback(async () => {
    try {
      await deleteDocumentById(workingTableType, id)

      addNotification({
        kind: 'success',
        message: t('Document:DELETE_SUCCESS')
      })

      dispatcher.crudEvent({ type: workingTableType, id, instanceId })

      onDelete({ id })
    } catch (error) {
      handleDeletionError(error)
    }
  }, [workingTableType, id])

  const getDocument = useCallback(
    () => loadDocumentIntoStore(workingTableType, id, mapper),
    [workingTableType, id, mapper]
  )

  const getDocumentAndResetFormState = useCallback(() => {
    setSubmitted(false)
    getDocument().then(() => {
      formRef.current.resetState()
    })
  }, [])

  // --------------------------------- Effects --------------------------------- //

  // Existing document opened from a link, in a grid of a different document type
  // Since it was not previously loaded, it must be fetched first
  useEffect(() => {
    if (!document && !isNew) {
      getDocument().then(() => {
        setLoading(false)
      })
    }
  }, [!!document, isNew, getDocument])

  // New document, two scenarios:
  // - The document is of the same type of the documents in the table, no need to refetch permissions
  // - The document is of a different type, its permissions must be fetched
  useEffect(() => {
    if (!document && isNew && (hasScenarioDep ? !!scenario : true)) {
      const onPermissionsLoaded = (permissions) => {
        createNewEmptyDocument(documentId, {
          mainSection,
          workingTableId,
          detailItemIds,
          permissions,
          defaultValueGetter,
          scenario,
          instanceId,
          mapper
        }).then(() => {
          setLoading(false)
        })
      }

      if (permissionsFromParent) {
        onPermissionsLoaded(permissionsFromParent)
      } else {
        getPermissionsForDocumentType(workingTableId, workingTableType).then(
          onPermissionsLoaded
        )
      }
    }
  }, [
    !!document,
    !!scenario,
    hasScenarioDep,
    isNew,
    workingTableType,
    workingTableId,
    documentId,
    mainSection,
    detailItemIds,
    permissionsFromParent,
    instanceId,
    mapper
  ])

  useEffect(() => {
    return synapse.subscribe(
      documentEvents.DOCUMENT_SAVE_REQUESTED,
      ({ id, callback }) => {
        if (createId(id) === documentId) {
          saveDocument().then(callback)
        }
      }
    )
  }, [saveDocument])

  useEffect(() => {
    return synapse.subscribe(documentEvents.DOCUMENT_TAB_CLOSED, (closedId) => {
      if (closedId.id === id.id && closedId.activityId === id.activityId) {
        closeTab()
      }
    })
  }, [id])

  useEffect(() => {
    return synapse.subscribe(
      documentEvents.DOCUMENT_REFRESHED,
      ({ id: refreshedId, activityId: refreshedActivityId }) => {
        if (id.id === refreshedId && id.activityId === refreshedActivityId) {
          getDocument()
        }
      }
    )
  }, [getDocument])

  useEffect(() => {
    return function onUnmount() {
      // Remove temporary document
      if (isNew) {
        documentsStore.update((prevState) => {
          const documentsById = { ...prevState.documentsById }
          delete documentsById[documentId]
          return { ...prevState, documentsById }
        })
      }
    }
  }, [])

  // --------------------------------- Import/Export --------------------------------- //

  const exportDocument = useCallback(async () => {
    addNotification({ message: t('Shell:ExportStarted') })
    const { body, fileName } = await ExportApi.exportDocument(
      workingTableType,
      workingTableId,
      id.id
    )
    saveAs(body, fileName)
  }, [t, workingTableType, workingTableId, id.id])

  const importDocument = useCallback(() => {
    openModal('import', {
      onSubmit: (file) =>
        ImportApi.importDocument(
          workingTableType,
          workingTableId,
          id.id,
          null,
          file
        ).then(getDocumentAndResetFormState)
    })
  }, [workingTableType, workingTableId, id.id])

  // --------------------------------- Action buttons --------------------------------- //

  const actionsHandler = useMemo(
    () => ({
      save: saveDocument,
      delete: () =>
        openModal('confirmAction', {
          message: 'confirmDeleteDocument',
          itemsCount: 1,
          onConfirm: deleteDocument
        }),
      export: exportDocument,
      import: importDocument,
      refresh: getDocumentAndResetFormState
    }),
    [
      saveDocument,
      deleteDocument,
      importDocument,
      exportDocument,
      getDocumentAndResetFormState
    ]
  )

  const actions = useMemo(
    () =>
      [
        { id: 'save', title: 'Document:ACTION_SAVE', icon: 'save' },
        { id: 'delete', title: 'Document:ACTION_DELETE', icon: 'trash-can' },
        { id: 'import', title: 'Document:ACTION_IMPORT', icon: 'import' },
        { id: 'export', title: 'Document:ACTION_EXPORT', icon: 'export' },
        { id: 'refresh', title: 'Document:ACTION_REFRESH', icon: 'renew' }
      ].map((action) => ({
        ...action,
        title: t(action.title),
        onClick: actionsHandler[action.id]
      })),
    [actionsHandler]
  )

  if (loading || !document) return <LoadingSkeleton />

  if (!permissionsManager.canRead()) {
    return <Forbidden canGoBack={false} text={t('Document:Forbidden')} />
  }

  const documentTitle = `${t(title)}${document.title ? ` - ${document.title}` : ''}`

  return (
    <div className="ublique-document-page">
      <div className="ublique-document-page--header">
        <h5>{documentTitle}</h5>
        <ActionBar
          isActionDisabled={isActionDisabled}
          isActionHidden={permissionsManager.isDocumentActionHidden}
          actions={actions}
        />
      </div>

      <div className="ublique-document-page--form-container">
        <DocumentForm
          ref={formRef}
          t={t}
          isNew={isNew}
          readOnly={readOnly}
          document={document}
          sections={sections}
          rules={rules}
          mapper={mapper}
          instanceId={instanceId}
          workingTableType={workingTableType}
          workingTableId={workingTableId}
          onChange={onDocumentChanged}
          permissionsManager={permissionsManager}
          submitted={submitted}
          fieldsMap={fieldsMap}
          refreshDocument={getDocumentAndResetFormState}
          detailsAsTabs
          defaultValueGetter={defaultValueGetter}
          scenario={scenario}
        />
      </div>
    </div>
  )
}

export default Document
