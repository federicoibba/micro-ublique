import React from 'react'
import { SkeletonText } from '@ublique/components/Skeleton'
import './index.scss'

const LoadingSkeleton = () => {
  return (
    <div className="ublique-document-page">
      <div className="document-skeleton">
        <SkeletonText heading width="50%" />
        <SkeletonText
          width="70%"
          heading
          paragraph
          lineCount={5}
          className="skeleton-main-section"
        />
      </div>
    </div>
  )
}

export default LoadingSkeleton
