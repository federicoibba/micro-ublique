import React from 'react'
import { isDocumentBeingEdited } from 'pages/InputView/selectors'
import { buildRowId } from 'utils/documents'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import './index.scss'

const EditingIndicator = ({ documentId }) => {
  const isBeingEdited = useStoreSelector(
    'documents',
    isDocumentBeingEdited(buildRowId(documentId))
  )
  return isBeingEdited ? <div className="input-view-editing-indicator" /> : null
}

export default EditingIndicator
