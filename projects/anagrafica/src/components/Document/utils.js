import { sortBy } from 'lodash'
import { validateFields } from 'utils/validation'

// eslint-disable-next-line no-prototype-builtins
const isValidation = (o) => o && o.hasOwnProperty('state')
const isEmptyObject = (o) => !o || !Object.keys(o).length

const isValidationWithState = (o) => isValidation(o) && typeof o.state === 'string'
const validationIs = (o, state) =>
  isValidationWithState(o) && o.state.startsWith(state)

const isInvalid = (o) => validationIs(o, 'invalid')

export const getValidationTree = (validated, ancestors = []) => {
  return Object.entries(validated).reduce((acc, [id, result]) => {
    //  Grid items or validation results
    if (Array.isArray(result) && result.length) {
      return [...acc, ...getValidationTree(result, [...ancestors, id])]
    }
    //  Grid item
    if (!isEmptyObject(result) && !isValidation(result)) {
      return [...acc, ...getValidationTree(result, [...ancestors, id])]
    }
    // Validation result
    if (isValidation(result) && isInvalid(result)) {
      return [...acc, { path: ancestors, ...result }]
    }

    return acc
  }, [])
}

const space = (n) => Array(n).fill('  ').join('')

export const getValidationErrorMessages = (errors, sectionsByField, t) => {
  const segments = sortBy(
    errors.reduce((acc, { path, invalidText }) => {
      if (!invalidText) return acc
      const errorId = path[0]
      const { title, index } = sectionsByField[errorId]
      return [
        ...acc,
        {
          sectionIndex: index,
          sectionTitle: title,
          message: [
            `${t(`Document:IN_SECTION`)} ${t(title)}`,
            ...path.reduce((acc, id) => {
              if (isFinite(id)) {
                return [...acc, `${t(`Document:IN_ROW`)} ${parseInt(id) + 1}`]
              }
              return acc
            }, []),
            t(invalidText)
          ]
        }
      ]
    }, []),
    'sectionIndex'
  ).map(({ message }) => message)

  let message = ''
  let lastSection, lastRow

  for (const messages of segments) {
    const section = messages[0]
    const row = messages.length > 2 ? messages[1] : ''
    const error = messages.length > 2 ? messages[2] : messages[1]

    if (section !== lastSection) {
      lastSection = section
      lastRow = null
      message += `\n${section}:\n`
    }
    if (row !== lastRow) {
      lastRow = row
      if (row) {
        message += `${space(1)}\u2022 ${row}:\n`
      }
    }

    if (row) {
      message += `${space(2)}\u2013 ${error}\n`
    } else {
      message += `${space(1)}\u2022 ${error}\n`
    }
  }

  return message
}

// Create a map of fields associated to the section they belong to, in order
export const buildFieldsMapWithSection = (sections) => {
  let index = 0
  return sections.reduce((acc, { fields, ...section }) => {
    return {
      ...acc,
      ...fields.reduce(
        (acc, field) => ({
          ...acc,
          [field.id]: { ...section, index: index++ }
        }),
        {}
      )
    }
  }, {})
}

export const validateDocument = ({ t, rules, fieldsMap, document = {} }) => {
  const validationResult = validateFields(document, rules)
  const invalidFields = getValidationTree(validationResult)

  const validationErrors = invalidFields.length
    ? getValidationErrorMessages(invalidFields, fieldsMap, t)
    : null

  return [validationResult, validationErrors]
}
