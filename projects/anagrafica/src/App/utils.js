import { matchPath } from 'react-router-dom'
import { DeliveryTruck24 } from '@carbon/icons-react'
import { saveAs } from 'file-saver'
import { synapse } from 'state'
import { compact } from 'lodash'
import { ExportApi } from 'api/export'
import { ImportApi } from 'api/import'
import { ExportTranslationsApi, ImportTranslationsApi } from 'api/translations'
import { addNotification } from 'components/Notifications/actions'
import { canCreate } from 'services/auth'
import { LOCALSTORAGE_THEME_KEY, DEFAULT_THEME } from 'config'
import { openModal } from 'components/Modal/actions'
import { documentEvents } from 'pages/InputView/constants'
import dispatcher from 'pages/InputView/eventsDispatcher'
import { updateInstanceWorkingTableCount } from './actions'

const MAIN = 'MAIN'

export const getTheme = () => {
  return localStorage.getItem(LOCALSTORAGE_THEME_KEY)
}

export const getInitialTheme = () => {
  return getTheme() || DEFAULT_THEME
}

export const setLocalStorageTheme = (theme) => {
  return localStorage.setItem(LOCALSTORAGE_THEME_KEY, theme)
}

export const mapInstance = (instance) => ({
  ...instance,
  sidebar: true,
  sidebarTitle: instance.title,
  href: `/instances/${instance.id}/workingtables`
})

export const mapWorkingTableWithT =
  (t, history, basename) => (wt, wtCounts, instanceID) => {
    const [, wtType] = wt.config.match(/value="(.*?)"/)
    const wtHref = `${basename}/${instanceID}/workingtables/${wt.id}/${wtType}/documents`
    const { pathname } = history.location
    const routeMatch = matchPath(pathname, {
      path: `${basename}/:i/workingtables/:w/:t/documents`,
      exact: true
    })
    return {
      ...wt,
      type: wtType,
      href: wtHref,
      icon: DeliveryTruck24,
      title: wt.title,
      count: wtCounts[wt.id] || null,
      label: wt.title,
      groups: wt.groups,
      groupTitle: wt.groups,
      actions: compact([
        canCreate(wt.permission) && {
          title: t('AddDocument'), // TODO: Eliminare o controllare i permessi del docment type, non della WT
          onClick: () => {
            const dispatchNewDocumentEvent = () => {
              dispatcher.emptyDocumentCreated({ type: wtType, activityId: wt.id })
            }
            if (routeMatch) {
              dispatchNewDocumentEvent()
            } else {
              const unsubscribe = synapse.subscribe(
                documentEvents.DOCUMENT_READY,
                () => {
                  dispatchNewDocumentEvent()
                  unsubscribe()
                }
              )
              history.push(wtHref)
            }
          }
        },
        canCreate(wt.permission) && {
          title: t('Import'),
          onClick: () => {
            openModal('import', {
              onSubmit: (file) =>
                ImportApi.importWorkingTable(wt.id, file).then(() => {
                  // eslint-disable-next-line eqeqeq
                  if (routeMatch?.params?.w == wt.id) {
                    dispatcher.refreshEvent({ overwrite: true })
                  }
                  updateInstanceWorkingTableCount()
                })
            })
          }
        },
        {
          title: t('Export'),
          onClick: async () => {
            addNotification({ message: t('ExportStarted') })
            const { body, fileName } = await ExportApi.exportWorkingTable(wt.id)
            saveAs(body, fileName)
          }
        }
      ])
    }
  }
export const mapTranslationsTableWithT = (t) => (wt) => {
  const wtHref = `/translations/translationTables/${wt.group}/${wt.type}/documents`
  return {
    ...wt,
    type: wt.type,
    href: wtHref,
    icon: DeliveryTruck24,
    title: wt.title,
    label: wt.title,
    groups: wt.group,
    groupTitle: wt.group,
    actions: [
      {
        title: t('Translations:Import'),
        onClick: () => {
          openModal('import', {
            onSubmit: (file) => ImportTranslationsApi.importTranslation(file)
          })
        }
      },
      {
        title: t('Translations:Export'),
        onClick: async () => {
          addNotification({ message: t('ExportStarted') })
          const nameSpace = `${wt.groups ? wt.groups : wt.group}${
            wt.type === MAIN ? '' : `.${wt.type}`
          }`
          const blob = await ExportTranslationsApi.exportTranslation(
            nameSpace,
            wt.type
          )
          saveAs(blob, `${nameSpace}-${wt.id}-export.xlsx`)
        }
      },
      {
        title: t('Translations:NewTranslation'),
        onClick: () => {
          openModal('newTranslation', {
            type: 'LABEL',
            group: wt.group,
            subgroup: wt.type
          })
        }
      }
    ]
  }
}

export const setConfigSidebar = (extRoutes, outputConfig, scenarioConfig) => {
  const routes = [...extRoutes]

  const { sidebar: outputSidebar = false } = outputConfig
  const { sidebar: scenarioSidebar = false } = scenarioConfig

  const output = routes.find((route) => route.id === 'output')
  const scenario = routes.find((route) => route.id === 'scenario')

  output.sidebar = outputSidebar
  output.routes[0].sidebar = outputSidebar

  scenario.sidebar = scenarioSidebar

  return routes
}
