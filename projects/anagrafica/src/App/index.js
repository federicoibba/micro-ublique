import React, { Suspense, useEffect } from 'react'

import { Router, Switch, Route } from 'react-router-dom'

import Theme from '@ublique/components/Theme'
import Loading from '@ublique/components/Loading'

import initI18n from 'i18n'
import useStoreSelector from 'utils/hooks/useStoreSelector'

import withWorkingTablesSidebar from '../components/InputViewShell'
import InputView from '../pages/InputView'
import { fetchModelsAndInstances } from './actions'

initI18n()

const App = ({ history, basename = '' }) => {
  const theme = useStoreSelector('theme')

  useEffect(() => {
    fetchModelsAndInstances()
  }, [])

  const WrappedInputView = React.useMemo(
    () => withWorkingTablesSidebar(InputView, basename),
    []
  )

  return (
    <Suspense fallback={<Loading />}>
      <Theme name={theme}>
        <Router history={history}>
          <Switch>
            <Route
              exact
              path={`${basename}/:instanceId/workingtables`}
              component={WrappedInputView}
            />
            <Route
              path={`${basename}/:instanceId/workingtables/:workingTableId/:type/documents`}
              component={WrappedInputView}
            />
            <Route component={() => <h3>Not found</h3>} />
          </Switch>
        </Router>
      </Theme>
    </Suspense>
  )
}

export default App
