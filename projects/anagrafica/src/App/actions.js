import { fetchConfig } from '../services/api/config'
import {
  getDocumentCount,
  getInstances,
  getWorkingTables
} from '../services/api/instances'
import { appConfigStore, instancesStore, sidebarStore } from '../services/state'
import { canRead } from 'services/auth'

export const fetchModelsAndInstances = async () => {
  const [models, instancesData] = await Promise.all([
    fetchConfig('model'),
    getInstances()
  ])

  const {
    body: { items: instances }
  } = instancesData

  instancesStore.set(instances)
  appConfigStore.set({ models })
}

export async function updateInstanceWorkingTableCount() {
  const wtIDs = sidebarStore.get((store) => store.items).map((wt) => wt.id)
  const wtCounts = await getDocumentCount(wtIDs)
  sidebarStore.update((store) => ({
    ...store,
    items: store.items.map((wt) => ({
      ...wt,
      count: wtCounts[wt.id] || null
    }))
  }))
}

export async function getWorkingTablesFromIstanceID(instanceID) {
  sidebarStore.set({ items: Array.from({ length: 5 }).fill({ loading: true }) })
  const workingTables = await getWorkingTables(instanceID)

  if (!workingTables) {
    resetSidebarStore()
    return
  }

  const {
    body: { items }
  } = workingTables

  const readableWts = items.filter(({ permission }) => canRead(permission))

  const wtIDs = readableWts.map((wt) => wt.id)
  const wtCounts = await getDocumentCount(wtIDs)

  return { wts: readableWts, wtCounts }
}

export const resetSidebarStore = () => sidebarStore.set({ items: [] })
