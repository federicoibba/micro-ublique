import { toPairs } from 'lodash'
import { LOGICAL_OP, NESTED_AND_IN_AND_ERROR } from '../constants'
import validateORconditions from './validateORconditions'
import validateRecursively from './validateRecursively'
import { resolveSelfKey } from './misc'

export default function validateANDconditions(
  { showValidationResult },
  rootFieldId,
  data,
  conditions
) {
  const conditionsPairs = toPairs(conditions)

  if (!conditionsPairs.length) return { isValid: true }

  const validationResultAcc = []

  for (const [fieldId, fieldConditions] of conditionsPairs) {
    if (fieldId === LOGICAL_OP.AND) {
      throw new Error(NESTED_AND_IN_AND_ERROR)
    }

    let result

    if (fieldId === LOGICAL_OP.OR) {
      result = validateORconditions(
        { showValidationResult },
        rootFieldId,
        data,
        fieldConditions
      )
    } else {
      const resolvedFieldId = resolveSelfKey(rootFieldId, fieldId)
      result = validateRecursively(
        { showValidationResult },
        resolvedFieldId,
        data,
        fieldConditions
      )
    }

    const { isValid, validationResult } = result

    if (!isValid) return { isValid: false } // Short circuit at the first invalid operation
    if (showValidationResult) {
      validationResultAcc.push(...validationResult)
    }
  }

  // No invalid operation, ALL conditions are valid
  return {
    isValid: true,
    validationResult: showValidationResult ? validationResultAcc : undefined
  }
}
