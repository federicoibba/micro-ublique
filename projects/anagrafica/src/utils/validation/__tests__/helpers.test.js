import {
  convertToFullNotation,
  resolveSelfKey,
  getFieldProperties,
  excludeUndefinedState
} from '../helpers'

describe('Tests helpers', () => {
  it('Tests convertToFullNotation', () => {
    const shorthandObj = {
      id: 'field1',
      type: 'input',
      validation: {
        $: { gt: 18 }
      }
    }
    const expected = {
      id: 'field1',
      type: 'input',
      states: {
        invalid: {
          when: {
            $: { gt: 18 }
          }
        }
      }
    }
    expect(convertToFullNotation(shorthandObj)).toEqual(expected)
  })

  it('Tests resolveSelfKey', () => {
    expect(resolveSelfKey('rootId', '$')).toEqual('rootId')
    expect(resolveSelfKey('rootId', '$.length')).toEqual('rootId.length')
    expect(resolveSelfKey('rootId', 'childId.length')).toEqual('childId.length')
  })

  it('Tests getFieldProperties', () => {
    expect(getFieldProperties('fieldId')).toEqual(['fieldId'])
    expect(getFieldProperties('fieldId.length')).toEqual(['fieldId', 'length'])
    expect(getFieldProperties('fieldId.nestedKey1.nestedKey2.nestedKey3')).toEqual([
      'fieldId',
      'nestedKey1.nestedKey2.nestedKey3'
    ])
  })

  it('Tests excludeUndefinedState', () => {
    const validationData = [{ state: 'visible', validationResult: [{ a: 22 }] }]
    expect(excludeUndefinedState([{ state: undefined }])).toEqual([])
    expect(excludeUndefinedState(validationData)).toEqual(validationData)
  })
})
