import validateRecursively from '../helpers/validateRecursively'
import { validateField } from '..'

describe('Tests validateRecursively', () => {
  it('With a simple condition', () => {
    const result = validateRecursively(
      { showValidationResult: true },
      'customerAge',
      { customerAge: 20 },
      { gt: 18 }
    )
    expect(result).toEqual({
      isValid: true,
      validationResult: [{ operation: 'gt', fieldId: 'customerAge', target: 18 }]
    })
  })

  it('With multiple simple conditions', () => {
    const result = validateRecursively(
      { showValidationResult: true },
      'customerAge',
      { customerAge: 20 },
      // All must be true (AND)
      {
        gt: 18,
        lt: 30,
        in: [18, 20, 22, 24, 26, 28]
      }
    )
    expect(result).toEqual({
      isValid: true,
      validationResult: [
        { operation: 'gt', fieldId: 'customerAge', target: 18 },
        { operation: 'lt', fieldId: 'customerAge', target: 30 },
        {
          operation: 'in',
          fieldId: 'customerAge',
          target: [18, 20, 22, 24, 26, 28]
        }
      ]
    })
  })

  it('With single conditions in OR', () => {
    const result = validateRecursively(
      { showValidationResult: true },
      'customerAge',
      { customerAge: 16 },
      {
        $or: [{ eq: 66 }, { gt: 18 }]
      }
    )
    expect(result).toEqual({ isValid: false })
  })

  it('With conditions in OR and AND', () => {
    const result = validateRecursively(
      { showValidationResult: true },
      'customerAge',
      { customerAge: 14 },
      {
        // One of these...
        $or: [{ neq: 66 }, { lt: 100 }],
        // ...AND this
        gt: 18
      }
    )
    expect(result).toEqual({ isValid: false })
  })

  it('With multiple conditions in OR', () => {
    const result = validateRecursively(
      { showValidationResult: true },
      'customerAge',
      { customerAge: 166 },
      {
        $or: [
          { neq: 166 }, // Either this...
          { gt: 50, lt: 100 } // ..OR both of these
        ]
      }
    )
    expect(result).toEqual({ isValid: false })
  })

  it('With multiple conditions in OR and AND', () => {
    const result = validateRecursively(
      { showValidationResult: true },
      'customerAge',
      { customerAge: 80 },
      {
        $or: [
          { neq: 166 }, // Either this...
          { gt: 50, lt: 100 } // ..OR both of these
        ],
        in: [60, 80, 100],
        neq: 0
      }
    )
    expect(result).toEqual({
      isValid: true,
      validationResult: [
        { operation: 'in', fieldId: 'customerAge', target: [60, 80, 100] },
        { operation: 'neq', fieldId: 'customerAge', target: 0 },
        { operation: 'neq', fieldId: 'customerAge', target: 166 }
      ]
    })
  })

  it('With nested conditions 1', () => {
    const result = validateRecursively(
      { showValidationResult: true },
      'customerAge',
      { customerAge: 80 },
      {
        $or: [{ lt: 1000, $or: [{ eq: 44 }, { gt: 40 }] }, { between: [2000, 4000] }],
        in: [60, 80, 100],
        eq: 80
      }
    )
    expect(result).toEqual({
      isValid: true,
      validationResult: [
        { operation: 'in', fieldId: 'customerAge', target: [60, 80, 100] },
        { operation: 'eq', fieldId: 'customerAge', target: 80 },
        { operation: 'lt', fieldId: 'customerAge', target: 1000 },
        { operation: 'gt', fieldId: 'customerAge', target: 40 }
      ]
    })
  })

  it('With nested conditions 2', () => {
    const result = validateRecursively(
      { showValidationResult: true },
      'customerAge',
      { customerAge: 80 },
      {
        $or: [
          {
            between: [0, 1000],
            $or: [{ eq: 44 }, { gt: 40, $or: [{ neq: '0', lt: 100 }, { eq: 80 }] }]
          },
          { eq: 100 }
        ]
      }
    )
    expect(result).toEqual({
      isValid: true,
      validationResult: [
        { operation: 'between', fieldId: 'customerAge', target: [0, 1000] },
        { operation: 'gt', fieldId: 'customerAge', target: 40 },
        { operation: 'neq', fieldId: 'customerAge', target: '0' },
        { operation: 'lt', fieldId: 'customerAge', target: 100 }
      ]
    })
  })
})

describe('Tests validation', () => {
  it('Evaluates correctly states with AND and OR conditions', () => {
    const field = {
      id: 'customerSurname',
      states: {
        invalid: {
          when: {
            'customerName.length': { gt: 5 },
            'customerSurname.length': { gt: 5 },
            $or: {
              'customerAge.length': { lt: 100, gt: 1 },
              customerName: { eq: 'Giuseppe' }
            }
          }
        }
      }
    }
    const data = {
      customerName: 'Giuseppe',
      customerSurname: 'Garibaldi',
      customerAge: 100
    }
    const validationResult = [
      {
        operation: 'gt',
        fieldId: 'customerName',
        fieldProperties: 'length',
        target: 5
      },
      {
        operation: 'gt',
        fieldId: 'customerSurname',
        fieldProperties: 'length',
        target: 5
      },
      { operation: 'eq', fieldId: 'customerName', target: 'Giuseppe' }
    ]
    const expected = [{ state: 'invalid', validationResult }]
    expect(validateField(data, field)).toEqual(expected)
  })

  it("Evaluates correctly states with the '$' keyword", () => {
    const field = {
      id: 'customerAge',
      states: {
        hidden: {
          when: {
            $: { lt: 1 } // $ refers to itself, so customerAge
          }
        }
      }
    }
    const data = {
      customerAge: 0
    }
    const validationResult = [{ operation: 'lt', fieldId: 'customerAge', target: 1 }]
    const expected = [{ state: 'hidden', validationResult }]
    expect(validateField(data, field)).toEqual(expected)
  })

  it('Evaluates correctly states with dynamic values', () => {
    const field = {
      id: 'customerSurname',
      states: {
        hidden: {
          when: {
            '$.length': { gt: '%customerAge' }
          }
        }
      }
    }
    const data = {
      customerAge: 4,
      customerSurname: 'Mario'
    }
    const validationResult = [
      {
        operation: 'gt',
        fieldId: 'customerSurname',
        fieldProperties: 'length',
        target: 4
      }
    ]
    const expected = [{ state: 'hidden', validationResult }]
    expect(validateField(data, field)).toEqual(expected)
  })

  it('Evaluates correctly states with the first one invalid', () => {
    const field = {
      id: 'customerSurname',
      states: {
        invalid: {
          when: {
            'customerName.length': { gt: 15 },
            $or: {
              customerAge: { lt: 100, gt: 1 },
              customerName: { neq: 'Giuseppe' }
            }
          }
        },
        disabled: {
          when: {
            customerSurname: { eq: 'Garibaldi' }
          }
        }
      }
    }
    const data = {
      customerName: 'Giuseppe',
      customerSurname: 'Garibaldi',
      customerAge: 100
    }
    const validationResult = [
      { operation: 'eq', fieldId: 'customerSurname', target: 'Garibaldi' }
    ]
    const expected = [{ state: 'disabled', validationResult }]
    expect(validateField(data, field)).toEqual(expected)
  })

  it('Evaluates correctly multiple valid states', () => {
    const field = {
      id: 'customerSurname',
      states: {
        invalid: {
          when: {
            'customerName.length': { gt: 6 },
            $or: {
              customerAge: { lt: 100, gt: 1 },
              customerName: { eq: 'Giuseppe' }
            }
          }
        },
        disabled: {
          when: {
            customerSurname: { eq: 'Garibaldi' }
          }
        }
      }
    }
    const data = {
      customerName: 'Giuseppe',
      customerSurname: 'Garibaldi',
      customerAge: 100
    }
    const validationResult1 = [
      {
        operation: 'gt',
        fieldId: 'customerName',
        fieldProperties: 'length',
        target: 6
      },
      { operation: 'eq', fieldId: 'customerName', target: 'Giuseppe' }
    ]
    const validationResult2 = [
      { operation: 'eq', fieldId: 'customerSurname', target: 'Garibaldi' }
    ]
    const expected = [
      { state: 'invalid', validationResult: validationResult1 },
      { state: 'disabled', validationResult: validationResult2 }
    ]
    expect(validateField(data, field)).toEqual(expected)
  })

  it('Evaluates correctly nested AND state inside OR - 1', () => {
    const field = {
      id: 'customerSurname',
      states: {
        invalid: {
          when: {
            $or: {
              'customerName.length': { gt: 6 },
              $and: {
                customerAge: { gt: 70 },
                customerName: { eq: 'Giuseppe' }
              }
            }
          }
        }
      }
    }
    const data = {
      customerName: 'Giuseppe',
      customerSurname: 'Garibaldi',
      customerAge: 100
    }
    const validationResult = [
      {
        operation: 'gt',
        fieldId: 'customerName',
        fieldProperties: 'length',
        target: 6
      }
    ]
    const expected = [{ state: 'invalid', validationResult: validationResult }]
    expect(validateField(data, field)).toEqual(expected)
  })

  it('Evaluates correctly nested AND state inside OR - 2', () => {
    const field = {
      id: 'customerSurname',
      states: {
        invalid: {
          when: {
            $or: {
              'customerName.length': { gt: 60 },
              $and: {
                customerAge: { gt: 70 },
                customerName: { eq: 'Giuseppe' }
              }
            }
          }
        }
      }
    }
    const data = {
      customerName: 'Giuseppe',
      customerSurname: 'Garibaldi',
      customerAge: 100
    }
    const validationResult = [
      {
        operation: 'gt',
        fieldId: 'customerAge',
        target: 70
      },
      { operation: 'eq', fieldId: 'customerName', target: 'Giuseppe' }
    ]
    const expected = [{ state: 'invalid', validationResult: validationResult }]
    expect(validateField(data, field)).toEqual(expected)
  })

  it('Evaluates correctly nested OR state inside AND - 1', () => {
    const field = {
      id: 'customerSurname',
      states: {
        invalid: {
          when: {
            'customerName.length': { gt: 6 },
            $or: {
              $and: {
                customerAge: { gt: 666 },
                customerName: { eq: 'Giuseppe' }
              },
              customerSurname: { neq: 'Cavour' }
            }
          }
        }
      }
    }
    const data = {
      customerName: 'Giuseppe',
      customerSurname: 'Garibaldi',
      customerAge: 100
    }
    const validationResult = [
      {
        operation: 'gt',
        fieldId: 'customerName',
        fieldProperties: 'length',
        target: 6
      },
      {
        operation: 'neq',
        fieldId: 'customerSurname',
        target: 'Cavour'
      }
    ]
    const expected = [{ state: 'invalid', validationResult: validationResult }]
    expect(validateField(data, field)).toEqual(expected)
  })

  it('Evaluates correctly nested OR state inside AND - 2', () => {
    const field = {
      id: 'customerSurname',
      states: {
        invalid: {
          when: {
            'customerName.length': { gt: 6 },
            $or: {
              $and: {
                customerAge: { gt: 70 },
                customerName: { eq: 'Giuseppe' }
              },
              customerSurname: { neq: 'Cavour' }
            }
          }
        }
      }
    }
    const data = {
      customerName: 'Giuseppe',
      customerSurname: 'Garibaldi',
      customerAge: 100
    }
    const validationResult = [
      {
        operation: 'gt',
        fieldId: 'customerName',
        fieldProperties: 'length',
        target: 6
      },
      {
        operation: 'gt',
        fieldId: 'customerAge',
        target: 70
      },
      {
        operation: 'eq',
        fieldId: 'customerName',
        target: 'Giuseppe'
      }
    ]
    const expected = [{ state: 'invalid', validationResult: validationResult }]
    expect(validateField(data, field)).toEqual(expected)
  })

  it('Evaluates correctly states with shorthand syntax', () => {
    const field = {
      id: 'customerName',
      validation: {
        $or: {
          $: { eq: 'forbidden' },
          customerAge: { between: [20, 30] }
        }
      }
    }
    const data = {
      customerName: 'Giuseppe',
      customerAge: 26
    }
    const validationResult = [
      { operation: 'between', fieldId: 'customerAge', target: [20, 30] }
    ]
    const expected = [{ state: 'invalid', validationResult }]
    expect(validateField(data, field)).toEqual(expected)
  })

  it('Evaluates correctly nested states when they are valid', () => {
    const field = {
      id: 'customerSurname',
      states: {
        invalid: {
          when: {
            // Both must evaluate to true
            'customerName.length': { gt: 5 },
            '$.length': { gt: 5 }
          },
          // Inner state that is evaluated only when the parent is valid
          states: {
            hidden: {
              when: {
                $: { in: ['Garibaldi', 'Cavour'] },
                customerName: { eq: 'forbidden' }
              }
            },
            minified: {
              when: {
                customerAge: { lt: 500 }
              }
            }
          }
        }
      }
    }
    const data = {
      customerName: 'forbidden',
      customerSurname: 'Garibaldi',
      customerAge: 100
    }
    const validationResult1 = [
      {
        operation: 'gt',
        fieldId: 'customerName',
        fieldProperties: 'length',
        target: 5
      },
      {
        operation: 'gt',
        fieldId: 'customerSurname',
        fieldProperties: 'length',
        target: 5
      }
    ]
    const validationResult2 = [
      {
        operation: 'in',
        fieldId: 'customerSurname',
        target: ['Garibaldi', 'Cavour']
      },
      { operation: 'eq', fieldId: 'customerName', target: 'forbidden' }
    ]
    const validationResult3 = [
      { operation: 'lt', fieldId: 'customerAge', target: 500 }
    ]
    const expected = [
      { state: 'hidden', validationResult: validationResult2 },
      { state: 'minified', validationResult: validationResult3 },
      { state: 'invalid', validationResult: validationResult1 }
    ]
    expect(validateField(data, field)).toEqual(expected)
  })

  it('Evaluates correctly nested states when they are invalid', () => {
    const field = {
      id: 'customerSurname',
      states: {
        invalid: {
          when: {
            // Both must evaluate to true
            'customerName.length': { gt: 5 },
            'customerSurname.length': { gt: 5 }
          },
          // Inner state is evaluated only when the parent is valid
          states: {
            hidden: {
              when: {
                customerName: { eq: 'forbidden' }
              }
            }
          }
        }
      }
    }
    const data = {
      customerName: 'Giuseppe',
      customerSurname: 'Garibaldi',
      customerAge: 100
    }
    const validationResult = [
      {
        operation: 'gt',
        fieldId: 'customerName',
        fieldProperties: 'length',
        target: 5
      },
      {
        operation: 'gt',
        fieldId: 'customerSurname',
        fieldProperties: 'length',
        target: 5
      }
    ]
    const expected = [{ state: 'invalid', validationResult }]
    expect(validateField(data, field)).toEqual(expected)
  })

  it("Does not return validation result when 'showValidationResult' is set to false", () => {
    const field = {
      id: 'customerSurname',
      states: {
        invalid: {
          when: {
            'customerName.length': { gt: 5 },
            'customerSurname.length': { gt: 5 }
          }
        }
      }
    }
    const data = {
      customerName: 'Giuseppe',
      customerSurname: 'Garibaldi',
      customerAge: 100
    }
    const expected = [{ state: 'invalid' }]
    expect(validateField(data, field, { showValidationResult: false })).toEqual(
      expected
    )
  })

  it('Returns undefined when no condition is valid', () => {
    const field = {
      id: 'customerSurname',
      states: {
        invalid: {
          when: {
            'customerName.length': { gt: 10 },
            'customerSurname.length': { gt: 5 }
          },
          states: {
            hidden: {
              when: {
                customerName: { eq: 'Giuseppe' }
              }
            }
          }
        }
      }
    }
    const data = {
      customerName: 'Giuseppe',
      customerSurname: 'Garibaldi',
      customerAge: 100
    }
    expect(validateField(data, field)).toEqual([{ state: undefined }])
  })

  it('Returns the specified default state when no other state is valid', () => {
    const field = {
      id: 'customerSurname',
      states: {
        defaultState: 'valid',
        invalid: {
          when: {
            customerAge: { lt: 18 }
          }
        }
      }
    }
    const data = {
      customerName: 'Giuseppe',
      customerSurname: 'Garibaldi',
      customerAge: 23
    }
    expect(validateField(data, field)).toEqual([{ state: 'valid' }])
  })

  it('Returns undefined when no state is present', () => {
    const field = {
      id: 'customerSurname',
      type: 'input'
    }
    const data = {
      customerName: 'Giuseppe',
      customerSurname: 'Garibaldi',
      customerAge: 100
    }
    expect(validateField(data, field)).toEqual([{ state: undefined }])
  })

  it("Ignores states that start with 'be_'", () => {
    const field = {
      id: 'customerSurname',
      states: {
        invalid: {
          when: {
            'customerName.length': { gt: 3 },
            'customerSurname.length': { gt: 5 }
          }
        },
        be_state1: {
          when: {
            customerName: { eq: 'Dante' }
          }
        },
        be_state2: {
          when: {
            customerName: { eq: 'Dante' },
            customerAge: { gt: 50 }
          }
        }
      }
    }
    const data = {
      customerName: 'Dante',
      customerSurname: 'Alighieri',
      customerAge: 77
    }
    const validationResult = [
      {
        operation: 'gt',
        fieldId: 'customerName',
        fieldProperties: 'length',
        target: 3
      },
      {
        operation: 'gt',
        fieldId: 'customerSurname',
        fieldProperties: 'length',
        target: 5
      }
    ]
    const expected = [{ state: 'invalid', validationResult }]
    expect(validateField(data, field)).toEqual(expected)
  })
})
