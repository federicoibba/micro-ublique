import { isString } from 'lodash'

const exists = (v) => {
  return !(v === null || v === false || typeof v === 'undefined' || v === '')
}

const regexClean = (re) => {
  if (isString(re) && re.match(/^\/.*\/$/)) {
    const [, regex] = re.split(/^\/(.*)\/$/)
    return regex
  }
  return re
}

export const operators = {
  gt: (a, b) => exists(a) && exists(b) && a > b,
  gte: (a, b) => exists(a) && exists(b) && a >= b,
  lt: (a, b) => exists(a) && exists(b) && a < b,
  lte: (a, b) => exists(a) && exists(b) && a <= b,
  eq: (a, b) => a === b,
  neq: (a, b) => a !== b,

  between: (v, [a, b]) => exists(a) && exists(b) && a < v && v < b,
  betweenle: (v, [a, b]) => exists(a) && exists(b) && a <= v && v < b,
  betweenre: (v, [a, b]) => exists(a) && exists(b) && a < v && v <= b,
  betweene: (v, [a, b]) => exists(a) && exists(b) && a <= v && v <= b,

  in: (v, ar) => ar.includes(v),
  nin: (v, ar) => !ar.includes(v),

  re: (v, re) => new RegExp(regexClean(re)).test(v),
  nre: (v, re) => !new RegExp(regexClean(re)).test(v),

  // eslint-disable-next-line prettier/prettier
  ex: (v, e) => exists(v) === Boolean(e),

  dlt: (a, b) => exists(a) && exists(b) && new Date(a) < new Date(b),
  dlte: (a, b) => exists(a) && exists(b) && new Date(a) <= new Date(b),
  dgt: (a, b) => exists(a) && exists(b) && new Date(a) > new Date(b),
  dgte: (a, b) => exists(a) && exists(b) && new Date(a) >= new Date(b)
}
