export const SHORT_FORM_KEY = 'validation'

export const SHORT_FORM_DEFAULT_STATE = 'invalid'

export const DEFAULT_STATE_KEY = 'defaultState'

export const RESOLVE_VALUE_ID = '%'

export const SELF_VALUE_ID = '$'

export const LOGICAL_OP = {
  AND: '$and',
  OR: '$or'
}

export const IGNORED_STATE_PREFIX = 'be_'

export const EXPLICIT_AND_ERROR =
  `"${LOGICAL_OP.AND}" not necessary: use the same object to group all the conditions ` +
  `that must evaluate to true together (e.g. { gt: 0, lte: 10, ${LOGICAL_OP.OR}: [...] }).`

export const NESTED_OR_IN_OR_ERROR = `"${LOGICAL_OP.OR}" inside another "${LOGICAL_OP.OR}" not supported.`

export const NESTED_AND_IN_AND_ERROR = `"${LOGICAL_OP.AND}" inside another "${LOGICAL_OP.AND}" not supported.`
