import { flatMap } from 'lodash'
import map from 'lodash/fp/map'
import prop from 'lodash/fp/prop'
import reject from 'lodash/fp/reject'
import { adapters } from '..'
import BaseMapper from './base'
import { extraKeys } from '.'

const INCLUDE_FIELD_KEY = 'sendToBackend'

export default class BackendDocumentMapper extends BaseMapper {
  constructor(params) {
    super(params)
    this.params = params
  }

  getFieldIdsToRemove = this.compose(
    map(adapters.getId),
    reject(prop(INCLUDE_FIELD_KEY))
  )

  removeExtraFields = (document) => {
    const { fieldsToResolve } = this.params

    const fieldsToRemove = flatMap(
      Object.values(fieldsToResolve),
      this.getFieldIdsToRemove
    )

    const fields = [...Object.values(extraKeys), ...fieldsToRemove]
    const transformedDocument = { ...document }

    fields.forEach((field) => {
      delete transformedDocument[field]
    })

    return transformedDocument
  }

  makeDetailMapper = (_) => {
    return this.removeExtraFields
  }

  process = this.compose(
    this.removeExtraFields,
    this.transformDetails(this.makeDetailMapper)
  )
}
