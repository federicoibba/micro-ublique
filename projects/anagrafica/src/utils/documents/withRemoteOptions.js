import React, {
  forwardRef,
  memo,
  useEffect,
  useMemo,
  useState,
  useCallback
} from 'react'
import { get, isString, head } from 'lodash'
import compose from 'lodash/fp/compose'
import prop from 'lodash/fp/prop'
import { useParams } from 'react-router-dom'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { documentsStore, synapse } from 'services/state'
import { documentEvents } from 'pages/InputView/constants'
import { extraKeys } from './DocumentMapper'
import { linkedDocsCache } from './RemoteOptions'
import {
  createQueryString,
  getOptionsCacheKey,
  getOptionsRemoteUrl,
  HEADER_FIELD_SYMBOL
} from '.'

const { DETAIL_ID_KEY, DOCUMENT_ID_KEY } = extraKeys

// Delete cache when document of type {type} in the same instance {instanceId} is created/updated
synapse.subscribe(documentEvents.DOCUMENT_CRUD, ({ type, instanceId }) => {
  linkedDocsCache.removeKey(`${type}:${instanceId}`)
})

const withRemoteOptions = (WrappedComponent) =>
  memo(
    forwardRef(function ComponentWithRemoteOptions(props, ref) {
      const {
        options: hardcodedOptions,
        documentType,
        optionsKey,
        excludeCurrentDocument,
        filterField,
        filterKey,
        filter,
        data,
        value,
        detailField,
        valueKey = 'value',
        labelKey = 'label',
        ...rest
      } = props
      const { instanceId } = useParams()
      const { [DOCUMENT_ID_KEY]: documentId, [DETAIL_ID_KEY]: detailId } = data

      const [options, setOptions] = useState(hardcodedOptions)

      const filterValue = useStoreSelector('documents', ({ documentsById }) => {
        if (!filterKey) return

        const document = documentsById[documentId]

        if (!document) return

        const isArrDep = Array.isArray(filterField) && Array.isArray(filterKey)
        const filterKeyArray = isArrDep ? filterKey : [filterKey]

        const values = filterKeyArray.map((f) => {
          if (detailId && !f.startsWith(HEADER_FIELD_SYMBOL)) {
            const detailRow = document[detailField].find(
              ({ [DETAIL_ID_KEY]: rowId }) => rowId === detailId
            )
            return detailRow?.[f]
          }
          return document[f.replace(HEADER_FIELD_SYMBOL, '')]
        })
        return isArrDep ? values : head(values)
      })

      const query = useMemo(
        () => createQueryString(filterField, filterValue, filter),
        [filterField, filterValue, filter]
      )

      const transformOptions = useMemo(
        () =>
          makeTransformOptions({
            optionsKey,
            documentId,
            valueKey,
            excludeCurrentDocument
          }),
        [optionsKey, documentId, valueKey, excludeCurrentDocument]
      )

      const remoteUrl = getOptionsRemoteUrl(instanceId, documentType, query)
      const cacheKey = getOptionsCacheKey(instanceId, documentType, query)

      const currentSelection = useMemo(() => {
        if (!options) return

        // This should only happen in multi-select, with multiple values
        if (Array.isArray(value)) {
          return value.map((v) => options.find((item) => get(item, valueKey) === v))
        }

        return options.find((item) => get(item, valueKey) === value)
      }, [options, value])

      const fetchOptions = useCallback(() => {
        linkedDocsCache
          .getOptions(cacheKey, remoteUrl)
          .then(compose(setOptions, transformOptions, prop('list')))
      }, [cacheKey, remoteUrl, transformOptions])

      useEffect(() => {
        if (hardcodedOptions) return

        fetchOptions()

        return synapse.subscribe(
          documentEvents.DOCUMENT_CRUD,
          ({ type: crudType, instanceId: crudInstanceId }) => {
            if (documentType === crudType && instanceId === crudInstanceId) {
              fetchOptions()
            }
          }
        )
      }, [hardcodedOptions, fetchOptions]) // documentType and instanceId are already included among fetchOptions dependencies

      useEffect(() => {
        // When there are options and no current selection, it means the chosen value was not found inside options
        // The field must then be reset, but ONLY if it previously had a value
        // An event is dispatched to let the document update flow handle everything
        if (options && value && !currentSelection) {
          const { api, columnApi, node, rowIndex, column } = rest
          api?.dispatchEvent({
            type: 'cellValueChanged',
            newValue: null,
            value: null,
            oldValue: value,
            data,
            api,
            columnApi,
            column,
            node,
            rowIndex
          })
        }
      }, [options, value, currentSelection])

      return (
        <WrappedComponent
          ref={ref}
          {...rest}
          value={value}
          valueKey={valueKey}
          labelKey={labelKey}
          options={options}
          documentType={documentType}
          currentSelection={currentSelection}
        />
      )
    })
  )

export default withRemoteOptions

const makeTransformOptions =
  ({ optionsKey, documentId, valueKey, excludeCurrentDocument }) =>
  (options) => {
    if (!options) return

    let transformedOptions = options

    if (optionsKey) {
      const { key, mapKey } = isString(optionsKey) ? { key: optionsKey } : optionsKey

      // When documents in details are nested it's not enough to just take `item[key]`
      // For example: { geoContainerZoneItems: [{ gcZoneDetailZoneName: { ... } }] }
      transformedOptions = transformedOptions.reduce((acc, item) => {
        const items = mapKey ? item[key]?.map(prop(mapKey)) : item[key]
        return [...acc, ...(items || [])]
      }, [])
    }

    if (excludeCurrentDocument) {
      const currentDocument = documentsStore.get(
        ({ documentsById }) => documentsById[documentId]
      )
      transformedOptions = transformedOptions.filter(
        (item) => item[valueKey] !== currentDocument[valueKey]
      )
    }

    return transformedOptions
  }
