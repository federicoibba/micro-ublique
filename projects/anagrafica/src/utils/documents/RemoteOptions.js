import prop from 'lodash/fp/prop'
import { addNotification } from 'components/Notifications/actions'
import { linkedDocumentsStore, synapse } from 'services/state'

const keyBy = (array = [], key) =>
  array.reduce((acc, item) => ({ ...acc, [item[key]]: item }), {})

export default class RemoteOptions {
  constructor(options = {}) {
    const { storeInstance, expirationTime = 300 } = options
    this.expirationTimeMs = expirationTime * 1000
    this.options = {}
    this.store = storeInstance
    this.timeouts = {}
  }

  saveUrisMapToStore(key, options) {
    this.store.update(({ _uriKeys_, documentsByUri, ...prevState }) => ({
      ...prevState,
      _uriKeys_: _uriKeys_.includes(key) ? _uriKeys_ : [..._uriKeys_, key],
      documentsByUri: Object.assign({}, documentsByUri, options.map)
    }))
  }

  saveUrisMapToStoreIfMissing(key, options) {
    const uriKeys = this.store.get(prop('_uriKeys_')) || []

    if (!uriKeys.includes(key)) {
      this.saveUrisMapToStore(key, options)
    }
  }

  // There might be multiple calls to getOptions for the same key (for example for every row in a table)
  // The method is debounced to only run once per key
  debouncedSaveUrisMapToStoreIfMissing(key, options) {
    if (this.timeouts[key]) return

    this.timeouts[key] = setTimeout(() => {
      this.saveUrisMapToStoreIfMissing(key, options)
      delete this.timeouts[key]
    }, 400)
  }

  getOptions(key, url) {
    if (!this.options[key]) {
      this.options[key] = synapse
        .fetch(url)
        .then(({ body }) => {
          this.removeKeyAfterExpiration(key)
          const result = {
            list: body,
            map: keyBy(body, 'uri')
          }
          if (this.store) this.saveUrisMapToStore(key, result) // Always update the store with fresh data
          return result
        })
        .catch((error) => {
          // Fast way to handle errors - should not happen in production!
          addNotification({
            kind: 'error',
            title: 'Error fetching options:',
            caption: error.message
          })
          console.error('[RemoteOptions]: Error fetching options', error)
        })
    }

    // Not a very clean way to check if options have already been saved in the store
    // The store might get reset somewhere else, while this cache is still full, so 'saveUrisMapToStore' would not be called
    if (this.store) {
      this.options[key].then((options) => {
        if (options) {
          this.debouncedSaveUrisMapToStoreIfMissing(key, options)
        }
      })
    }

    return this.options[key]
  }

  removeKeyAfterExpiration(key) {
    setTimeout(() => {
      this.removeKey(key)
    }, this.expirationTimeMs)
  }

  removeKey(key) {
    for (const cacheKey of Object.keys(this.options)) {
      if (cacheKey === key || cacheKey.startsWith(key)) {
        delete this.options[cacheKey]
      }
    }
  }
}

export const linkedDocsCache = new RemoteOptions({
  storeInstance: linkedDocumentsStore
})
