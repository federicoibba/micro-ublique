import React, { PureComponent, createRef } from 'react'
import { validateField } from 'utils/validation'
import { getPropsFromFieldState } from '.'

const validateFieldOptions = { showValidationResult: false }

export default (
  WrappedComponent,
  { isPopup = true, stopEditingOnChange = false } = {}
) =>
  class extends PureComponent {
    constructor(props) {
      super(props)
      const { value } = this.props
      this.state = {
        value,
        fieldState: this.getFieldState(value)
      }
      this.inputRef = createRef()
    }

    getFieldState = (newValue) => {
      const { data, column, rules = {} } = this.props
      const { colId } = column

      const validationResult = validateField(
        { ...data, [colId]: newValue },
        { id: colId, ...rules[colId] },
        validateFieldOptions
      )

      return getPropsFromFieldState(validationResult)
    }

    afterGuiAttached = () => {
      this.inputRef.current?.focus?.()
    }

    setValue = (value) => {
      return this.setState({ value, fieldState: this.getFieldState(value) }, () => {
        if (isPopup && stopEditingOnChange) {
          this.props.stopEditing()
        }
      })
    }

    isPopup = () => {
      return isPopup
    }

    getGui = () => {
      return this.inputRef.current
    }

    getValue = () => {
      return this.state.value
    }

    isCancelAfterEnd = () => {
      const { allowEmpty } = this.props
      return allowEmpty ? false : !!this.state.fieldState.invalid
    }

    get forwardProps() {
      const {
        keyPress,
        charPress,
        column,
        colDef,
        rowIndex,
        node,
        // data,
        api,
        cellStartedEdit,
        columnApi,
        $scope,
        onKeyDown,
        stopEditing,
        eGridCell,
        parseValue,
        formatValue,
        agGridReact,
        frameworkComponentWrapper,
        reactContainer,
        ...props
      } = this.props
      return props
    }

    render() {
      const { column, style } = this.props
      const { colId } = column
      return (
        <WrappedComponent
          hideLabel
          id={colId}
          {...this.forwardProps}
          {...this.state.fieldState}
          ref={this.inputRef}
          onKeyDown={this.onKeyDown}
          value={this.state.value}
          onChange={this.setValue}
          style={{
            ...style,
            minWidth: Math.max(250, column.actualWidth)
          }}
        />
      )
    }
  }
