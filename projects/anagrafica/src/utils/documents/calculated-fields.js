import { compact, get, isString, isUndefined, keyBy } from 'lodash'
import moize from 'moize'
import { linkedDocsCache } from 'utils/documents/RemoteOptions'
import { adapters, getOptionsCacheKey, getOptionsRemoteUrl } from '.'

// Round floating point numbers to avoid results like 1.0999999997
const round = (n, precision = 2) =>
  parseFloat(n.toFixed(precision).replace(/\.00$/, ''))

// Note: when editing numbers, they are saved as strings in the document
const operations = {
  addition: (_) => (n1, n2) => round(+n1 + +n2),
  subtraction: (_) => (n1, n2) => round(n1 - n2),
  multiplication: (_) => (n1, n2) => round(n1 * n2),
  division: (_) => (n1, n2) => round(n1 / n2),
  concat:
    ({ separator = '_' }) =>
    (s1, s2) =>
      compact([s1, s2]).join(separator)
}

const matchLink = (value) => (isString(value) && value.match(/ev:\/\/(.*)\/.*/)) || []

const resolveLink = (instanceId, documentType) =>
  linkedDocsCache.getOptions(
    getOptionsCacheKey(instanceId, documentType),
    getOptionsRemoteUrl(instanceId, documentType)
  )

const resolveLinkedValue = async (instanceId, documentType, [key, fieldKey]) => {
  const { map: linkedDocs } = await resolveLink(instanceId, documentType)
  return linkedDocs[key]?.[fieldKey]
}

const join = (...args) => [].concat(...args).join('.')

const getEntity = (entity) => (Array.isArray(entity) ? entity : [entity, {}])

const createOptionsMap = moize(
  (documents, optionsKey, key) => {
    return keyBy(
      documents.reduce((acc, doc) => [...acc, ...doc[optionsKey]], []),
      key
    )
  },
  { maxArgs: 1, maxSize: 2, maxAge: 1000 * 60 * 10 }
)

const wrapOperation = (op) => (acc, value) =>
  !isUndefined(acc) ? op(acc, value) : value

export const calculateField = async ({
  instanceId,
  operation,
  operands,
  document,
  fieldsMap = {}
}) => {
  const [operationName, operationConfig] = getEntity(operation)
  const op = wrapOperation(operations[operationName](operationConfig))

  let acc

  for (const operand of operands) {
    const [key, operandConfig] = getEntity(operand)
    const { isDetail } = operandConfig
    const [rootKey, subKey, ...nestedKeys] = key.split('.')

    const value = get(document, rootKey)

    // Detail items, in an array. They are aggregated to produce a single value
    // If there is a calculated field, this is calculated before proceeding
    if (Array.isArray(value)) {
      const config = fieldsMap[subKey]

      const { operation: detailOperation, operands: detailOperands } =
        adapters.getCellParams(config)

      let aggregatedValue

      for (const item of value) {
        const detailValue = await calculateField({
          instanceId,
          fieldsMap,
          operation: detailOperation || operation,
          operands: detailOperands || [[join(subKey, nestedKeys), operandConfig]],
          document: item
        })
        aggregatedValue = op(aggregatedValue, detailValue)
      }
      acc = op(acc, aggregatedValue)
      continue
    }

    // Key nested in a detail item inside a linked document
    // Special case, because this value is not saved as a uri, so it's not that straightforward to detected it's a link
    if (isDetail && value) {
      const config = fieldsMap[rootKey]

      const { documentType, optionsKey, valueKey } = adapters.getCellParams(config)

      const { list: linkedDocs } = await resolveLink(instanceId, documentType)
      const optionsMap = createOptionsMap(linkedDocs, optionsKey, valueKey)
      acc = op(acc, get(optionsMap, [value, subKey]))
      continue
    }

    const [, documentType] = matchLink(value)

    // Linked document, saved as uri. Must be resolved
    if (documentType) {
      const linkedValue = await resolveLinkedValue(instanceId, documentType, [
        value,
        subKey
      ])
      acc = op(acc, linkedValue)
      continue
    }

    // Value not found in the document, if it's calculated must be calculated before proceeding
    if (isUndefined(value)) {
      const config = fieldsMap[rootKey]

      const { operation, operands } = adapters.getCellParams(config)

      if (config.type !== 'calculated') continue

      const calculatedValue = await calculateField({
        instanceId,
        fieldsMap,
        document,
        operation,
        operands
      })

      acc = op(acc, calculatedValue)
      continue
    }

    // Simple value that can be directly processed
    acc = op(acc, value)
  }

  return acc
}
