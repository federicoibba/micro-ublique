import useStoreSelector from 'utils/hooks/useStoreSelector'
import { SUPER_USER_ROLE } from './isUserAuthorized'

const selectUserRoles = (state) => state?.roles

export const useUserRoles = () => {
  return useStoreSelector('user', selectUserRoles)
}

export const useIsSuperuser = () => {
  const roles = useUserRoles() || []

  return roles.includes(SUPER_USER_ROLE)
}
