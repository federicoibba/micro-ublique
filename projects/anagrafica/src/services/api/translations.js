import { synapse } from 'state'
import { BASE_URL } from 'config'
import { addNotification } from 'components/Notifications/actions'

const MAIN = 'MAIN'

export const exportData = async (nameSpace, type) => {
  try {
    const payload = {
      onlyLabels: type === MAIN,
      relativePaths: [nameSpace]
    }
    const queryParams = new URLSearchParams(payload).toString()

    const res = await synapse.fetch(
      `${BASE_URL}/admin/translations/export?${queryParams}`
    )
    return res
  } catch (error) {
    console.log('Error: ', error)
  }
}

export const addNewTranslation = async (payload) => {
  try {
    const res = await synapse.fetch(
      `${BASE_URL}/admin/translations`,
      {
        method: 'POST'
      },
      payload
    )
    return res
  } catch (error) {
    console.log('Error: ', error)
  }
}

export async function getTranslationsByType(group, subGroup) {
  const type = subGroup === MAIN ? '' : `/${subGroup}`
  const translations = await synapse
    .fetch(`${BASE_URL}/admin/translations/table/${group}${type}`)
    .catch(console.error)

  if (!translations || !translations.body) {
    console.error(`Couldn't get translations for section ${group}`)
    return { items: [], metadata: {} }
  }

  return translations.body
}

export const updateTranslationRow = async (body, t) => {
  try {
    const payload = { updates: body }
    const res = await synapse.fetch(
      `${BASE_URL}/admin/translations`,
      {
        method: 'PUT'
      },
      payload
    )
    if (res.body) {
      addNotification({
        kind: 'success',
        title: t('Translations:ROW_UPDATED'),
        message: ''
      })
    }
    return res
  } catch (error) {
    console.error('Error: ', error)
  }
}

const createExportApiInstance = () => {
  return {
    async exportTranslation(nameSpace, type) {
      const payload = {
        onlyLabels: type === MAIN,
        relativePaths: [nameSpace]
      }
      const queryParams = new URLSearchParams(payload).toString()

      const { body } = await synapse.fetch(
        `${BASE_URL}/admin/translations/export?${queryParams}`
      )
      return body
    },
    async exportAllTranslation() {
      const { body } = await synapse.fetch(`${BASE_URL}/admin/translations/export`)
      return body
    }
  }
}

const createImportApiInstance = () => {
  return {
    async importTranslation(file) {
      const formData = new FormData()

      if (file) {
        formData.append('file', file)
      }

      return synapse.fetch(
        `${BASE_URL}/admin/translations/import`,
        { method: 'POST' },
        formData
      )
    }
  }
}

const createResultTranslationAPI = () => {
  return {
    async resultTranslation(value) {
      const queryParam = `?term=${value}`
      return synapse.fetch(`${BASE_URL}/admin/translations/search${queryParam}`, {
        method: 'GET'
      })
    }
  }
}

export const ResultTranslationApi = createResultTranslationAPI()
export const ExportTranslationsApi = createExportApiInstance()
export const ImportTranslationsApi = createImportApiInstance()
