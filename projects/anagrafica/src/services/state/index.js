/**
 * Created by fgallucci on 24/06/2020.
 */

import Synapse from '@ublique/synapse/client'
// import { isEmpty, uniqueId } from 'lodash'

// import { initialState as initialScenarioState } from 'pages/Scenario/state'
import {
  initialDocumentState,
  initialLinkedDocumentsState
} from 'pages/InputView/state'
// import { initialState as initialWizardState } from 'components/Wizard/state'
import { initialState as initialModalState } from 'components/Modal/state'
// import { initialState as initialAdminState } from 'pages/Administration/state'

// import { getInitialTheme } from 'App/utils'
// import { refreshToken } from 'pages/Login/actions'
// import { VERSION } from '../../_version'

const initialAppConfigState = {
  // appVersion: VERSION,
  models: {}
}

export const initialSidebarState = { items: [] }

// const allowedStatuses = [400, 401, 403, 404, 500]

// // If the backend returns an object, the message is mapped in 'UNKNOWN_ERROR'
// // Otherwise, the given message is shown
// const getErrorMessage = ({ message }) => {
//   try {
//     JSON.parse(message)
//     return 'UNKNOWN_ERROR'
//   } catch (e) {
//     return message
//   }
// }

export const synapse = Synapse({
  dev: process.env.NODE_ENV === 'development',
  fetch: {
    // onError: (error) => {
    //   console.error('[FETCH ERROR]', error)
    //   errorsStore.update((errors) => {
    //     const token = localStorage.getItem('token')
    //     const errorCode = error.status ?? error.code
    //     const error401 = errors.find(
    //       (error) => error.status === 401 || error.code === 401
    //     )
    //     if (errorCode === 401) {
    //       // If an error 401 has been already saved, it just returns the errors list
    //       if (!isEmpty(error401)) {
    //         return errors
    //       }
    //       // If there is a token and the request returns 401, then Unauthorized is shown
    //       // Otherwise, the token is probably expired
    //       error.message = isEmpty(token) ? 'Unauthorized' : 'SessionExpired'
    //     }
    //     return [
    //       allowedStatuses.includes(error.status || error.code) && {
    //         ...error,
    //         id: uniqueId('error_'),
    //         message: getErrorMessage(error)
    //       },
    //       ...errors
    //     ].filter(Boolean)
    //   })
    // },
    // onRefreshToken: refreshToken
  }
})

export const userStore = synapse.store({})
export const errorsStore = synapse.store([])
export const instancesStore = synapse.store([])
export const themeStore = synapse.store('dark')
export const documentsStore = synapse.store(initialDocumentState)
export const documentEditStore = synapse.store({}) // TODO: delete
export const modalStore = synapse.store(initialModalState)
export const notificationsStore = synapse.store({})
// export const scenarioStore = synapse.store(initialScenarioState)
export const permissionsStore = synapse.store({})
// export const wizardStore = synapse.store(initialWizardState)
export const filterStore = synapse.store({})
export const translationsStore = synapse.store({})
export const sidebarStore = synapse.store(initialSidebarState)
// export const adminStore = synapse.store(initialAdminState)
export const appConfigStore = synapse.store(initialAppConfigState)
export const linkedDocumentsStore = synapse.store(initialLinkedDocumentsState)
export const appReadyStore = synapse.store(false)

export const globalStore = {
  user: userStore,
  instances: instancesStore,
  theme: themeStore,
  errors: errorsStore,
  documents: documentsStore,
  linkedDocuments: linkedDocumentsStore,
  documentEdit: documentEditStore, // TODO: delete
  modal: modalStore,
  notifications: notificationsStore,
  // scenario: scenarioStore,
  // wizard: wizardStore,
  permissions: permissionsStore,
  filter: filterStore,
  translations: translationsStore,
  sidebarList: sidebarStore,
  // admin: adminStore,
  appConfig: appConfigStore,
  appReady: appReadyStore
}
