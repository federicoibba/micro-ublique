import { useState, useEffect } from 'react'
import { synapse, userStore } from 'state'

export function useStorageSession() {
  const [initialized, setInitialized] = useState(false)

  useEffect(() => {
    const getUser = async () => {
      const user = await synapse.user()
      userStore.set(user)
      setInitialized(true)
    }

    getUser()
  }, [])

  return initialized
}

export default useStorageSession
