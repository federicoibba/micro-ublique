/**
 * Created by fgallucci on 24/06/2020.
 */

import { synapse as s } from './synapse'
import { stores, routes, languages, modals } from 'utils/imports'
// import { isEmpty, uniqueId } from 'lodash'

// import { initialState as initialModalState } from 'components/Modal/state'

// import { getInitialTheme } from 'App/utils'
// import { refreshToken } from 'pages/Login/actions'
// import { VERSION } from '../../_version'

// const initialAppConfigState = {
//   appVersion: VERSION,
//   models: {},
//   scenarioWizardModel: {},
//   outputConfigRoutes: {
//     sidebar: false,
//     children: null
//   }
// }

export const initialSidebarState = { items: [] }

// const allowedStatuses = [400, 401, 403, 404, 500]

// If the backend returns an object, the message is mapped in 'UNKNOWN_ERROR'
// Otherwise, the given message is shown TODO da capire meglio
// const getErrorMessage = ({ message }) => {
//   try {
//     JSON.parse(message)
//     return 'UNKNOWN_ERROR'
//   } catch (e) {
//     return message
//   }
// }

export const synapse = s

export const userStore = synapse.store({})
// export const errorsStore = synapse.store([])
// export const instancesStore = synapse.store([])
export const themeStore = synapse.store('dark')
// export const documentsStore = synapse.store(initialDocumentState)
// export const modalStore = synapse.store(initialModalState)
export const notificationsStore = synapse.store({})
// export const permissionsStore = synapse.store({})
export const filterStore = synapse.store({})
// export const translationsStore = synapse.store({})
export const sidebarStore = synapse.store(initialSidebarState)
// export const adminStore = synapse.store(initialAdminState)
// export const appConfigStore = synapse.store(initialAppConfigState)
// export const linkedDocumentsStore = synapse.store(initialLinkedDocumentsState)
// export const appReadyStore = synapse.store(false)
console.log(stores)
export const globalStore = {
  user: userStore,
  // instances: instancesStore,
  theme: themeStore,
  // errors: errorsStore,
  // documents: documentsStore,
  // linkedDocuments: linkedDocumentsStore,
  // modal: modalStore,
  notifications: notificationsStore,
  // permissions: permissionsStore,
  // filter: filterStore,
  // translations: translationsStore,
  sidebarList: sidebarStore,
  ...stores
  // admin: adminStore,
  // appConfig: appConfigStore,
  // appReady: appReadyStore
}
