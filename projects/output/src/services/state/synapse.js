import Synapse from '@ublique/synapse/client'

// Temporary fix for circular dependency
export const synapse = Synapse({
  dev: process.env.NODE_ENV === 'development'
  // fetch: {
  //   onError: (error) => {
  //     console.error('[FETCH ERROR]', error)

  //     errorsStore.update((errors) => {
  //       const token = localStorage.getItem('token')
  //       const errorCode = error.status ?? error.code
  //       const error401 = errors.find(
  //         (error) => error.status === 401 || error.code === 401
  //       )

  //       if (errorCode === 401) {
  //         // If an error 401 has been already saved, it just returns the errors list
  //         if (!isEmpty(error401)) {
  //           return errors
  //         }

  //         // If there is a token and the request returns 401, then Unauthorized is shown
  //         // Otherwise, the token is probably expired
  //         error.message = isEmpty(token) ? 'Unauthorized' : 'SessionExpired'
  //       }

  //       return [
  //         allowedStatuses.includes(error.status || error.code) && {
  //           ...error,
  //           id: uniqueId('error_'),
  //           message: getErrorMessage(error)
  //         },
  //         ...errors
  //       ].filter(Boolean)
  //     })
  //   }
  //   // onRefreshToken: refreshToken
  // }
})
