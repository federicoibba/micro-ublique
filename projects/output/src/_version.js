/*
 * NOTE: Do not edit this file.
 * It is replaced at build time to avoid exposing the package.json in production.
 */
// import { version } from '../package.json'

export const VERSION = '1.0.0'
