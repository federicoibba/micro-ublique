import { utc, min, max } from 'moment'
import { tourEntity } from 'api/entities'

const { VEHICLE_CLASS, VEHICLE_CODE, ID, TIME_START, TIME_END, NAME } = tourEntity

const TZ_OFFSET = new Date().getTimezoneOffset()

export const getResourceId = (vClass, vCode) => `${vClass}_${vCode}`

// Bryntum Scheduler doesn't use utc internally, but local time
const offsetDate = (date) => {
  return utc(date)[TZ_OFFSET < 0 ? 'add' : 'subtract'](TZ_OFFSET, 'minutes').format()
}

// Extract unique vehicles from tours, to use as Scheduler resources
export const getVehiclesAsResources = (tours) => {
  const processed = {}
  const resources = []

  for (const {
    [VEHICLE_CLASS]: vehicleClass,
    [VEHICLE_CODE]: vehicleCode
  } of tours) {
    if (!processed[vehicleCode]) {
      resources.push({
        id: getResourceId(vehicleClass, vehicleCode),
        name: vehicleCode
      })
      processed[vehicleCode] = true
    }
  }

  return resources
}

// Map tours as Scheduler events
export const getToursAsEvents = (tours) => {
  return tours.map(
    ({
      [ID]: tourId,
      [NAME]: tourName,
      [VEHICLE_CLASS]: vehicleClass,
      [VEHICLE_CODE]: vehicleCode,
      [TIME_START]: startDate,
      [TIME_END]: endDate
    }) => ({
      id: tourId,
      resourceId: getResourceId(vehicleClass, vehicleCode),
      name: tourName,
      startDate: offsetDate(startDate),
      endDate: offsetDate(endDate)
      // cls: 'ublique-scheduler-event'
    })
  )
}

export const getMixAndMaxDates = (tours) => {
  const startDateMoments = tours.map((t) => utc(t[TIME_START]))
  const endDateMoments = tours.map((t) => utc(t[TIME_END]))
  return [offsetDate(min(startDateMoments)), offsetDate(max(endDateMoments))]
}

export const getViewPreset = ([minDate, maxDate]) => {
  const dayDifference = utc(maxDate).diff(minDate, 'days')
  return dayDifference < 2 ? 'hourAndDay' : 'weekAndDay'
}
