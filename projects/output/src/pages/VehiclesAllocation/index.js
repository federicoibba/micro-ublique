import React, { useEffect, useMemo } from 'react'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import Scheduler from '@ublique/components/Scheduler'
import Loading from '@ublique/components/Loading'
import { keyBy } from 'lodash'
import { useTranslation } from 'react-i18next'

import { tourEntity } from 'api/entities'
import { openSlidePanel } from '../CommonFeatures/actions'
import ToursManagementLayout from '../CommonFeatures/components/ToursManagementLayout'
import useFilters from '../CommonFeatures/components/Filters/useFilters'
import TourDetailPanel from '../GanttAndMap/components/TourDetailPanel'
import Tooltip from '../GanttAndMap/components/Tooltip'
import { getToursList } from '../ToursManagement/actions/table'
import useScenarioSubscriptions from '../useScenarioSubscriptions'
import { featureTypes } from '../GanttAndMap/Map/constants'
import {
  getVehiclesAsResources,
  getToursAsEvents,
  getMixAndMaxDates,
  getViewPreset,
  getResourceId
} from './helpers'

import './index.scss'

const { VEHICLE_CLASS, VEHICLE_CODE, ID } = tourEntity

const VehiclesAllocation = ({ basename }) => {
  useScenarioSubscriptions({ basename })

  const { t, i18n } = useTranslation('OutputView')

  const { tours, stops, orders } = useStoreSelector('toursManagement')
  const {
    selectedPlanId,
    slidePanel: { opened, data }
  } = useStoreSelector('outputViewCommon')

  const schedulerRef = React.useRef()
  const planTours = tours[selectedPlanId]
  const filteredTours = useFilters(planTours, 'tour')

  const toursById = useMemo(() => keyBy(planTours, tourEntity.ID), [planTours])

  const columns = useMemo(
    () => [
      {
        text: t('VehiclesAllocation.SchedulerColumns.Vehicle'),
        field: 'name',
        flex: 1
      }
    ],
    [t]
  )

  const { startDate, endDate, viewPreset, resources, events } = useMemo(() => {
    if (!planTours) return {}
    const [startDate, endDate] = getMixAndMaxDates(planTours)
    return {
      startDate,
      endDate,
      viewPreset: getViewPreset([startDate, endDate]),
      resources: getVehiclesAsResources(planTours),
      events: getToursAsEvents(planTours)
    }
  }, [planTours])

  useEffect(() => {
    if (selectedPlanId && !planTours) {
      getToursList(selectedPlanId)
    }
  }, [selectedPlanId])

  // Use the scheduler internal filtering system, otherwise it is bugged
  useEffect(() => {
    if (!planTours) return

    const { eventStore, resourceStore } = schedulerRef.current.schedulerProInstance

    if (filteredTours.length === planTours.length) {
      resourceStore.clearFilters()
      eventStore.clearFilters()
      return
    }

    const filteredToursMap = filteredTours.reduce(
      (acc, tour) => ({ ...acc, [tour[ID]]: true }),
      {}
    )
    const filteredResourcesMap = filteredTours.reduce(
      (acc, tour) => ({
        ...acc,
        [getResourceId(tour[VEHICLE_CLASS], tour[VEHICLE_CODE])]: true
      }),
      {}
    )

    resourceStore.filter({
      filters: ({ data }) => filteredResourcesMap[data.id],
      replace: true
    })
    eventStore.filter({
      filters: ({ data }) => filteredToursMap[data.id],
      replace: true
    })
  }, [filteredTours])

  const onEventClick = ({ eventRecord }) => {
    openSlidePanel('tourDetail', { tourId: eventRecord.originalData.id })
  }

  if (!events) return <Loading />

  return (
    <ToursManagementLayout
      t={t}
      tours={planTours}
      filteredTours={filteredTours}
      contentTitle={t('VehiclesAllocation.HeaderTitle')}
      headerActions={false}
    >
      <div className="vehicles-allocation-view">
        <Scheduler
          ref={schedulerRef}
          readOnly
          maxZoomLevel={18}
          minZoomLevel={14}
          // zoomOnMouseWheel={false}
          columns={columns}
          viewPreset={viewPreset}
          startDate={startDate}
          endDate={endDate}
          resources={resources}
          events={events}
          locale={i18n.language}
          nonWorkingTimeFeature
          scheduleTooltipFeature={false}
          onEventClick={onEventClick}
          eventTooltipRenderer={({ eventRecord }) => (
            <Tooltip
              data={{
                tourId: eventRecord.originalData.id,
                type: featureTypes.TOUR
              }}
              toursById={toursById}
            />
          )}
        />
      </div>
      <TourDetailPanel
        t={t}
        editMode={false}
        opened={opened === 'tourDetail'}
        data={data}
        toursById={toursById}
        stops={stops}
        orders={orders}
      />
    </ToursManagementLayout>
  )
}

export default VehiclesAllocation
