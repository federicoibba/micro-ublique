import React from 'react'
import SimpleDataTable from '@ublique/components/SimpleDataTable'
import { orderEntity, stopEntity } from 'api/entities'
import { formatValue } from '../TourDetailPanel/helpers'
import './index.scss'

const dateKeys = [
  stopEntity.ARRIVAL_TIME,
  stopEntity.DEPARTURE_TIME,
  orderEntity.TIME_END
]

const TourTable = ({ headers = [], data = [] }) => {
  return (
    <SimpleDataTable
      isSortable={false}
      rows={data}
      headers={headers}
      render={({ rows, headers, getHeaderProps }) => (
        <SimpleDataTable.TableContainer>
          <SimpleDataTable.Table className="tour-sequence-table">
            <SimpleDataTable.TableHead>
              <SimpleDataTable.TableRow>
                {headers.map((header, index) => (
                  <SimpleDataTable.TableHeader
                    key={index}
                    {...getHeaderProps({ header })}
                    isSortHeader={false}
                  >
                    {header.header}
                  </SimpleDataTable.TableHeader>
                ))}
              </SimpleDataTable.TableRow>
            </SimpleDataTable.TableHead>

            <SimpleDataTable.TableBody>
              {rows.map(({ id, cells }) => (
                <SimpleDataTable.TableRow key={id}>
                  {cells.map(({ id, value, info }) => {
                    const isDate = dateKeys.includes(info.header)
                    return (
                      <SimpleDataTable.TableCell
                        key={id}
                        className="tour-sequence-table__cell"
                      >
                        <div className="align-text">
                          {isDate ? formatValue('time', value) : value}
                        </div>
                      </SimpleDataTable.TableCell>
                    )
                  })}
                </SimpleDataTable.TableRow>
              ))}
            </SimpleDataTable.TableBody>
          </SimpleDataTable.Table>
        </SimpleDataTable.TableContainer>
      )}
    />
  )
}

export default React.memo(TourTable)
