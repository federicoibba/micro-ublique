import { compact, isNil, toPairs } from 'lodash'
import { utc } from 'moment'
import { orderEntity, posEntity, stopEntity } from 'api/entities'
import { statsItems } from './config'

const DATE_FORMAT = 'DD/MM/YYYY [@] HH:mm'

const joinExisting = (array, char) => compact(array).join(char)

const stopPosKeyMapping = {
  pos: stopEntity.POS,
  description: posEntity.DESCRIPTION,
  streetName: posEntity.STREET_NAME,
  streetNumber: posEntity.STREET_NUMBER,
  city: posEntity.CITY
}

export const getStopLocation = (
  stop,
  { separator = ', ', keyMapping = stopPosKeyMapping } = {}
) => {
  const { pos, description, streetName, streetNumber, city } = keyMapping
  return joinExisting(
    [
      stop[pos],
      stop[description],
      joinExisting([stop[streetName], stop[streetNumber]], ' '),
      stop[city]
    ],
    separator
  )
}

const formattersMap = {
  time: (date) => utc(date).format(DATE_FORMAT),
  timeRange: ([startDate, endDate]) =>
    `${utc(startDate).format(DATE_FORMAT)} - ${utc(endDate).format(DATE_FORMAT)}`,
  distance: (value) => `${value} km`,
  percentage: (value) => `${value * 100}%`
}

export const formatValue = (type, value) => {
  if (isNil(value)) return '\u2014'
  const formatter = formattersMap[type]
  return formatter ? formatter(value) : value
}

export const getKPIsItems = (t, tourData) => {
  return statsItems.map(({ key, i18nKey, type }) => ({
    id: key,
    title: t(`GanttAndMapView.TourDetailPanel.KPIs.${i18nKey}`),
    value: formatValue(type, tourData[key])
  }))
}

export const createTourSequenceData = (t, stops = []) => {
  return stops.map((stop) => ({
    id: stop[stopEntity.ID],
    title: t('GanttAndMapView.TourDetailPanel.StopBoxTitle', {
      stopNumber: stop[stopEntity.STOP_NUMBER]
    }),
    rowInfo: [
      {
        id: 'node',
        title: t('GanttAndMapView.StopsTableColumnHeaders.Location'),
        value: getStopLocation(stop)
      },
      {
        id: 'distance',
        title: t('GanttAndMapView.StopsTableColumnHeaders.TotDistance'),
        value: stop[stopEntity.DISTANCE]
      }
    ],
    data: [{ id: 'stopDataRow', ...stop }]
  }))
}

export const createOrdersSequenceData = (t, orders = {}) => {
  return toPairs(orders).reduce(
    (acc, [stopId, orders]) => ({
      ...acc,
      [stopId]: orders.map((order, i) => ({
        id: order[orderEntity.ID],
        title: t('GanttAndMapView.TourDetailPanel.OrderBoxTitle', {
          orderNumber: i + 1
        }),
        rowInfo: [
          {
            id: 'code',
            title: t('GanttAndMapView.OrdersTableColumnHeaders.OrderNumber'),
            value: order[orderEntity.NUMBER]
          },
          {
            id: 'timeWindow',
            title: t('GanttAndMapView.OrdersTableColumnHeaders.DeliveryTimeRange'),
            value: [order[orderEntity.TW_START], order[orderEntity.TM_END]],
            type: 'timeRange'
          }
        ],
        data: [{ id: 'orderDataRow', ...order }]
      }))
    }),
    {}
  )
}
