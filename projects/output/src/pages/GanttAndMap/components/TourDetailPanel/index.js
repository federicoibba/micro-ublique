import React, { useEffect, useMemo, useState, useCallback } from 'react'
import { ModalActions } from '@ublique/platform'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import Button from '@ublique/components/Button'
import { get, isEmpty } from 'lodash'
import { Add20 } from '@carbon/icons-react'
import StatsPanel from 'components/StatsPanel'
import SlidePanel from 'components/SlidePanel'
import { scenarioEntity, tourEntity } from 'api/entities'
import {
  getStopOrdersList,
  getTourStopsList
} from 'pages/ToursManagement/actions/table'
import { LoaderSmall } from 'components/Loading'
import { useEntityList } from 'pages/CommonFeatures/hooks/useEntityList'
import { usePushToOutputRoute } from 'pages/CommonFeatures/hooks/usePushToOutputRoute'
import { closeSlidePanel } from '../../../CommonFeatures/actions'
import TourDetailSection from '../TourDetailSection'
import TourResources from '../TourResources'
import TourSequence from '../TourSequence'
import { getColumnsConfig, getActionsConfig } from './config'
import {
  getKPIsItems,
  createTourSequenceData,
  createOrdersSequenceData
} from './helpers'
import './index.scss'

const { openModal } = ModalActions

const entities = ['distributionCenters']

const panelStyle = { width: '80%', maxWidth: 1360 }

const selectScenarioName = (state) => state.scenario[scenarioEntity.NAME]

const TourDetailPanel = ({
  opened,
  t,
  stops,
  orders,
  editMode,
  toursById = {},
  data = {}
}) => {
  const [loading, setLoading] = useState(false)

  const { tourId } = data
  const activeTour = toursById[tourId]
  const tourName = get(activeTour, tourEntity.NAME)
  const tourStops = stops[tourId]
  const scenarioName = useStoreSelector('outputViewCommon', selectScenarioName)
  const pushToOutputRoute = usePushToOutputRoute()

  useEffect(() => {
    // Autoclose panel when a tour gets deleted
    if (!activeTour) closeSlidePanel()
  }, [activeTour])

  useEffect(() => {
    if (opened && activeTour && isEmpty(tourStops)) {
      setLoading(true)
      getTourStopsList(tourId).then(() => setLoading(false))
    }
  }, [tourStops, tourId, activeTour, opened])

  const { distributionCenters } = useEntityList({
    scenarioName,
    entities,
    returnData: true
  })

  const onAccordionClick = useCallback(({ stopId, hasOrders }) => {
    if (!hasOrders) {
      getStopOrdersList(stopId)
    }
  }, [])

  const { stopColumnDefs, orderColumnDefs } = useMemo(() => getColumnsConfig(t), [t])

  const { stopActions, orderActions } = useMemo(
    () => getActionsConfig(t, distributionCenters, pushToOutputRoute),
    [t, distributionCenters, pushToOutputRoute]
  )

  const slidePanelProps = {
    opened,
    onClose: closeSlidePanel,
    className: 'tour-detail-panel',
    style: panelStyle,
    title: t('GanttAndMapView.TourDetailPanel.Title', { tourName }),
    closeText: t('Common:Close')
  }

  if (!activeTour) {
    return (
      <SlidePanel {...slidePanelProps}>
        <div />
      </SlidePanel>
    )
  }

  return (
    <SlidePanel {...slidePanelProps}>
      <div className="left-column">
        <StatsPanel
          title={t('GanttAndMapView.TourDetailPanel.KPIsSectionTitle')}
          items={getKPIsItems(t, activeTour)}
        />
      </div>

      <div className="right-column">
        <TourDetailSection
          title={t('GanttAndMapView.TourDetailPanel.ResourcesTitle')}
        >
          <TourResources editMode={editMode} t={t} tour={activeTour} />
        </TourDetailSection>

        <TourDetailSection
          title={t('GanttAndMapView.TourDetailPanel.TourSequenceTitle')}
          actionComponent={
            editMode ? (
              <Button kind="ghost" onClick={() => openModal('addStop', { tourId })}>
                <Add20 />
                {t('ToursManagement.Modals.AddStopTitle')}
              </Button>
            ) : undefined
          }
        >
          {loading ? (
            <LoaderSmall />
          ) : (
            <TourSequence
              t={t}
              tourId={tourId}
              editMode={editMode}
              stops={createTourSequenceData(t, tourStops)}
              orders={createOrdersSequenceData(t, orders)}
              defaultStopActions={stopActions}
              defaultOrderActions={orderActions}
              stopColumnDefs={stopColumnDefs}
              orderColumnDefs={orderColumnDefs}
              onAccordionClick={onAccordionClick}
            />
          )}
        </TourDetailSection>
      </div>
    </SlidePanel>
  )
}

export default TourDetailPanel
