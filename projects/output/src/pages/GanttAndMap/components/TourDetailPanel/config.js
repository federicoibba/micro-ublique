import { ModalActions } from '@ublique/platform'
import { enableUndo } from 'pages/CommonFeatures/actions'
import { makeGroundedOrderNotification } from 'pages/CommonFeatures/notificationCreators'
import { putOrdersToGround } from 'pages/ToursManagement/actions/table'
import { orderEntity, stopEntity, tourEntity } from 'api/entities'

const { openModal } = ModalActions

const getDefaultStopActions = (t) => [
  {
    title: t('GanttAndMapView.Actions.Delete'),
    onClick: ({ id, parentId }) =>
      openModal('deleteEntity', { type: 'stop', id, tourId: parentId })
  }
]

const getDefaultOrderActions = (t, distributionCenters, pushToOutputRoute) => [
  {
    title: t('GanttAndMapView.Actions.EditUld'),
    onClick: ({ id, parentId, tourId, record }) => {
      openModal('editOrderData', {
        type: 'order',
        id,
        stopId: parentId,
        tourId,
        record
      })
    }
  },
  {
    title: t('GanttAndMapView.Actions.PutOrderToGround'),
    onClick: async ({ record, isLast }) => {
      const notification = makeGroundedOrderNotification(
        t,
        record[orderEntity.CODE],
        pushToOutputRoute
      )
      await putOrdersToGround(record, distributionCenters, isLast, notification)
      enableUndo()
    }
  }
]

const getStopColumnDefs = (t) => {
  const tt = (x) => t(`GanttAndMapView.StopsTableColumnHeaders.${x}`)
  return [
    {
      key: stopEntity.QUANTITY,
      header: tt('Quantity')
    },
    {
      key: stopEntity.NUM_ORDERS,
      header: tt('NumberOfOrders')
    },
    {
      key: stopEntity.COST,
      header: tt('TotCost')
    },
    {
      key: stopEntity.TIME,
      header: tt('TotTime')
    },
    {
      key: stopEntity.ARRIVAL_TIME,
      header: tt('ArrivalTime')
    },
    {
      key: stopEntity.DEPARTURE_TIME,
      header: tt('DepartureTime')
    },
    {
      key: stopEntity.SERVICE_TIME,
      header: tt('TotServiceTime')
    },
    {
      key: stopEntity.WAITING_TIME,
      header: tt('TotWaitingTime')
    }
  ]
}

const getOrderColumnDefs = (t) => {
  const tt = (x) => t(`GanttAndMapView.OrdersTableColumnHeaders.${x}`)
  return [
    {
      key: orderEntity.POINT_OF_SERVICE,
      header: tt('PointOfService')
    },
    {
      key: orderEntity.LOAD_UNIT,
      header: tt('UnitLoadType')
    },
    {
      key: orderEntity.QUANTITY,
      header: tt('Quantity')
    },
    {
      key: orderEntity.PACKAGES,
      header: tt('TotalPackages')
    },
    {
      key: orderEntity.PRODUCT_TYPE,
      header: tt('Product')
    },
    {
      key: orderEntity.SERVICE_TYPE,
      header: tt('ServiceType')
    },
    {
      key: orderEntity.DISTRIBUTION_CENTER,
      header: tt('DistributionCenter')
    }
  ]
}

export const statsItems = [
  { key: tourEntity.SATURATION, i18nKey: 'Saturation', type: 'percentage' },
  { key: tourEntity.TOTAL_DISTANCE, i18nKey: 'TotDistance' },
  { key: tourEntity.TOTAL_COST, i18nKey: 'TotCost' },
  { key: tourEntity.NUM_STOPS, i18nKey: 'NumberOfStops' },
  { key: tourEntity.NUM_ORDERS, i18nKey: 'NumberOfOrders' },
  { key: tourEntity.TOTAL_TIME, i18nKey: 'TotTime' }
]

export const getColumnsConfig = (t) => ({
  stopColumnDefs: getStopColumnDefs(t),
  orderColumnDefs: getOrderColumnDefs(t)
})

export const getActionsConfig = (t, distributionCenters, pushToOutputRoute) => ({
  stopActions: getDefaultStopActions(t),
  orderActions: getDefaultOrderActions(t, distributionCenters, pushToOutputRoute)
})
