import React from 'react'
import { toPairs } from 'lodash'
import './style.scss'

const CheckTourResults = ({ data, t }) => {
  return (
    <div className="gantt-map-checktour">
      <p className="title">{t('Filters.Violations')}:</p>
      {toPairs(data).map(([key, { label }]) => (
        <p key={key}>
          {key !== '*' && <span>{key}: </span>}
          <span>{label}</span>
        </p>
      ))}
    </div>
  )
}

export default CheckTourResults
