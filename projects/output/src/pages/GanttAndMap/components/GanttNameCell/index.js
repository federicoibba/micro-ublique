import React from 'react'
import cn from 'classnames'
import { get } from 'lodash'
import { WarningAlt20 } from '@carbon/icons-react'
import { stopEntity, tourEntity } from 'api/entities'
import './index.scss'

const violationsInfoMap = {
  tour: tourEntity.HAS_VIOLATIONS,
  stop: stopEntity.HAS_VIOLATIONS
}

const GanttNameCell = ({ value, record }) => {
  const {
    type,
    originalData: { data: tourData }
  } = record
  const hasViolations = get(tourData, violationsInfoMap[type])

  return (
    <div className={cn('gantt-custom-cell', { 'invalid-cell': hasViolations })}>
      {hasViolations && <WarningAlt20 className="warning-icon" />}
      {value}
    </div>
  )
}

export default GanttNameCell
