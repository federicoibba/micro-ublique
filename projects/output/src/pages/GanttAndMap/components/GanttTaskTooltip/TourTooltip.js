import React from 'react'
import { tourEntity } from 'api/entities'
import TimeIndicator from './TimeIndicator'
import CheckTourResults from '../CheckTourResults'

const TourTooltip = ({ t, startDate, endDate, taskRecord }) => {
  const tourData = taskRecord.originalData.data
  const hasViolations = tourData[tourEntity.HAS_VIOLATIONS]
  const checkTourResults = tourData[tourEntity.CHECK_TOUR_RESULTS]

  return (
    <div className="gantt-task-tooltip">
      <h5>
        {t('GanttAndMapView.GanttTooltip.TourTitle', {
          tourName: tourData[tourEntity.NAME]
        })}
      </h5>

      <TimeIndicator
        label={t('GanttAndMapView.GanttTooltip.TourStartTime')}
        time={startDate}
      />
      <TimeIndicator
        label={t('GanttAndMapView.GanttTooltip.TourEndTime')}
        time={endDate}
      />

      {hasViolations && <CheckTourResults data={checkTourResults} t={t} />}
    </div>
  )
}

export default TourTooltip
