import React from 'react'
import { useTranslation } from 'react-i18next'
import TourTooltip from './TourTooltip'
import StopTooltip from './StopTooltip'
import './style.scss'

const tooltipMap = {
  tour: TourTooltip,
  stop: StopTooltip,
  stopFinal: StopTooltip
}

const GanttTaskTooltip = (props) => {
  const { t } = useTranslation('OutputView')

  const Tooltip = tooltipMap[props.taskRecord.type]

  return <Tooltip {...props} t={t} />
}

export default GanttTaskTooltip
