import React from 'react'
import moment from 'moment'

const formatTime = (time) => moment(time).format('HH:mm')

const TimeIndicator = ({ label, time }) => (
  <p className="tooltip-time-indicator">
    <span className="label">{label}: </span>
    <span className="value">{formatTime(time)}</span>
  </p>
)

export default TimeIndicator
