import React from 'react'
import { stopEntity, posEntity } from 'api/entities'
import CheckTourResults from '../CheckTourResults'
import TimeIndicator from './TimeIndicator'

const StopTooltip = ({ t, startDate, endDate, taskRecord }) => {
  const stopData = taskRecord.originalData.data
  const isFinal = taskRecord.type === 'stopFinal'
  const hasViolations = stopData[stopData.HAS_VIOLATIONS]
  const checkTourResults = stopData[stopData.CHECK_TOUR_RESULTS]

  return (
    <div className="gantt-task-tooltip">
      <h5>
        {t('GanttAndMapView.GanttTooltip.StopTitle', {
          stopNumber: stopData[stopEntity.STOP_NUMBER],
          stopDescription: stopData[posEntity.DESCRIPTION]
        })}
      </h5>

      <TimeIndicator
        label={t('GanttAndMapView.GanttTooltip.StopArrivalTime')}
        time={startDate}
      />
      {!isFinal && (
        <TimeIndicator
          label={t('GanttAndMapView.GanttTooltip.StopDepartureTime')}
          time={endDate}
        />
      )}

      {hasViolations && <CheckTourResults data={checkTourResults} t={t} />}
    </div>
  )
}

export default StopTooltip
