import React from 'react'
import SequenceItem from './SequenceItem'
import './index.scss'

const TourSequence = ({
  t,
  editMode,
  onAccordionClick,
  tourId,
  stops = [],
  orders = {},
  defaultStopActions = [],
  stopColumnDefs = [],
  defaultOrderActions = [],
  orderColumnDefs = []
}) => {
  const columnDefs = {
    stop: stopColumnDefs,
    order: orderColumnDefs
  }
  const defaultActions = editMode
    ? {
        stop: defaultStopActions,
        order: defaultOrderActions
      }
    : undefined

  return stops.map(({ id, title, selectable, data, rowInfo }) => (
    <SequenceItem
      type="stop"
      t={t}
      key={id}
      id={id}
      parentId={tourId}
      tourId={tourId}
      title={title}
      selectable={selectable}
      rowInfo={rowInfo}
      data={data}
      orders={orders}
      columnDefs={columnDefs}
      defaultActions={defaultActions}
      onAccordionClick={onAccordionClick}
      onlyItemRemaining={stops.length === 1}
    />
  ))
}

export default React.memo(TourSequence)
