import React from 'react'
import cn from 'classnames'
import { first, get } from 'lodash'
import { Accordion, AccordionItem } from '@ublique/components/Accordion'
import Button from '@ublique/components/Button'
import { stopEntity } from 'api/entities'
import { formatValue } from '../TourDetailPanel/helpers'
import TourTable from '../TourTable'

const HeaderAction = ({ title, onClick }) => (
  <Button className="header-action-button" kind="ghost" onClick={onClick}>
    {title}
  </Button>
)

const SequenceItem = ({
  t,
  id,
  parentId,
  selectable,
  title,
  defaultActions,
  columnDefs,
  data,
  rowInfo,
  orders,
  type,
  className,
  tourId,
  onAccordionClick,
  onlyItemRemaining
}) => {
  const renderAccordion = () => {
    const stopOrders = orders[id]

    const onAccordionClickHandler = () => {
      if (onAccordionClick) {
        onAccordionClick({ stopId: id, hasOrders: !!stopOrders })
      }
    }

    return (
      <Accordion className="orders-accordion">
        <AccordionItem
          title={t('GanttAndMapView.TourDetailPanel.OrdersAccordionTitle')}
          open={false}
          onClick={onAccordionClickHandler}
        >
          {stopOrders &&
            (stopOrders.length > 0
              ? stopOrders.map((orderProps) => (
                  <SequenceItem
                    className="order-accordion-item"
                    key={orderProps.id}
                    {...orderProps}
                    type="order"
                    parentId={id}
                    tourId={parentId}
                    defaultActions={defaultActions}
                    columnDefs={columnDefs}
                    onlyItemRemaining={stopOrders.length === 1}
                  />
                ))
              : t('GanttAndMapView.TourDetailPanel.NoOrdersFound'))}
        </AccordionItem>
      </Accordion>
    )
  }

  return (
    <div className={cn('tour-sequence', className)}>
      <div className="tour-sequence__header">
        <div className="header-title">
          {selectable && (
            <input
              id={id}
              className="custom-checkbox"
              type="checkbox"
              onChange={() => console.log(id)}
            />
          )}
          <h5>{title}</h5>
        </div>

        {defaultActions && (
          <div className="header-actions">
            {defaultActions[type].map(({ title, onClick }, i) => (
              <HeaderAction
                key={`${title}-${i}`}
                title={title}
                onClick={() =>
                  onClick &&
                  onClick({
                    id,
                    parentId,
                    tourId,
                    record: first(data),
                    isLast: onlyItemRemaining
                  })
                }
              />
            ))}
          </div>
        )}
      </div>

      <div className="tour-sequence__body">
        <div className="info-row">
          {rowInfo.map(({ id, title, type, value }) => (
            <div className="info-col" key={id}>
              <h5 className="info-title">{title}</h5>
              <p className="info-subtitle">{formatValue(type, value)}</p>
            </div>
          ))}
        </div>

        <TourTable headers={columnDefs[type]} data={data} />

        {get(data, `0.${stopEntity.NUM_ORDERS}`) > 0 && renderAccordion()}
      </div>
    </div>
  )
}

export default SequenceItem
