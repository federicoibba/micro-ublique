import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
// import { NotificationActions } from '@ublique/platform'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { tourEntity } from 'api/entities'

import DetectUndoModal from '../../../CommonFeatures/components/DetectUndoModal'
import { makeWaitForCheckTourNotification } from '../../../CommonFeatures/notificationCreators'
import { fetchToursResetStopsAndOrders } from '../../../ToursManagement/actions/table'
import { featureTypes } from '../../Map/constants'
import { getScenarioItineraries } from '../../actions'
import OrdersTable from './OrdersTable'
import StopHeader from './StopHeader'
import { ops, config, getDataFromPos, getDataFromStop } from './helpers'

import './index.scss'

const OrdersSelectionModal = ({ open, close, data }) => {
  const { t } = useTranslation(['OutputView', 'Common'])

  const { destinationTour, draggedFeature } = data
  const { type } = draggedFeature
  const operation = getOperationType(type, destinationTour)
  const { titleKey, handler } = config[operation]

  const { orders, stops, pos } = useStoreSelector('toursManagement')
  const { scenario, selectedPlanId } = useStoreSelector('outputViewCommon')

  const [selectedOrders, setSelectedOrders] = useState([])
  const [loading, setLoading] = useState(false)

  const getData = type === featureTypes.STOP ? getDataFromStop : getDataFromPos

  const { currentPos, serviceOrders, keyMapping, getOrders, getPos } = getData({
    ...draggedFeature,
    planId: selectedPlanId,
    stops,
    orders,
    pos
  })

  useEffect(() => {
    if (!serviceOrders) getOrders()
  }, [serviceOrders])

  useEffect(() => {
    if (!currentPos) getPos()
  }, [currentPos])

  useEffect(() => {
    // Automatically select all orders
    if (serviceOrders) {
      setSelectedOrders(serviceOrders)
    }
  }, [!!serviceOrders])

  const onSubmit = async () => {
    setLoading(true)
    const tourId = destinationTour?.[tourEntity.ID]
    await handler({ orders: selectedOrders, stop: currentPos, tourId })
    close()
    // NotificationActions.addNotification(makeWaitForCheckTourNotification(t))
    getScenarioItineraries(scenario.scenarioCode)
    // Update both source and destination tour, when present
    fetchToursResetStopsAndOrders([tourId, draggedFeature.tourId])
  }

  return (
    <DetectUndoModal
      className="orders-selection-modal"
      open={open}
      onRequestClose={close}
      secondaryButtonText={t('Common:Cancel')}
      primaryButtonText={t('Common:Confirm')}
      onRequestSubmit={onSubmit}
      modalHeading={t(`GanttAndMapView.Modals.${titleKey}`, {
        tourName: destinationTour?.[tourEntity.NAME]
      })}
      primaryButtonDisabled={selectedOrders.length === 0}
      confirmLoading={loading}
      confirmLoadingMessage={t('Common:Submitting')}
    >
      <StopHeader t={t} data={currentPos} keyMapping={keyMapping} />
      <OrdersTable
        t={t}
        header={t('GanttAndMapView.Modals.OrdersList')}
        selectionEnabled={operation !== ops.MOVE_STOP}
        onSelectionChanged={setSelectedOrders}
        data={serviceOrders}
      />
    </DetectUndoModal>
  )
}

// Helpers

const getOperationType = (type, destinationTour) => {
  if (type === featureTypes.STOP) {
    return destinationTour
      ? ops.MOVE_STOP // Move stop to destination tour
      : ops.GROUND_ORDERS // Put entire stop and its orders to ground
  }
  if (type === featureTypes.GROUNDED_ORDER && destinationTour) {
    return ops.MOVE_ORDERS // Move grounded orders to destination tour
  }
  return ops.UNKNOWN
}

export default OrdersSelectionModal
