import React from 'react'
import { getStopLocation } from '../TourDetailPanel/helpers'

// Show info about the stop
const StopHeader = ({ data, t, keyMapping }) => {
  if (!data) return null

  return (
    <div className="stop-header">
      <h5>{t('GanttAndMapView.StopsTableColumnHeaders.Location')}</h5>
      <p>
        {getStopLocation(data, {
          separator: ' | ',
          keyMapping
        })}
      </p>
    </div>
  )
}

export default StopHeader
