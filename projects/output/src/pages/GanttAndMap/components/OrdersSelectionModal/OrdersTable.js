import React from 'react'
import DataTable from '@ublique/components/DataTable'
import { compact } from 'lodash'
import { orderEntity } from 'api/entities'

const defaultColumnDef = {
  suppressMovable: true,
  sortable: true,
  suppressMenu: true
}

const getColumnDefs = (t, selectionEnabled) =>
  compact([
    selectionEnabled && {
      colId: 'checkbox',
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 60,
      maxWidth: 60,
      pinned: 'left'
    },
    {
      headerName: t('OrderCode'),
      field: orderEntity.CODE
    },
    {
      headerName: t('OrderNumber'),
      field: orderEntity.NUMBER
    },
    {
      headerName: t('Description'),
      field: orderEntity.DESCRIPTION
    },
    {
      headerName: t('City'),
      field: orderEntity.CITY
    }
  ])

const OrdersTable = ({
  t,
  header,
  data,
  onSelectionChanged,
  selectionEnabled = true
}) => {
  const ordersT = (x) => t(`GroundedOrders.TableColumnHeaders.${x}`)

  const onFirstDataRendered = ({ api }) => {
    if (selectionEnabled) {
      api.selectAll()
    }
  }

  return (
    <div className="order-selection-table">
      {header && <h5>{header}</h5>}
      <DataTable
        onFirstDataRendered={onFirstDataRendered}
        columnDefs={getColumnDefs(ordersT, selectionEnabled)}
        rowData={data}
        onRowsSelect={selectionEnabled ? onSelectionChanged : undefined}
        rowSelection="multiple"
        defaultColDef={defaultColumnDef}
        autoSizeColumnsOnFirstRender
        canSearch={false}
        suppressRowClickSelection
        suppressColumnVirtualisation
      />
    </div>
  )
}

export default OrdersTable
