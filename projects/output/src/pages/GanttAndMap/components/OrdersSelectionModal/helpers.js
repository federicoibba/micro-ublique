import { noop } from 'lodash'
import { stopEntity } from 'api/entities'
import {
  getPosData,
  getPosGroundedSoList,
  getStopOrdersList,
  getTourStopsList
} from '../../../ToursManagement/actions/table'
import {
  moveOrdersToTourFromMap,
  moveStopToTourFromMap,
  putOrdersToGroundFromMap
} from '../../actions'

export const ops = {
  UNKNOWN: 0,
  GROUND_ORDERS: 1,
  MOVE_STOP: 2,
  MOVE_ORDERS: 3
}

export const config = {
  [ops.GROUND_ORDERS]: {
    titleKey: 'PutOrdersToGround',
    handler: putOrdersToGroundFromMap
  },
  [ops.MOVE_STOP]: {
    titleKey: 'MoveStopToTourX',
    handler: moveStopToTourFromMap
  },
  [ops.MOVE_ORDERS]: {
    titleKey: 'MoveOrdersToTourX',
    handler: moveOrdersToTourFromMap
  },
  [ops.UNKNOWN]: {
    handler: noop
  }
}

export const getDataFromStop = (params) => {
  const { stops, orders, tourId, stopId } = params
  const tourStops = stops[tourId]
  return {
    currentPos: tourStops?.find((stop) => stop[stopEntity.ID] === stopId),
    serviceOrders: orders[stopId],
    getOrders: () => getStopOrdersList(stopId),
    getPos: () => getTourStopsList(tourId)
  }
}

export const getDataFromPos = (params) => {
  const { orders, pos, posName, planId } = params
  return {
    currentPos: pos[posName],
    serviceOrders: orders[posName],
    getOrders: () => getPosGroundedSoList(planId, posName),
    getPos: () => getPosData(planId, posName),
    keyMapping: {
      pos: 'soPosName',
      description: 'soPosDescription',
      streetName: 'soPosStreetName',
      streetNumber: 'soPosStreetNumber',
      city: 'soPosCity'
    }
  }
}
