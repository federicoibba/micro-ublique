import React, { useState, useMemo } from 'react'
import { kebabCase, map, reduce } from 'lodash'
import { Dropdown } from '@ublique/components/Dropdown'
import Button from '@ublique/components/Button'
import Grid from '@ublique/components/Grid'
import { InlineLoading } from '@ublique/components/Loading'
import { useEntityList } from 'pages/CommonFeatures/hooks/useEntityList'
import { tourEntity } from 'api/entities'
import { updateTour } from 'api/ToursManagement'
import { getCleanedRecord } from 'pages/CommonFeatures/helpers'
import { getToursList } from 'pages/ToursManagement/actions/table'
import { enableUndo } from 'pages/CommonFeatures/actions'
import './index.scss'

const entities = ['drivers', 'vehicles']

const Field = ({ title, value }) => (
  <div className="resources-field">
    <span className="resources-field__title">{title}</span>
    <span className="resources-field__value">{value}</span>
  </div>
)

const DropdownItem = ({ editMode, title, label, value, onChange, items = [] }) =>
  editMode ? (
    <Dropdown
      id={kebabCase(title)}
      items={items}
      titleText={title}
      label={label}
      initialSelectedItem={value}
      onChange={onChange}
    />
  ) : (
    <Field value={value} title={title} />
  )

const TourResources = ({ editMode, t, tour }) => {
  const [vehicleCode, setVehicleCode] = useState(tour[tourEntity.VEHICLE_CODE])
  const [driverName, setDriverName] = useState(tour[tourEntity.DRIVER_NAME])
  const [loading, setLoading] = useState(false)

  const { drivers, vehicles } = useEntityList({ entities, returnData: true })

  const vehicleCapacityMap = useMemo(
    () =>
      reduce(
        vehicles,
        (acc, { vehicleCode, vcMaxCapacityUlEquivalent }) => ({
          ...acc,
          [vehicleCode]: vcMaxCapacityUlEquivalent
        }),
        {}
      ),
    [vehicles]
  )

  const onSave = async () => {
    const updatedRecord = getCleanedRecord(tour)
    if (vehicleCode) updatedRecord[tourEntity.VEHICLE_CODE] = vehicleCode
    if (driverName) updatedRecord[tourEntity.DRIVER_NAME] = driverName
    setLoading(true)
    await updateTour(tour[tourEntity.ID], updatedRecord)
    await getToursList(tour[tourEntity.PLAN_ID])
    setLoading(false)
    enableUndo()
  }

  return (
    <Grid>
      <Grid.Row>
        <Grid.Column sm={4} md={4} lg={4}>
          <DropdownItem
            editMode={editMode}
            items={drivers}
            title={t('GanttAndMapView.TourDetailPanel.DriverName')}
            label={t('Common:Select')}
            value={driverName}
            onChange={({ selectedItem }) => setDriverName(selectedItem)}
          />
        </Grid.Column>

        <Grid.Column sm={4} md={4} lg={4}>
          <DropdownItem
            editMode={editMode}
            items={map(vehicles, 'vehicleCode')}
            title={t('GanttAndMapView.TourDetailPanel.VehicleCode')}
            label={t('Common:Select')}
            value={vehicleCode}
            onChange={({ selectedItem }) => setVehicleCode(selectedItem)}
          />
        </Grid.Column>

        <Grid.Column sm={4} md={8} lg={editMode ? 2 : 4}>
          <Field
            title={t('GanttAndMapView.TourDetailPanel.VehicleCapacity')}
            value={vehicleCapacityMap[vehicleCode]}
          />
        </Grid.Column>

        {editMode && (
          <Grid.Column sm={4} md={8} lg={2}>
            <div className="resource-save-button">
              {loading ? (
                <InlineLoading
                  status="active"
                  className="resource-save-button--loading"
                  description={t('Common:Save')}
                />
              ) : (
                <Button
                  disabled={
                    vehicleCode === tour[tourEntity.VEHICLE_CODE] &&
                    driverName === tour[tourEntity.DRIVER_NAME]
                  }
                  onClick={onSave}
                >
                  {t('Common:Save')}
                </Button>
              )}
            </div>
          </Grid.Column>
        )}
      </Grid.Row>
    </Grid>
  )
}

export default React.memo(TourResources)
