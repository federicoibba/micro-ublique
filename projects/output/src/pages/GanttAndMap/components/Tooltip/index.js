import React from 'react'
import { useTranslation } from 'react-i18next'
import { featureTypes } from '../../Map/constants'
import Stop from './Stop'
import Tour from './Tour'
import Pos from './Pos'
import './index.scss'

const { TOUR, STOP, GROUNDED_ORDER } = featureTypes

const tooltipsMap = {
  [TOUR]: Tour,
  [STOP]: Stop,
  [GROUNDED_ORDER]: Pos
}

const Tooltip = ({ data = {}, toursById }) => {
  const { type, lat, lng, ...rest } = data
  const tourData = toursById?.[rest.tourId]

  const { t } = useTranslation('OutputView')

  const Component = tooltipsMap[type]

  return (
    <div className="map-tooltip-content">
      <Component t={t} {...rest} tourData={tourData} />
    </div>
  )
}

export default Tooltip
