import React from 'react'
import { WarningAlt20 } from '@carbon/icons-react'
import { tourEntity } from 'api/entities'
import CheckTourResults from '../CheckTourResults'

const Tour = ({ t, tourData }) => {
  const {
    [tourEntity.NAME]: tourName,
    [tourEntity.VEHICLE_CODE]: vehicleCode,
    [tourEntity.NUM_STOPS]: numStops,
    [tourEntity.HAS_VIOLATIONS]: hasViolations,
    [tourEntity.CHECK_TOUR_RESULTS]: checkTourResults
  } = tourData

  return (
    <>
      <div className="name">
        {hasViolations && (
          <div className="danger-icon">
            <WarningAlt20 />
          </div>
        )}
        <span>{t('GanttAndMapView.MapTooltip.TourName', { tourName })}</span>
      </div>
      <div>
        <p>
          {t('GanttAndMapView.MapTooltip.VehicleCode')}: {vehicleCode}
        </p>
        <p>{t('GanttAndMapView.MapTooltip.TourNumStop', { count: numStops })}</p>
      </div>
      {checkTourResults && <CheckTourResults data={checkTourResults} t={t} />}
    </>
  )
}

export default Tour
