import React, { useEffect } from 'react'
import { WarningAlt20 } from '@carbon/icons-react'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import Loading from '@ublique/components/Loading'
import { posEntity, stopEntity, tourEntity } from 'api/entities'
import { getTourStopsList } from '../../../ToursManagement/actions/table'
import CheckTourResults from '../CheckTourResults'

const pendingPromises = {}

const Stop = ({ t, tourId, stopId, tourData = {} }) => {
  const tourStops = useStoreSelector('toursManagement', (state) => state.stops)[
    tourId
  ]

  const currentStop = (tourStops || []).find(
    ({ [stopEntity.ID]: id }) => id === stopId
  )

  // Fetch stops if they are missing and there is no pending API request
  useEffect(() => {
    if (tourId && !tourStops && !pendingPromises[tourId]) {
      pendingPromises[tourId] = getTourStopsList(tourId).then(() => {
        delete pendingPromises[tourId]
      })
    }
  }, [tourId, tourStops])

  if (!currentStop) {
    return (
      <div className="loading-container">
        <Loading small withOverlay={false} />
      </div>
    )
  }

  const {
    [stopEntity.STOP_NUMBER]: number,
    [stopEntity.CHECK_TOUR_RESULTS]: checkTourResults,
    [posEntity.DESCRIPTION]: posDescription,
    [posEntity.NAME]: posName
  } = currentStop

  return (
    <>
      <div className="name">
        {!!checkTourResults && (
          <div className="danger-icon">
            <WarningAlt20 />
          </div>
        )}
        <span>
          {t('GanttAndMapView.MapTooltip.TourName', {
            tourName: tourData[tourEntity.NAME]
          })}
        </span>
      </div>
      <div>
        <p>
          {t('GanttAndMapView.MapTooltip.StopPosition', {
            stopNumber: number,
            totalStops: tourStops.length
          })}
        </p>
        <p>
          <span className="pos-name">{posName}:</span>
          <span className="pos-description">{posDescription}</span>
        </p>
      </div>
      {checkTourResults && <CheckTourResults data={checkTourResults} t={t} />}
    </>
  )
}

export default React.memo(Stop)
