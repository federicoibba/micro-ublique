import React, { useEffect } from 'react'
import prop from 'lodash/fp/prop'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import Loading from '@ublique/components/Loading'
import { getPosData } from 'pages/ToursManagement/actions/table'

const Pos = ({ t, posName }) => {
  const planId = useStoreSelector('outputViewCommon', prop('selectedPlanId'))
  const currentPos = useStoreSelector('toursManagement', prop(`pos.${posName}`))

  useEffect(() => {
    if (!currentPos) {
      getPosData(planId, posName)
    }
  }, [planId, posName])

  if (!currentPos) {
    return (
      <div className="loading-container">
        <Loading small withOverlay={false} />
      </div>
    )
  }

  const { soPosName, soPosDescription } = currentPos

  return (
    <div>
      <p>
        <span className="pos-name">{soPosName}:</span>
        <span className="pos-description">{soPosDescription}</span>
      </p>
    </div>
  )
}

export default Pos
