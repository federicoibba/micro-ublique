import React from 'react'
import PropTypes from 'prop-types'
import './index.scss'

const TourDetailSection = ({ title, children, actionComponent }) => (
  <div className="tour-detail-section">
    <div className="tour-detail-section__header">
      <h4>{title}</h4>
      {actionComponent && (
        <div className="tour-detail-section__header-right-component">
          {actionComponent}
        </div>
      )}
    </div>
    {children}
  </div>
)

TourDetailSection.propTypes = {
  title: PropTypes.string,
  children: PropTypes.element,
  actionComponent: PropTypes.element
}

export default React.memo(TourDetailSection)
