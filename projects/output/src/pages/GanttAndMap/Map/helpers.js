import { chunk, dropRight, flatMap, isNumber } from 'lodash'
import { tourEntity } from 'api/entities'
import { geoJsonColors, geoJsonSourceIds, featureTypes } from './constants'

const { INVALID_FEATURE, ACTIVE_FEATURE, INACTIVE_FEATURE } = geoJsonColors

const LTD_1DEG_KM = 110.574
const LNG_1DEG_KM = 111.32

const offsetCoordinatesNE = (coords, m = 100) => {
  const ltd = coords.ltd + m / 1000 / LTD_1DEG_KM
  const ltdRad = coords.ltd * (Math.PI / 180)
  const lng1DegKmAdjusted = LNG_1DEG_KM * Math.cos(ltdRad)
  const lng = coords.lng + m / 1000 / lng1DegKmAdjusted
  return [lng, ltd]
}

const getBaseColor = (toursById, tourId) => {
  const hasViolations = toursById[tourId]?.[tourEntity.HAS_VIOLATIONS]
  return hasViolations ? INVALID_FEATURE : `#${tourId.substr(0, 6)}`
}

const getArrayCoordinatesFromString = (string) => {
  const coordinates = string.match(/\d+(\.\d+)?/g)?.map(parseFloat)
  return chunk(coordinates, 2)
}

const addIndex = (feature, index) =>
  Object.assign({}, feature, {
    properties: Object.assign({}, feature.properties, { index })
  })

export const createTourGeoJSONLine =
  (toursById) =>
  ({ stops, tourId }) => {
    const totalStops = stops.length - 1

    const coordinates = flatMap(stops, ({ polyline }, i) => {
      const points = getArrayCoordinatesFromString(polyline)
      // The last point in a stop is the same as the first point in the next stop
      const pointsToAdd = i === totalStops ? points : dropRight(points)
      return pointsToAdd
    })

    return {
      type: 'Feature',
      properties: {
        tourId,
        type: featureTypes.TOUR,
        color: getBaseColor(toursById, tourId),
        invalid: coordinates.length === 0
        // lineDashArray: hasViolations ? [3, 2] : [1, 0]
      },
      geometry: {
        type: 'LineString',
        coordinates
      }
    }
  }

const createStopsGeoJSONPoint =
  (toursById) =>
  ({ stops, tourId }) => {
    const processedStops = {}
    const features = []
    const color = getBaseColor(toursById, tourId)

    for (const { from, to } of stops) {
      const points = [from, to]

      for (const { id, ltd, lng } of points) {
        if (processedStops[id]) continue

        features.push({
          type: 'Feature',
          properties: {
            draggable: true,
            type: featureTypes.STOP,
            tourId,
            stopId: id,
            color
          },
          geometry: {
            type: 'Point',
            coordinates: [lng, ltd]
          }
        })
        processedStops[id] = true
      }
    }

    return features
  }

const createPosGeoJSONPoint = ({ posName, coordinates }) => {
  return {
    type: 'Feature',
    properties: { type: featureTypes.GROUNDED_ORDER, draggable: true, posName },
    geometry: {
      type: 'Point',
      // coordinates: [coordinates.lng, coordinates.ltd]
      coordinates: offsetCoordinatesNE(coordinates)
    }
  }
}

export const createStopsGeoJSON = (data = [], toursById) => ({
  type: 'FeatureCollection',
  features: data.flatMap(createStopsGeoJSONPoint(toursById)).map(addIndex)
})

export const createToursGeoJSON = (data = [], toursById) => ({
  type: 'FeatureCollection',
  features: data.map(createTourGeoJSONLine(toursById))
})

export const createPosGeoJSON = (data = []) => ({
  type: 'FeatureCollection',
  features: data.map(createPosGeoJSONPoint).map(addIndex)
})

export const filterGeoJSON = (filteredIds, geoJSON) => ({
  type: 'FeatureCollection',
  features: geoJSON.features
    .filter(({ properties }) => filteredIds[properties.tourId])
    .map(addIndex)
})

const isActive = (tourId) => ['==', ['get', 'tourId'], tourId]

const getColor = (activeTour) => [
  'case',
  isActive(activeTour),
  ACTIVE_FEATURE,
  activeTour ? INACTIVE_FEATURE : ['get', 'color']
]

const getOpacity = (activeTour) => [
  'case',
  isActive(activeTour),
  1,
  activeTour ? 0.2 : 1
]

export const createToursSource = (source, activeTour) => ({
  id: geoJsonSourceIds.TOURS,
  type: 'geojson',
  data: source,
  layers: [
    {
      id: `${geoJsonSourceIds.TOURS}-layer`,
      source: geoJsonSourceIds.TOURS,
      type: 'line',
      paint: {
        'line-color': getColor(activeTour),
        'line-opacity': getOpacity(activeTour),
        'line-width': 3
        // 'line-gap-width': 1
      },
      layout: {
        'line-join': 'round',
        'line-cap': 'round'
      }
    }
  ]
})

export const createStopsSource = (source, activeTour) => ({
  id: geoJsonSourceIds.STOPS,
  type: 'geojson',
  data: source,
  layers: [
    {
      id: `${geoJsonSourceIds.STOPS}-layer`,
      source: geoJsonSourceIds.STOPS,
      type: 'circle',
      paint: {
        'circle-color': getColor(activeTour),
        'circle-opacity': getOpacity(activeTour),
        'circle-radius': 6,
        'circle-stroke-width': 1,
        'circle-stroke-color': '#fff'
      }
    }
  ]
})

export const createGroundedOrdersSource = (source) => ({
  id: geoJsonSourceIds.GROUNDED_ORDERS,
  type: 'geojson',
  data: source,
  layers: [
    {
      id: `${geoJsonSourceIds.GROUNDED_ORDERS}-layer`,
      source: geoJsonSourceIds.GROUNDED_ORDERS,
      type: 'symbol',
      layout: {
        'icon-image': 'grounded-order',
        'icon-allow-overlap': true
      }
    }
  ]
})

// Ugly, but the easiest solution to work with MapboxGL custom symbol
export const createHTMLImage = (src, size) => {
  const { w, h } = isNumber(size) ? { w: size, h: size } : size
  const HTMLImage = new Image()
  HTMLImage.width = w
  HTMLImage.height = h
  HTMLImage.src = src
  return HTMLImage
}
