export const MAP_STYLE = 'mapbox://styles/mapbox/streets-v11'

export const geoJsonColors = {
  ACTIVE_FEATURE: 'orangered',
  INACTIVE_FEATURE: 'grey',
  INVALID_FEATURE: 'crimson'
}

export const geoJsonSourceIds = {
  TOURS: 'gj_tours',
  STOPS: 'gj_stops',
  GROUNDED_ORDERS: 'gj_grounded-orders'
}

export const featureTypes = {
  TOUR: 'tour',
  STOP: 'stop',
  GROUNDED_ORDER: 'grounded-order'
}
