import React from 'react'
import { compact } from 'lodash'
import { INTERACTIVE_OUTPUT_MAP } from 'config'
// import groundedOrderSvg from '../../../../images/package.svg'
import groundedOrderPng from '../../../images/package.png'
import Tooltip from '../components/Tooltip'
import { geoJsonSourceIds } from './constants'
import {
  createToursGeoJSON,
  createStopsSource,
  createToursSource,
  filterGeoJSON,
  createStopsGeoJSON,
  createGroundedOrdersSource,
  createPosGeoJSON
} from './helpers'

// Configuration used to create the different GeoJSON layers that display all map data
export const geoJsonConfig = compact([
  {
    id: geoJsonSourceIds.TOURS,
    getGeoJson: (itineraries, toursById) =>
      createToursGeoJSON(itineraries?.tours, toursById),
    filterGeoJson: filterGeoJSON,
    getSource: createToursSource
  },
  {
    id: geoJsonSourceIds.STOPS,
    getGeoJson: (itineraries, toursById) =>
      createStopsGeoJSON(itineraries?.tours, toursById),
    filterGeoJson: filterGeoJSON,
    getSource: createStopsSource
  },
  INTERACTIVE_OUTPUT_MAP && {
    id: geoJsonSourceIds.GROUNDED_ORDERS,
    getGeoJson: (itineraries) => createPosGeoJSON(itineraries?.posWithGroundedSo),
    filterGeoJson: (_, geoJson) => geoJson, // No filtering here for now
    getSource: createGroundedOrdersSource
  }
])

export const transition = { animate: true, speed: 2 }

export const initialViewport = {
  width: '100%',
  height: 600,
  zoom: 6,
  latitude: 41.2925,
  longitude: 12.5736
}

export const getPopupProps = (toursById) => ({
  closeOnClickOutside: true,
  offsetTop: -6,
  component: (props) => <Tooltip {...props} toursById={toursById} />,
  closeButton: false
})

export const layerImages = {
  'grounded-order': groundedOrderPng
}
