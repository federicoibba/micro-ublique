import React, { useCallback, useMemo, useRef, useEffect } from 'react'
import { throttle } from 'lodash'
import Mapbox from '@ublique/components/MapboxGL'
import { ModalActions } from '@ublique/platform'
import { INTERACTIVE_OUTPUT_MAP } from 'config'
import { openSlidePanel } from '../../CommonFeatures/actions'
import { setActiveTour } from '../actions'
import { getPopupProps, initialViewport, layerImages, geoJsonConfig } from './config'
import { featureTypes, geoJsonSourceIds, MAP_STYLE } from './constants'

const MapComponent = ({
  itineraries,
  filteredTourIds,
  toursById,
  editMode,
  activeTour: activeTourData,
  initialCoordinates: { latitude, longitude }
}) => {
  const { id: activeTour = null, animated = true } = activeTourData || {}

  const mapRef = useRef()

  const popupProps = useMemo(() => getPopupProps(toursById), [toursById])

  const viewport = useMemo(
    () => ({
      ...initialViewport,
      latitude: latitude ?? initialViewport.latitude,
      longitude: longitude ?? initialViewport.longitude
    }),
    [latitude, longitude]
  )

  // ----------------------------------- GeoJSON layers ----------------------------------- //
  // These memos are chained and each builds on the result of the previous one

  const geoJsonsList = useMemo(
    () =>
      geoJsonConfig.map(({ getGeoJson, ...config }) => ({
        ...config,
        geoJson: getGeoJson(itineraries, toursById)
      })),
    [itineraries, toursById]
  )

  const filteredGeoJsonsList = useMemo(
    () =>
      filteredTourIds
        ? geoJsonsList.map(({ filterGeoJson, geoJson, ...config }) => ({
            ...config,
            geoJson: filterGeoJson(filteredTourIds, geoJson)
          }))
        : geoJsonsList,
    [geoJsonsList, filteredTourIds]
  )

  const [layers, interactiveLayerIds] = useMemo(
    () => [
      filteredGeoJsonsList.map(({ getSource, geoJson }) =>
        getSource(geoJson, activeTour)
      ),
      filteredGeoJsonsList.map(({ id }) => `${id}-layer`)
    ],
    [filteredGeoJsonsList, activeTour]
  )

  // -------------------------------- End of GeoJSON layers -------------------------------- //

  useEffect(() => {
    if (!activeTour || animated === false) return

    // Zoom to selected tour
    const toursGeoJson = filteredGeoJsonsList.find(
      ({ id }) => id === geoJsonSourceIds.TOURS
    )?.geoJson
    const [activeTourGeoJSON] = toursGeoJson.features.filter(
      ({ properties }) => properties.tourId === activeTour
    )
    if (activeTourGeoJSON && !activeTourGeoJSON.properties.invalid) {
      mapRef.current.flyToFeature(activeTourGeoJSON, false)
    }
  }, [activeTour])

  const onFeatureClick = useCallback(
    (feature) => {
      const { type, tourId } = feature.properties
      if (
        editMode
          ? type === featureTypes.TOUR // Avoid conflicts between click and drag
          : type !== featureTypes.GROUNDED_ORDER
      ) {
        openSlidePanel('tourDetail', { tourId })
        setActiveTour(tourId ? { id: tourId, from: 'map' } : null)
      }
    },
    [editMode]
  )

  // Reset the active tour when clicking on an empty spot
  const onMapClick = useCallback(({ features }) => {
    if (features.length === 0) {
      setActiveTour(null)
    }
  }, [])

  const onFeatureDrag = useCallback(
    throttle((features, _, evt) => {
      const destinationFeature = features.find(
        ({ source }) => source === geoJsonSourceIds.TOURS
      )

      // Show popup with information on the destination tour, following the cursor
      mapRef.current.toggleFeaturePopup(destinationFeature, evt)

      // Dragging over nothing
      if (!destinationFeature && activeTour) {
        return setActiveTour(null)
      }

      // Dragging over a different tour feature
      if (destinationFeature && destinationFeature.properties.tourId !== activeTour) {
        return setActiveTour({
          id: destinationFeature.properties.tourId,
          from: 'map',
          animated: false
        })
      }
    }, 100),
    [activeTour]
  )

  const onFeatureDragEnd = useCallback(
    (_, draggedFeature) => {
      const destinationTour = toursById[activeTour]

      if (
        draggedFeature.type === featureTypes.STOP
          ? activeTour !== draggedFeature.tourId // If it's a stop, do not allow dragging over the same tour
          : !!destinationTour // If it's not a stop, do not allow dragging over nothing
      ) {
        ModalActions.openModal('ordersSelection', {
          draggedFeature,
          destinationTour
        })
      }

      // Sometimes after opening the modal there may be a hover/click triggered
      setTimeout(() => {
        mapRef.current.closePopup()
        setActiveTour(null)
      }, 200)
    },
    [toursById, activeTour]
  )

  return (
    <Mapbox
      ref={mapRef}
      showPopupOnMarkerClick
      showPopupOnFeatureClick
      showPopupOnFeatureHover
      viewportConfig={viewport}
      mapStyle={MAP_STYLE}
      interactiveLayerIds={interactiveLayerIds}
      onFeatureClick={onFeatureClick}
      onMapClick={onMapClick}
      hoverOnFeaturesOnly
      popupProps={popupProps}
      layers={layers}
      layerImages={layerImages}
      resetFeaturePositionAfterDrag
      {...(INTERACTIVE_OUTPUT_MAP && {
        onFeatureDrag,
        onFeatureDragEnd,
        featureDragEnabled: editMode
      })}
    />
  )
}

export default MapComponent
