import { synapse } from 'services/state/synapse'

export const initialState = {
  activeTour: null,
  itineraries: {},
  initialMapCoordinates: {}
}

export const toursGanttMapStore = synapse.store(initialState)

export default {
  toursGanttMap: toursGanttMapStore
}
