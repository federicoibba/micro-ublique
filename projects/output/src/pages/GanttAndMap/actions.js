import compose from 'lodash/fp/compose'
import { orderEntity, stopEntity } from 'api/entities'
import {
  fetchScenarioItineraries,
  fetchScenarioMapToolCenter,
  updateOrdersList,
  updateStop
} from 'api/ToursManagement'
import { updateState } from 'utils'
import { outputViewCommonStore } from '../CommonFeatures/state'
import { getGroundedOrders } from '../GroundedOrders/actions'
import { toursGanttMapStore, initialState } from './state'

// Add 'data' to 'itineraries' at the path 'planId.key', merging if necessary
const processItinerariesData = (key, data) => (itineraries) => {
  if (!data) return itineraries

  const it = { ...itineraries }

  for (const { planId, ...item } of data) {
    if (!it[planId]?.[key]) {
      it[planId] = { ...(it[planId] || {}), [key]: [] }
    }

    it[planId] = {
      ...it[planId],
      [key]: [...it[planId][key], item]
    }
  }

  return it
}

export const setActiveTour = (tour) => {
  updateState(toursGanttMapStore, { activeTour: tour })
}

export const getScenarioItineraries = async (scenarioCode) => {
  try {
    const { tours, posWithGroundedSo } = await fetchScenarioItineraries(scenarioCode)
    const itineraries = compose(
      processItinerariesData('posWithGroundedSo', posWithGroundedSo),
      processItinerariesData('tours', tours)
    )({})
    updateState(toursGanttMapStore, { mapError: false, itineraries })
  } catch (_) {
    updateState(toursGanttMapStore, { mapError: true })
  }
}

export const resetGanttMapState = () => {
  updateState(toursGanttMapStore, { ...initialState, mapError: undefined })
}

export const getInitialMapCoordinates = async (scenarioCode) => {
  const coordinates = await fetchScenarioMapToolCenter(scenarioCode)
  updateState(toursGanttMapStore, { initialMapCoordinates: coordinates })
}

export const putOrdersToGroundFromMap = async ({ orders }) => {
  const planId = outputViewCommonStore.get((state) => state.selectedPlanId)
  const updatedOrders = orders.map((order) => ({
    ...order,
    [orderEntity.TOUR_ID]: null,
    [orderEntity.STOP_ID]: null
  }))
  await updateOrdersList(updatedOrders)
  getGroundedOrders(planId)
}

export const moveStopToTourFromMap = ({ stop, tourId }) => {
  const updatedStop = { ...stop }
  updatedStop[stopEntity.TOUR_ID] = tourId
  return updateStop(stop[stopEntity.ID], updatedStop)
}

export const moveOrdersToTourFromMap = async ({ orders, tourId }) => {
  const planId = outputViewCommonStore.get((state) => state.selectedPlanId)
  const updatedOrders = orders.map((order) => ({
    ...order,
    [orderEntity.TOUR_ID]: tourId
  }))
  await updateOrdersList(updatedOrders)
  getGroundedOrders(planId)
}
