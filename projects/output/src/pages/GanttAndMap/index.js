import React, { useEffect, useMemo } from 'react'
import { isEmpty, keyBy, reduce } from 'lodash'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { Button } from '@ublique/components/Button'
import { synapse } from 'services/state'
import withStore from 'utils/hoc/withStore'
import { scenarioEntity, tourEntity } from 'api/entities'
import ToursManagementLayout from '../CommonFeatures/components/ToursManagementLayout'
import useFilters from '../CommonFeatures/components/Filters/useFilters'
import { resetOpenedTourRows } from '../CommonFeatures/actions'
import { getToursList, toggleEditMode } from '../ToursManagement/actions/table'
import { CHECK_TOUR_COMPLETED } from '../CommonFeatures/hooks/useCheckTourSubscription'
import useScenarioSubscriptions from '../useScenarioSubscriptions'
import TourDetailPanel from './components/TourDetailPanel'
import {
  getInitialMapCoordinates,
  getScenarioItineraries,
  setActiveTour
} from './actions'
// import Gantt from './Gantt'
import Map from './Map'
import './index.scss'

const GanttAndMapView = ({
  outputViewCommon,
  toursManagement,
  toursGanttMap,
  basename
}) => {
  useScenarioSubscriptions({ basename })
  const { t } = useTranslation('OutputView')

  const { scenarioId: scenarioCode } = useParams()

  const { slidePanel, selectedPlanId, scenario } = outputViewCommon
  const { activeTour, itineraries, mapError, initialMapCoordinates } = toursGanttMap
  const { tours, stops, orders, editMode } = toursManagement
  const { opened, data } = slidePanel

  const planTours = tours[selectedPlanId]
  const toursById = useMemo(() => keyBy(planTours, tourEntity.ID), [planTours])

  const filteredTours = useFilters(planTours, 'tour')

  const filteredTourIds = useMemo(
    () =>
      reduce(
        filteredTours,
        (acc, { [tourEntity.ID]: tourId }) => ({ ...acc, [tourId]: true }),
        {}
      ),
    [filteredTours]
  )

  useEffect(() => {
    if (selectedPlanId && !filteredTours) {
      getToursList(selectedPlanId)
    }
  }, [selectedPlanId])

  useEffect(() => {
    getScenarioItineraries(scenarioCode)
    if (isEmpty(initialMapCoordinates)) {
      getInitialMapCoordinates(scenarioCode)
    }
  }, [scenarioCode])

  useEffect(() => {
    return synapse.subscribe(CHECK_TOUR_COMPLETED, () => {
      getScenarioItineraries(scenarioCode)
    })
  }, [])

  useEffect(() => {
    return function onUnmount() {
      setActiveTour(null)
      resetOpenedTourRows()
    }
  }, [])

  const canEdit = scenario[scenarioEntity.STATUS] === 'DRAFT'

  return (
    <ToursManagementLayout
      t={t}
      tours={planTours}
      filteredTours={filteredTours}
      contentTitle={t('ToursManagement.MapTitle')}
    >
      {canEdit && (
        <div className="actions-row">
          <Button onClick={toggleEditMode}>
            {editMode
              ? t('ToursManagement.Actions.DisableEditing')
              : t('ToursManagement.Actions.EnableEditing')}
          </Button>
        </div>
      )}

      <div className="gantt-and-map-section">
        {/*  <div className='gantt-section'>
          <Gantt
            t={t}
            currentLanguage={currentLanguage}
            activeTour={activeTour}
            tourData={filteredTours}
            filteredTourIds={filteredTourIds}
            stopsData={stops}
            planId={selectedPlanId}
          />
        </div> */}

        <div className="map-section">
          {mapError && (
            <span className="map-error">{t('GanttAndMapView.MapError')}</span>
          )}
          <Map
            t={t}
            editMode={editMode}
            activeTour={activeTour}
            toursById={toursById}
            itineraries={itineraries[selectedPlanId]}
            initialCoordinates={initialMapCoordinates}
            filteredTourIds={
              filteredTours?.length === planTours?.length
                ? null // No filter applied
                : filteredTourIds
            }
          />
        </div>
      </div>

      <TourDetailPanel
        t={t}
        editMode={canEdit && editMode}
        opened={opened === 'tourDetail'}
        data={data}
        toursById={toursById}
        stops={stops}
        orders={orders}
      />
    </ToursManagementLayout>
  )
}

export default withStore(React.memo(GanttAndMapView), [
  'outputViewCommon',
  'toursManagement',
  'toursGanttMap'
])
