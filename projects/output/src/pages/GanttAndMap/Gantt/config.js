import { stopEntity, tourEntity } from 'api/entities'
import GanttNameCell from '../components/GanttNameCell'

export const projectConfig = {
  calendar: 'general',
  // startDate: '2020-10-19T00:00:00',
  hoursPerDay: 24,
  daysPerWeek: 5,
  daysPerMonth: 20
}

export const interactiveCellFields = ['name']

export const disabledFeatures = [
  'sort',
  'taskMenu',
  'baselines',
  'projectLines',
  'taskEdit'
]

export const subGridConfigs = {
  left: { width: 180 },
  locked: { width: 200 },
  normal: { flex: 1 }
}

export const viewPreset = {
  base: 'hourAndDay',
  headers: [
    {
      unit: 'day',
      dateFormat: 'dddd DD MMMM'
    },
    {
      unit: 'hour',
      dateFormat: 'HH:mm'
    }
  ]
}

export const customCells = {
  name: GanttNameCell
}

export const fieldsMap = [
  ['vehicleCode', [tourEntity.VEHICLE_CODE]],
  ['vehicleClass', [tourEntity.VEHICLE_CLASS]],
  ['saturation', [tourEntity.SATURATION]],
  ['totalDistance', [tourEntity.TOTAL_DISTANCE, stopEntity.DISTANCE]],
  ['totalTime', [tourEntity.TOTAL_TIME, stopEntity.TIME]],
  ['totalDrivingTime', [tourEntity.TOTAL_DRIVING_TIME, stopEntity.DRIVING_TIME]],
  ['totalServiceTime', [tourEntity.TOTAL_SERVICE_TIME, stopEntity.SERVICE_TIME]],
  ['totalWaitingTime', [tourEntity.TOTAL_WAITING_TIME]]
]
