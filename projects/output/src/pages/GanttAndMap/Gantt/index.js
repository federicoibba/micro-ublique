import React, { useCallback, useMemo, useState, useEffect, useRef } from 'react'
import { isUndefined } from 'lodash'
import Gantt from '@ublique/components/Gantt'
import { LoaderSmall } from 'components/Loading'
import ganttColumns from '../../../../config/ganttColumns.json'
import {
  openSlidePanel,
  resetOpenedTourRows,
  toggleOpenedTourRows
} from '../../CommonFeatures/actions'
import { getTourStopsList } from '../../ToursManagement/actions/table'
import GanttTaskTooltip from '../components/GanttTaskTooltip'
import { setActiveTour } from '../actions'
import {
  getColumsConfig,
  createGanttData,
  getDisabledFeatures,
  hasPlaceholder
} from './helpers'
import {
  subGridConfigs,
  projectConfig,
  viewPreset,
  interactiveCellFields,
  disabledFeatures
} from './config'
import './index.scss'

const disabledFeaturesProps = getDisabledFeatures(disabledFeatures)

const GanttComponent = ({
  t,
  currentLanguage,
  activeTour,
  stopsData,
  tourData,
  filteredTourIds,
  planId
}) => {
  const [loading, setLoading] = useState(false)
  const [openedNodes, setOpenedNodes] = useState([])
  const [scrollTop, setScrollTop] = useState(0)

  const ganttRef = useRef()

  const columnDefs = useMemo(
    () => getColumsConfig(t, ganttColumns),
    [currentLanguage]
  )

  const ganttData = useMemo(
    () => createGanttData(tourData, stopsData),
    [tourData, stopsData]
  )

  const scrollTaskIntoView = useCallback((id) => {
    const ganttInstance = ganttRef.current?.ganttInstance
    if (ganttInstance) {
      const task = ganttInstance.eventStore.findRecord('id', id)
      ganttInstance.scrollTaskIntoView(task, {
        block: 'center',
        animate: false
      })
    }
  }, [])

  const resetOpenedNodes = useCallback(() => {
    ganttRef.current?.ganttInstance.collapseAll()
    setOpenedNodes([])
    resetOpenedTourRows()
  }, [])

  useEffect(() => {
    setActiveTour(null)
  }, [planId, currentLanguage])

  useEffect(() => {
    const ganttInstance = ganttRef.current?.ganttInstance

    if (ganttInstance) {
      if (!activeTour) {
        resetOpenedNodes()
        return
      }

      if (activeTour.from === 'gantt') return

      ganttInstance.expand(activeTour.id).then(() => {
        if (stopsData[activeTour.id]) {
          scrollTaskIntoView(activeTour.id)
        }
      })
    }
  }, [activeTour])

  // When data changes, the gantt internal state is reset, so previously
  // opened nodes are re-opened and the scrolling position is restored
  useEffect(() => {
    const ganttInstance = ganttRef.current?.ganttInstance

    if (ganttInstance) {
      openedNodes.forEach((nodeId) => {
        // Trying to expand a nodeId not currently in the gantt results in a crash
        if (filteredTourIds[nodeId]) {
          ganttInstance.expand(nodeId)
        }
      })

      ganttInstance.scrollVerticallyTo(scrollTop, { animate: false })
    }
  }, [tourData, stopsData])

  const onCellClick = useCallback(({ record, source }) => {
    if (record.type === 'tour') {
      const tourId = record.id
      setScrollTop(source.state.scroll.scrollTop)
      openSlidePanel('tourDetail', { tourId })
      setActiveTour({ id: tourId, from: 'gantt' })
    }
  }, [])

  const onNodeExpandedHandler = useCallback(({ record, source }) => {
    const tourId = record.id
    const hasPlaceholderStop = hasPlaceholder(record.originalData)

    setActiveTour({ id: tourId, from: 'gantt' })
    toggleOpenedTourRows(tourId)
    setOpenedNodes((state) => (state.includes(tourId) ? state : [...state, tourId]))

    if (hasPlaceholderStop) {
      setScrollTop(source.state.scroll.scrollTop)
      setLoading(true)
      getTourStopsList(tourId).then(() => {
        setLoading(false)
        setTimeout(() => {
          scrollTaskIntoView(tourId)
        }, 40)
      })
    }
  }, [])

  const onNodeCollapsedHandler = useCallback(({ record }) => {
    const tourId = record.id
    toggleOpenedTourRows(tourId)
    setOpenedNodes((state) => state.filter((id) => id !== tourId))
  }, [])

  return (
    <div className="gantt-wrapper">
      {(loading || isUndefined(tourData)) && <LoaderSmall className="gantt-loader" />}
      <Gantt
        key={`${planId}-${currentLanguage}`}
        ref={ganttRef}
        readOnly
        locale={currentLanguage}
        project={projectConfig}
        columns={columnDefs}
        viewPreset={viewPreset}
        data={ganttData}
        subGridConfigs={subGridConfigs}
        taskTooltipRenderer={GanttTaskTooltip}
        zoomOnMouseWheel={false}
        zoomOnTimeAxisDoubleClick={false}
        onCellClick={onCellClick}
        interactiveCellFields={interactiveCellFields}
        onNodeExpanded={onNodeExpandedHandler}
        onNodeCollapsed={onNodeCollapsedHandler}
        {...disabledFeaturesProps}
      />
    </div>
  )
}

export default GanttComponent
