import { get } from 'lodash'
import { tourEntity, stopEntity, posEntity } from '../../../../api/entities'
import { getDateWithoutTimezoneOffset } from '../../CommonFeatures/helpers'
import { customCells, fieldsMap } from './config'

const utcTime = (time) => {
  const date = getDateWithoutTimezoneOffset(time)
  return date._isValid ? date.format() : undefined
}

export const getDisabledFeatures = (features) =>
  features.reduce((acc, feature) => ({ ...acc, [`${feature}Feature`]: false }), {})

const mapFieldsForGantt = (type, fields = []) =>
  fieldsMap.reduce((acc, [field, [tourKey, stopKey]]) => {
    const key = type === 'tour' ? tourKey : stopKey
    return {
      ...acc,
      [field]: fields[key]?.toString() ?? '-'
    }
  }, {})

export const getColumsConfig = (t, columns) =>
  columns.map((column) => ({
    ...column,
    text: t(`GanttAndMapView.ToursTableColumnHeaders.${column.headerI18nKey}`),
    autoWidth: true,
    renderer: customCells[column.field]
  }))

const createGanttStopData = (stops, tourId) => {
  // console.log(`mapping ${tourId} stops`)
  return stops.map((stop) => {
    const arrivalTime = utcTime(stop[stopEntity.ARRIVAL_TIME])
    const departureTime = utcTime(stop[stopEntity.DEPARTURE_TIME])
    return {
      ...mapFieldsForGantt('stop', stop),
      id: stop[stopEntity.ID],
      data: stop,
      name: stop[posEntity.DESCRIPTION],
      startDate: arrivalTime,
      constraintDate: arrivalTime,
      constraintType: 'muststarton',
      endDate: departureTime || arrivalTime,
      type: stop[stopEntity.DEPARTURE_TIME] ? 'stop' : 'stopFinal'
    }
  })
}

// When the stops haven't been loaded yet, a placeholder is used to show the expandable cell and the task bar
const createStopPlaceholder = (tour) => {
  const startDate = utcTime(tour[tourEntity.TIME_START])
  const endDate = utcTime(tour[tourEntity.TIME_END])
  return [
    {
      id: `${tour[tourEntity.ID]}-placeholder`,
      name: '...',
      startDate,
      constraintDate: startDate,
      constraintType: 'muststarton',
      endDate: endDate || startDate,
      cls: 'placeholder-task',
      type: 'stop'
    }
  ]
}

export const createGanttData = (tours = [], stops) => {
  return tours.map((tour) => {
    const tourId = tour[tourEntity.ID]
    const isInvalid = tour[tourEntity.HAS_VIOLATIONS]
    const tourStops = stops[tourId]

    return {
      ...mapFieldsForGantt('tour', tour),
      id: tourId,
      data: tour,
      name: tour[tourEntity.NAME],
      style: isInvalid ? 'background: crimson;' : undefined,
      type: 'tour',
      children: tourStops
        ? createGanttStopData(tourStops, tourId)
        : createStopPlaceholder(tour)
    }
  })
}

export const hasPlaceholder = (data) =>
  get(data, 'children.0.id') === `${data.id}-placeholder`
