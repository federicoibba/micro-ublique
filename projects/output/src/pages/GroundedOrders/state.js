import { synapse } from 'services/state/synapse'
import groundedOrdersColumns from 'config/groundedOrdersTableColumns.json'

export const initialState = {
  columnsConfig: groundedOrdersColumns,
  orders: {},
  loading: false
}

export const groundedOrdersStore = synapse.store(initialState)

export default {
  groundedOrders: groundedOrdersStore
}
