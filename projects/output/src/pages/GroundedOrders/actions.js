import { get } from 'lodash'
// import { NotificationActions } from '@ublique/platform'
import { updateState } from 'utils'
import { createNewTour, fetchGroundedOrders } from 'api/GroundedOrders'
import { orderEntity, tourEntity } from 'api/entities'
import { makeGroundedOrderCreateTourNotification } from '../CommonFeatures/notificationCreators'
import { groundedOrdersStore, initialState } from './state'

// const { addNotification } = NotificationActions

export const getGroundedOrders = async (planId, { reset } = {}) => {
  // updateState(groundedOrdersStore, { loading: true })
  const response = await fetchGroundedOrders(planId)
  groundedOrdersStore.update((state) => ({
    ...state,
    // loading: false,
    orders: {
      ...(reset ? {} : state.orders),
      [planId]: response.items
    }
  }))
}

export const filterGroundedOrdersByPoS = async (record, orders) => {
  const selectedPoS = record[orderEntity.POINT_OF_SERVICE]
  const filteredOrders = orders.filter((order) => {
    return order[orderEntity.POINT_OF_SERVICE] === selectedPoS
  })
  updateState(groundedOrdersStore, { selectedOrdersByPdv: filteredOrders })
}

export const deleteGroundedOrdersByPdv = () => {
  updateState(groundedOrdersStore, { selectedOrdersByPdv: [] })
}

export const createTour = async (tour, t, pushToOutputRoute) => {
  const response = await createNewTour(tour)
  const notification = makeGroundedOrderCreateTourNotification(
    t,
    response[tourEntity.NAME],
    pushToOutputRoute
  )
  if (notification) {
    // addNotification(notification)
  }
}

export const resetGroundedOrdersState = () => groundedOrdersStore.set(initialState)

export const resetGroundedOrdersStateForUndo = (planId) => {
  groundedOrdersStore.update((state) => ({
    ...state,
    orders: {
      [planId]: get(state, ['orders', planId])
    }
  }))
}
