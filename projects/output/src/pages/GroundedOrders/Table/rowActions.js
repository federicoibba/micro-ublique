import { ModalActions } from '@ublique/platform'
import makeRowActions from '../../CommonFeatures/components/RowActions'

const { openModal } = ModalActions

const groundedOrdersActionsConfig = [
  {
    i18nKey: 'AddToTour',
    onClick: ({ selectedItems }) => openModal('addTour', selectedItems)
  },
  {
    i18nKey: 'CreateNewTour',
    onClick: ({ selectedItems }) => openModal('createTour', selectedItems)
  }
]

export default makeRowActions(groundedOrdersActionsConfig)
