import React, { useState, useEffect, useRef, useCallback } from 'react'
import { keyBy } from 'lodash'
import { useTranslation } from 'react-i18next'
import DataTable from '@ublique/components/DataTable'
// import { FileUploaderButton } from '@ublique/components/FileUploader'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { orderEntity } from 'api/entities'
import { useColumnDefs } from '../../CommonFeatures/hooks/useColumnDefs'
import { deleteGroundedOrdersByPdv } from '../actions'
import GroundedOrdersRowActions from './rowActions'
import EditModeSwitcher from '../../ToursManagement/components/EditModeSwitcher'
import { TourOverflowMenu } from '../components/OveflowMenu'
import { defaultColDef } from '../../ToursManagement/TableConfig'
import { getGroundedOrdersColumnDefs } from './config'

// const body = document.querySelector('body')
const frameworkComponents = { TourOverflowMenu }
const GROUNDED_ORDERS_TABLE_PAGE_SIZE = 10

const getRowNodeId = (data) => data[orderEntity.ID]

const isEditModeActiveSelector = (state) => state.editMode

const GroundedOrdersTable = ({ groundedOrders, selectedPlanId }) => {
  const {
    columnsConfig,
    selectedOrdersByPdv,
    orders: { [selectedPlanId]: planOrders }
  } = groundedOrders

  const gridParamsRef = useRef()
  const { t } = useTranslation('OutputView')

  const [selectedRows, setSelectedRows] = useState([])

  const editMode = useStoreSelector('toursManagement', isEditModeActiveSelector)

  const columnsT = useCallback(
    (x) => t(`GroundedOrders.TableColumnHeaders.${x}`),
    [t]
  )

  const { columnDefs, canEdit } = useColumnDefs({
    t: columnsT,
    editMode,
    columnsConfig,
    getColumnDefs: getGroundedOrdersColumnDefs
  })

  useEffect(() => {
    /* if (selectedPlanId) {
      getToursList(selectedPlanId)
    } */
    if (selectedOrdersByPdv && selectedOrdersByPdv.length > 0 && gridParamsRef) {
      gridParamsRef.current.api.deselectAll()

      setSelectedRows(selectedOrdersByPdv)
      const selectedOrdersByPdvDict = keyBy(selectedOrdersByPdv, orderEntity.ID)
      gridParamsRef.current.api.forEachNode((rowNode) => {
        if (selectedOrdersByPdvDict[rowNode.id]) {
          rowNode.setSelected(true)
        }
      })
    }
  }, [selectedPlanId, selectedOrdersByPdv])

  const localeTextFunc = useCallback((x) => t(`agGrid:${x}`), [t])

  const onGridReady = (params) => {
    gridParamsRef.current = params
  }

  const handleDeselectRows = () => {
    deleteGroundedOrdersByPdv()
    gridParamsRef.current.api.deselectAll()
  }

  /* const handleSendFile = (e) => {
    const file = e.target.files[0]
    const formData = new FormData() // eslint-disable-line
    formData.append('file', file)
    synapse.fetch(
      'http://localhost:3000/upload_files',
      { method: 'POST' },
      formData
    )
  } */

  return (
    <div className="pos-relative" data-floating-menu-container>
      {selectedRows.length > 0 && (
        <GroundedOrdersRowActions
          onCancel={handleDeselectRows}
          selectedRows={selectedRows}
        />
      )}
      {/* selectedRows.length === 0 && (
        <div className='uploadButtonWrapper'>
          <FileUploaderButton
            // accept={['.jpg', '.png']}
            buttonKind='tertiary'
            onChange={handleSendFile}
            labelText={t('GroundedOrders.UploadOrders')}
          />
        </div>
      ) */}

      <DataTable
        immutableData
        getRowNodeId={getRowNodeId}
        onGridReady={onGridReady}
        frameworkComponents={frameworkComponents}
        defaultColDef={defaultColDef}
        columnDefs={columnDefs}
        rowData={planOrders}
        onRowsSelect={setSelectedRows}
        // popupParent={body}
        rowSelection="multiple"
        componentWrappingElement="span"
        suppressRowClickSelection
        canExportCSV
        sortable
        animateRows
        localeTextFunc={localeTextFunc}
        pagination
        paginationPageSize={GROUNDED_ORDERS_TABLE_PAGE_SIZE}
        autoSizeColumnsOnFirstRender
        isAutoSizeAll
        extraHeaderComponent={
          canEdit ? (
            <EditModeSwitcher
              enabled={editMode}
              enableText={t('ToursManagement.Actions.EnableEditing')}
              disableText={t('ToursManagement.Actions.DisableEditing')}
            />
          ) : undefined
        }
      />
    </div>
  )
}

export default React.memo(GroundedOrdersTable)
