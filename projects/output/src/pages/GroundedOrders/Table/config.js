import {
  commonRowActionColDef,
  commonRowCheckboxColDef
} from '../../ToursManagement/TableConfig/common'

export const getGroundedOrdersColumnDefs = ({
  baseColumnDefs,
  editMode,
  canEdit
}) => {
  if (!canEdit) return baseColumnDefs

  const fullColumns = [
    {
      hide: !editMode,
      width: 60,
      minWidth: 60,
      ...commonRowCheckboxColDef
    },
    ...baseColumnDefs
  ]

  if (editMode) {
    fullColumns.push({
      cellRenderer: 'TourOverflowMenu',
      ...commonRowActionColDef,
      maxWidth: 70
    })
  }
  return fullColumns
}
