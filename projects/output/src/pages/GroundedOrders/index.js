import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import Grid from '@ublique/components/Grid'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import SectionHeader from '../../components/SectionHeader'
import HeaderActions from '../CommonFeatures/components/HeaderActions'
import { getGroundedOrders } from './actions'
import GroundedOrdersTable from './Table'
import useScenarioSubscriptions from '../useScenarioSubscriptions'

const getSelectedPlanId = (state) => state.selectedPlanId

const GroundedOrders = ({ basename }) => {
  useScenarioSubscriptions({ basename })
  const { t } = useTranslation('OutputView')

  const selectedPlanId = useStoreSelector('outputViewCommon', getSelectedPlanId)
  const groundedOrders = useStoreSelector('groundedOrders')

  useEffect(() => {
    if (selectedPlanId && !groundedOrders.orders[selectedPlanId]) {
      getGroundedOrders(selectedPlanId)
    }
  }, [selectedPlanId])

  return (
    <div className="tours-management-layout">
      <Grid fullWidth>
        <Grid.Row>
          <Grid.Column>
            <SectionHeader
              title={t('GroundedOrders.HeaderTitle')}
              rightComponent={<HeaderActions />}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <GroundedOrdersTable
              groundedOrders={groundedOrders}
              selectedPlanId={selectedPlanId}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}

export default React.memo(GroundedOrders)
