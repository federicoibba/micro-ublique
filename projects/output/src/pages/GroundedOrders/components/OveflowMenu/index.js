import React from 'react'
import { get, map } from 'lodash'
import { useTranslation } from 'react-i18next'
import { OverflowMenu, OverflowMenuItem } from '@ublique/components/OverflowMenu'
import withStore from 'utils/hoc/withStore'
import { tourMenuConfig } from './TourMenu'
import { orderEntity } from 'api/entities'
import './index.scss'

const makeCellRenderer = (items, idKey, connected) => {
  const CellRenderer = ({ data, groundedOrders }) => {
    const { t } = useTranslation('OutputView')

    return (
      <OverflowMenu flipped className="table-overflow-menu">
        {map(items, ({ text, i18nKey, onClick }, i) => (
          <OverflowMenuItem
            key={`${data[idKey]}-${i}`}
            onClick={() =>
              onClick({
                t,
                id: data[idKey],
                record: data,
                orders: get(groundedOrders, `orders.${data[orderEntity.PLAN_ID]}`, [])
              })
            }
            itemText={i18nKey ? t(`ToursManagement.Actions.${i18nKey}`) : text}
          />
        ))}
      </OverflowMenu>
    )
  }

  return withStore(CellRenderer, ['groundedOrders'])
}
export const TourOverflowMenu = makeCellRenderer(tourMenuConfig, orderEntity.ID)
