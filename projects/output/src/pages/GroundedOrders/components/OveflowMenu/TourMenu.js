import { ModalActions } from '@ublique/platform'
import { filterGroundedOrdersByPoS } from '../../actions'

const { openModal } = ModalActions

export const tourMenuConfig = [
  {
    i18nKey: 'EditUld',
    onClick: ({ id, record }) => openModal('editUld', { id, record })
  },
  {
    i18nKey: 'GroupByPoS',
    onClick: ({ record, orders }) => filterGroundedOrdersByPoS(record, orders)
  }
]
