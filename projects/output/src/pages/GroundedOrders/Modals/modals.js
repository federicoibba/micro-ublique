import CreateTour from './CreateTour'
import EditUld from './EditUld'
import AddToTour from './AddToTour'

export default {
  createTour: CreateTour,
  editUld: EditUld,
  addTour: AddToTour
}
