import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { get } from 'lodash'
import Grid from '@ublique/components/Grid'
import DataTable from '@ublique/components/DataTable'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { getToursList, resetTourData } from 'pages/ToursManagement/actions/table'
import Modal from 'pages/CommonFeatures/components/DetectUndoModal'
import { updateOrdersList } from 'api/ToursManagement'
import { orderEntity, tourEntity } from 'api/entities'
import { getGroundedOrders } from '../../actions'
import { defaultColumnDef, getColumnDefs, frameworkComponents } from './config'
import '../index.scss'

const getSelectedPlanIdSelector = (state) => state.selectedPlanId
const getToursSelector = (state) => state.tours

const AddTour = ({ open, close, data }) => {
  const { t } = useTranslation(['OutputView', 'Common'])

  const planId = useStoreSelector('outputViewCommon', getSelectedPlanIdSelector)
  const tours = useStoreSelector('toursManagement', getToursSelector)

  const [tourId, setTourId] = useState(null)
  const [loading, setLoading] = useState(false)

  const localeTextFunc = useCallback((x) => t(`agGrid:${x}`), [t])
  const columnsT = useCallback(
    (x) => t(`ToursManagement.ToursTableColumnHeaders.${x}`),
    [t]
  )

  const { [planId]: planTours } = tours
  const columnDefs = getColumnDefs(columnsT)

  useEffect(() => {
    if (!planTours) {
      getToursList(planId)
    }
  }, [])

  const handleSubmit = async () => {
    const updatedOrders = data.map((order) => ({
      ...order,
      [orderEntity.TOUR_ID]: tourId
    }))
    setLoading(true)
    await updateOrdersList(updatedOrders)
    await getGroundedOrders(planId)
    resetTourData(planId, tourId)
    close()
  }

  return (
    <Modal
      className="grounded-orders-modal-createTour"
      open={open}
      onRequestClose={close}
      secondaryButtonText={t('Common:Cancel')}
      primaryButtonText={t('Common:Confirm')}
      onRequestSubmit={handleSubmit}
      modalHeading={t('GroundedOrders.Modals.AddTourTitle')}
      confirmLoading={loading}
      confirmLoadingMessage={t('Common:Submitting')}
      primaryButtonDisabled={!tourId}
    >
      <Grid fullWidth>
        <Grid.Row>
          <Grid.Column>
            <DataTable
              className="tour-selection-table"
              columnDefs={columnDefs}
              rowData={planTours}
              onRowsSelect={([selectedRow]) =>
                setTourId(get(selectedRow, tourEntity.ID))
              }
              rowSelection="single"
              defaultColDef={defaultColumnDef}
              autoSizeColumnsOnFirstRender
              suppressColumnVirtualisation
              suppressRowClickSelection
              canSearch={false}
              pagination
              paginationPageSize={8}
              localeTextFunc={localeTextFunc}
              frameworkComponents={frameworkComponents}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Modal>
  )
}
export default React.memo(AddTour)
