import toursTableColumns from 'config/toursTableColumns.json'
import GridDate from 'pages/CommonFeatures/components/GridDate'
import { mapColumnsToAgGridColumnDefs } from '../../../CommonFeatures/mappers'

export const defaultColumnDef = {
  suppressMovable: true,
  sortable: true,
  suppressMenu: true,
  filter: true,
  floatingFilter: true
}

export const getColumnDefs = (t) => [
  {
    colId: 'checkbox',
    checkboxSelection: true,
    suppressMenu: true,
    suppressColumnsToolPanel: true,
    filter: false,
    sortable: false,
    resizable: false,
    width: 60,
    maxWidth: 60,
    pinned: 'left'
  },
  ...mapColumnsToAgGridColumnDefs(t, toursTableColumns).map((col) => ({
    ...col,
    cellRenderer: undefined,
    suppressMovable: true,
    suppressColumnsToolPanel: true,
    cellRendererParams: undefined
  }))
]

export const frameworkComponents = {
  agDateInput: GridDate
}
