import React, { useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import moment from 'moment'
import { find, get, reduce, values } from 'lodash'
import withStore from 'utils/hoc/withStore'
import Dropdown from '@ublique/components/Dropdown'
import DatePicker from '@ublique/components/DatePicker'
import TimePicker from '@ublique/components/TimePicker'
import TextInput from '@ublique/components/TextInput'
import Grid from '@ublique/components/Grid'
import { getToursList } from 'pages/ToursManagement/actions/table'
import { useEntityList } from 'pages/CommonFeatures/hooks/useEntityList'
import { usePushToOutputRoute } from 'pages/CommonFeatures/hooks/usePushToOutputRoute'
import Modal from 'pages/CommonFeatures/components/DetectUndoModal'
import { tourEntity } from 'api/entities'
import { datePickerLocales } from '../../../CommonFeatures/helpers'
import { createTour, getGroundedOrders } from '../../actions'
import '../index.scss'

const tourTypes = [
  {
    id: 'PRI',
    i18nKey: 'TourTypePrimary'
  },
  {
    id: 'SEC',
    i18nKey: 'TourTypeSecondary'
  }
]

const entities = ['vehicles', 'vehicleClasses']

const isNotComplete = (obj) => values(obj).some((val) => val === '')

const itemToString = (item) => item.text

const CreateTour = ({ open, close, data, outputViewCommon }) => {
  const { t, i18n } = useTranslation(['OutputView', 'Common'])
  const pushToOutputRoute = usePushToOutputRoute()
  const { vehicles, vehicleClasses, selectedPlanId } = outputViewCommon

  const translatedTourTypes = tourTypes.map(({ id, i18nKey }) => ({
    id,
    text: t(`GroundedOrders.Modals.${i18nKey}`)
  }))

  const [loading, setLoading] = useState(false)

  const [newTourData, setNewTourData] = useState({
    time: '',
    selectedDate: '',
    vehicleClass: '',
    vehicleCode: '',
    tourType: '',
    tourName: ''
  })

  useEntityList({ entities })

  const vehiclesByVehicleClass = useMemo(
    () =>
      reduce(
        vehicles,
        (acc, { vehicleClass, vehicleCode }) => ({
          ...acc,
          [vehicleClass]: [...get(acc, vehicleClass, []), vehicleCode]
        }),
        {}
      ),
    [vehicles]
  )

  const setTourData = (newValues) => {
    setNewTourData((state) => ({ ...state, ...newValues }))
  }

  const handleSubmit = async () => {
    const { selectedDate, time, tourType, tourName, vehicleClass, vehicleCode } =
      newTourData
    setLoading(true)
    const date = moment(selectedDate).format('DD-MM-YYYY')
    const fullDate = moment.utc(`${date} ${time}`, 'DD-MM-YYYY hh:mm').toISOString()
    const tour = {
      orders: data,
      [tourEntity.TYPE]: tourType,
      [tourEntity.VEHICLE_CLASS]: vehicleClass,
      [tourEntity.VEHICLE_CODE]: vehicleCode,
      [tourEntity.TIME_START]: fullDate,
      [tourEntity.PLAN_ID]: selectedPlanId,
      [tourEntity.NAME]: tourName
    }
    await createTour(tour, t, pushToOutputRoute)
    await Promise.all([
      getGroundedOrders(selectedPlanId),
      getToursList(selectedPlanId)
    ])
    close()
  }

  const selectedTourType = find(tourTypes, (id) => id === newTourData.tourType)

  return (
    <Modal
      className="grounded-orders-modal-createTour"
      open={open}
      onRequestClose={close}
      secondaryButtonText={t('Common:Cancel')}
      primaryButtonText={t('Common:Confirm')}
      onRequestSubmit={handleSubmit}
      modalHeading={t('GroundedOrders.Modals.CreateTourTitle')}
      confirmLoading={loading}
      confirmLoadingMessage={t('Common:Submitting')}
      primaryButtonDisabled={isNotComplete(newTourData)}
    >
      <Grid fullWidth className="grid">
        <h5 className="title">{t('GroundedOrders.Modals.CreateTourNameType')}</h5>
        <Grid.Row>
          <Grid.Column sm={4} lg={4}>
            <TextInput
              id="tour-name"
              labelText={t('GroundedOrders.Modals.CreateTourName')}
              placeholder={t('GroundedOrders.Modals.CreateTourName')}
              value={newTourData.tourName}
              onChange={({ target }) => setTourData({ tourName: target.value })}
            />
          </Grid.Column>
          <Grid.Column sm={4} lg={4}>
            <Dropdown
              id="tour-type"
              items={translatedTourTypes}
              titleText={t('GroundedOrders.Modals.TourTypes')}
              label={t('Common:Select')}
              itemToString={itemToString}
              selectedItem={selectedTourType}
              onChange={({ selectedItem }) =>
                setTourData({ tourType: selectedItem.id })
              }
            />
          </Grid.Column>
        </Grid.Row>
        <h5 className="title">{t('GroundedOrders.Modals.CreateTourVehicle')}</h5>
        <Grid.Row>
          <Grid.Column sm={4} lg={4}>
            <Dropdown
              id="vehicle-class"
              items={vehicleClasses || []}
              titleText={t('GroundedOrders.Modals.VehicleClass')}
              label={t('Common:Select')}
              selectedItem={newTourData.vehicleClass}
              onChange={({ selectedItem }) =>
                setTourData({ vehicleClass: selectedItem, vehicleCode: '' })
              }
            />
          </Grid.Column>
          <Grid.Column sm={4} lg={4}>
            <Dropdown
              id="vehicle-code"
              disabled={!newTourData.vehicleClass}
              items={vehiclesByVehicleClass[newTourData.vehicleClass] || []}
              titleText={t('GroundedOrders.Modals.VehicleCode')}
              label={t('Common:Select')}
              selectedItem={newTourData.vehicleCode}
              onChange={({ selectedItem }) =>
                setTourData({ vehicleCode: selectedItem })
              }
            />
          </Grid.Column>
        </Grid.Row>
        <h5 className="title">
          {t('GroundedOrders.Modals.CreateTourDepartureDateTime')}
        </h5>
        <Grid.Row>
          <Grid.Column sm={4} lg={4}>
            <DatePicker
              key={`datePicker_lang_${i18n.language}`}
              datePickerType="single"
              dateFormat="d/m/Y"
              value={newTourData.selectedDate}
              onChange={([date]) => setTourData({ selectedDate: date })}
              className="datePicker"
              locale={datePickerLocales[i18n.language] || 'it'}
            >
              <DatePicker.Input
                id="tour-types"
                labelText={t('GroundedOrders.Modals.CreateTourDate')}
                placeholder={t('Common:DatePlaceholder')}
                autoComplete="off"
              />
            </DatePicker>
          </Grid.Column>
          <Grid.Column sm={4} lg={4}>
            <TimePicker
              id="create-tour-time-picker"
              labelText={t('GroundedOrders.Modals.CreateTourHour')}
              value={newTourData.time}
              onChange={(e) => setTourData({ time: e.target.value })}
              type="time"
              className="timePicker"
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Modal>
  )
}
export default withStore(React.memo(CreateTour), ['outputViewCommon'])
