import React, { useEffect, useState } from 'react'
import { map } from 'lodash'
import { useTranslation } from 'react-i18next'
import Tabs from '@ublique/components/Tabs'
import Tab from '@ublique/components/Tabs/Tab'
import Grid from '@ublique/components/Grid'
import Loading from '@ublique/components/Loading'
import PlanSelect from '../../CommonFeatures/components/PlanSelect'
import { getToursList } from '../../ToursManagement/actions/table'
import StatsRenderer from '../StatsRenderer'
import { tabsConfig } from './Tabs'
import stats, { I18N_NS } from './config'
import './index.scss'

const FocusTabs = React.memo(({ t, setActiveTab, activeTab, data }) => {
  return (
    <Tabs onTabChange={setActiveTab}>
      {map(tabsConfig, ({ title, component: Component }, index) => (
        <Tab key={`tab-${index}`} title={t(title)} active={activeTab === index}>
          <Component t={t} data={data} isVisible={activeTab === index} />
        </Tab>
      ))}
    </Tabs>
  )
})

const PlanView = ({ data, loading, selectedPlanId, isVisible }) => {
  const [activeTab, setActiveTab] = useState(0)

  const { t } = useTranslation('OutputView')

  useEffect(() => {
    if (selectedPlanId && !data.tours) {
      getToursList(selectedPlanId)
    }
  }, [selectedPlanId])

  if (loading) return <Loading />

  return (
    <div className="plan-kpis-view">
      <Grid fullWidth className="statistics-grid tab-header tab-header--plan">
        <Grid.Row>
          <Grid.Column sm={4} md={4} xlg={9}>
            <h2>{t('KPIs.HeaderTitle')}</h2>
          </Grid.Column>
          <Grid.Column sm={4} md={4} xlg={3}>
            <PlanSelect />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <StatsRenderer
        t={t}
        ns={I18N_NS}
        config={stats}
        data={data}
        isVisible={isVisible}
      />
      <h2 className="header-inner">{t('KPIs.Focus')}</h2>
      {isVisible && (
        <div className="focus-tabs">
          <FocusTabs
            t={t}
            data={data}
            activeTab={activeTab}
            setActiveTab={setActiveTab}
            planId={selectedPlanId}
          />
        </div>
      )}
    </div>
  )
}

export default React.memo(PlanView)
