import React from 'react'
import StatsRenderer from '../../StatsRenderer'
import ToursConfig from './Tours'
// import CostsConfig from './Costs'
import SaturationConfig from './Saturation'
import TimesConfig from './Times'
import DistancesConfig from './Distances'

const createTabViewComponent = (config, ns = 'KPIs.Plan') =>
  React.memo(({ data, t, isVisible }) => (
    <StatsRenderer ns={ns} config={config} data={data} t={t} isVisible={isVisible} />
  ))

export const tabsConfig = [
  {
    title: 'KPIs.Plan.ToursTab',
    component: createTabViewComponent(ToursConfig)
  },
  {
    title: 'KPIs.Plan.SaturationTab',
    component: createTabViewComponent(SaturationConfig)
  },
  {
    title: 'KPIs.Plan.TimesTab',
    component: createTabViewComponent(TimesConfig)
  },
  /* {
    title: 'KPIs.Plan.CostsTab',
    component: createTabViewComponent(CostsConfig)
  }, */
  {
    title: 'KPIs.Plan.DistancesTab',
    component: createTabViewComponent(DistancesConfig)
  }
]
