import { planEntity, tourEntity } from 'api/entities'
import { getPadding, getVerticalBarChartAxesProps } from '../../StatsRenderer/helpers'

const horizontalBarChart = {
  legend: {
    enabled: false
  },
  padding: getPadding(10),
  series: [
    {
      type: 'bar',
      xKey: 'label',
      yKeys: ['value']
    }
  ],
  axes: [
    {
      type: 'number',
      position: 'bottom',
      label: { color: 'text01' }
    },
    {
      type: 'category',
      position: 'left',
      tick: { width: 0 },
      label: { color: 'text01' }
    }
  ]
}

const firstRow = [
  {
    id: planEntity.TOT_TOURS,
    colSpan: { sm: 4, md: 4, lg: 3 }
  },
  {
    id: planEntity.TOT_VEHICLES,
    colSpan: { sm: 4, md: 4, lg: 3 }
  },
  {
    id: 'stopsPerTour',
    colSpan: { sm: 4, md: 4, lg: 3 },
    chart: {
      ...horizontalBarChart,
      dataConfig: {
        yKeys: [
          { id: planEntity.MAX_STOP_PER_TOUR, label: 'Max' },
          { id: planEntity.AVG_STOP_PER_TOUR, label: 'Avg' },
          { id: planEntity.MIN_STOP_PER_TOUR, label: 'Min' }
        ]
      }
    }
  },
  {
    id: planEntity.REUSE,
    type: 'percentage',
    colSpan: { sm: 4, md: 4, lg: 3 }
  }
]

const secondRow = [
  {
    id: 'numJourneysPerVehicleClass',
    taller: true,
    colSpan: { sm: 4, md: 8, lg: 12 },
    chart: {
      legend: {
        enabled: false
      },
      dataConfig: {
        type: 'composite',
        xKey: tourEntity.VEHICLE_CLASS
      },
      series: [
        {
          type: 'column',
          xKey: 'label',
          yKeys: ['value']
        }
      ],
      axes: [
        {
          type: 'category',
          position: 'bottom',
          tick: { width: 0 },
          label: { color: 'text01' },
          title: { text: 'VehicleClass', color: 'text01' }
        },
        {
          type: 'number',
          position: 'left',
          label: { color: 'text01' },
          title: { text: 'NumberOfJourneys', color: 'text01' }
        }
      ]
    }
  }
]

const thirdRow = [
  {
    id: 'numJourneysPerVector',
    taller: true,
    colSpan: { sm: 4, md: 8, lg: 12 },
    chart: {
      legend: {
        enabled: false
      },
      dataConfig: {
        type: 'composite',
        xKey: tourEntity.CARRIER_NAME
      },
      series: [
        {
          type: 'column',
          xKey: 'label',
          yKeys: ['value']
        }
      ],
      axes: getVerticalBarChartAxesProps('CarrierName', 'NumberOfJourneys')
    }
  }
]

export default [firstRow, secondRow, thirdRow]
