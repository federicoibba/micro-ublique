import React from 'react'
import { renderToStaticMarkup } from 'react-dom/server'
import { tourEntity } from 'api/entities'
import ValueFormatChartTooltip from 'pages/CommonFeatures/components/ValueFormatChartTooltip'
import { getPadding, getVerticalBarChartAxesProps } from '../../StatsRenderer/helpers'
import { formatMinutes } from 'utils'

const FormattedMinutesTooltip = (props) =>
  renderToStaticMarkup(
    <ValueFormatChartTooltip valueFormatter={formatMinutes} {...props} />
  )

const firstRow = [
  {
    id: 'totalPlanTime',
    taller: true,
    colSpan: { sm: 4, md: 8, lg: 3 },
    chart: {
      legend: {
        position: 'bottom',
        color: 'text01'
      },
      dataConfig: {
        type: 'pieComposite',
        yKeys: [
          tourEntity.TOTAL_DRIVING_TIME,
          tourEntity.TOTAL_SERVICE_TIME,
          tourEntity.TOTAL_WAITING_TIME,
          tourEntity.TOTAL_DOWNTIME
        ]
      },
      padding: getPadding(10),
      series: [
        {
          titleType: 'totalMinutes',
          type: 'pie',
          labelKey: 'label',
          label: { enabled: false },
          angleKey: 'value',
          innerRadiusOffset: -20,
          tooltipRenderer: FormattedMinutesTooltip
        }
      ]
    }
  },
  {
    id: 'totalTimePerJourney',
    taller: true,
    colSpan: { sm: 4, md: 8, lg: 9 },
    chart: {
      legend: {
        enabled: true,
        position: 'bottom',
        color: 'text01'
      },
      dataConfig: {
        maxItems: 12,
        type: 'composite',
        xKey: tourEntity.NAME,
        translateKeys: true, // Uses translated dataKeys, for correct visualization in the legend
        sortedBy: tourEntity.TOTAL_TIME,
        yKeys: [
          tourEntity.TOTAL_TIME,
          tourEntity.TOTAL_DRIVING_TIME,
          tourEntity.TOTAL_SERVICE_TIME,
          tourEntity.TOTAL_WAITING_TIME,
          tourEntity.TOTAL_DOWNTIME
        ]
      },
      series: [
        {
          type: 'column',
          xKey: 'label',
          translateKeys: true,
          yKeys: [
            tourEntity.TOTAL_TIME,
            tourEntity.TOTAL_DRIVING_TIME,
            tourEntity.TOTAL_SERVICE_TIME,
            tourEntity.TOTAL_WAITING_TIME,
            tourEntity.TOTAL_DOWNTIME
          ],
          grouped: true
        }
      ],
      axes: getVerticalBarChartAxesProps('Tour', 'TimeHour')
    }
  }
]

export default [firstRow]
