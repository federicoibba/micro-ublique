import { tourEntity } from 'api/entities'
import { getVerticalBarChartAxesProps } from '../../StatsRenderer/helpers'

const firstRow = [
  {
    id: 'saturationPerTour',
    taller: true,
    colSpan: { sm: 4, md: 8, lg: 12 },
    chart: {
      legend: {
        enabled: false
      },
      dataConfig: {
        type: 'composite',
        xKey: tourEntity.NAME,
        yKeys: [tourEntity.SATURATION],
        sortedBy: 'value'
      },
      series: [
        {
          type: 'column',
          xKey: 'label',
          yKeys: ['value']
        }
      ],
      axes: getVerticalBarChartAxesProps('Tour', 'SaturationPercentage', {
        yMin: 0,
        yMax: 1.1
      })
    }
  }
]

export default [firstRow]
