import { tourEntity } from 'api/entities'
import { getVerticalBarChartAxesProps } from '../../StatsRenderer/helpers'

const firstRow = [
  {
    id: 'totalDistancePerTour',
    taller: true,
    colSpan: { sm: 4, md: 8, lg: 12 },
    chart: {
      legend: {
        enabled: false
      },
      dataConfig: {
        type: 'composite',
        xKey: tourEntity.NAME,
        yKeys: [tourEntity.TOTAL_DISTANCE],
        sortedBy: 'value'
      },
      series: [
        {
          type: 'column',
          xKey: 'label',
          yKeys: ['value']
        }
      ],
      axes: getVerticalBarChartAxesProps('Tour', 'Distance')
    }
  }
]

const secondRow = [
  {
    id: 'totalDistancePerVector',
    taller: true,
    colSpan: { sm: 4, md: 8, xlg: 6 },
    chart: {
      legend: {
        enabled: false
      },
      dataConfig: {
        type: 'composite',
        xKey: tourEntity.CARRIER_NAME,
        yKeys: [tourEntity.TOTAL_DISTANCE]
      },
      series: [
        {
          type: 'column',
          xKey: 'label',
          yKeys: ['value']
        }
      ],
      axes: getVerticalBarChartAxesProps('CarrierName', 'Distance')
    }
  },
  {
    id: 'totalDistancePerVehicleClass',
    taller: true,
    colSpan: { sm: 4, md: 8, xlg: 6 },
    chart: {
      legend: {
        enabled: false
      },
      dataConfig: {
        type: 'composite',
        xKey: tourEntity.VEHICLE_CLASS,
        yKeys: [tourEntity.TOTAL_DISTANCE]
      },
      series: [
        {
          type: 'column',
          xKey: 'label',
          yKeys: ['value']
        }
      ],
      axes: getVerticalBarChartAxesProps('VehicleClass', 'Distance')
    }
  }
]

export default [firstRow, secondRow]
