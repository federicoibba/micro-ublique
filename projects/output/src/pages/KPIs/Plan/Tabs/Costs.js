import { tourEntity } from 'api/entities'
import { getVerticalBarChartAxesProps } from '../../StatsRenderer/helpers'

const firstRow = [
  {
    id: 'totalCostPerType',
    colSpan: { sm: 4, md: 8, lg: 3 }
  },
  {
    id: 'costsPerVector',
    taller: true,
    colSpan: { sm: 4, md: 8, lg: 9 },
    chart: {
      legend: {
        enabled: false
      },
      dataConfig: {
        type: 'composite',
        xKey: tourEntity.CARRIER_NAME,
        yKeys: [tourEntity.TOTAL_COST]
      },
      series: [
        {
          type: 'column',
          xKey: 'label',
          yKeys: ['value']
        }
      ],
      axes: getVerticalBarChartAxesProps('CarrierName', 'CostRange')
    }
  }
]

export default [firstRow]
