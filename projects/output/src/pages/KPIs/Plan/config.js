import { planEntity } from 'api/entities'
import { getPadding } from '../StatsRenderer/helpers'

export const I18N_NS = 'KPIs.Plan'

const horizontalBarChart = {
  legend: {
    enabled: false
  },
  padding: getPadding(10),
  series: [
    {
      type: 'bar',
      xKey: 'label',
      yKeys: ['value']
    }
  ],
  axes: [
    {
      type: 'number',
      position: 'bottom',
      label: { color: 'text01' }
    },
    {
      type: 'category',
      position: 'left',
      tick: { width: 0 },
      label: { color: 'text01' }
    }
  ]
}

const firstRow = [
  {
    id: planEntity.TOT_DISTANCE,
    type: 'distance',
    colSpan: { sm: 4, md: 4, lg: 3 }
  },
  {
    id: 'tourDistance',
    type: 'distance',
    colSpan: { sm: 4, md: 4, lg: 3 },
    chart: {
      ...horizontalBarChart,
      dataConfig: {
        yKeys: [
          { id: planEntity.MAX_DISTANCE, label: 'Max' },
          { id: planEntity.AVG_DISTANCE, label: 'Avg' },
          { id: planEntity.MIN_DISTANCE, label: 'Min' }
        ]
      }
    }
  },
  {
    id: planEntity.TOT_COST,
    type: 'currency',
    colSpan: { sm: 4, md: 4, lg: 3 }
  },
  {
    id: 'tourCost',
    type: 'currency',
    colSpan: { sm: 4, md: 4, lg: 3 },
    chart: {
      ...horizontalBarChart,
      dataConfig: {
        yKeys: [
          { id: planEntity.MAX_COST, label: 'Max' },
          { id: planEntity.AVG_COST, label: 'Avg' },
          { id: planEntity.MIN_COST, label: 'Min' }
        ]
      }
    }
  }
]

const secondRow = [
  {
    id: planEntity.AVG_SATURATION,
    type: 'percentage',
    colSpan: { sm: 4, md: 4, lg: 3 }
  },
  {
    id: 'tourSaturation',
    type: 'percentage',
    colSpan: { sm: 4, md: 4, lg: 3 },
    chart: {
      ...horizontalBarChart,
      dataConfig: {
        yKeys: [
          { id: planEntity.MAX_SATURATION, label: 'Max' },
          { id: planEntity.AVG_SATURATION, label: 'Avg' },
          { id: planEntity.MIN_SATURATION, label: 'Min' }
        ]
      }
    }
  },
  {
    id: planEntity.TOT_WORKED_ORDERS,
    colSpan: { sm: 4, md: 4, lg: 3 }
  },
  /* {
    id: 'totOrders',
    colSpan: { sm: 4, md: 4, lg: 3 },
    chart: {
      legend: {
        position: 'bottom',
        color: 'text01'
      },
      padding: getPadding(10),
      dataConfig: {
        yKeys: [planEntity.TOT_WORKED_ORDERS, planEntity.TOT_UNWORKED_ORDERS]
      },
      series: [
        {
          titleType: 'total',
          type: 'pie',
          labelKey: 'label',
          label: { enabled: false },
          angleKey: 'value',
          innerRadiusOffset: -20
        }
      ]
    }
  }, */
  {
    id: 'misc',
    type: 'multiple',
    colSpan: { sm: 4, md: 4, lg: 3 },
    content: [{ id: planEntity.TOT_UNIT_LOAD }, { id: planEntity.TOT_VISITED_POS }]
  }
]

export default [firstRow, secondRow]
