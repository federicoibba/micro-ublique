import { synapse } from 'services/state/synapse'

export const initialState = {
  loading: false
}

export const planKPIsStore = synapse.store(initialState)

export default {
  planKPIs: planKPIsStore
}
