import React from 'react'
import { isEmpty } from 'lodash'
import { useTranslation } from 'react-i18next'
import Loading from '@ublique/components/Loading'
import StatsRenderer from '../StatsRenderer'
import stats, { I18N_NS } from './config'
import { useLocation } from 'react-router'

const ScenarioView = ({ loading, data, isVisible }) => {
  const { t } = useTranslation('OutputView')

  const location = useLocation()
  console.log('🚀 ~ file: index.js ~ line 13 ~ ScenarioView ~ location', location)

  if (loading) return <Loading />

  if (isEmpty(data)) return <p>No data</p>

  return (
    <div className="scenario-kpis-view">
      <div className="tab-header tab-header--scenario">
        <h2>{t('KPIs.HeaderTitle')}</h2>
      </div>
      <StatsRenderer
        config={stats}
        data={data}
        t={t}
        isVisible={isVisible}
        ns={I18N_NS}
      />
    </div>
  )
}

export default React.memo(ScenarioView)
