import { scenarioEntity } from 'api/entities'

export const I18N_NS = 'KPIs.Scenario'

const firstRow = [
  {
    id: scenarioEntity.TOT_DISTANCE,
    type: 'distance',
    colSpan: { sm: 4, md: 4, lg: 3 }
  },
  {
    id: scenarioEntity.TOT_COST,
    type: 'currency',
    colSpan: { sm: 4, md: 4, lg: 3 }
  },
  {
    id: scenarioEntity.AVG_SATURATION,
    type: 'percentage',
    colSpan: { sm: 4, md: 4, lg: 3 }
  },
  {
    id: scenarioEntity.TOT_TOURS,
    colSpan: { sm: 4, md: 4, lg: 3 }
  }
]

const secondRow = [
  {
    id: scenarioEntity.REUSE,
    type: 'percentage',
    colSpan: { sm: 4, md: 8, lg: 6 },
    chart: {
      renderAs: 'extraContent', // Displays the chart on the right of the value
      legend: {
        enabled: false
      },
      dataConfig: {
        yKeys: [scenarioEntity.TOT_TOURS, scenarioEntity.TOT_VEHICLES]
      },
      series: [
        {
          type: 'bar',
          xKey: 'label',
          yKeys: ['value']
        }
      ],
      axes: [
        {
          type: 'number',
          position: 'bottom',
          label: { color: 'text01' }
        },
        {
          type: 'category',
          position: 'left',
          tick: { width: 0 },
          label: { color: 'text01' }
        }
      ]
    }
  },
  {
    id: scenarioEntity.TOT_WORKED_ORDERS,
    colSpan: { sm: 4, md: 4, lg: 3 }
  },
  /* {
    id: 'totOrders',
    colSpan: { sm: 4, md: 4, lg: 3 },
    chart: {
      legend: {
        position: 'bottom',
        color: 'text01'
      },
      dataConfig: {
        yKeys: [
          scenarioEntity.TOT_WORKED_ORDERS,
          scenarioEntity.TOT_UNWORKED_ORDERS
        ]
      },
      padding: getPadding(10),
      series: [
        {
          titleType: 'total',
          type: 'pie',
          labelKey: 'label',
          label: { enabled: false },
          angleKey: 'value',
          innerRadiusOffset: -20
        }
      ]
    }
  }, */
  {
    id: 'misc',
    type: 'multiple',
    colSpan: { sm: 4, md: 4, lg: 3 },
    content: [
      { id: scenarioEntity.TOT_UNIT_LOAD },
      { id: scenarioEntity.TOT_VISITED_POS }
    ]
  }
]

export default [firstRow, secondRow]
