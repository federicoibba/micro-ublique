import React, { useMemo, useState, useRef, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import Tabs from '@ublique/components/Tabs'
import Tab from '@ublique/components/Tabs/Tab'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { scenarioEntity } from 'api/entities'
import SectionHeader from '../../components/SectionHeader'
import ScenarioView from './Scenario'
import PlanView from './Plan'
import './index.scss'
import useScenarioSubscriptions from '../useScenarioSubscriptions'

const getToursSelector = (state) => state.tours

const KPIsComponent = ({ basename }) => {
  useScenarioSubscriptions({ basename })

  const { t } = useTranslation('OutputView')

  const tabsRef = useRef()
  const { scenarioId: scenarioCode } = useParams()

  const { scenario, plans, scenarioLoading, plansLoading, selectedPlanId } =
    useStoreSelector('outputViewCommon')
  const tours = useStoreSelector('toursManagement', getToursSelector)

  const [activeTab, setActiveTab] = useState(0)

  useEffect(() => {
    if (tabsRef.current && !scenarioLoading) {
      tabsRef.current.scrollIntoView()
    }
  }, [scenarioLoading])

  const plansData = plans[selectedPlanId]
  const planToursData = tours[selectedPlanId]

  const planAndToursData = useMemo(
    () => ({ ...plansData, tours: planToursData }),
    [plansData, planToursData]
  )

  const scenarioName = scenario[scenarioEntity.NAME]
    ? `${scenario[scenarioEntity.NAME]} - `
    : ''

  return (
    <div className="KPIs-view">
      <SectionHeader title={`${scenarioName}${t('KPIs.HeaderTitle')}`} />
      <div ref={tabsRef}>
        <Tabs onTabChange={setActiveTab}>
          <Tab title={t('KPIs.ScenarioTab')}>
            <ScenarioView
              id={scenarioCode}
              data={scenario}
              loading={scenarioLoading}
              isVisible={activeTab === 0}
            />
          </Tab>
          <Tab title={t('KPIs.PlanTab')}>
            <PlanView
              data={planAndToursData}
              loading={plansLoading}
              isVisible={activeTab === 1}
              selectedPlanId={selectedPlanId}
            />
          </Tab>
        </Tabs>
      </div>
    </div>
  )
}

export default React.memo(KPIsComponent)
