import { fetchVehicleClassSaturationData } from 'api/KPIs'
import { planKPIsStore, initialState } from './state'

export const getPlanVehicleSaturationData = async (
  planId,
  key = 'vehicleClassSaturation'
) => {
  planKPIsStore.update((state) => ({
    ...state,
    chartData: {
      ...(state.chartData || {}),
      [key]: { loading: true }
    }
  }))
  const response = await fetchVehicleClassSaturationData(planId)
  planKPIsStore.update((state) => ({
    ...state,
    chartData: {
      ...(state.chartData || {}),
      [key]: {
        loading: false,
        data: response
      }
    }
  }))
}

export const resetPlanKPIsState = () => planKPIsStore.set(initialState)
