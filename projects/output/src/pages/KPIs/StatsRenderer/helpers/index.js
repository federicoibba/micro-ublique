import React from 'react'
import { renderToStaticMarkup } from 'react-dom/server'
import { first, isInteger, isNil, map } from 'lodash'
import { getPlanVehicleSaturationData } from '../../actions'
export { makeChartData, createScatterSeries } from './chartDataGenerator'

const typeUnitMap = {
  distance: 'km',
  currency: '€',
  percentage: '%'
}

export const fetchBackendDataMap = {
  saturationPerVehicleClass: getPlanVehicleSaturationData
}

export const getPadding = (padding) => ({
  top: padding,
  right: padding,
  bottom: padding,
  left: padding
})

export const getVerticalBarChartAxesProps = (
  xAxisTitle,
  yAxisTitle,
  { yMin, yMax } = {}
) => [
  {
    type: 'category',
    position: 'bottom',
    tick: { width: 0 },
    label: { color: 'text01' },
    title: { text: xAxisTitle, color: 'text01' }
  },
  {
    type: 'number',
    position: 'left',
    label: { color: 'text01' },
    title: { text: yAxisTitle, color: 'text01' },
    min: yMin,
    max: yMax
  }
]

export const getChartContentPropName = (type) =>
  type === 'extraContent' ? 'extraContent' : 'content'

export const getTitle = (t, text, type) =>
  type ? t(text, { unit: typeUnitMap[type] }) : t(text)

export const getValue = (value, type) => {
  if (isNil(value)) return 'no data'
  const v = type === 'percentage' ? value * 100 : value
  const formattedValue = v.toFixed(isInteger(v) ? 0 : 2)
  return type ? `${formattedValue} ${typeUnitMap[type]}` : formattedValue
}

export const translateAxesTitles = (t, axesData) =>
  map(axesData, (data) => {
    if (!data.title || !data.title?.text) return data
    return {
      ...data,
      title: {
        ...data.title,
        text: t(data.title.text)
      }
    }
  })

export const translateSeries = (t, series) =>
  map(series, (seriesConfig) =>
    seriesConfig.translateKeys
      ? { ...seriesConfig, yKeys: map(seriesConfig.yKeys, t) }
      : seriesConfig
  )

export const makeScatterTooltipRenderer =
  (t) =>
  ({ datum, title }) => {
    const { count, minSaturation, maxSaturation } = datum

    const Component = (
      <div className="scatter-tooltip">
        <div className="scatter-tooltip__header">{title}</div>
        <div className="scatter-tooltip__data">
          <span>
            {t('VehiclesNumber')}: {count}
          </span>
          <span>
            {t('SaturationRange')}: {`${minSaturation}-${maxSaturation}%`}
          </span>
        </div>
      </div>
    )

    return renderToStaticMarkup(Component)
  }

export const isBarChart = ({ series = [] }) => first(series)?.type === 'column'
