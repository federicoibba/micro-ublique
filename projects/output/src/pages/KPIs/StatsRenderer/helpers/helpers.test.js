import {
  getOccurrences,
  getTotalValueOfOccurrences,
  makeDataFromItem,
  makeCompositeDataByOccurrences,
  makeCompositeDataByValueSum
} from './chartDataGenerator'

const t = (x) => x

const mockData = [
  {
    id: 'tour1',
    carrierName: 'Carrier 1',
    vehicleClass: 'Class 1',
    totalDistance: 100,
    totalCost: 1000
  },
  {
    id: 'tour2',
    carrierName: 'Carrier 1',
    vehicleClass: 'Class 1',
    totalDistance: 400,
    totalCost: 1200
  },
  {
    id: 'tour3',
    carrierName: 'Carrier 2',
    vehicleClass: 'Class 1',
    totalDistance: 200,
    totalCost: 1900
  },
  {
    id: 'tour4',
    carrierName: 'Carrier 2',
    vehicleClass: 'Class 2',
    totalDistance: 300,
    totalCost: 3000
  },
  {
    id: 'tour5',
    carrierName: 'Carrier 2',
    vehicleClass: 'Class 2',
    totalDistance: 600,
    totalCost: 1600
  },
  {
    id: 'tour6',
    carrierName: 'Carrier 5',
    vehicleClass: 'Class 4',
    totalDistance: 80,
    totalCost: 2000
  }
]

describe('Tests helpers to create chart data from plain objects', () => {
  it('Tests getOccurrences', () => {
    const result1 = getOccurrences(mockData, 'carrierName')
    const result2 = getOccurrences(mockData, 'vehicleClass')
    expect(result1).toEqual({
      'Carrier 1': 2,
      'Carrier 2': 3,
      'Carrier 5': 1
    })
    expect(result2).toEqual({
      'Class 1': 3,
      'Class 2': 2,
      'Class 4': 1
    })
  })

  it('Tests getTotalValueOfOccurrences with one y key', () => {
    const result1 = getTotalValueOfOccurrences(mockData, 'carrierName', [
      'totalDistance'
    ])
    const result2 = getTotalValueOfOccurrences(mockData, 'vehicleClass', [
      'totalCost'
    ])
    expect(result1).toEqual({
      'Carrier 1': { totalDistance: 500 },
      'Carrier 2': { totalDistance: 1100 },
      'Carrier 5': { totalDistance: 80 }
    })
    expect(result2).toEqual({
      'Class 1': { totalCost: 4100 },
      'Class 2': { totalCost: 4600 },
      'Class 4': { totalCost: 2000 }
    })
  })

  it('Tests getTotalValueOfOccurrences with multiple y keys', () => {
    const result = getTotalValueOfOccurrences(mockData, 'carrierName', [
      'totalDistance',
      'totalCost'
    ])
    expect(result).toEqual({
      'Carrier 1': { totalDistance: 500, totalCost: 2200 },
      'Carrier 2': { totalDistance: 1100, totalCost: 6500 },
      'Carrier 5': { totalDistance: 80, totalCost: 2000 }
    })
  })

  it('Tests makeDataFromItem', () => {
    const result = makeDataFromItem(t, mockData[0], ['totalDistance', 'totalCost'])
    expect(result).toEqual([
      {
        label: 'totalDistance',
        value: 100
      },
      {
        label: 'totalCost',
        value: 1000
      }
    ])
  })

  it('Tests makeCompositeDataByOccurrences', () => {
    const result = makeCompositeDataByOccurrences(t, mockData, {
      xKey: 'carrierName'
    })
    expect(result).toEqual([
      {
        label: 'Carrier 1',
        value: 2
      },
      {
        label: 'Carrier 2',
        value: 3
      },
      {
        label: 'Carrier 5',
        value: 1
      }
    ])
  })

  it('Tests makeCompositeDataByValueSum with one y key', () => {
    const result = makeCompositeDataByValueSum(t, mockData, {
      xKey: 'vehicleClass',
      yKeys: ['totalDistance']
    })
    expect(result).toEqual([
      {
        label: 'Class 1',
        value: 700
      },
      {
        label: 'Class 2',
        value: 900
      },
      {
        label: 'Class 4',
        value: 80
      }
    ])
  })

  it('Tests makeCompositeDataByValueSum with multiple y keys', () => {
    const result = makeCompositeDataByValueSum(t, mockData, {
      xKey: 'vehicleClass',
      yKeys: ['totalDistance', 'totalCost']
    })
    expect(result).toEqual([
      {
        label: 'Class 1',
        totalDistance: 700,
        totalCost: 4100
      },
      {
        label: 'Class 2',
        totalDistance: 900,
        totalCost: 4600
      },
      {
        label: 'Class 4',
        totalDistance: 80,
        totalCost: 2000
      }
    ])
  })
})
