import { map, toPairs, isString, reduce, get, orderBy, isEmpty } from 'lodash'
import { colorTheme } from 'components/Charts/helpers'
import { makeScatterTooltipRenderer } from '.'

const createChartItem = (label, value) => ({ label, value })

const createGroupedChartItem = (label, values) => ({ label, ...values })

export const makeCompositeDataByOccurrences = (_, list, { xKey }) => {
  const occurrences = getOccurrences(list, xKey)
  return map(toPairs(occurrences), ([key, value]) => createChartItem(key, value))
}

export const makeCompositeDataByValueSum = (
  t,
  list,
  { xKey, yKeys, translateKeys }
) => {
  const occurrencesByValue = getTotalValueOfOccurrences(list, xKey, yKeys)

  return map(toPairs(occurrencesByValue), ([key, values]) => {
    // Use generic 'value' as key
    if (yKeys.length === 1) {
      const [value] = Object.values(values)
      return createChartItem(key, value)
    }

    // Use the actual value keys as key
    const translatedValues = translateKeys
      ? reduce(values, (acc, value, key) => ({ ...acc, [t(key)]: value }), {})
      : values
    return createGroupedChartItem(key, translatedValues)
  })
}

export const makeDataFromItem = (t, itemData, yKeys) => {
  return map(yKeys, (item) => {
    const { id, label } = isString(item) ? { id: item, label: item } : item
    return createChartItem(t(label), itemData[id])
  })
}

export const getOccurrences = (list, key) =>
  reduce(
    list,
    (acc, item) => {
      const value = item[key]
      return { ...acc, [value]: get(acc, value, 0) + 1 }
    },
    {}
  )

export const getTotalValueOfOccurrences = (list, key, valueKeys) =>
  reduce(
    list,
    (acc, item) => {
      const value = item[key]
      return reduce(
        valueKeys,
        (itemAcc, valueKey) => ({
          ...itemAcc,
          [value]: {
            ...itemAcc[value],
            [valueKey]:
              get(itemAcc, `${value}.${valueKey}`, 0) + get(item, valueKey, 0)
          }
        }),
        acc
      )
    },
    {}
  )

export const makeChartData = (t, dataConfig, data) => {
  if (!dataConfig) return []

  const { type, translateKeys, yKeys, xKey, sortedBy } = dataConfig

  if (type === 'composite') {
    const makeDataFn = yKeys
      ? makeCompositeDataByValueSum
      : makeCompositeDataByOccurrences

    const chartData = makeDataFn(t, data.tours, { xKey, yKeys, translateKeys })
    const sortKey = translateKeys ? t(sortedBy) : sortedBy
    return sortedBy ? orderBy(chartData, sortKey, 'desc') : chartData
  }

  if (type === 'pieComposite') {
    const compositeData = makeCompositeDataByValueSum(t, data.tours, {
      xKey,
      yKeys
    })

    if (isEmpty(compositeData)) return []

    const [{ label, ...rest }] = compositeData
    return map(toPairs(rest), ([key, value]) => createChartItem(t(key), value))
  }

  return makeDataFromItem(t, data, yKeys)
}

export const createScatterSeries = (t, data) => {
  const highestCount = reduce(data, (acc, { count }) => Math.max(acc, count), 0)
  return map(data, ({ vehicleClass, ...rest }, i) => {
    const color = colorTheme[i % (colorTheme.length - 1)]
    return {
      type: 'scatter',
      title: vehicleClass,
      data: [rest],
      xKey: 'count',
      yKey: 'avgSaturation',
      sizeKey: 'count',
      fill: color,
      stroke: color,
      tooltipRenderer: makeScatterTooltipRenderer(t),
      marker: {
        shape: 'circle',
        size: 200 * (rest.count / highestCount)
      }
    }
  })
}
