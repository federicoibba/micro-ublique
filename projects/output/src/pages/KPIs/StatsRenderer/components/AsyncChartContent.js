import React, { useEffect } from 'react'
import { get } from 'lodash'
import Charts from 'components/Charts'
import { LoaderSmall } from 'components/Loading'
import { planEntity } from 'api/entities'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import {
  translateAxesTitles,
  createScatterSeries,
  fetchBackendDataMap
} from '../helpers'

const AsyncChartContent = ({ t, data, item }) => {
  const { id, chart } = item
  const chartData = useStoreSelector('planKPIs', (state) =>
    get(state, ['chartData', id])
  )

  useEffect(() => {
    const fetchData = fetchBackendDataMap[id]
    if (fetchData && !chartData?.data) {
      fetchData(data[planEntity.ID], id)
    }
  }, [])

  if (chartData?.loading) {
    return (
      <div className="loader-centered">
        <LoaderSmall />
      </div>
    )
  }

  // Only used for scatter chart; it should be made more generic if other charts from backend will be added
  const options = {
    ...chart,
    ...(chart.axes && { axes: translateAxesTitles(t, chart.axes) }),
    series: createScatterSeries(t, chartData?.data || [])
  }

  return <Charts options={options} calcSize />
}

export default AsyncChartContent
