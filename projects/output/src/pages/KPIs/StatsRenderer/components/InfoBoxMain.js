import React, { useMemo, useState } from 'react'
import cn from 'classnames'
import { get } from 'lodash'
import InfoBox from '@ublique/components/InfoBox'
import ArrowsButton from 'pages/CommonFeatures/components/ArrowsButton'
import {
  getTitle,
  getValue,
  getChartContentPropName,
  isBarChart,
  makeChartData
} from '../helpers'
import SyncChartContent from './SyncChartContent'

export const MAX_COLUMNS_ON_X = 16

// Map config to state data and title via id
const InfoBoxMain = ({ t, data, item, className }) => {
  const [offset, setOffset] = useState(0)

  const { id, type, chart } = item
  const maxItems = get(chart, 'dataConfig.maxItems', MAX_COLUMNS_ON_X)
  const value = get(data, id)

  const chartData = useMemo(
    () => (chart ? makeChartData(t, chart.dataConfig, data) : []),
    [t, data]
  )

  const getChartContentProp = () => {
    if (!chart) return {}

    return {
      [getChartContentPropName(chart.renderAs)]: (
        <SyncChartContent
          chartData={chartData}
          maxItems={maxItems}
          offset={offset}
          item={item}
          t={t}
        />
      ),
      headerRightContent:
        isBarChart(chart) && chartData.length > maxItems ? (
          <ArrowsButton
            prevDisabled={offset === 0}
            onPrevClick={() => setOffset((x) => Math.max(0, x - maxItems))}
            onNextClick={() => setOffset((x) => x + maxItems)}
            nextDisabled={offset + maxItems >= chartData.length}
          />
        ) : undefined
    }
  }

  return (
    <InfoBox
      className={cn(className, {
        'extra-content-box': chart?.renderAs === 'extraContent'
      })}
      title={getTitle(t, item.id, type)}
      value={getValue(value, type)}
      {...getChartContentProp()}
    />
  )
}

export default InfoBoxMain
