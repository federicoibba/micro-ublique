import React, { useMemo } from 'react'
import { slice } from 'lodash'
import Charts from 'components/Charts'
import { translateAxesTitles, translateSeries } from '../helpers'

const SyncChartContent = ({ t, chartData, maxItems, item, offset }) => {
  const { chart } = item

  const options = useMemo(
    () => ({
      ...chart,
      ...(chart.axes && { axes: translateAxesTitles(t, chart.axes) }),
      series: translateSeries(t, chart.series)
    }),
    [chart, t]
  )

  const data =
    chartData.length > maxItems
      ? slice(chartData, offset, offset + maxItems)
      : chartData

  return <Charts options={{ ...options, data }} calcSize />
}

export default SyncChartContent
