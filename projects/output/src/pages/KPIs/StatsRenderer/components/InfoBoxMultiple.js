import React from 'react'
import { map, get } from 'lodash'
import { getValue } from '../helpers'
import InfoBox from '@ublique/components/InfoBox'

// Map config to state data and title via id
const InfoBoxMultiple = ({ t, data, item, className }) => {
  const content = map(item.content, ({ id }) => {
    const value = get(data, id)

    return {
      id,
      title: t(id),
      value: getValue(value)
    }
  })

  return <InfoBox.Multiple className={className} content={content} />
}

export default InfoBoxMultiple
