import React from 'react'
import { map } from 'lodash'
import Grid from '@ublique/components/Grid'
import { InfoBoxMain, InfoBoxMultiple } from './components'

const DEFAULT_TRANSLATION_NS = 'KPIs'

const renderStatsItem = (t, data) => (item) => {
  const { id, type, taller, colSpan = {} } = item
  const InfoBoxComponent = type === 'multiple' ? InfoBoxMultiple : InfoBoxMain
  return (
    <Grid.Column key={id} {...colSpan}>
      <InfoBoxComponent
        className={`info-box-item${taller ? '--tall' : ''}`}
        t={t}
        data={data}
        item={item}
      />
    </Grid.Column>
  )
}

const StatsRenderer = ({
  config,
  data,
  t,
  isVisible,
  ns = DEFAULT_TRANSLATION_NS
}) => {
  // Inside hidden tabs charts don't calculate size properly (it's always 0),
  // so the component is mounted only if it is currently visible
  if (!isVisible) return null

  const tWithNs = (string, options) => t(`${ns}.${string}`, options)
  return (
    <Grid fullWidth className="statistics-grid">
      {map(config, (rows, i) => (
        <Grid.Row key={`row-${i}`} className="kpis-row">
          {map(rows, renderStatsItem(tWithNs, data))}
        </Grid.Row>
      ))}
    </Grid>
  )
}

export default React.memo(StatsRenderer)
