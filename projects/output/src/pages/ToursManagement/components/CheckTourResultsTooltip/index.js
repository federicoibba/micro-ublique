import React, { PureComponent } from 'react'
import { useTranslation } from 'react-i18next'
import { includes } from 'lodash'
import { tourEntity } from 'api/entities'
import { getCheckTourResults } from '../../../CommonFeatures/helpers'
import './index.scss'

// Fields that will display messages not related to any other field (key '*')
const fieldsWithAsterisk = [tourEntity.NAME]

const getCheckTourResultsKey = (colId) =>
  includes(fieldsWithAsterisk, colId) ? '*' : colId

const TooltipBody = ({ label, params }) => {
  const { t } = useTranslation('CheckTourResults')
  return (
    <div className="check-tour-results-tooltip__content">
      <p>{t(label, params)}</p>
    </div>
  )
}

export default class CheckTourResultsTooltip extends PureComponent {
  constructor(props) {
    super(props)
    console.log(
      '🚀 ~ file: index.js ~ line 26 ~ CheckTourResultsTooltip ~ constructor ~ props',
      props
    )
    const { api, rowIndex, colDef } = props
    const fieldKey = getCheckTourResultsKey(colDef.colId)
    const { data } = api.getDisplayedRowAtIndex(rowIndex)
    this.results = getCheckTourResults(data, fieldKey)
  }

  getReactContainerClasses() {
    return this.results ? ['check-tour-results-tooltip'] : []
  }

  render() {
    return this.results ? <TooltipBody {...this.results} /> : null
  }
}
