import React, { Component } from 'react'
import { getCheckTourResults } from '../../../CommonFeatures/helpers'
import './index.scss'

class HighlightedCell extends Component {
  render() {
    const { value, valueFormatted, data, column } = this.props
    const checkTourResults = getCheckTourResults(data, column.colId)
    const cellValue = valueFormatted || value || null

    if (!checkTourResults) return cellValue

    return (
      <div className={checkTourResults.type}>
        <span>{cellValue}</span>
      </div>
    )
  }
}

export default HighlightedCell
