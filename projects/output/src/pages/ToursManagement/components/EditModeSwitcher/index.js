import React from 'react'
import Button from '@ublique/components/Button'
import { editModeColumns } from '../../TableConfig/common'
import { toggleEditMode } from '../../actions/table'
import './index.scss'

const toggleVisibility = (api, columnApi, enabled) => {
  editModeColumns.forEach((colId) => {
    columnApi.setColumnVisible(colId, !enabled) // Not necessary, only for the animation
  })
  if (enabled) api.deselectAll()
}

const EditModeSwitcher = ({
  enabled,
  enableText,
  disableText,
  columnApi,
  gridApi
}) => {
  const onClickHandler = () => {
    // Stops table
    gridApi.forEachDetailGridInfo(
      ({ api: detailApi, columnApi: detailColumnApi }) => {
        toggleVisibility(detailApi, detailColumnApi, enabled)
      }
    )
    // Tours table
    toggleVisibility(gridApi, columnApi, enabled)

    toggleEditMode()
  }

  return (
    <Button className="edit-mode-switcher" onClick={onClickHandler}>
      {enabled ? disableText : enableText}
    </Button>
  )
}

export default EditModeSwitcher
