import { get } from 'lodash'
import { ModalActions } from '@ublique/platform'
import { stopEntity } from 'api/entities'

const { openModal } = ModalActions

const getStopTourId = (items) => get(items, `0.${stopEntity.TOUR_ID}`)

export const stopActionsConfig = [
  {
    i18nKey: 'MoveStop',
    onClick: ({ selectedItems }) => {
      openModal('moveToTour', {
        type: 'stop',
        tourId: getStopTourId(selectedItems),
        selectedItems
      })
    }
  },
  {
    i18nKey: 'DeleteStop',
    onClick: ({ selectedItems }) => {
      openModal('deleteEntity', {
        type: 'stop',
        tourId: getStopTourId(selectedItems),
        selectedItems
      })
    }
  }
]
