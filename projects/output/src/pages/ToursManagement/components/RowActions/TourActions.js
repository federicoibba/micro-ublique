// import { get } from 'lodash'
import { ModalActions } from '@ublique/platform'
// import { tourEntity } from 'api/entities'

// const getTourPlanId = (items) => get(items, `0.${tourEntity.PLAN_ID}`)

const { openModal } = ModalActions

export const tourActionsConfig = [
  /*  {
    i18nKey: 'MoveTour',
    onClick: ({ selectedItems }) => {
      openModal('moveToPlan', {
        type: 'tour',
        planId: getTourPlanId(selectedItems),
        selectedItems
      })
    }
  }, */
  {
    i18nKey: 'DeleteTour',
    onClick: ({ selectedItems }) => {
      openModal('deleteEntity', { type: 'tour', selectedItems })
    }
  }
]
