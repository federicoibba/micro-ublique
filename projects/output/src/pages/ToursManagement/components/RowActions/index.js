import makeRowActions from '../../../CommonFeatures/components/RowActions'
import { tourActionsConfig } from './TourActions'
import { stopActionsConfig } from './StopActions'
import { orderActionsConfig } from './OrderActions'

export const TourRowActions = makeRowActions(tourActionsConfig)
export const StopRowActions = makeRowActions(stopActionsConfig)
export const OrderRowActions = makeRowActions(orderActionsConfig)
