import { get } from 'lodash'
import { ModalActions } from '@ublique/platform'
import { orderEntity } from 'api/entities'
import { makeGroundedOrdersNotification } from 'pages/CommonFeatures/notificationCreators'
import { enableUndo } from 'pages/CommonFeatures/actions'
import { putOrdersToGround } from '../../actions/table'

const { openModal } = ModalActions

const getOrderTourId = (items) => get(items, `0.${orderEntity.TOUR_ID}`)
const getOrderStopId = (items) => get(items, `0.${orderEntity.STOP_ID}`)

export const orderActionsConfig = [
  {
    i18nKey: 'GroundOrder',
    onClick: async ({
      t,
      selectedItems,
      allRowsSelected,
      context,
      pushToOutputRoute
    }) => {
      const notification = makeGroundedOrdersNotification(
        t,
        selectedItems.length,
        pushToOutputRoute
      )
      await putOrdersToGround(
        selectedItems,
        context.distributionCenters,
        allRowsSelected,
        notification
      )
      enableUndo()
    }
  },
  {
    i18nKey: 'MoveOrder',
    onClick: ({ selectedItems, allRowsSelected }) =>
      openModal('moveToTour', {
        type: 'order',
        selectedItems,
        allRowsSelected,
        tourId: getOrderTourId(selectedItems),
        stopId: getOrderStopId(selectedItems)
      })
  }
]
