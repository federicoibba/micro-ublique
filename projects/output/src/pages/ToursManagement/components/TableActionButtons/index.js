import React from 'react'
import { saveAs } from 'file-saver'
import { useTranslation } from 'react-i18next'

import Button from '@ublique/components/Button'
import { exportExcel } from 'api/Common'
import EditModeSwitcher from '../EditModeSwitcher'

import './index.scss'

const TableActionButtons = ({
  editModeEnabled,
  showEditModeSwitcher,
  planId,
  scenarioCode,
  ...rest
}) => {
  const { t, i18n } = useTranslation('OutputView')

  const onExport = async () => {
    const { blob, filename } = await exportExcel(scenarioCode, planId, i18n.language)
    saveAs(blob, filename)
  }

  return (
    <div className="table-action-buttons">
      {showEditModeSwitcher && (
        <EditModeSwitcher
          enabled={editModeEnabled}
          enableText={t('ToursManagement.Actions.EnableEditing')}
          disableText={t('ToursManagement.Actions.DisableEditing')}
          {...rest}
        />
      )}
      <Button className="export-button" onClick={onExport}>
        {t('ToursManagement.Actions.Export')}
      </Button>
    </div>
  )
}

export default React.memo(TableActionButtons)
