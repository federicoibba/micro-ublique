import React from 'react'
import './index.scss'

const EmptyPanelData = ({ message }) => (
  <div className="slide-panel-empty-data">
    <h4>{message}</h4>
  </div>
)

export default EmptyPanelData
