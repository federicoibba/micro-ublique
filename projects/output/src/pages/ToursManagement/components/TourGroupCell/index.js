import React from 'react'
import { WarningAlt20 } from '@carbon/icons-react'
import { tourEntity } from 'api/entities'
import './index.scss'

const TourGroupCell = ({ data }) => {
  const { [tourEntity.NAME]: tourName, [tourEntity.HAS_VIOLATIONS]: hasViolations } =
    data

  return (
    <div className="tourId-field">
      <span>{tourName}</span>
      {hasViolations && (
        <span className="warning-icon">
          <WarningAlt20 />
        </span>
      )}
    </div>
  )
}

export default TourGroupCell
