import React from 'react'
import { filter, map } from 'lodash'
import { useTranslation } from 'react-i18next'
import { OverflowMenu, OverflowMenuItem } from '@ublique/components/OverflowMenu'
import withStore from 'utils/hoc/withStore'
import { tourEntity, stopEntity, orderEntity } from 'api/entities'
import { usePushToOutputRoute } from 'pages/CommonFeatures/hooks/usePushToOutputRoute'
import { tourMenuConfig, tourMenuConfigConsolidated } from './TourMenu'
import { stopMenuConfig, stopMenuConfigConsolidated } from './StopMenu'
import { orderMenuConfig } from './OrderMenu'
import './index.scss'

const makeCellRenderer = (items, idKey, connected) => {
  const CellRenderer = ({ data, toursManagement, context, api }) => {
    const { t } = useTranslation('OutputView')
    const pushToOutputRoute = usePushToOutputRoute()

    let filteredItems = items

    if (connected && !toursManagement.editMode) {
      filteredItems = filter(items, 'alwaysVisible')
    }

    return (
      <OverflowMenu flipped className="table-overflow-menu">
        {map(filteredItems, ({ text, i18nKey, onClick }, i) => (
          <OverflowMenuItem
            key={`${data[idKey]}-${i}`}
            onClick={() =>
              onClick({
                t,
                id: data[idKey],
                tourId: data[stopEntity.TOUR_ID] || context?.tourId, // context.tourId & context.stopId are used by orders
                stopId: context?.stopId,
                context,
                record: data,
                gridApi: api,
                pushToOutputRoute
              })
            }
            itemText={i18nKey ? t(`ToursManagement.Actions.${i18nKey}`) : text}
          />
        ))}
      </OverflowMenu>
    )
  }

  return connected ? withStore(CellRenderer, ['toursManagement']) : CellRenderer
}

export const TourOverflowMenu = makeCellRenderer(
  tourMenuConfig,
  tourEntity.ID,
  true // Connected to get updated editMode status
)
export const TourOverflowMenuConsolidated = makeCellRenderer(
  tourMenuConfigConsolidated,
  tourEntity.ID
)

export const StopOverflowMenu = makeCellRenderer(stopMenuConfig, stopEntity.ID, true)
export const StopOverflowMenuConsolidated = makeCellRenderer(
  stopMenuConfigConsolidated,
  stopEntity.ID
)

export const OrderOverflowMenu = makeCellRenderer(orderMenuConfig, orderEntity.ID)
