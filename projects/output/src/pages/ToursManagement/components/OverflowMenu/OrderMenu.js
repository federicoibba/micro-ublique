import { ModalActions } from '@ublique/platform'
import { enableUndo } from 'pages/CommonFeatures/actions'
import { orderEntity } from 'api/entities'
import { makeGroundedOrderNotification } from '../../../CommonFeatures/notificationCreators'
import { putOrdersToGround } from '../../actions/table'

const { openModal } = ModalActions

const isLastItem = (gridApi) => {
  let itemsCount = 0
  gridApi.forEachNode(() => itemsCount++)
  return itemsCount === 1
}

export const orderMenuConfig = [
  {
    i18nKey: 'GroundOrder',
    onClick: async ({ t, record, gridApi, context, pushToOutputRoute }) => {
      const notification = makeGroundedOrderNotification(
        t,
        record[orderEntity.CODE],
        pushToOutputRoute
      )
      await putOrdersToGround(
        record,
        context.distributionCenters,
        isLastItem(gridApi),
        notification
      )
      enableUndo()
    }
  },
  {
    i18nKey: 'MoveOrder',
    onClick: ({ id, record, tourId, gridApi }) => {
      openModal('moveToTour', {
        type: 'order',
        id,
        record,
        tourId,
        allRowsSelected: isLastItem(gridApi)
      })
    }
  },
  {
    i18nKey: 'EditUld',
    onClick: ({ id, stopId, tourId, record }) =>
      openModal('editOrderData', { type: 'order', id, stopId, tourId, record })
  }
  /* {
    i18nKey: 'EditCompartment',
    onClick: () => openModal('editCompartment', {})
  } */
]
