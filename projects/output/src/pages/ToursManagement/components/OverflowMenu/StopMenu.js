import { ModalActions } from '@ublique/platform'
import { openSlidePanel } from '../../../CommonFeatures/actions'

const { openModal } = ModalActions

const seeServiceOrdersItem = {
  i18nKey: 'SeeOrders',
  alwaysVisible: true,
  onClick: ({ id, context, record }) =>
    openSlidePanel('orders', { id, tour: context?.tour, record })
}

export const stopMenuConfigConsolidated = [seeServiceOrdersItem]

export const stopMenuConfig = [
  seeServiceOrdersItem,
  {
    i18nKey: 'MoveStop',
    onClick: ({ id, tourId, record }) =>
      openModal('moveToTour', { type: 'stop', id, tourId, record })
  },
  {
    i18nKey: 'DeleteStop',
    onClick: ({ id, tourId }) =>
      openModal('deleteEntity', { type: 'stop', id, tourId })
  }
]
