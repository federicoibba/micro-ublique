import { ModalActions } from '@ublique/platform'
import { compact } from 'lodash'
import { openSlidePanel } from 'pages/CommonFeatures/actions'
import { ENABLE_COMPARTMENTS } from 'config'

const { openModal } = ModalActions

const showCompartments = {
  i18nKey: 'ShowCompartments',
  alwaysVisible: true,
  onClick: ({ id, record }) => openSlidePanel('compartments', { id, record })
}

export const tourMenuConfigConsolidated = [showCompartments]

export const tourMenuConfig = compact([
  {
    i18nKey: 'DeleteTour',
    onClick: ({ id, record }) =>
      openModal('deleteEntity', { type: 'tour', id, record })
  },
  {
    i18nKey: 'EditTourData',
    onClick: ({ id, record, gridApi }) =>
      openModal('editTourData', { type: 'tour', id, record, gridApi })
  },
  ENABLE_COMPARTMENTS && showCompartments
])
