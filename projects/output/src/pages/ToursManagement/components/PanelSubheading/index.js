import React from 'react'
import './index.scss'

const InfoRow = ({ label, value }) => (
  <p>
    <span className="label">{label}: </span>
    <span className="value">{value}</span>
  </p>
)

const PanelSubheading = ({ items }) => {
  return (
    <div className="panel-subheading">
      {items.map((col, i1) => (
        <div key={`col-${i1}`}>
          {col.map(({ label, value }, i2) => (
            <InfoRow key={`item-${i1}-${i2}`} label={label} value={value} />
          ))}
        </div>
      ))}
    </div>
  )
}

export default PanelSubheading
