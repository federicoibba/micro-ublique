import { makeRemoveTourData } from './table'

jest.mock('state', () => ({
  synapse: {
    store: jest.fn()
  }
}))

const state = {
  editMode: false,
  tours: {
    plan1: [{ tourId: 'tour1-1' }],
    plan2: [{ tourId: 'tour2-1' }]
  },
  stops: {
    'tour1-1': [{ stopId: 'stop1-1-1' }, { stopId: 'stop1-1-2' }],
    'tour2-1': [{ stopId: 'stop2-1-1' }, { stopId: 'stop2-1-2' }]
  },
  orders: {
    'stop1-1-1': [{ id: 'orderX1' }, { id: 'orderX5' }],
    'stop1-1-2': [{ id: 'orderX2' }],
    'stop2-1-1': [{ id: 'orderX3' }],
    'stop2-1-2': [{ id: 'orderX4' }]
  }
}

describe('Tours management actions', () => {
  it('Tests removeTourData', () => {
    const removeTourData = makeRemoveTourData('plan1', 'tour1-1')
    const expected = {
      editMode: false,
      tours: {
        plan2: [{ tourId: 'tour2-1' }]
      },
      stops: {
        'tour2-1': [{ stopId: 'stop2-1-1' }, { stopId: 'stop2-1-2' }]
      },
      orders: {
        'stop2-1-1': [{ id: 'orderX3' }],
        'stop2-1-2': [{ id: 'orderX4' }]
      }
    }
    expect(removeTourData(state)).toEqual(expected)
  })
})
