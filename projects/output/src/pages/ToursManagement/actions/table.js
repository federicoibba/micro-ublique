import {
  sortBy,
  map,
  isArray,
  get,
  forEach,
  isEmpty,
  reduce,
  some,
  includes,
  compact,
  keyBy
} from 'lodash'
import {
  fetchTourStopsList,
  fetchToursList,
  fetchStopOrdersList,
  updateOrder,
  updateStop,
  updateOrdersList,
  fetchPosData,
  fetchPosGroundedSo,
  fetchTour
} from 'api/ToursManagement'
import { updateState } from 'utils'
import { NotificationActions } from '@ublique/platform'
import { initialState, toursManagementStore } from '../state'
import { orderEntity, stopEntity, tourEntity } from 'api/entities'
import { getGroundedOrders } from '../../GroundedOrders/actions'
import {
  createKeysWithDictionary,
  TOUR_VALIDATION_STATUS_KEY
} from '../../CommonFeatures/helpers'
import { outputViewCommonStore } from 'pages/CommonFeatures/state'

const { addNotification } = NotificationActions

export const toggleEditMode = () =>
  toursManagementStore.update((prevState) => ({
    ...prevState,
    editMode: !prevState.editMode
  }))

export const getToursList = async (planId) => {
  updateState(toursManagementStore, { loading: true })
  const response = await fetchToursList(planId)

  const tours = map(response, (tour) => ({
    ...tour,
    // Hardcoded dictionaries for now, for those array fields
    // that will be checked against other arrays (multi select filters)
    ...createKeysWithDictionary(['tourZoneList', 'tourPointOfServiceList'], tour),
    [TOUR_VALIDATION_STATUS_KEY]: tour[tourEntity.HAS_VIOLATIONS]
      ? 'invalid'
      : 'valid'
  }))

  toursManagementStore.update((state) => ({
    ...state,
    loading: false,
    tours: { ...state.tours, [planId]: tours }
  }))

  return tours
}

export const getTourStopsList = async (tourId) => {
  try {
    const response = await fetchTourStopsList(tourId)
    const sortedStops = sortBy(response, 'stopStopNumber')
    toursManagementStore.update((state) => {
      return {
        ...state,
        stops: {
          ...state.stops,
          [tourId]: sortedStops
        }
      }
    })
  } catch ({ status }) {
    console.warn('No stop found')
  }
}

export const getStopOrdersList = async (stopId) => {
  const response = await fetchStopOrdersList(stopId)
  toursManagementStore.update((state) => ({
    ...state,
    orders: {
      ...state.orders,
      [stopId]: response
    }
  }))
}

export const clearStopsAndOrders = () => {
  updateState(toursManagementStore, { stops: {}, orders: {} })
}

const isDepotOrder = (distributionCenters, order) =>
  includes(distributionCenters, order[orderEntity.POINT_OF_SERVICE])

export const putOrdersToGround = async (
  orders,
  distributionCenters,
  allOrders,
  notification
) => {
  const areOrders = isArray(orders)

  const getEntityId = (idKey) =>
    areOrders ? get(orders, `0.${idKey}`) : orders[idKey]

  const ordersList = areOrders ? orders : [orders]

  const planId = getEntityId(orderEntity.PLAN_ID)
  const tourId = getEntityId(orderEntity.TOUR_ID)
  const stopId = getEntityId(orderEntity.STOP_ID)

  const shouldRefetchData =
    allOrders ||
    isEmpty(distributionCenters) ||
    some(ordersList, (order) => isDepotOrder(distributionCenters, order))

  const updatedOrders = map(ordersList, (order) => ({
    ...order,
    [orderEntity.TOUR_ID]: null,
    [orderEntity.STOP_ID]: null
  }))
  if (areOrders) {
    await updateOrdersList(updatedOrders)
  } else {
    const [order] = updatedOrders
    await updateOrder(order[orderEntity.ID], order)
  }

  if (notification) addNotification(notification)

  getGroundedOrders(planId) // Update grounded orders on the other section (asynchronously, don't wait)

  await getStopOrdersList(stopId) // Update orders being viewed

  // Deleting certain types of orders affects tours and stops
  // In those cases, refetch all data
  if (shouldRefetchData) await updateToursAndStops(planId, [tourId])
}

const setTourDetailLoading = (tourId, loading) => {
  toursManagementStore.update((state) => ({
    ...state,
    tourDetailLoading: {
      ...state.tourDetailLoading,
      [tourId]: loading
    }
  }))
}

export const changeStopPosition = async (tourId, stop, newPosition) => {
  if (stop[stopEntity.STOP_NUMBER] === newPosition) return false

  const updatedStop = { ...stop }
  const stopId = stop[stopEntity.ID]
  updatedStop[stopEntity.STOP_NUMBER] = newPosition

  setTourDetailLoading(tourId, true)
  await updateStop(stopId, updatedStop)
  await getTourStopsList(tourId)
  setTourDetailLoading(tourId, false)
  return true
}

export const resetToursManagementState = () => toursManagementStore.set(initialState)

export const makeRemoveTourData = (planId, tourId) => (state) => {
  const newTours = Object.assign({}, state.tours)
  const newStops = Object.assign({}, state.stops)
  const newOrders = Object.assign({}, state.orders)

  delete newTours[planId] // To have updated tour data if the number of stops changes
  forEach(newStops[tourId], ({ [stopEntity.ID]: stopId }) => {
    delete newOrders[stopId]
  })
  delete newStops[tourId]

  return Object.assign({}, state, {
    tours: newTours,
    stops: newStops,
    orders: newOrders
  })
}

// Removes all data related to a tour
export const resetTourData = (planId, tourId) => {
  const removeTourData = makeRemoveTourData(planId, tourId)
  toursManagementStore.update(removeTourData)
}

export const resetServiceOrders = (tourId) => {
  toursManagementStore.update((state) => {
    const newOrders = Object.assign({}, state.orders)
    forEach(state.stops[tourId], ({ [stopEntity.ID]: stopId }) => {
      delete newOrders[stopId]
    })
    return { ...state, orders: newOrders }
  })
}

// Refetch tours and the selected tours stops, if those stops still exist
export const updateToursAndStops = async (planId, tourIds) => {
  const tours = await getToursList(planId)

  const activeTours = []

  for (let i = 0; i < tours.length; i++) {
    const tourId = get(tours, `${i}.${tourEntity.ID}`)

    if (tourIds.includes(tourId)) {
      activeTours.push(tourId)
      if (activeTours.length === tourIds.length) break
    }
  }

  // All tours are invalid, don't fetch their stops
  if (isEmpty(activeTours)) return

  return Promise.all(map(activeTours, getTourStopsList))
}

export const resetToursManagementStateForUndo = (planId, activeTourIds) => {
  toursManagementStore.update((state) => {
    // Only keep stops that are being viewed
    const stops = reduce(
      activeTourIds,
      (acc, tourId) => ({ ...acc, [tourId]: get(state, ['stops', tourId]) }),
      {}
    )
    return {
      ...state,
      tours: { [planId]: get(state, ['tours', planId]) },
      stops,
      orders: {}
    }
  })
}

export const getPosData = async (planId, posName) => {
  const pos = await fetchPosData(planId, posName)
  toursManagementStore.update((state) => ({
    ...state,
    pos: { ...state.pos, [pos.soPosName]: pos }
  }))
}

export const getPosGroundedSoList = async (planId, posName) => {
  const orders = await fetchPosGroundedSo(planId, posName)
  toursManagementStore.update((state) => ({
    ...state,
    orders: { ...state.orders, [posName]: orders }
  }))
}

export const fetchToursResetStopsAndOrders = async (tourIdsArray) => {
  const tourIds = compact(tourIdsArray)
  const planId = outputViewCommonStore.get((state) => state.selectedPlanId)
  const updatedTours = await Promise.all(map(tourIds, fetchTour))
  const updatedToursById = keyBy(updatedTours, tourEntity.ID)

  toursManagementStore.update((state) => {
    const stops = { ...state.stops }
    for (const tourId of tourIds) delete stops[tourId]

    return {
      ...state,
      tours: {
        ...state.tours,
        [planId]: state.tours[planId].map((tour) => {
          const updatedTour = updatedToursById[tour[tourEntity.ID]]
          return updatedTour || tour
        })
      },
      stops,
      orders: {}
    }
  })
}
