import { synapse } from 'services/state/synapse'
import toursColumns from 'config/toursTableColumns.json'
import stopsColumns from 'config/stopsTableColumns.json'
import ordersColumns from 'config/ordersTableColumns.json'

export const initialState = {
  editMode: false,
  columnsConfig: {
    tours: toursColumns,
    stops: stopsColumns,
    orders: ordersColumns
  },
  tours: {},
  stops: {},
  orders: {},
  pos: {},
  tourDetailLoading: {}
}

export const toursManagementStore = synapse.store(initialState)

export default {
  toursManagement: toursManagementStore
}
