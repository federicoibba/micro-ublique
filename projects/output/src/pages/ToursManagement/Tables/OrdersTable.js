import React, { useCallback, useRef, useState } from 'react'
import DataTable from '@ublique/components/DataTable'
import { useColumnDefs } from '../../CommonFeatures/hooks/useColumnDefs'
import { useEntityList } from '../../CommonFeatures/hooks/useEntityList'
import useFilters from '../../CommonFeatures/components/Filters/useFilters'
import { OrderRowActions } from '../components/RowActions'
import {
  getOrdersColumnDefs,
  ORDERS_TABLE_PAGE_SIZE,
  defaultColDef
} from '../TableConfig'
import { frameworkComponents } from '../TableConfig/Orders'

const entities = ['distributionCenters']

const OrdersTable = ({ columnsConfig, editMode, stopId, tourId, tourOrders, t }) => {
  const gridParamsRef = useRef()
  const [selectedRows, setSelectedRows] = useState([])

  const filteredOrders = useFilters(tourOrders, 'order')

  const columnsT = useCallback(
    (x) => t(`ToursManagement.OrdersTableColumnHeaders.${x}`),
    [t]
  )

  const { columnDefs } = useColumnDefs({
    t: columnsT,
    editMode,
    columnsConfig: columnsConfig.orders,
    getColumnDefs: getOrdersColumnDefs
  })

  const { distributionCenters } = useEntityList({ entities, returnData: true })

  const localeTextFunc = useCallback((x) => t(`agGrid:${x}`), [t])

  const onGridReady = (params) => {
    gridParamsRef.current = params
  }

  const shouldShowRowActions =
    (filteredOrders || []).length > 0 && selectedRows.length > 0 && editMode

  return (
    <div className="orders-table pos-relative" data-floating-menu-container>
      {shouldShowRowActions && (
        <OrderRowActions
          onCancel={() => gridParamsRef.current.api.deselectAll()}
          selectedRows={selectedRows}
          allRowsSelected={selectedRows.length === filteredOrders.length}
          context={{ distributionCenters }}
        />
      )}
      <DataTable
        onGridReady={onGridReady}
        columnDefs={columnDefs}
        rowData={filteredOrders}
        onRowsSelect={setSelectedRows}
        defaultColDef={defaultColDef}
        rowSelection="multiple"
        componentWrappingElement="span"
        suppressRowClickSelection
        frameworkComponents={frameworkComponents}
        context={{ tourId, stopId, distributionCenters }}
        pagination
        paginationPageSize={ORDERS_TABLE_PAGE_SIZE}
        localeTextFunc={localeTextFunc}
        autoSizeColumnsOnFirstRender
        suppressColumnVirtualisation
        suppressExcelExport
      />
    </div>
  )
}

export default OrdersTable
