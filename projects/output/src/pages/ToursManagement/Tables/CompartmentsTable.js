import React, { useCallback, useMemo } from 'react'
import DataTable from '@ublique/components/DataTable'
import { memoize } from 'lodash'
import compartmentsColumns from 'config/compartmentsTableColumns.json'
import compartmentOrdersColumns from 'config/compartmentOrdersTableColumns.json'
import { compartmentEntity } from 'api/entities'
import { fetchTourCompartmentOrders } from 'api/ToursManagement'
import { useColumnDefs } from '../../CommonFeatures/hooks/useColumnDefs'
import { COMPARTMENTS_TABLE_PAGE_SIZE, defaultColDef } from '../TableConfig'

const detailGridDefaultCol = {
  suppressMovable: true,
  sortable: true,
  menuTabs: []
}

export const CompartmentsTable = ({ t, language, data = [] }) => {
  const columnsT = useCallback(
    (x) => t(`ToursManagement.CompartmentsTableColumnHeaders.${x}`),
    [t]
  )

  const localeTextFunc = useCallback((x) => t(`agGrid:${x}`), [t])

  const { columnDefs } = useColumnDefs({
    t: columnsT,
    editMode: false,
    columnsConfig: compartmentsColumns
  })

  const { columnDefs: detailColumnDefs } = useColumnDefs({
    t: columnsT,
    editMode: false,
    columnsConfig: compartmentOrdersColumns
  })

  const cachedFetchTourCompartmentOrders = useMemo(
    () => memoize(fetchTourCompartmentOrders),
    []
  )

  const detailCellRendererParams = useMemo(
    () => ({
      autoHeight: true,
      detailGridOptions: {
        columnDefs: detailColumnDefs,
        pagination: true,
        paginationPageSize: COMPARTMENTS_TABLE_PAGE_SIZE,
        defaultColDef: detailGridDefaultCol,
        localeTextFunc
      },
      getDetailRowData: ({ successCallback, data }) => {
        const compartmentId = data[compartmentEntity.ID]
        cachedFetchTourCompartmentOrders(compartmentId).then(successCallback)
      }
    }),
    [detailColumnDefs]
  )

  return (
    <DataTable
      key={language}
      className="compartments-table"
      masterDetail
      detailCellRendererParams={detailCellRendererParams}
      columnDefs={columnDefs}
      rowData={data}
      defaultColDef={defaultColDef}
      rowSelection="multiple"
      componentWrappingElement="span"
      suppressRowClickSelection
      pagination
      paginationPageSize={COMPARTMENTS_TABLE_PAGE_SIZE}
      localeTextFunc={localeTextFunc}
      autoSizeColumnsOnFirstRender
      suppressColumnVirtualisation
      suppressExcelExport
    />
  )
}

export default CompartmentsTable
