import React, { useState, useEffect, useCallback } from 'react'
import { useTranslation } from 'react-i18next'
import DataTable from '@ublique/components/DataTable'
import { TourRowActions } from '../components/RowActions'
import { tourEntity } from 'api/entities'
import { useColumnDefs } from '../../CommonFeatures/hooks/useColumnDefs'
import { useCheckTourSubscription } from '../../CommonFeatures/hooks/useCheckTourSubscription'
import { toggleOpenedTourRows } from '../../CommonFeatures/actions'
import TableActionButtons from '../components/TableActionButtons'
import {
  getToursColumnDefs,
  defaultColDef,
  TOURS_TABLE_PAGE_SIZE
} from '../TableConfig'
import { frameworkComponents } from '../TableConfig/Tours'
import { getToursList } from '../actions/table'
import './index.scss'

const DETAIL_ROW_OFFSET = 200
// const body = document.querySelector('body')

const getRowNodeId = (data) => data[tourEntity.ID]

const getDetailRowHeight = ({ node, data, api }) => {
  if (node && node.detail) {
    const detailRowId = data[tourEntity.ID]
    const { rowHeight, headerHeight } = api.getSizesForCurrentTheme()
    const detailGrid = api.getDetailGridInfo(`detail_${detailRowId}`)
    if (detailGrid) {
      const allDetailRowHeight = detailGrid.api.getDisplayedRowCount() * rowHeight
      return allDetailRowHeight + headerHeight + DETAIL_ROW_OFFSET
    }
  }
}

const ToursTable = ({
  filteredTours,
  columnsConfig,
  editMode,
  selectedPlanId,
  scenarioCode
}) => {
  const { t } = useTranslation('OutputView')

  const columnsT = useCallback(
    (x) => t(`ToursManagement.ToursTableColumnHeaders.${x}`),
    [t]
  )

  const { columnDefs, canEdit } = useColumnDefs({
    t: columnsT,
    editMode,
    columnsConfig,
    getColumnDefs: getToursColumnDefs,
    checkTourCell: true
  })

  const [gridParams, setGridParams] = useState({})
  const [selectedRows, setSelectedRows] = useState([])

  useEffect(() => {
    if (selectedPlanId && !filteredTours) {
      getToursList(selectedPlanId)
    }
  }, [selectedPlanId])

  useCheckTourSubscription({ gridApi: gridParams.api, isTour: true })

  const onRowGroupOpened = useCallback(({ data }) => {
    toggleOpenedTourRows(data[tourEntity.ID])
  }, [])

  const localeTextFunc = useCallback((x) => t(`agGrid:${x}`), [t])

  return (
    <div className="pos-relative" data-floating-menu-container>
      {selectedRows.length > 0 && editMode && (
        <TourRowActions
          onCancel={() => gridParams.api.deselectAll()}
          selectedRows={selectedRows}
        />
      )}
      <DataTable
        immutableData
        getRowNodeId={getRowNodeId}
        onGridReady={setGridParams}
        frameworkComponents={frameworkComponents}
        defaultColDef={defaultColDef}
        columnDefs={columnDefs}
        rowData={filteredTours}
        onRowsSelect={setSelectedRows}
        getRowHeight={getDetailRowHeight}
        // popupParent={body}
        autoSizeColumnsOnFirstRender
        detailCellRenderer="StopsTable"
        rowSelection="multiple"
        componentWrappingElement="span"
        suppressRowClickSelection
        canShowCheckbox
        canExportCSV
        sortable
        animateRows
        masterDetail
        pagination
        paginationPageSize={TOURS_TABLE_PAGE_SIZE}
        localeTextFunc={localeTextFunc}
        onRowGroupOpened={onRowGroupOpened}
        suppressExcelExport
        extraHeaderComponent={
          <TableActionButtons
            showEditModeSwitcher={canEdit}
            editModeEnabled={editMode}
            planId={selectedPlanId}
            scenarioCode={scenarioCode}
          />
        }
      />
    </div>
  )
}

export default React.memo(ToursTable)
