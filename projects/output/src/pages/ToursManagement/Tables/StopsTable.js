import React, { useEffect, useCallback, useState } from 'react'
import { get } from 'lodash'
import { useTranslation } from 'react-i18next'
import { Add24 } from '@carbon/icons-react'
import withStore from 'utils/hoc/withStore'
import { ModalActions } from '@ublique/platform'
import DataTable from '@ublique/components/DataTable'
import Button from '@ublique/components/Button'
import { changeStopPosition, getTourStopsList } from '../actions/table'
import { useColumnDefs } from '../../CommonFeatures/hooks/useColumnDefs'
import { tourEntity, stopEntity } from 'api/entities'
import { defaultColDef } from '../TableConfig'
import { StopRowActions } from '../components/RowActions'
import { getStopColumnDefs, frameworkComponents } from '../TableConfig/Stops'
import useFilters from '../../CommonFeatures/components/Filters/useFilters'
import { enableUndo } from '../../CommonFeatures/actions'
import { useCheckTourSubscription } from '../../CommonFeatures/hooks/useCheckTourSubscription'

const { openModal } = ModalActions

const getRowNodeId = (data) => data[stopEntity.ID]

const StopsTable = ({ node, toursManagement, data, api: masterApi }) => {
  const { stops, editMode, columnsConfig, tourDetailLoading } = toursManagement
  const tourId = data[tourEntity.ID]
  const rowData = get(stops, tourId)

  const { t } = useTranslation('OutputView')
  const [gridParams, setGridParams] = useState()
  const [selectedRows, setSelectedRows] = useState([])

  const filteredStops = useFilters(rowData || [], 'stop')

  const columnsT = useCallback(
    (x) => t(`ToursManagement.StopsTableColumnHeaders.${x}`),
    [t]
  )

  const { columnDefs } = useColumnDefs({
    t: columnsT,
    editMode,
    columnsConfig: columnsConfig.stops,
    getColumnDefs: getStopColumnDefs,
    checkTourCell: true
  })

  useEffect(() => {
    if (!rowData) {
      getTourStopsList(tourId)
    }
    return () => {
      const detailRowId = node.id
      masterApi.removeDetailGridInfo(detailRowId)
    }
  }, [])

  useCheckTourSubscription({ gridApi: gridParams?.api, tourId })

  // Set correct height for master rows after every change in stops length
  // A custom detail cell renderer don't seem to automatically set height based on content
  useEffect(() => {
    const { detailNode } = masterApi.getRowNode(tourId) || {}
    // When gridParams is defined, the detail grid has been registered,
    // so the master can access its properties for height calculation
    if (detailNode && gridParams) {
      detailNode.setRowHeight(null) // Triggers 'getRowHeight' on the master on this row only
      masterApi.onRowHeightChanged()
    }
  }, [filteredStops.length, gridParams])

  useEffect(() => {
    if (gridParams) {
      if (tourDetailLoading[tourId]) {
        gridParams.api.showLoadingOverlay()
      } else {
        gridParams.api.hideOverlay()
      }
    }
  }, [tourDetailLoading[tourId]])

  const onGridReady = useCallback(
    (params) => {
      const detailRowId = node.id
      const gridInfo = {
        id: detailRowId,
        api: params.api,
        columnApi: params.columnApi
      }
      masterApi.addDetailGridInfo(detailRowId, gridInfo)
      // At this point, the detail grid is registered and accessible from the master
      setGridParams(params)
    },
    [masterApi, node.id]
  )

  const changeStopPositionHandler = useCallback(async ({ node, overIndex }) => {
    const hasChanged = await changeStopPosition(
      data[tourEntity.ID],
      node.data,
      overIndex + 1
    )
    if (hasChanged) enableUndo()
  }, [])

  const localeTextFunc = useCallback((x) => t(`agGrid:${x}`), [t])

  const renderActions = () =>
    selectedRows.length > 0 && editMode ? (
      <StopRowActions
        onCancel={() => gridParams.api.deselectAll()}
        selectedRows={selectedRows}
      />
    ) : (
      <div className="action-buttons-row">
        <Button kind="ghost" onClick={() => openModal('addStop', { tourId })}>
          <Add24 />
          {t('ToursManagement.Modals.AddStopTitle')}
        </Button>
      </div>
    )

  return (
    <div className="detail-row-wrapper">
      {editMode && renderActions()}
      <DataTable
        immutableData
        getRowNodeId={getRowNodeId}
        frameworkComponents={frameworkComponents}
        id="detailGrid"
        rowData={filteredStops}
        onGridReady={onGridReady}
        componentWrappingElement="span"
        canSearch={false}
        columnDefs={columnDefs}
        defaultColDef={defaultColDef}
        rowSelection="multiple"
        suppressRowClickSelection
        rowDragManaged
        onRowDragEnd={changeStopPositionHandler}
        onRowsSelect={setSelectedRows}
        localeTextFunc={localeTextFunc}
        autoSizeColumnsOnFirstRender
        suppressExcelExport
        suppressColumnVirtualisation
        context={{ tour: data }}
      />
    </div>
  )
}

export default withStore(React.memo(StopsTable), ['toursManagement'])
