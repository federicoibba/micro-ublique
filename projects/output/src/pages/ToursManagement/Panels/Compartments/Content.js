import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { tourEntity } from 'api/entities'
import { fetchTourCompartments } from 'api/ToursManagement'
import PanelSubheading from '../../components/PanelSubheading'
import CompartmentsTable from '../../Tables/CompartmentsTable'
import EmptyPanelData from '../../components/EmptyPanelData'

const CompartmentsContent = ({ tour }) => {
  const { [tourEntity.ID]: tourId } = tour

  const { t, i18n } = useTranslation('OutputView')
  const [compartmentsData, setCompartmentsData] = useState()

  useEffect(() => {
    fetchTourCompartments(tourId)
      .then(setCompartmentsData)
      .catch(() => {
        setCompartmentsData([])
      })
  }, [tourId])

  const hasNoCompartments = compartmentsData && compartmentsData.length === 0

  const extraInfo = tour
    ? [
        [
          {
            label: t('ToursManagement.ToursTableColumnHeaders.VehicleCode'),
            value: tour[tourEntity.VEHICLE_CODE]
          },
          {
            label: t('ToursManagement.ToursTableColumnHeaders.VehicleClass'),
            value: tour[tourEntity.VEHICLE_CLASS]
          }
        ]
      ]
    : []

  return (
    <>
      <PanelSubheading items={extraInfo} />
      {hasNoCompartments ? (
        <EmptyPanelData message={t('ToursManagement.Modals.NoCompartmentFound')} />
      ) : (
        <CompartmentsTable t={t} language={i18n.language} data={compartmentsData} />
      )}
    </>
  )
}

export default CompartmentsContent
