import React from 'react'
import { tourEntity } from 'api/entities'
import SlidePanel from 'components/SlidePanel'
import { closeSlidePanel } from '../../../CommonFeatures/actions'
import Content from './Content'

const panelStyle = { width: '70%', maxWidth: 1280 }

const CompartmentsPanel = ({ t, opened, data = {} }) => {
  const { record } = data

  return (
    <SlidePanel
      style={panelStyle}
      opened={opened}
      onClose={closeSlidePanel}
      closeText={t('Common:Close')}
      title={t('ToursManagement.Modals.CompartmentsPanelSubtitle')}
      label={
        record
          ? t('ToursManagement.Modals.CompartmentsPanelTitle', {
              tourName: record[tourEntity.NAME]
            })
          : undefined
      }
    >
      <Content tour={record} />
    </SlidePanel>
  )
}

export default React.memo(CompartmentsPanel)
