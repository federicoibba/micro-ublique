import React, { useEffect } from 'react'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { posEntity, tourEntity } from 'api/entities'
import { getStopOrdersList } from '../../actions/table'
import PanelSubheading from '../../components/PanelSubheading'
import EmptyPanelData from '../../components/EmptyPanelData'
import { OrdersTable } from '../../Tables'

const Content = ({ t, tour, stop, stopId }) => {
  const { orders, columnsConfig, editMode } = useStoreSelector('toursManagement')
  const tourOrders = orders[stopId]
  const tourId = tour[tourEntity.ID]
  const posName = stop[posEntity.NAME]
  const posDescription = stop[posEntity.DESCRIPTION]
  const hasNoOrders = tourOrders && tourOrders.length === 0

  useEffect(() => {
    if (!tourOrders) {
      getStopOrdersList(stopId)
    }
  }, [stopId])

  const extraInfo = tour
    ? [
        [
          {
            label: t('ToursManagement.ToursTableColumnHeaders.VehicleCode'),
            value: tour[tourEntity.VEHICLE_CODE]
          },
          {
            label: t('ToursManagement.ToursTableColumnHeaders.VehicleClass'),
            value: tour[tourEntity.VEHICLE_CLASS]
          }
        ],
        [
          {
            label: t('ToursManagement.ToursTableColumnHeaders.DriverName'),
            value: tour[tourEntity.DRIVER_NAME]
          },
          {
            label: t('ToursManagement.StopsTableColumnHeaders.PointOfService'),
            value: `${posName} ${posDescription}`
          }
        ]
      ]
    : []

  return (
    <>
      <PanelSubheading items={extraInfo} />
      {hasNoOrders ? (
        <EmptyPanelData message={t('ToursManagement.Modals.NoOrdersFound')} />
      ) : (
        <OrdersTable
          t={t}
          tourId={tourId}
          stopId={stopId}
          columnsConfig={columnsConfig}
          editMode={editMode}
          tourOrders={tourOrders}
        />
      )}
    </>
  )
}

export default Content
