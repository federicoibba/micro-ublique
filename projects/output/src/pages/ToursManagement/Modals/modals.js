import DeleteEntity from './DeleteEntity'
import EditTourData from './EditTourData'
import MoveToTour from './MoveToTour'
import MoveToPlan from './MoveToPlan'
import EditOrderData from './EditOrderData'
import AddStop from './AddStop'

export default {
  deleteEntity: DeleteEntity,
  editTourData: EditTourData,
  editOrderData: EditOrderData,
  moveToTour: MoveToTour,
  moveToPlan: MoveToPlan,
  addStop: AddStop
}
