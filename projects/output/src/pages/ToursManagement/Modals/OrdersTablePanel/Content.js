import React, { useEffect } from 'react'
import { posEntity, tourEntity } from 'api/entities'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { getStopOrdersList } from '../../actions/table'
import { OrdersTable } from '../../Tables'

const InfoRow = ({ label, value }) => (
  <p>
    <span className="label">{label}: </span>
    <span className="value">{value}</span>
  </p>
)

const NoOrders = ({ t }) => (
  <div className="orders-panel-no-data">
    <h4>{t('ToursManagement.Modals.NoOrdersFound')}</h4>
  </div>
)

const Content = ({ t, tour, stop, stopId }) => {
  const { orders, columnsConfig, editMode } = useStoreSelector('toursManagement')
  const tourOrders = orders[stopId]
  const tourId = tour[tourEntity.ID]
  const posName = stop[posEntity.NAME]
  const posDescription = stop[posEntity.DESCRIPTION]
  const hasNoOrders = tourOrders && tourOrders.length === 0

  useEffect(() => {
    if (!tourOrders) {
      getStopOrdersList(stopId)
    }
  }, [stopId])

  return (
    <>
      {tour && (
        <div className="orders-panel-extra-info">
          <div>
            <InfoRow
              label={t('ToursManagement.ToursTableColumnHeaders.VehicleCode')}
              value={tour[tourEntity.VEHICLE_CODE]}
            />
            <InfoRow
              label={t('ToursManagement.ToursTableColumnHeaders.VehicleClass')}
              value={tour[tourEntity.VEHICLE_CLASS]}
            />
          </div>
          <div>
            <InfoRow
              label={t('ToursManagement.ToursTableColumnHeaders.DriverName')}
              value={tour[tourEntity.DRIVER_NAME]}
            />
            <InfoRow
              label={t('ToursManagement.StopsTableColumnHeaders.PointOfService')}
              value={`${posName} ${posDescription}`}
            />
          </div>
        </div>
      )}
      {hasNoOrders ? (
        <NoOrders t={t} />
      ) : (
        <OrdersTable
          t={t}
          tourId={tourId}
          stopId={stopId}
          columnsConfig={columnsConfig}
          editMode={editMode}
          tourOrders={tourOrders}
        />
      )}
    </>
  )
}

export default Content
