import React from 'react'
import { stopEntity, tourEntity } from 'api/entities'
import SlidePanel from 'components/SlidePanel'
import { closeSlidePanel } from '../../../CommonFeatures/actions'
import Content from './Content'

const panelStyle = { width: '70%', maxWidth: 1280 }

const OrdersTablePanel = ({ t, opened, data = {} }) => {
  const { id, tour, record = {} } = data

  const stopNumber = record[stopEntity.STOP_NUMBER]

  return (
    <SlidePanel
      style={panelStyle}
      opened={opened}
      onClose={closeSlidePanel}
      title={
        stopNumber
          ? t('ToursManagement.Modals.OrdersTableSubtitle', { stopNumber })
          : undefined
      }
      label={
        tour
          ? t('ToursManagement.Modals.OrdersTableTitle', {
              tourName: tour[tourEntity.NAME]
            })
          : undefined
      }
    >
      <Content t={t} stopId={id} tour={tour} stop={record} />
    </SlidePanel>
  )
}

export default React.memo(OrdersTablePanel)
