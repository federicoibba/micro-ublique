import React, { useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { isUndefined, map } from 'lodash'
import Dropdown from '@ublique/components/Dropdown'
import DatePicker from '@ublique/components/DatePicker'
import TimePicker from '@ublique/components/TimePicker'
import Grid from '@ublique/components/Grid'
import { updateTour } from 'api/ToursManagement'
import { tourEntity } from 'api/entities'
import { getCleanedRecord } from '../../../CommonFeatures/helpers'
import { getToursList } from '../../actions/table'
import { useEntityList } from 'pages/CommonFeatures/hooks/useEntityList'
import Modal from 'pages/CommonFeatures/components/DetectUndoModal'
import * as moment from 'moment'

const entities = ['vehicles', 'drivers']

const EditTourData = ({ open, close, data }) => {
  const { t } = useTranslation(['OutputView', 'Common'])

  const [vehicle, setVehicle] = useState()
  const [driver, setDriver] = useState()
  const [loading, setLoading] = useState(false)
  const { id, record, gridApi } = data
  const [tourStartTime, setTourStartTime] = useState()
  const { drivers, vehicles } = useEntityList({ entities, returnData: true })
  const vehiclesList = useMemo(() => map(vehicles, 'vehicleCode'), [vehicles])

  const startDate = moment.utc(record.tourTimeStart)

  const upDateStartTime = (timeValue) => {
    if (isUndefined(timeValue)) return
    const hour = timeValue.split(':')[0]
    const minutes = timeValue.split(':')[1]
    const newStartTime = moment
      .utc(record.tourTimeStart)
      .set({
        hour: hour,
        minutes: minutes
      })
      .format()
    setTourStartTime(newStartTime)
  }

  const updateTourData = async () => {
    const updatedRecord = getCleanedRecord(record)

    if (vehicle) updatedRecord[tourEntity.VEHICLE_CODE] = vehicle
    if (driver) updatedRecord[tourEntity.DRIVER_NAME] = driver
    if (tourStartTime) updatedRecord[tourEntity.TIME_START] = tourStartTime

    setLoading(true)
    await updateTour(id, updatedRecord)
    await getToursList(record[tourEntity.PLAN_ID])
    close()

    // Needed to properly update 'record' after an edit, otherwise the previous record will be passed
    gridApi.refreshCells({ force: true })
  }
  return (
    <Modal
      className="table-action-modal"
      open={open}
      onRequestClose={close}
      secondaryButtonText={t('Common:Cancel')}
      primaryButtonText={t('Common:Confirm')}
      onRequestSubmit={updateTourData}
      modalHeading={t('ToursManagement.Modals.EditTourDataTitle')}
      primaryButtonDisabled={!(vehicle || driver || tourStartTime)}
      confirmLoading={loading}
      confirmLoadingMessage={t('Common:Submitting')}
    >
      <Grid fullWidth>
        <Grid.Row>
          <Grid.Column>
            <Dropdown
              id="vehicle"
              items={vehiclesList}
              titleText={t('ToursManagement.ToursTableColumnHeaders.VehicleCode')}
              label={t('Common:Select')}
              selectedItem={vehicle || record[tourEntity.VEHICLE_CODE]}
              onChange={({ selectedItem }) => setVehicle(selectedItem)}
            />
          </Grid.Column>
          <Grid.Column>
            <Dropdown
              id="driver"
              items={drivers || []}
              titleText={t('ToursManagement.ToursTableColumnHeaders.DriverName')}
              label={t('Common:Select')}
              selectedItem={driver || record[tourEntity.DRIVER_NAME]}
              onChange={({ selectedItem }) => setDriver(selectedItem)}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <DatePicker
              type="simple"
              /*   disable='true'
              dateFormat='d/m/Y' */
            >
              <DatePicker.Input
                labelText={t('ToursManagement.ToursTableModal.DATE')}
                id="date"
                readOnly
                value={startDate.format('DD-MM-YYYY')}
              />
            </DatePicker>
          </Grid.Column>
          <Grid.Column>
            <TimePicker
              id="create-tour-time-picker"
              labelText={t('GroundedOrders.Modals.CreateTourHour')}
              onChange={(e) => {
                upDateStartTime(e.target.value)
              }}
              value={startDate.format('HH:mm')}
              type="time"
              className="timePicker"
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Modal>
  )
}

export default EditTourData
