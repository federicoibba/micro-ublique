import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useRouteMatch } from 'react-router-dom'
import { map } from 'lodash'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import {
  deleteTour,
  deleteStop,
  deleteToursList,
  deleteStopsList
} from 'api/ToursManagement'
import { tourEntity, stopEntity, scenarioEntity } from 'api/entities'
import Modal from 'pages/CommonFeatures/components/DetectUndoModal'
import { getGroundedOrders } from 'pages/GroundedOrders/actions'
import { getScenarioItineraries } from 'pages/GanttAndMap/actions'
import { sections } from 'pages/routes'
import { getToursList, updateToursAndStops } from '../../actions/table'

const entityTypeMap = {
  tour: {
    translations: {
      title: 'DeleteTourTitle',
      message: 'DeleteTourMessage'
    },
    idKey: tourEntity.ID,
    deleteSingle: deleteTour,
    deleteMultiple: deleteToursList
  },
  stop: {
    translations: {
      title: 'DeleteStopTitle',
      message: 'DeleteStopMessage'
    },
    idKey: stopEntity.ID,
    deleteSingle: deleteStop,
    deleteMultiple: deleteStopsList
  }
}

const DeleteEntity = ({ open, close, data }) => {
  const isGanttAndMapView = useRouteMatch({
    path: `/output/:scenarioId/${sections.GANTT_MAP}`,
    strict: true
  })

  const { t } = useTranslation(['OutputView', 'Common'])
  const { selectedPlanId: planId, scenario } = useStoreSelector('outputViewCommon')

  const [loading, setLoading] = useState(false)

  const { type, id, tourId, selectedItems } = data
  const { translations, deleteSingle, deleteMultiple, idKey } = entityTypeMap[type]

  const title = t(`ToursManagement.Modals.${translations.title}`)
  const message = t(`ToursManagement.Modals.${translations.message}`, {
    count: selectedItems ? selectedItems.length : 1
  })

  const reloadDataAndClose = async () => {
    if (isGanttAndMapView) {
      getScenarioItineraries(scenario[scenarioEntity.CODE])
    }
    if (type === 'tour') {
      await getToursList(planId)
    }
    if (type === 'stop') {
      await updateToursAndStops(planId, [tourId])
    }

    getGroundedOrders(planId)
    close()
  }

  const deleteEntityById = async () => {
    setLoading(true)
    await deleteSingle(id)
    await reloadDataAndClose()
  }

  const deleteEntitiesList = async () => {
    setLoading(true)
    await deleteMultiple(map(selectedItems, idKey))
    await reloadDataAndClose()
  }

  return (
    <Modal
      open={open}
      onRequestClose={close}
      confirmLoading={loading}
      confirmLoadingMessage={t('Common:Submitting')}
      secondaryButtonText={t('Common:Cancel')}
      primaryButtonText={t('Common:Confirm')}
      onRequestSubmit={selectedItems ? deleteEntitiesList : deleteEntityById}
      modalHeading={title}
    >
      <p>{message}</p>
    </Modal>
  )
}

export default DeleteEntity
