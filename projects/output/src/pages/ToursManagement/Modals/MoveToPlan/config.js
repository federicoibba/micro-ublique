import { planEntity } from 'api/entities'

export const defaultColumnDef = {
  suppressMovable: true,
  sortable: true,
  suppressMenu: true
}

export const getPlanTableColumnDef = (t) => [
  {
    colId: 'checkbox',
    checkboxSelection: true,
    width: 60,
    maxWidth: 60,
    pinned: 'left'
  },
  {
    headerName: t('PlanId'),
    field: planEntity.NAME
  },
  {
    headerName: t('TotalLoadUnitEquivalent'),
    field: planEntity.TOTAL_LOAD_UNIT_EQ
  },
  {
    headerName: t('TotalPackages'),
    field: planEntity.TOT_PACKAGES
  },
  {
    headerName: t('NumberOfTours'),
    field: planEntity.TOT_TOURS
  },
  {
    headerName: t('NumberOfVehicles'),
    field: planEntity.TOT_VEHICLES
  },
  {
    headerName: t('ReusePercentage'),
    field: planEntity.REUSE
  },
  {
    headerName: t('AverageStopPerTour'),
    field: planEntity.AVG_STOP_PER_TOUR
  },
  {
    headerName: t('Saturation'),
    field: planEntity.AVG_SATURATION
  },
  {
    headerName: t('TotDistance'),
    field: planEntity.TOT_DISTANCE
  }
]
