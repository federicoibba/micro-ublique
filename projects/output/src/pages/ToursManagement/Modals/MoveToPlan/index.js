import React, { useState, useMemo } from 'react'
import { values, map, reject, get } from 'lodash'
import { useTranslation } from 'react-i18next'
import Grid from '@ublique/components/Grid'
import DataTable from '@ublique/components/DataTable'
import withStore from 'utils/hoc/withStore'
import { updateTour, updateToursList } from 'api/ToursManagement'
import { tourEntity, planEntity } from 'api/entities'
import Modal from 'pages/CommonFeatures/components/DetectUndoModal'
import { getToursList } from '../../actions/table'
import { getPlanTableColumnDef, defaultColumnDef } from './config'
import { getCleanedRecord } from '../../../CommonFeatures/helpers'

const MoveToPlan = ({ open, close, data, outputViewCommon }) => {
  const { plans, selectedPlanId } = outputViewCommon
  const { id, selectedItems, record } = data

  const { t } = useTranslation(['OutputView', 'Common'])
  const [planId, setPlanId] = useState()
  const [loading, setLoading] = useState(false)

  const columnDefs = useMemo(() => {
    const headersT = (key) => t(`ToursManagement.ToursTableColumnHeaders.${key}`)
    return getPlanTableColumnDef(headersT)
  }, [t])

  // Exclude current plan from the list
  const availablePlans = useMemo(
    () => reject(values(plans), [planEntity.ID, selectedPlanId]),
    [plans, selectedPlanId]
  )

  const reloadDataAndClose = async () => {
    await Promise.all([getToursList(selectedPlanId), getToursList(planId)])
    close()
  }

  const addSingleToPlan = async () => {
    setLoading(true)

    const updatedRecord = getCleanedRecord(record)
    updatedRecord[tourEntity.PLAN_ID] = planId

    await updateTour(id, updatedRecord)
    await reloadDataAndClose()
  }

  const addMultipleToPlan = async () => {
    setLoading(true)

    const updatedRecords = map(selectedItems, (item) => {
      const cleanedRecord = getCleanedRecord(item)
      cleanedRecord[tourEntity.PLAN_ID] = planId
      return cleanedRecord
    })

    await updateToursList(updatedRecords)
    await reloadDataAndClose()
  }

  return (
    <Modal
      className="table-action-modal plan-selection"
      open={open}
      onRequestClose={close}
      confirmLoading={loading}
      confirmLoadingMessage={t('Common:Submitting')}
      secondaryButtonText={t('Common:Cancel')}
      primaryButtonText={t('Common:Confirm')}
      onRequestSubmit={selectedItems ? addMultipleToPlan : addSingleToPlan}
      modalHeading={t('ToursManagement.Modals.MoveTour', {
        name: selectedItems
          ? get(selectedItems, `0.${tourEntity.NAME}`)
          : record[tourEntity.NAME],
        count: selectedItems ? selectedItems.length : 1
      })}
      primaryButtonDisabled={!planId}
    >
      <Grid fullWidth>
        <Grid.Row>
          <Grid.Column>
            <DataTable
              className="plan-selection-table"
              columnDefs={columnDefs}
              rowData={availablePlans}
              onRowsSelect={([selectedRow]) => setPlanId(selectedRow?.planId)}
              rowSelection="single"
              defaultColDef={defaultColumnDef}
              autoSizeColumnsOnFirstRender
              canSearch={false}
              suppressRowClickSelection
              suppressColumnVirtualisation
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Modal>
  )
}

export default withStore(MoveToPlan, ['outputViewCommon'])
