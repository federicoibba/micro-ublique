import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import Dropdown from '@ublique/components/Dropdown'
import Grid from '@ublique/components/Grid'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { stopEntity } from 'api/entities'
import { createStop } from 'api/ToursManagement'
import { getToursList, getTourStopsList } from '../../actions/table'
import { useEntityList } from 'pages/CommonFeatures/hooks/useEntityList'
import Modal from 'pages/CommonFeatures/components/DetectUndoModal'

const entities = ['distributionCenters']

const AddStop = ({ open, close, data }) => {
  const { t } = useTranslation(['OutputView', 'Common'])

  const { distributionCenters, selectedPlanId } = useStoreSelector('outputViewCommon')

  const [distributionCenter, setDistributionCenter] = useState()
  const [loading, setLoading] = useState(false)

  useEntityList({ entities })

  const { tourId } = data

  const handleSubmit = async () => {
    const newStop = {
      [stopEntity.TOUR_ID]: tourId,
      [stopEntity.POS]: distributionCenter
    }
    setLoading(true)
    await createStop(newStop)
    await getToursList(selectedPlanId)
    await getTourStopsList(tourId)
    close()
  }

  return (
    <Modal
      className="table-action-modal"
      open={open}
      onRequestClose={close}
      secondaryButtonText={t('Common:Cancel')}
      primaryButtonText={t('Common:Confirm')}
      onRequestSubmit={handleSubmit}
      modalHeading={t('ToursManagement.Modals.AddStopTitle')}
      primaryButtonDisabled={!distributionCenter}
      confirmLoading={loading}
      confirmLoadingMessage={t('Common:Submitting')}
    >
      <Grid fullWidth>
        <Grid.Row>
          <Grid.Column lg={6}>
            <Dropdown
              id="distribution-center-selection"
              items={distributionCenters || []}
              titleText={t(
                'ToursManagement.ToursTableColumnHeaders.DistributionCenter'
              )}
              label={t('Common:Select')}
              selectedItem={distributionCenter}
              onChange={({ selectedItem }) => setDistributionCenter(selectedItem)}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Modal>
  )
}

export default AddStop
