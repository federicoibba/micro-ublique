import { filter, sortBy } from 'lodash'
import {
  updateStop,
  updateStopsList,
  updateOrder,
  updateOrdersList
} from 'api/ToursManagement'
import { stopEntity, tourEntity, orderEntity } from 'api/entities'
import { DICTIONARY_SUFFIX } from '../../../CommonFeatures/helpers'

export const entityTypeMap = {
  stop: {
    translations: {
      title: 'MoveStop'
    },
    moveSingle: updateStop,
    moveMultiple: updateStopsList,
    field: stopEntity.TOUR_ID
  },
  order: {
    translations: {
      title: 'MoveOrder'
    },
    moveSingle: updateOrder,
    moveMultiple: updateOrdersList,
    field: orderEntity.TOUR_ID
  }
}

export const getSelectedPlanIdSelector = (state) => state.selectedPlanId

export const getToursSelector = (state) => state.tours

export const itemToString = (item) => (item ? item[tourEntity.NAME] : '')

export const filterToursByPointOfService = (currentTourId, pointOfService, tours) =>
  filter(
    tours,
    ({
      [tourEntity.ID]: tourId,
      [`${tourEntity.POS_LIST}${DICTIONARY_SUFFIX}`]: pointsOfServiceDict
    }) => tourId !== currentTourId && !!pointsOfServiceDict[pointOfService]
  )

export const sortByName = (data) => sortBy(data, tourEntity.NAME)

export const comboBoxFilter = ({ item, inputValue }) => {
  return item[tourEntity.NAME].toLowerCase().includes(inputValue.toLowerCase())
}
