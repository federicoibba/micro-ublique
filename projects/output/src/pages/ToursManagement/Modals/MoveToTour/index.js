import React, { useState, useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { reject, map, get, isEmpty } from 'lodash'
import { ComboBox } from '@ublique/components/Dropdown'
import Grid from '@ublique/components/Grid'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { tourEntity, orderEntity } from 'api/entities'
import Modal from 'pages/CommonFeatures/components/DetectUndoModal'
import {
  getStopOrdersList,
  updateToursAndStops,
  resetServiceOrders
} from '../../actions/table'
import {
  getSelectedPlanIdSelector,
  getToursSelector,
  entityTypeMap,
  itemToString,
  filterToursByPointOfService,
  sortByName,
  comboBoxFilter
} from './helpers'
import './index.scss'

const MoveToTour = ({ open, close, data }) => {
  const planId = useStoreSelector('outputViewCommon', getSelectedPlanIdSelector)
  const tours = useStoreSelector('toursManagement', getToursSelector)

  const { type, id, tourId, stopId, record, selectedItems } = data
  const { translations, moveSingle, moveMultiple, field } = entityTypeMap[type]
  const { [planId]: planTours } = tours

  const { t } = useTranslation(['OutputView', 'Common'])
  const [selectedTour, setSelectedTour] = useState(null)
  const [loading, setLoading] = useState(false)

  // Exclude current stop/order tour
  // For orders, only include tours that contain the selected order's point of service
  const availableTours = useMemo(() => {
    if (type === 'order') {
      const pointOfService = selectedItems
        ? get(selectedItems, `0.${orderEntity.POINT_OF_SERVICE}`)
        : get(record, orderEntity.POINT_OF_SERVICE)
      return sortByName(
        filterToursByPointOfService(tourId, pointOfService, planTours)
      )
    }
    return sortByName(reject(planTours, [tourEntity.ID, tourId]))
  }, [planTours, tourId])

  const selectedTourId = get(selectedTour, tourEntity.ID)

  const reloadDataAndClose = async () => {
    if (type === 'order') {
      await getStopOrdersList(stopId || record[orderEntity.STOP_ID])
      resetServiceOrders(selectedTourId) // Reset all destination orders, to fetch updated data
    }
    await updateToursAndStops(planId, [tourId, selectedTourId])
    close()
  }

  const moveSingleToTour = async () => {
    setLoading(true)
    const updatedRecord = { ...record }
    updatedRecord[field] = selectedTourId
    await moveSingle(id, updatedRecord)
    await reloadDataAndClose()
  }

  const moveMultipleToTour = async () => {
    setLoading(true)
    const updatedRecords = map(selectedItems, (item) => ({
      ...item,
      [field]: selectedTourId
    }))
    await moveMultiple(updatedRecords)
    await reloadDataAndClose()
  }

  const noValidToursFound = type === 'order' && isEmpty(availableTours)

  const renderContent = () => {
    if (noValidToursFound)
      return <p>{t('ToursManagement.Modals.NoToursWithSamePosFound')}</p>

    return (
      <ComboBox
        id="tour"
        items={availableTours}
        titleText={t('ToursManagement.Modals.Tour')}
        itemToString={itemToString}
        placeholder={t('Common:Select')}
        selectedItem={selectedTour}
        onChange={({ selectedItem }) => setSelectedTour(selectedItem)}
        shouldFilterItem={comboBoxFilter}
      />
    )
  }

  return (
    <Modal
      className="table-action-modal"
      open={open}
      onRequestClose={close}
      confirmLoading={loading}
      confirmLoadingMessage={t('Common:Submitting')}
      secondaryButtonText={t('Common:Cancel')}
      primaryButtonText={t('Common:Confirm')}
      onRequestSubmit={selectedItems ? moveMultipleToTour : moveSingleToTour}
      modalHeading={t(`ToursManagement.Modals.${translations.title}`)}
      primaryButtonDisabled={!selectedTourId}
      type="moveToTour"
    >
      <Grid fullWidth>
        <Grid.Row>
          <Grid.Column lg={noValidToursFound ? 12 : 6}>{renderContent()}</Grid.Column>
        </Grid.Row>
      </Grid>
    </Modal>
  )
}

export default MoveToTour
