import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { toNumber } from 'lodash'
import { NumberInput } from '@ublique/components/NumberInput'
import Grid from '@ublique/components/Grid'
import { orderEntity } from 'api/entities'
import { updateOrder } from 'api/ToursManagement'
import Modal from 'pages/CommonFeatures/components/DetectUndoModal'
import {
  getStopOrdersList,
  getToursList,
  getTourStopsList
} from '../../actions/table'

const EditOrderData = ({ open, close, data }) => {
  const { record, id, tourId, stopId } = data

  const { t } = useTranslation(['OutputView', 'Common'])

  const [quantity, setQuantity] = useState(record[orderEntity.QUANTITY])
  const [loading, setLoading] = useState(false)

  const onQuantityChange = ({ target }, dir) => {
    setQuantity((qty) => {
      if (dir) return dir === 'up' ? qty + 1 : qty - 1
      return toNumber(target.value)
    })
  }

  const onSubmit = async () => {
    const updatedRecord = { ...record, id: undefined }
    updatedRecord[orderEntity.QUANTITY] = quantity
    setLoading(true)
    await updateOrder(id, updatedRecord)
    await getStopOrdersList(stopId)
    getToursList(record[orderEntity.PLAN_ID])
    getTourStopsList(tourId)
    close()
  }

  return (
    <Modal
      className="table-action-modal"
      open={open}
      onRequestClose={close}
      secondaryButtonText={t('Common:Cancel')}
      primaryButtonText={t('Common:Confirm')}
      modalHeading={t('ToursManagement.Modals.EditTourDataTitle')}
      confirmLoading={loading}
      confirmLoadingMessage={t('Common:Submitting')}
      primaryButtonDisabled={!quantity}
      onRequestSubmit={onSubmit}
    >
      <Grid fullWidth>
        <Grid.Row>
          <Grid.Column lg={6}>
            <NumberInput
              id="soQuantity"
              min={0}
              value={quantity}
              label={t('ToursManagement.Modals.Quantity')}
              onChange={onQuantityChange}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Modal>
  )
}

export default EditOrderData
