import { ENABLE_COMPARTMENTS } from 'config'
import StopsTable from '../Tables/StopsTable'
import GridDate from '../../CommonFeatures/components/GridDate'
import {
  TourOverflowMenu,
  TourOverflowMenuConsolidated
} from '../components/OverflowMenu'
import HighlightedCell from '../components/HighlightedCell'
import TourGroupCell from '../components/TourGroupCell'
import CheckTourResultsTooltip from '../components/CheckTourResultsTooltip'
import { commonRowActionColDef, commonRowCheckboxColDef } from './common'
import { ACTIONS_COL_ID_VISIBLE } from './constants'

const getConsolidatedColumns = (baseColumns) =>
  ENABLE_COMPARTMENTS
    ? [
        ...baseColumns,
        {
          cellRenderer: 'TourOverflowMenuConsolidated',
          ...commonRowActionColDef,
          colId: ACTIONS_COL_ID_VISIBLE
        }
      ]
    : baseColumns

export const getToursColumnDefs = ({ baseColumnDefs, editMode, canEdit }) => {
  if (!canEdit) return getConsolidatedColumns(baseColumnDefs)

  const fullColumns = [
    {
      hide: !editMode,
      width: 50,
      minWidth: 50,
      ...commonRowCheckboxColDef
    },
    ...baseColumnDefs
  ]

  if (editMode || ENABLE_COMPARTMENTS) {
    fullColumns.push({
      cellRenderer: 'TourOverflowMenu',
      ...commonRowActionColDef,
      colId: ACTIONS_COL_ID_VISIBLE
    })
  }

  return fullColumns
}

export const frameworkComponents = {
  TourOverflowMenu,
  TourOverflowMenuConsolidated,
  StopsTable,
  HighlightedCell,
  CheckTourTooltip: CheckTourResultsTooltip,
  TourGroupCell,
  agDateInput: GridDate
}
