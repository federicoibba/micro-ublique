import { CHECKBOX_COL_ID, ACTIONS_COL_ID } from './constants'

export const editModeColumns = [CHECKBOX_COL_ID, ACTIONS_COL_ID]

export const defaultColDef = {
  flex: 1,
  sortable: true,
  filter: true,
  filterParams: { applyMiniFilterWhileTyping: true },
  animateRows: true,
  floatingFilter: true,
  resizable: true,
  suppressMovable: true,
  menuTabs: ['generalMenuTab', 'filterMenuTab']
}

// Checkbox to select rows
export const commonRowCheckboxColDef = {
  colId: CHECKBOX_COL_ID,
  headerName: '',
  headerCheckboxSelectionFilteredOnly: true,
  checkboxSelection: true,
  headerCheckboxSelection: true,
  suppressMenu: true,
  suppressColumnsToolPanel: true,
  filter: false,
  sortable: false,
  editable: false,
  resizable: false,
  lockPosition: true,
  pinned: 'left'
}

// Overflow menu with row actions
export const commonRowActionColDef = {
  colId: ACTIONS_COL_ID,
  headerName: '',
  width: 70,
  minWidth: 70,
  maxWidth: 70,
  suppressMenu: true,
  suppressColumnsToolPanel: true,
  sortable: false,
  editable: false,
  resizable: false,
  filter: false,
  pinned: 'right'
}
