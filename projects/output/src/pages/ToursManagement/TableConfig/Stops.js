import {
  StopOverflowMenu,
  StopOverflowMenuConsolidated
} from '../components/OverflowMenu'
import HighlightedCell from '../components/HighlightedCell'
import CheckTourResultsTooltip from '../components/CheckTourResultsTooltip'
import GridDate from '../../CommonFeatures/components/GridDate'
import { commonRowCheckboxColDef, commonRowActionColDef } from './common'
import { ACTIONS_COL_ID_VISIBLE } from './constants'

export const getStopColumnDefs = ({ t, baseColumnDefs, editMode, canEdit }) => {
  return !canEdit
    ? [
        ...baseColumnDefs,
        {
          cellRenderer: 'StopOverflowMenuConsolidated',
          ...commonRowActionColDef,
          colId: ACTIONS_COL_ID_VISIBLE
        }
      ]
    : [
        {
          hide: !editMode,
          width: 80,
          maxWidth: 80,
          rowDrag: (params) => !params.node.group,
          rowDragText: () => t('ToursManagement.Actions.MoveStop'),
          ...commonRowCheckboxColDef
        },
        ...baseColumnDefs,
        {
          cellRenderer: 'StopOverflowMenu',
          ...commonRowActionColDef,
          colId: ACTIONS_COL_ID_VISIBLE
        }
      ]
}

export const frameworkComponents = {
  StopOverflowMenu,
  StopOverflowMenuConsolidated,
  agDateInput: GridDate,
  CheckTourTooltip: CheckTourResultsTooltip,
  HighlightedCell
}
