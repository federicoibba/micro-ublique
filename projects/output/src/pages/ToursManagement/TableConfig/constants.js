export const CHECKBOX_COL_ID = 'checkboxCol'
export const ACTIONS_COL_ID = 'actionsCol'
export const ACTIONS_COL_ID_VISIBLE = 'actionsColAlwaysVisible'

export const TOURS_TABLE_PAGE_SIZE = 10
export const ORDERS_TABLE_PAGE_SIZE = 10
export const COMPARTMENTS_TABLE_PAGE_SIZE = 10
