import GridDate from '../../CommonFeatures/components/GridDate'
import { OrderOverflowMenu } from '../components/OverflowMenu'
import { commonRowActionColDef, commonRowCheckboxColDef } from './common'

export const getOrdersColumnDefs = ({ baseColumnDefs, editMode, canEdit }) => {
  if (!canEdit) return baseColumnDefs

  const fullColumns = [
    {
      hide: !editMode,
      width: 60,
      maxWidth: 60,
      ...commonRowCheckboxColDef
    },
    ...baseColumnDefs
  ]

  if (editMode) {
    fullColumns.push({
      hide: !editMode,
      cellRenderer: 'OrderOverflowMenu',
      ...commonRowActionColDef
    })
  }

  return fullColumns
}

export const frameworkComponents = { OrderOverflowMenu, agDateInput: GridDate }
