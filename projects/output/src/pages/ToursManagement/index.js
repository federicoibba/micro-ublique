import React, { useEffect } from 'react'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { useTranslation } from 'react-i18next'
import useFilters from '../CommonFeatures/components/Filters/useFilters'
import ToursManagementLayout from '../CommonFeatures/components/ToursManagementLayout'
import { scenarioEntity } from 'api/entities'
import { resetOpenedTourRows } from '../CommonFeatures/actions'
import { ToursTable } from './Tables'
import OrdersTablePanel from './Panels/OrdersTable'
import CompartmentsPanel from './Panels/Compartments'
import useScenarioSubscriptions from '../useScenarioSubscriptions'

const ToursManagementComponent = ({ basename }) => {
  useScenarioSubscriptions({ basename })
  const { t } = useTranslation('OutputView')

  const { tours, editMode, columnsConfig } = useStoreSelector('toursManagement')
  const {
    slidePanel,
    selectedPlanId,
    scenario: { [scenarioEntity.ID]: scenarioId, [scenarioEntity.CODE]: scenarioCode }
  } = useStoreSelector('outputViewCommon')

  const { opened, data } = slidePanel
  const planTours = tours[selectedPlanId]
  const filteredTours = useFilters(planTours, 'tour')

  useEffect(() => {
    return resetOpenedTourRows
  }, [])

  return (
    <ToursManagementLayout
      t={t}
      tours={planTours}
      filteredTours={filteredTours}
      contentTitle={t('ToursManagement.TableTitle')}
    >
      <ToursTable
        filteredTours={filteredTours}
        columnsConfig={columnsConfig.tours}
        editMode={editMode}
        selectedPlanId={selectedPlanId}
        scenarioId={scenarioId}
        scenarioCode={scenarioCode}
      />
      <OrdersTablePanel t={t} opened={opened === 'orders'} data={data} />
      <CompartmentsPanel t={t} opened={opened === 'compartments'} data={data} />
    </ToursManagementLayout>
  )
}

export default React.memo(ToursManagementComponent)
