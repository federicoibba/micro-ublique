export const sections = {
  KPIS: 'KPIOverview',
  TOURS: 'ToursManagement',
  GANTT_MAP: 'GanttAndMap',
  GROUNDED_ORDERS: 'GroundedOrders'
}

export default []
