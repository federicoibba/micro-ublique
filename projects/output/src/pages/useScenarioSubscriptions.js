import { useEffect } from 'react'
import { isEmpty, map } from 'lodash'
import { useTranslation } from 'react-i18next'
import { matchPath, useHistory, useParams } from 'react-router-dom'
// import { NotificationActions } from '@ublique/platform'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { synapse } from 'services/state'
import { scenarioEntity } from 'api/entities'
import { resetGanttMapState } from './GanttAndMap/actions'
import { CHECK_TOUR_COMPLETED } from './CommonFeatures/hooks/useCheckTourSubscription'
import { resetGroundedOrdersState } from './GroundedOrders/actions'
import { resetToursManagementState } from './ToursManagement/actions/table'
import { makeCheckTourResultNotification } from './CommonFeatures/notificationCreators'
import {
  getInitialUndoState,
  getScenarioData,
  getPlansData,
  resetCommonState,
  resetFilterState,
  resetUndoState,
  updateDataAfterCheckTour
} from './CommonFeatures/actions'

// const { addNotification } = NotificationActions

const REVISION = null // TODO: handle revision, take it from url params or route state

const useScenarioSubscriptions = ({ basename }) => {
  const history = useHistory()
  const { scenarioId: scenarioCode } = useParams()
  const { selectedPlanId, scenario } = useStoreSelector('outputViewCommon')
  const { t } = useTranslation(['OutputView', 'CheckTourResults', 'Common'])

  useEffect(() => {
    // Refetch scenario and plan KPIs every time the scenario object gets emptied
    if (isEmpty(scenario)) {
      getScenarioData(scenarioCode, REVISION).then(
        ({ [scenarioEntity.ID]: scenarioUuid }) => {
          getPlansData(scenarioUuid)
          getInitialUndoState(scenarioUuid)
        }
      )
    }
  }, [scenario])

  useEffect(() => {
    return history.listen(({ pathname }) => {
      const match = matchPath(pathname, {
        path: `${basename}/:scenarioId`,
        exact: false
      })

      if (!match) {
        console.log('Resetting output view state')
        resetCommonState()
        resetFilterState()
        resetFilterState()
        resetGroundedOrdersState()
        resetToursManagementState()
        resetGanttMapState()
        resetUndoState()
      }
    })
  }, [history])

  useEffect(() => {
    if (!selectedPlanId) return

    const unsubscribe = synapse.subscribe('checktour', async ({ data }) => {
      const { type, tours } = JSON.parse(data)

      if (type === 'CHECKTOUR') {
        await updateDataAfterCheckTour()
        synapse.publish(CHECK_TOUR_COMPLETED, tours)
        const n = makeCheckTourResultNotification(t, map(tours, 'tourName'))
        // addNotification(n)
      }
    })

    return unsubscribe
  }, [selectedPlanId, t])

  return null
}

export default useScenarioSubscriptions
