import { keyBy, get, toPairs, reduce, keys, unset, map, reject } from 'lodash'
import { updateState } from 'utils'
import { fetchPlansData, getLastAction } from 'api/ToursManagement'
import { planEntity } from 'api/entities'
import {
  fetchDriversList,
  fetchPointsOfServiceList,
  fetchUnitsOfMeasureList,
  fetchVehicleClassesList,
  fetchVehiclesList,
  fetchDistributionCentersList
} from 'api/Common'
import {
  outputViewCommonStore,
  filtersStore,
  undoStore,
  initialFiltersState,
  commonInitialState,
  initialUndoState
} from './state'
import { fetchScenarioKPIs } from 'api/KPIs'
import {
  resetToursManagementStateForUndo,
  updateToursAndStops
} from '../ToursManagement/actions/table'

export const getScenarioData = async (scenarioId, revision) => {
  updateState(outputViewCommonStore, { scenarioLoading: true })
  try {
    const scenarioData = await fetchScenarioKPIs(scenarioId, revision)
    updateState(outputViewCommonStore, {
      scenarioLoading: false,
      scenario: scenarioData
    })
    return scenarioData
  } catch ({ status }) {
    if (status === 404)
      updateState(outputViewCommonStore, {
        scenarioLoading: false,
        scenario: null
      })
    return {}
  }
}

export const getPlansData = async (scenarioId) => {
  updateState(outputViewCommonStore, { plansLoading: true })
  const plansData = await fetchPlansData(scenarioId)
  updateState(outputViewCommonStore, {
    plansLoading: false,
    plans: keyBy(plansData, planEntity.ID),
    selectedPlanId: get(plansData, `0.${planEntity.ID}`) // Default to the first one
  })
}

export const setSelectedPlanId = (planId) => {
  updateState(outputViewCommonStore, { selectedPlanId: planId })
  resetAllFilters()
}

export const setActiveTab = (tabId) => {
  updateState(outputViewCommonStore, { activeTab: tabId })
}

export const cloneActiveFiltersToTemp = (exclusions = []) => {
  const shouldFilterBeExcluded = (id) => exclusions.includes(id)

  filtersStore.update((state) => {
    const filterPairs = toPairs(state.active)
    const temp = reduce(
      filterPairs,
      (acc, [filterId, filterData]) =>
        shouldFilterBeExcluded(filterId) ? acc : { ...acc, [filterId]: filterData },
      {}
    )
    return { ...state, temp }
  })
}

export const setActiveFilter = (id, value, temp = false) => {
  filtersStore.update((state) => {
    const key = !temp ? 'active' : 'temp'
    return {
      ...state,
      [key]: { ...state[key], [id]: value }
    }
  })
}

export const applyTempFilters = () => {
  filtersStore.update((state) => {
    const active = { ...state.active, ...state.temp }
    for (const filterId in active) {
      if (!active[filterId]) unset(active, filterId)
    }
    return { ...state, active, temp: {} }
  })
}

export const resetTempFilters = () => {
  filtersStore.update((state) => {
    const filterKeys = keys(state.temp)
    const temp = reduce(
      filterKeys,
      (acc, filterId) => ({ ...acc, [filterId]: undefined }),
      {}
    )
    return { ...state, temp }
  })
}

export const resetAllFilters = () => {
  updateState(filtersStore, { active: {}, temp: {} })
}

export const openSlidePanel = (id, data) => {
  updateState(outputViewCommonStore, { slidePanel: { opened: id, data } })
}

export const closeSlidePanel = () => {
  outputViewCommonStore.update((state) => ({
    ...state,
    slidePanel: { ...state.slidePanel, opened: null }
  }))
}

const entitiesToApiMap = {
  drivers: fetchDriversList,
  vehicles: fetchVehiclesList,
  vehicleClasses: fetchVehicleClassesList,
  pointsOfService: fetchPointsOfServiceList,
  unitsOfMeasure: fetchUnitsOfMeasureList,
  distributionCenters: fetchDistributionCentersList
}

export const getEntitiesList = async (scenarioCode, entities) => {
  updateState(outputViewCommonStore, { loading: true })
  const results = await Promise.all(
    map(entities, (entity) => {
      const fetchData = entitiesToApiMap[entity]
      return fetchData(scenarioCode)
    })
  )
  const entitiesLists = reduce(
    entities,
    (acc, entity, i) => ({ ...acc, [entity]: results[i] }),
    {}
  )
  updateState(outputViewCommonStore, { loading: false, ...entitiesLists })
  return entitiesLists
}

export const resetCommonState = () => outputViewCommonStore.set(commonInitialState)

export const resetFilterState = () => filtersStore.set(initialFiltersState)

export const resetOpenedTourRows = () => {
  updateState(undoStore, { openedTourRows: [] })
}

export const toggleOpenedTourRows = (rowId) => {
  undoStore.update((state) => {
    const openedRows = state.openedTourRows
    return {
      ...state,
      openedTourRows: openedRows.includes(rowId)
        ? reject(openedRows, (id) => id === rowId)
        : [...openedRows, rowId]
    }
  })
}

export const enableUndo = () => updateState(undoStore, { hasActionsToUndo: true })

export const disableUndo = () => updateState(undoStore, { hasActionsToUndo: false })

export const resetUndoState = () => undoStore.set(initialUndoState)

export const getInitialUndoState = async (scenarioId) => {
  const { ceRevision } = await getLastAction(scenarioId)
  if (ceRevision >= 2) enableUndo()
}

const resetScenarioAndPlansKPIs = () => {
  updateState(outputViewCommonStore, { plans: {}, scenario: {} })
}

const getOpenedRowsSelector = (state) => state.openedTourRows
const getActivePlanIdSelector = (state) => state.selectedPlanId

export const updateDataAfterCheckTour = async () => {
  const planId = outputViewCommonStore.get(getActivePlanIdSelector)
  const activeTourIds = undoStore.get(getOpenedRowsSelector)
  resetToursManagementStateForUndo(planId, activeTourIds)
  resetScenarioAndPlansKPIs()
  await updateToursAndStops(planId, activeTourIds)
}
