import { useMemo } from 'react'
import { scenarioEntity } from 'api/entities'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { mapColumnsToAgGridColumnDefs } from '../mappers'

const isScenarioDraftSelector = (state) =>
  state.scenario[scenarioEntity.STATUS] === 'DRAFT'

export const useColumnDefs = ({
  t,
  editMode,
  columnsConfig,
  getColumnDefs,
  checkTourCell
}) => {
  const isScenarioDraft = useStoreSelector(
    'outputViewCommon',
    isScenarioDraftSelector
  )

  const baseColumnDefs = useMemo(
    () => mapColumnsToAgGridColumnDefs(t, columnsConfig, checkTourCell),
    [t, columnsConfig]
  )

  const columnDefs = useMemo(
    () =>
      getColumnDefs
        ? getColumnDefs({
            baseColumnDefs,
            canEdit: isScenarioDraft,
            editMode,
            t
          })
        : baseColumnDefs,
    [t, editMode]
  )

  return { columnDefs, canEdit: isScenarioDraft }
}
