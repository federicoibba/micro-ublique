import { useEffect } from 'react'
import { find, map, compact } from 'lodash'
import { synapse } from 'services/state'

export const CHECK_TOUR_COMPLETED = 'checkTourCompleted'

export const useCheckTourSubscription = ({ gridApi, tourId, isTour }) => {
  useEffect(() => {
    if (!gridApi) return

    const unsubscribe = synapse.subscribe(CHECK_TOUR_COMPLETED, (tours) => {
      // Don't refresh stops that don't belong to modified tours
      if (tourId && !find(tours, { tourId })) return

      // Refresh only tours that have been modified by the check tour process
      const rowNodes = isTour
        ? compact(map(tours, ({ tourId }) => gridApi.getRowNode(tourId)))
        : undefined
      gridApi.refreshCells({ force: true, rowNodes })
    })

    return unsubscribe
  }, [gridApi])
}
