import useStoreSelector from 'utils/hooks/useStoreSelector'
import { get } from 'lodash'
import { useMemo } from 'react'
import { useHistory } from 'react-router'

const selectScenarioCode = (state) => get(state, ['scenario', 'scenarioCode'])

export const usePushToOutputRoute = () => {
  const history = useHistory()
  const scenarioCode = useStoreSelector('outputViewCommon', selectScenarioCode)

  const pushToOutputRoute = useMemo(
    () => (path) => history.push(`/output/${scenarioCode}/${path}`),
    [scenarioCode]
  )

  return pushToOutputRoute
}
