import { useEffect, useState } from 'react'
import { isEmpty, reduce, get } from 'lodash'
import { scenarioEntity } from 'api/entities'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { getEntitiesList } from '../actions'

export const useEntityList = ({ scenarioName, entities, returnData = false }) => {
  const outputViewCommon = useStoreSelector('outputViewCommon')

  const [missingEntities, availableEntities] = reduce(
    entities,
    ([missing, available], entity) =>
      outputViewCommon[entity]
        ? [missing, { ...available, [entity]: outputViewCommon[entity] }]
        : [[...missing, entity], available],
    [[], {}]
  )
  const [data, setData] = useState(availableEntities)

  useEffect(() => {
    if (!isEmpty(missingEntities)) {
      const sn =
        scenarioName || get(outputViewCommon, ['scenario', scenarioEntity.NAME])

      if (!sn) return

      getEntitiesList(sn, missingEntities).then((data) => {
        if (returnData) setData(data)
      })
    }
  }, [scenarioName, entities])

  return data
}
