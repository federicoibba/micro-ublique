import moment from 'moment'
import { get, keys, endsWith, keyBy, reduce } from 'lodash'
import { tourEntity, stopEntity, orderEntity } from '../../api/entities'

export const DICTIONARY_SUFFIX = '_Dict'
export const TOUR_VALIDATION_STATUS_KEY = 'tourValidationStatus'

export const createKeysWithDictionary = (keys, record) =>
  reduce(
    keys,
    (acc, key) => ({
      ...acc,
      [`${key}${DICTIONARY_SUFFIX}`]: keyBy(record[key])
    }),
    {}
  )

// Remove extra keys added for frontend logic
export const getCleanedRecord = (record) => {
  const cleanedRecord = { ...record }
  keys(cleanedRecord).forEach((key) => {
    if (endsWith(key, DICTIONARY_SUFFIX)) delete cleanedRecord[key]
  })
  delete cleanedRecord[TOUR_VALIDATION_STATUS_KEY]
  return cleanedRecord
}

export const datePickerLocales = {
  'it-IT': 'it',
  'en-GB': 'en'
}

const getEntityType = (data) => {
  if (data[tourEntity.ID]) return 'tour'
  if (data[stopEntity.ID]) return 'stop'
  return 'order'
}

const checkTourResultsKeys = {
  tour: tourEntity.CHECK_TOUR_RESULTS,
  stop: stopEntity.CHECK_TOUR_RESULTS,
  order: orderEntity.CHECK_TOUR_RESULTS
}

export const getCheckTourResults = (data, fieldId) => {
  const type = getEntityType(data)
  const checkTourResultsKey = checkTourResultsKeys[type]
  return get(data, [checkTourResultsKey, fieldId])
}

// Used for comparisons (isSame, isBetween, etc.) with other dates provided by a date picker
// moment.utc doesn't seem to work very well in those cases
export const getDateWithoutTimezoneOffset = (date) => {
  const momentDate = moment(date)
  const offset = momentDate._tzm
  return momentDate[offset > 0 ? 'subtract' : 'add'](Math.abs(offset), 'minutes')
}
