import React from 'react'
import UndoButton from '../UndoButton'
import PlanSelect from '../PlanSelect'
import './index.scss'

const HeaderActions = () => (
  <div className="header-actions">
    <UndoButton />
    <PlanSelect />
  </div>
)

export default React.memo(HeaderActions)
