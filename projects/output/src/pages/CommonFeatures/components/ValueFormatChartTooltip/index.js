import React from 'react'
import { identity } from 'lodash'
import './index.scss'

const ValueFormatChartTooltip = ({
  title,
  color,
  datum,
  labelKey,
  angleKey,
  valueFormatter = identity
}) => {
  return (
    <div className="custom-chart-tooltip">
      {title && (
        <div className="title" style={{ backgroundColor: color }}>
          {title}
        </div>
      )}
      <div className="value">
        {datum[labelKey]}: {valueFormatter(datum[angleKey])}
      </div>
    </div>
  )
}

export default ValueFormatChartTooltip
