import React from 'react'
import { useTranslation } from 'react-i18next'
import { map } from 'lodash'
import Button from '@ublique/components/Button'
import { usePushToOutputRoute } from '../../hooks/usePushToOutputRoute'
import './index.scss'

const makeRowActions =
  (items) =>
  ({ onCancel, allRowsSelected, context, selectedRows = [] }) => {
    const { t } = useTranslation(['OutputView', 'Common'])
    const pushToOutputRoute = usePushToOutputRoute()

    const actions = onCancel
      ? [...items, { text: t('Common:Cancel'), onClick: onCancel }]
      : items

    return (
      <div className="row-actions">
        <div className="text-box">
          <span id="selectedRows">
            {t('Common:SelectedItem', { count: selectedRows.length })}
          </span>
        </div>
        <div className="button-box">
          {map(actions, ({ i18nKey, onClick, text }, i) => (
            <Button
              key={`button-${i}`}
              kind="ghost"
              onClick={() =>
                onClick({
                  t,
                  selectedItems: selectedRows,
                  allRowsSelected,
                  context,
                  pushToOutputRoute
                })
              }
            >
              {i18nKey ? t(`ToursManagement.Actions.${i18nKey}`) : text}
            </Button>
          ))}
        </div>
      </div>
    )
  }

export default makeRowActions
