import React, { useState, useRef } from 'react'
import { useTranslation } from 'react-i18next'
import { useRouteMatch } from 'react-router-dom'
import cn from 'classnames'
import { Undo32 } from '@carbon/icons-react'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { Button } from '@ublique/components/Button'
import Loading from '@ublique/components/Loading'
import { undoLastAction } from 'api/ToursManagement'
import { scenarioEntity } from 'api/entities'
import { getScenarioItineraries } from 'pages/GanttAndMap/actions'
import { sections } from 'pages/routes'
import { getGroundedOrders } from '../../../GroundedOrders/actions'
import { disableUndo } from '../../actions'
import './index.scss'

const getEditModeSelector = (state) => state.editMode

const LoadingIcon = () => (
  <Loading className="undo-button__loading-icon" withOverlay={false} />
)

const UndoButton = () => {
  const isGanttAndMapView = useRouteMatch({
    path: `/output/:scenarioId/${sections.GANTT_MAP}`,
    strict: true
  })

  const { t } = useTranslation('Common')
  const [loading, setLoading] = useState(false)
  const buttonRef = useRef()

  const { hasActionsToUndo } = useStoreSelector('toursManagementUndo')
  const { scenario, selectedPlanId } = useStoreSelector('outputViewCommon')
  const editMode = useStoreSelector('toursManagement', getEditModeSelector)

  if (scenario[scenarioEntity.STATUS] !== 'DRAFT') return null

  const onUndo = async () => {
    buttonRef.current.blur()
    setLoading(true)
    const { ceRevision } = await undoLastAction(scenario[scenarioEntity.ID])

    getGroundedOrders(selectedPlanId, { reset: true })

    if (isGanttAndMapView) {
      getScenarioItineraries(scenario[scenarioEntity.CODE])
    }

    // No more actions to undo
    if (ceRevision <= 2) disableUndo()

    setLoading(false)
  }

  const disabled = !editMode || !hasActionsToUndo

  return (
    <Button
      ref={buttonRef}
      className={cn('undo-button', { 'undo-button--is-loading': loading })}
      renderIcon={loading ? LoadingIcon : Undo32}
      disabled={disabled}
      onClick={!loading ? onUndo : undefined}
      iconDescription={t('Undo')}
      tooltipPosition="bottom"
      hasIconOnly
    />
  )
}

export default UndoButton
