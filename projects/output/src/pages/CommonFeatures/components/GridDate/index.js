import React, { PureComponent, useCallback, useEffect, useRef } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { useTranslation } from 'react-i18next'
import DatePicker from '@ublique/components/DatePicker'
import { datePickerLocales } from '../../helpers'
import './index.scss'

const DateInput = ({ id, value, onChange, container, gridApi }) => {
  const { t, i18n } = useTranslation('OutputView')
  const ref = useRef()

  const onScroll = useCallback(() => {
    if (ref.current) {
      ref.current.cal.close()
    }
  }, [])

  useEffect(() => {
    const content = document.querySelector('.main-content')
    const picker = ref.current

    if (picker) {
      picker.cal.calendarContainer.classList.add('ag-custom-component-popup')
      gridApi.addEventListener('bodyScroll', onScroll)
      content?.addEventListener('scroll', onScroll)
    }

    return () => {
      gridApi.removeEventListener('bodyScroll', onScroll)
      content?.removeEventListener('scroll', onScroll)
    }
  }, [])

  return (
    <DatePicker
      ref={ref}
      key={`datePicker_lang_${i18n.language}`}
      className="grid-date-picker"
      datePickerType="single"
      dateFormat="d/m/Y"
      value={value}
      onChange={onChange}
      locale={datePickerLocales[i18n.language] || 'it'}
      appendTo={container}
    >
      <DatePicker.Input
        id={id}
        placeholder={t('Common:DatePlaceholder')}
        autoComplete="off"
        labelText="date"
        hideLabel
      />
    </DatePicker>
  )
}

class GridDate extends PureComponent {
  state = { date: null }
  elementId = uuidv4()

  getDate() {
    return this.state.date
  }

  setDate(date) {
    this.setState({ date })
  }

  onDateChanged = ([date]) => {
    const { onDateChanged } = this.props
    this.setState({ date }, onDateChanged)
  }

  render() {
    const { date } = this.state
    const { frameworkComponentWrapper } = this.props
    return (
      <DateInput
        id={this.elementId}
        value={date}
        onChange={this.onDateChanged}
        gridApi={frameworkComponentWrapper.agGridReact.api}
      />
    )
  }
}

export default GridDate
