import React from 'react'
import Grid from '@ublique/components/Grid'
import SectionHeader from 'components/SectionHeader'
import HeaderActions from '../../../CommonFeatures/components/HeaderActions'
import Filters from '../../../CommonFeatures/components/Filters'
import './index.scss'

const ToursManagementLayout = ({
  contentTitle,
  children,
  t,
  tours,
  headerActions = true,
  filteredTours = []
}) => {
  return (
    <div className="tours-management-layout">
      <Grid fullWidth>
        <Grid.Row>
          <Grid.Column>
            <SectionHeader
              title={t('ToursManagement.HeaderTitle')}
              rightComponent={headerActions ? <HeaderActions /> : undefined}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row className="filters-row">
          <Grid.Column>
            <Filters toursList={tours} resultsCount={filteredTours.length} />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <h2 className="section-title">{contentTitle}</h2>
            {children}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}

export default React.memo(ToursManagementLayout)
