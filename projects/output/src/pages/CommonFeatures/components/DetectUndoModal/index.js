import React, { useState } from 'react'
import Modal from '@ublique/components/LoadingModal'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { enableUndo } from '../../actions'
import ErrorMessage from './ErrorMessage'

const hasActionsToUndoSelector = (state) => state.hasActionsToUndo

const DetectUndoModal = ({
  children,
  onRequestSubmit,
  type,
  confirmLoading,
  primaryButtonDisabled,
  ...props
}) => {
  const [error, setError] = useState()

  const hasActionsToUndo = useStoreSelector(
    'toursManagementUndo',
    hasActionsToUndoSelector
  )

  const onSubmit = () => {
    if (onRequestSubmit) {
      onRequestSubmit()
        .then(() => {
          if (!hasActionsToUndo) enableUndo()
        })
        .catch(setError)
    }
  }

  const loading = error ? false : confirmLoading
  const disabled = error ? true : primaryButtonDisabled

  return (
    <Modal
      {...props}
      confirmLoading={loading}
      primaryButtonDisabled={disabled}
      onRequestSubmit={onSubmit}
    >
      {error ? <ErrorMessage error={error} type={type} /> : children}
    </Modal>
  )
}

export default React.memo(DetectUndoModal)
