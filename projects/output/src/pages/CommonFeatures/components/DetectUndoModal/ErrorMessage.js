import React from 'react'
import { Warning32 } from '@carbon/icons-react'
import { useTranslation } from 'react-i18next'
import { get } from 'lodash'

const errorStyle = { marginTop: 20 }

const MoveToTour409Error = ({ t, error }) => (
  <p>
    {t('ToursManagement.Modals.MoveStopToTourError', {
      tourName: error.tourName,
      pointOfService: error.pointOfService,
      serviceCode: error.serviceOrderCode
    })}
  </p>
)

const GenericError = ({ t, error }) => (
  <div>
    <p>{t('ToursManagement.Modals.GenericError')}</p>
    <p style={errorStyle}>
      {error.error} - {error.message}
    </p>
  </div>
)

const messagesByError = {
  moveToTour: {
    409: MoveToTour409Error
  }
}

const ErrorMessage = ({ type, error }) => {
  const { t } = useTranslation('OutputView')

  const ErrorContent = get(messagesByError, [type, error.code]) || GenericError

  return (
    <div className="error-message">
      <span>
        <Warning32 />
      </span>
      <ErrorContent t={t} error={error} />
    </div>
  )
}

export default ErrorMessage
