import React from 'react'
import { useTranslation } from 'react-i18next'
import { ChevronLeft24, ChevronRight24 } from '@carbon/icons-react'
import { Button } from '@ublique/components/Button'
import './index.scss'

const ArrowsButton = ({
  prevDisabled,
  onPrevClick,
  onNextClick,
  nextDisabled,
  prevDescription,
  nextDescription
}) => {
  const { t } = useTranslation('Common')

  return (
    <div className="arrows-button">
      <Button
        kind="tertiary"
        renderIcon={ChevronLeft24}
        onClick={onPrevClick}
        disabled={prevDisabled}
        iconDescription={prevDescription || t('Previous')}
      />
      <Button
        kind="tertiary"
        renderIcon={ChevronRight24}
        onClick={onNextClick}
        disabled={nextDisabled}
        iconDescription={nextDescription || t('Next')}
      />
    </div>
  )
}

export default React.memo(ArrowsButton)
