import React from 'react'
import { get, isArray, map, reduce, set, toPairs } from 'lodash'
import { filterTypesMap } from './components'

const getInvalidTypeComponent = (type) => () =>
  (
    <div>
      Invalid filter type: <strong>{type}</strong>
    </div>
  )

export const makeRenderField =
  (t, currentLanguage, listsData, activeFilters, customProps) =>
  ({ id, type, i18nTitleKey }) => {
    const Component = filterTypesMap[type] || getInvalidTypeComponent(type)
    const { selected } = get(activeFilters, id, {})

    return (
      <Component
        id={id}
        t={t}
        currentLanguage={currentLanguage}
        items={get(listsData, [id, 'items'], [])}
        levels={get(listsData, [id, 'levels'])}
        title={i18nTitleKey ? t(`Filters.${i18nTitleKey}`) : undefined}
        selected={selected}
        {...customProps}
      />
    )
  }

export const getFilterGroups = (config) =>
  reduce(
    config,
    (acc, { quickFilter, ...item }) => {
      const key = quickFilter ? 'quickFilters' : 'advancedFilters'
      return { ...acc, [key]: [...(acc[key] || []), item] }
    },
    {}
  )

export const mapValues = (values) =>
  map(values, ({ from, to, id, label, ...rest }) => ({
    ...rest,
    id: id || label,
    text: label,
    from,
    to
  }))

// Info on how to extract data to populate filter menus
export const createFiltersDataMap = (config) =>
  reduce(
    config,
    (acc, { id, levels, fields, values }) => {
      const data = values
        ? { type: 'values', data: mapValues(values) }
        : { type: 'dataKey', data: id, levels }

      return {
        ...acc,
        ...(fields?.length > 0 && createFiltersDataMap(fields)),
        ...(id && { [id]: data })
      }
    },
    {}
  )

export const getFiltersMenuData = (toursList, filtersDataMap) => {
  const filtersDataPairs = toPairs(filtersDataMap)

  // Hardcoded values from filters config
  const hardcodedLists = reduce(
    filtersDataPairs,
    (acc, [listName, { type, data }]) => {
      return type === 'values' ? { ...acc, [listName]: { items: data } } : acc
    },
    {}
  )

  const processedItems = {}

  // Create lists dynamically gathering values from tour data
  const dynamicLists = reduce(
    toursList,
    (acc, tour) => {
      const newAcc = { ...acc }

      for (const filterDataPair of filtersDataPairs) {
        const [listName, { type, levels, data: dataKey }] = filterDataPair

        if (type === 'values') continue

        const value = tour[dataKey]

        if (!value && levels) {
          newAcc[listName] = { levels }
          continue
        }

        if (value) {
          const values = isArray(value) ? value : [value]

          newAcc[listName] = {
            levels,
            items: [
              ...get(newAcc, [listName, 'items'], []),
              ...reduce(
                values,
                (acc, value) => {
                  // Don't include falsy values or values that are the same
                  if (!value || get(processedItems, [listName, value])) {
                    return acc
                  }
                  set(processedItems, [listName, value], true)
                  return [...acc, { id: value, text: value }]
                },
                []
              )
            ]
          }
        }
      }

      return newAcc
    },
    {}
  )

  return { ...hardcodedLists, ...dynamicLists }
}
