import React from 'react'
import { map } from 'lodash'
import { useTranslation } from 'react-i18next'
import Grid from '@ublique/components/Grid'
import { Section } from './components'
import { makeRenderField } from './helpers'

const renderItem = (t, currentLanguage, listsData, activeFilters) => (item, i) => {
  const { id, fields, i18nTitleKey } = item
  const renderField = makeRenderField(t, currentLanguage, listsData, activeFilters, {
    tempFilters: true
  })

  if (fields) {
    return (
      <Section
        key={`section-${i}`}
        title={i18nTitleKey ? t(`Filters.${i18nTitleKey}`) : undefined}
      >
        <Grid.Row>
          {map(fields, (field) => (
            <Grid.Column key={field.id} md={4}>
              {renderField(field)}
            </Grid.Column>
          ))}
        </Grid.Row>
      </Section>
    )
  }

  return <Grid.Row key={id}>{renderField(item)}</Grid.Row>
}

const AdvancedFilters = ({ config, listsData, activeFilters }) => {
  const { t, i18n } = useTranslation('OutputView')

  return (
    <Grid>{map(config, renderItem(t, i18n.language, listsData, activeFilters))}</Grid>
  )
}

export default React.memo(AdvancedFilters)
