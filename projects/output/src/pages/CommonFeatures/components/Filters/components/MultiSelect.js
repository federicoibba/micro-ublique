import React, { useState, useEffect } from 'react'
import { map } from 'lodash'
import UbliqueMultiSelect from '@ublique/components/MultiSelect'
import { useTranslation } from 'react-i18next'
import { setActiveFilter } from '../../../actions'

const itemToString = (item) => (item ? item.text : '')

const getMultiSelect = ({ filterable } = {}) =>
  React.memo(({ id, title, levels, selected, tempFilters, items = [] }) => {
    const { t } = useTranslation('Common')
    const [wasSelected, setWasSelected] = useState(!selected)

    useEffect(() => {
      if (!wasSelected && selected) {
        setWasSelected(true)
      } else if (wasSelected && !selected) {
        setWasSelected(false)
      }
    }, [selected])

    const setFilter = ({ selectedItems }) => {
      const isReset = selectedItems.length === 0
      const filterData = !isReset
        ? {
            levels,
            type: 'item',
            value: map(selectedItems, 'text'),
            selected: selectedItems
          }
        : undefined
      setActiveFilter(id, filterData, tempFilters)
    }

    const getKey = () => `key-${wasSelected && !selected}`

    const Component = filterable ? UbliqueMultiSelect.Filterable : UbliqueMultiSelect

    return (
      <Component
        key={getKey()}
        id={id}
        titleText={title}
        label={t('Select')}
        placeholder={t('Select')}
        items={items}
        itemToString={itemToString}
        onChange={setFilter}
        initialSelectedItems={selected}
      />
    )
  })

export { getMultiSelect }
