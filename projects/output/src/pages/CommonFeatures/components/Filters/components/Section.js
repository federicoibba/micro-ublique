import React from 'react'
import '../index.scss'

const Section = ({ children, title }) => {
  return (
    <div className="filters-section-header">
      {title && <h3>{title}</h3>}
      {children}
    </div>
  )
}

export default Section
