import React from 'react'
import DatePicker from '@ublique/components/DatePicker'
import { setActiveFilter } from '../../../actions'
import { datePickerLocales } from '../../../helpers'

const DateRange = ({
  t,
  currentLanguage,
  title,
  tempFilters,
  levels,
  selected = []
}) => {
  const setFilter = (dates) => {
    if (dates.length === 2) {
      const filterData = {
        levels,
        type: 'dateRange',
        value: dates,
        selected: dates
      }
      setActiveFilter('dateRange', filterData, tempFilters)
    }
  }

  return (
    <div>
      {title && <span>{title}</span>}
      <DatePicker
        key={`datePicker_lang_${currentLanguage}`}
        datePickerType="range"
        locale={datePickerLocales[currentLanguage] || 'it'}
        dateFormat="d/m/Y"
        onChange={setFilter}
        value={selected}
      >
        <DatePicker.Input
          id="date-picker-input-id-start"
          placeholder={t('Common:DatePlaceholder')}
          labelText={t('Common:StartDate')}
          autoComplete="off"
        />
        <DatePicker.Input
          id="date-picker-input-id-finish"
          placeholder={t('Common:DatePlaceholder')}
          labelText={t('Common:EndDate')}
          autoComplete="off"
        />
      </DatePicker>
    </div>
  )
}

export default React.memo(DateRange)
