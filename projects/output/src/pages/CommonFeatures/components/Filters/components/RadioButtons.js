import React from 'react'
import { isArray, join, map, words } from 'lodash'
import { RadioButtonGroup, RadioButton } from '@ublique/components/RadioButton'
import { setActiveFilter } from '../../../actions'

const RadioButtons = ({ t, id, selected, levels, tempFilters, items = [] }) => {
  const setFilter = (value) => {
    const filterData = {
      levels,
      type: 'item',
      value: words(value),
      selected: value
    }
    setActiveFilter(id, filterData, tempFilters)
  }

  return (
    <RadioButtonGroup
      name="radio-button-group"
      orientation="vertical"
      onChange={setFilter}
      valueSelected={selected}
    >
      {map(items, ({ id, i18nKey, value }) => (
        <RadioButton
          key={id}
          value={isArray(value) ? join(value, ',') : value}
          labelText={t(`Filters.${i18nKey}`)}
        />
      ))}
    </RadioButtonGroup>
  )
}

export default React.memo(RadioButtons)
