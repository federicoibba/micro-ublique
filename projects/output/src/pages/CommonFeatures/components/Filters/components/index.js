import RadioButtons from './RadioButtons'
import DateRange from './DateRange'
import { getMultiSelect } from './MultiSelect'
import { getSelect } from './Select'

export { default as Section } from './Section'

const filterable = { filterable: true }

export const filterTypesMap = {
  dateRange: DateRange,
  select: getSelect(),
  selectFilterable: getSelect(filterable),
  multiSelect: getMultiSelect(),
  multiSelectFilterable: getMultiSelect(filterable),
  radiobuttons: RadioButtons
}
