import React from 'react'
import { useTranslation } from 'react-i18next'
import Dropdown, { ComboBox } from '@ublique/components/Dropdown'
import { setActiveFilter } from '../../../actions'

const itemToString = (item) => (item ? item.text : '')

const getSelect = ({ filterable } = {}) =>
  React.memo(({ title, id, tempFilters, levels, selected = '', items = [] }) => {
    const { t } = useTranslation('Common')

    const setFilter = ({ selectedItem }) => {
      const isReset = selectedItem === null

      if (isReset) {
        setActiveFilter(id, undefined, tempFilters)
      } else {
        const { from, to } = selectedItem
        const isRange = from && to
        const filterData = {
          levels,
          type: isRange ? 'range' : 'item',
          value: isRange ? [from, to] : selectedItem.text,
          selected: selectedItem
        }
        setActiveFilter(id, filterData, tempFilters)
      }
    }

    const Component = filterable ? ComboBox : Dropdown

    return (
      <Component
        id={id}
        items={items}
        titleText={title}
        itemToString={itemToString}
        label={t('Select')}
        placeholder={t('Select')}
        onChange={setFilter}
        selectedItem={selected}
      />
    )
  })

export { getSelect }
