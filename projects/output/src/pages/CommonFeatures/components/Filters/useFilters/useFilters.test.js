import { getFilteredData } from '.'

jest.mock('utils/hooks/useStoreSelector', () => ({
  useStoreSelector: jest.fn()
}))

const mockedTourData = [
  {
    id: 'tour1',
    saturation: 44,
    driver: 'John',
    zones: ['Zone1', 'Zone2', 'Zone3'],
    zones_Dict: { Zone1: 'Zone1', Zone2: 'Zone2', Zone3: 'Zone3' },
    timeStart: '2020-09-10T08:38:50+00:00',
    timeEnd: '2020-09-19T08:38:50+00:00'
  },
  {
    id: 'tour2',
    saturation: 76,
    driver: 'John',
    zones: ['Zone2', 'Zone3'],
    zones_Dict: { Zone2: 'Zone2', Zone3: 'Zone3' },
    timeStart: '2020-09-12T08:38:50+00:00',
    timeEnd: '2020-09-18T08:38:50+00:00'
  },
  {
    id: 'tour3',
    saturation: 88,
    driver: 'Mark',
    zones: ['Zone1', 'Zone2', 'Zone3', 'Zone4'],
    zones_Dict: {
      Zone1: 'Zone1',
      Zone2: 'Zone2',
      Zone3: 'Zone3',
      Zone4: 'Zone4'
    },
    timeStart: '2020-09-03T08:38:50+00:00',
    timeEnd: '2020-09-10T08:38:50+00:00'
  },
  {
    id: 'tour4',
    saturation: 59,
    driver: 'Annie',
    zones: ['Zone3'],
    zones_Dict: { Zone3: 'Zone3' },
    timeStart: '2020-08-21T08:38:50+00:00',
    timeEnd: '2020-09-02T08:38:50+00:00'
  },
  {
    id: 'tour5',
    saturation: 91,
    driver: 'Jacob',
    zones: ['Zone4', 'Zone5'],
    zones_Dict: { Zone4: 'Zone4', Zone5: 'Zone5' },
    timeStart: '2020-08-11T08:38:50+00:00',
    timeEnd: '2020-08-31T08:38:50+00:00'
  },
  {
    id: 'tour6',
    saturation: 74,
    driver: 'Mark',
    zones: ['Zone1', 'Zone3'],
    zones_Dict: { Zone1: 'Zone1', Zone3: 'Zone3' },
    timeStart: '2020-07-29T08:38:50+00:00',
    timeEnd: '2020-08-22T08:38:50+00:00'
  }
]

const mockedStopData = [
  { id: 'stop1', soDriver: 'John', soZone: 'Zone1' },
  { id: 'stop2', soDriver: 'Mark', soZone: 'Zone2' },
  { id: 'stop3', soDriver: 'Mark', soZone: 'Zone3' },
  { id: 'stop4', soDriver: 'Annie', soZone: 'Zone4' },
  { id: 'stop5', soDriver: 'Jacob', soZone: 'Zone5' },
  { id: 'stop6', soDriver: 'John', soZone: 'Zone2' }
]

describe('Tests useFilters hook', () => {
  it('Tests getFilteredData with no filters applied', () => {
    const filters = {}
    expect(getFilteredData('tour', filters, mockedTourData)).toEqual(mockedTourData)
  })

  it('Tests getFilteredData with a single item filter', () => {
    const filters = { driver: { type: 'item', value: 'John' } }
    const result = [mockedTourData[0], mockedTourData[1]]
    expect(getFilteredData('tour', filters, mockedTourData)).toEqual(result)
  })

  it('Tests getFilteredData with a single item filter - target field is an array', () => {
    const filters = { zones: { type: 'item', value: 'Zone2' } }
    const result = [mockedTourData[0], mockedTourData[1], mockedTourData[2]]
    expect(getFilteredData('tour', filters, mockedTourData)).toEqual(result)
  })

  it('Tests getFilteredData with a single item filter - both filter and target field are arrays', () => {
    const filters = { zones: { type: 'item', value: ['Zone5', 'Zone4'] } }
    const result = [mockedTourData[4]]
    expect(getFilteredData('tour', filters, mockedTourData)).toEqual(result)
  })

  it('Tests getFilteredData with a single range filter', () => {
    const filters = {
      saturation: { type: 'range', value: [78, 100] }
    }
    const result = [mockedTourData[2], mockedTourData[4]]
    expect(getFilteredData('tour', filters, mockedTourData)).toEqual(result)
  })

  it('Tests getFilteredData with a single dateRange filter', () => {
    const filters = {
      dateRange: {
        type: 'dateRange',
        value: ['2020-07-01T00:00:00+00:00', '2020-08-31T23:59:59+00:00'],
        levels: { tour: { key: ['timeStart', 'timeEnd'] } }
      }
    }
    const result = [mockedTourData[4], mockedTourData[5]]
    expect(getFilteredData('tour', filters, mockedTourData)).toEqual(result)
  })

  it('Tests getFilteredData with multiple filters combined', () => {
    const filters = {
      dateRange: {
        type: 'dateRange',
        value: ['2020-09-01T00:00:00+00:00', '2020-09-12T23:59:59+00:00'],
        levels: { tour: { key: ['timeStart', 'timeEnd'] } }
      },
      saturation: { type: 'range', value: [60, 90] },
      zones: { type: 'item', value: ['Zone1', 'Zone3'] }
    }
    const result = [mockedTourData[2]]
    expect(getFilteredData('tour', filters, mockedTourData)).toEqual(result)
  })

  it('Tests getFilteredData on stop level with filters that do no apply to stops', () => {
    const filters = {
      driver: { type: 'item', value: 'Mark' },
      zones: { type: 'item', value: ['Zone1', 'Zone3'] }
    }
    expect(getFilteredData('stop', filters, mockedStopData)).toEqual(mockedStopData)
  })

  it('Tests getFilteredData on stop level with filters that do apply to stops', () => {
    const filters = {
      driver: {
        levels: { stop: { key: 'soDriver' } },
        type: 'item',
        value: 'Mark'
      },
      zones: {
        levels: { stop: { key: 'soZone' } },
        type: 'item',
        value: ['Zone1', 'Zone3']
      }
    }
    const result = [mockedStopData[2]]
    expect(getFilteredData('stop', filters, mockedStopData)).toEqual(result)
  })
})
