import { every, includes, isArray, some } from 'lodash'
import { DICTIONARY_SUFFIX, getDateWithoutTimezoneOffset } from '../../../helpers'

const makeIsEqual = (fieldId, filterValue) => (item) => {
  const value = item[fieldId]

  // This should be present in array fields that can be filtered through other arrays, for better performance
  // If not found, revert to an intersection - case not handled, as it should not happen
  const valuesDict = item[`${fieldId}${DICTIONARY_SUFFIX}`]

  // Multi select
  if (isArray(filterValue)) {
    return valuesDict
      ? some(filterValue, (val) => !!valuesDict[val])
      : includes(filterValue, value)
  }

  // Single select
  return valuesDict ? !!valuesDict[filterValue] : value === filterValue
}

const makeIsInRange =
  (fieldId, [start, end]) =>
  (item) => {
    const value = item[fieldId]
    return value >= start && value <= end
  }

const makeIsInDateRange =
  (fieldId, [startDate, endDate]) =>
  (item) => {
    const dateValue = getDateWithoutTimezoneOffset(item[fieldId])
    return dateValue.isBetween(startDate, endDate, 'days', '[]')
  }

// Creates the composite predicate that validates all chosen filters on an item
export const makeFilterItemPredicate = (predicatesList) => (item) =>
  every(predicatesList, (predicateFn) => predicateFn(item))

export const predicateCreatorsMap = {
  item: makeIsEqual,
  range: makeIsInRange,
  dateRange: makeIsInDateRange
}
