import { useMemo } from 'react'
import { filter, isArray, isEmpty, map, reduce, toPairs } from 'lodash'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { predicateCreatorsMap, makeFilterItemPredicate } from './helpers'

const getDefaultFilterLevel = (key) => ({ tour: { key } })

const activeFiltersSelector = (state) => state.active

export const getFilteredData = (level, filters, data) => {
  // List of predicates that will be run on the item fields interested by the selected filters
  const predicates = reduce(
    toPairs(filters),
    (acc, [filterId, filterData]) => {
      if (!filterData) return acc

      const { type, value, levels = getDefaultFilterLevel(filterId) } = filterData

      const validLevel = levels[level]

      // Skip filter, as it does not concern this level
      if (!validLevel) return acc

      const { key } = validLevel
      const makePredicate = predicateCreatorsMap[type]

      const newPredicate = isArray(key)
        ? map(key, (filterId) => makePredicate(filterId, value)) // Same predicate for multiple fields (only used for date range)
        : [makePredicate(key, value)]

      return [...acc, ...newPredicate]
    },
    []
  )

  return filter(data, makeFilterItemPredicate(predicates))
}

const useFilters = (data, level) => {
  const activeFilters = useStoreSelector('filters', activeFiltersSelector)

  const filteredData = useMemo(() => {
    if (isEmpty(activeFilters)) return data

    return getFilteredData(level, activeFilters, data)
  }, [activeFilters, data])

  return filteredData
}

export default useFilters
