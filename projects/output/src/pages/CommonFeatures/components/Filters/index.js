import React, { useMemo } from 'react'
import { isEmpty, keys, map, reject } from 'lodash'
import { useTranslation } from 'react-i18next'
import { Button } from '@ublique/components/Button'
import withStore from 'utils/hoc/withStore'
import { ModalActions } from '@ublique/platform'
import { cloneActiveFiltersToTemp, resetAllFilters } from '../../actions'
import {
  createFiltersDataMap,
  getFiltersMenuData,
  makeRenderField,
  getFilterGroups
} from './helpers'

const { openModal } = ModalActions

const Filters = ({ outputViewCommon, filters, toursList = [], resultsCount = 0 }) => {
  const { filtersConfig } = outputViewCommon
  const { active } = filters

  const { t, i18n } = useTranslation(['OutputView', 'Common'])

  const { quickFilters, advancedFilters, quickFiltersIds } = useMemo(() => {
    const filterGroups = getFilterGroups(filtersConfig)
    const quickFiltersIds = map(filterGroups.quickFilters, 'id')
    return { ...filterGroups, quickFiltersIds }
  }, [filtersConfig])

  const listsData = useMemo(() => {
    const filtersDataMap = createFiltersDataMap(filtersConfig) // TODO: memoize?
    return getFiltersMenuData(toursList, filtersDataMap)
  }, [filtersConfig, toursList])

  const openAdvancedFiltersModal = () => {
    cloneActiveFiltersToTemp(quickFiltersIds)
    openModal('FiltersModal', { config: advancedFilters, listsData })
  }

  const { length: appliedFiltersNum } = reject(keys(active), (key) =>
    quickFiltersIds.includes(key)
  )

  const renderField = makeRenderField(t, i18n.language, listsData, active)

  const noTours = isEmpty(toursList)

  return (
    <div>
      <h4>{t('Filters.FiltersTitle')}</h4>

      <div className="filters-wrapper">
        <div className="quick-filters">
          {map(quickFilters, (item) => (
            <div key={item.id} className="quick-filter-item">
              {renderField(item)}
            </div>
          ))}
        </div>

        <div className="filters-actions">
          <div className="filters-actions__buttons">
            <Button
              kind="primary"
              onClick={openAdvancedFiltersModal}
              disabled={noTours}
            >
              <span>{t('Filters.MoreFilters')}</span>
              {appliedFiltersNum > 0 && (
                <span className="filters-count">({appliedFiltersNum})</span>
              )}
            </Button>

            <Button kind="ghost" onClick={resetAllFilters} disabled={noTours}>
              {t('Common:Reset')}
            </Button>
          </div>

          <div className="results-info">
            <span>{t('Filters.AvailableTour', { count: resultsCount })}</span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default withStore(React.memo(Filters), ['outputViewCommon', 'filters'])
