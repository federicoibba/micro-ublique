import React from 'react'
import { useTranslation } from 'react-i18next'
import Modal from '@ublique/components/Modal'
import withStore from 'utils/hoc/withStore'
import { applyTempFilters, resetTempFilters } from '../../actions'
import AdvancedFilters from './AdvancedFilters'

const FiltersModal = ({ open, filters, close, data }) => {
  const { config, listsData } = data
  const { temp } = filters

  const { t } = useTranslation(['OutputView', 'Common'])

  const onClose = () => {
    close()
    resetTempFilters()
  }

  const onSubmit = () => {
    applyTempFilters()
    close()
  }

  return (
    <Modal
      className="advanced-filters-modal"
      open={open}
      onRequestClose={onClose}
      modalHeading={t('Filters.AdvancedFilters')}
      secondaryButtonText={t('Common:Reset')}
      primaryButtonText={t('Common:Apply')}
      onRequestSubmit={onSubmit}
      onSecondarySubmit={resetTempFilters}
      aria-label="Scrollable filters content"
      hasScrollingContent
    >
      <AdvancedFilters config={config} listsData={listsData} activeFilters={temp} />
    </Modal>
  )
}

export default withStore(FiltersModal, ['filters'])
