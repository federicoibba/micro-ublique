import React, { useMemo } from 'react'
import { sortBy, values } from 'lodash'
import { useTranslation } from 'react-i18next'
import Dropdown from '@ublique/components/Dropdown'
import withStore from 'utils/hoc/withStore'
import { planEntity } from 'api/entities'
import {
  resetOpenedTourRows,
  setSelectedPlanId
} from '../../../CommonFeatures/actions'

const itemToString = (item) => item[planEntity.NAME]

const onChange = ({ selectedItem }) => {
  setSelectedPlanId(selectedItem[planEntity.ID])
  resetOpenedTourRows()
}

const PlanSelect = ({ outputViewCommon }) => {
  const { selectedPlanId, plans = {} } = outputViewCommon

  const { t } = useTranslation('OutputView')
  const plansArray = useMemo(() => sortBy(values(plans), planEntity.NAME), [plans])

  return (
    <Dropdown
      id="plans-list"
      items={plansArray}
      selectedItem={plans[selectedPlanId]}
      itemToString={itemToString}
      onChange={onChange}
      titleText={t('KPIs.PlanTab')}
      label=""
    />
  )
}

export default withStore(React.memo(PlanSelect), ['outputViewCommon'])
