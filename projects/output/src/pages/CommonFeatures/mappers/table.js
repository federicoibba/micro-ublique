import { isString, reduce } from 'lodash'
import moment from 'moment'
import { getDateWithoutTimezoneOffset } from '../helpers'
import { tourEntity } from 'api/entities'

const BASE_TIME_FORMAT = 'YYYY/MM/DD HH:mm'

const dateComparator = (filterLocalDateAtMidnight, cellValue) => {
  const cellDate = getDateWithoutTimezoneOffset(cellValue)
  if (cellDate.isSame(filterLocalDateAtMidnight, 'day')) return 0
  if (cellDate.isBefore(filterLocalDateAtMidnight)) return -1
  return 1
}

const groupCellProps = {
  cellRenderer: 'agGroupCellRenderer',
  suppressMovable: true,
  suppressColumnsToolPanel: true,
  pinned: 'left',
  cellRendererParams: {
    innerRenderer: 'TourGroupCell'
  }
}

const getCheckTourCell = (colId) => ({
  cellRenderer: 'HighlightedCell',
  tooltipField: colId,
  tooltipComponent: 'CheckTourTooltip'
})

const filterTypesMap = {
  text: 'agTextColumnFilter',
  number: 'agNumberColumnFilter',
  date: 'agDateColumnFilter'
}

const colIdPropsMap = {
  [tourEntity.NAME]: groupCellProps
}

const makeTimeFormatter = (timeFormat) => ({
  valueFormatter: ({ value }) => {
    if (!timeFormat) return value
    const formatString = isString(timeFormat) ? timeFormat : BASE_TIME_FORMAT
    const momentDate = moment.utc(value)
    return momentDate._isValid ? momentDate.format(formatString) : '\u2014'
  },
  filter: filterTypesMap.date,
  filterParams: { comparator: dateComparator }
})

const getFilterType = (type) => {
  const filter = filterTypesMap[type]
  return filter ? { filter } : {}
}

export const mapColumnsToAgGridColumnDefs = (t, columnConfig, checkTourCell) =>
  reduce(
    columnConfig,
    (acc, { timeFormat, headerI18nKey, type, hidden, ...restConfig }) => {
      if (hidden) return acc
      return [
        ...acc,
        {
          ...restConfig,
          field: restConfig.colId,
          headerName: headerI18nKey ? t(headerI18nKey) : '',
          ...(timeFormat && makeTimeFormatter(timeFormat)),
          ...(checkTourCell && getCheckTourCell(restConfig.colId)),
          ...(colIdPropsMap[restConfig.colId] || {}),
          ...getFilterType(type)
        }
      ]
    },
    []
  )
