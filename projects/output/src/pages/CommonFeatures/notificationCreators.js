import { v4 as uuidv4 } from 'uuid'
import { join } from 'lodash'
import { ModalActions } from '@ublique/platform'
import { sections } from '../routes'
import { closeSlidePanel } from './actions'

const { closeAllModals } = ModalActions

const NOTIFICATIONS_NS = 'ToursManagement.Notifications'

export const makeGroundedOrderNotification = (t, orderId, pushToOutputRoute) => ({
  id: uuidv4(),
  message: t(`${NOTIFICATIONS_NS}.GroundedOrderMessage`, { orderId }),
  action: {
    title: t(`${NOTIFICATIONS_NS}.GroundedOrderAction`),
    onClick: () => {
      pushToOutputRoute(sections.GROUNDED_ORDERS)
      closeSlidePanel()
      closeAllModals()
    }
  }
})

export const makeGroundedOrdersNotification = (t, ordersNum, pushToOutputRoute) => ({
  id: uuidv4(),
  message: t(`${NOTIFICATIONS_NS}.GroundedOrdersMessage`, { count: ordersNum }),
  action: {
    title: t(`${NOTIFICATIONS_NS}.GroundedOrderAction`),
    onClick: () => {
      pushToOutputRoute(sections.GROUNDED_ORDERS)
      closeSlidePanel()
      closeAllModals()
    }
  }
})

export const makeGroundedOrderCreateTourNotification = (
  t,
  tourId,
  pushToOutputRoute
) => ({
  id: uuidv4(),
  message: t(`${NOTIFICATIONS_NS}.CreateTourMessage`, { tourId }),
  action: {
    title: t(`${NOTIFICATIONS_NS}.CreateTourAction`),
    onClick: () => {
      pushToOutputRoute(sections.TOURS)
      closeAllModals()
    }
  }
})

export const makeCheckTourResultNotification = (t, tourNames) => ({
  id: uuidv4(),
  title: t('CheckTourResults:NotificationTitle'),
  message: t('CheckTourResults:NotificationText', {
    count: tourNames.length,
    tourNames: join(tourNames, ', ')
  })
})

export const makeWaitForCheckTourNotification = (t) => ({
  id: uuidv4(),
  title: t('GanttAndMapView.Notifications.WaitForCheckTourTitle'),
  message: t('GanttAndMapView.Notifications.WaitForCheckTourMessage')
})
