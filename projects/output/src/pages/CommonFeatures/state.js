import { synapse } from 'services/state/synapse'
import filtersConfig from 'config/filters.json'

export const commonInitialState = {
  slidePanel: { opened: null },
  filtersConfig,
  scenario: {},
  plans: {}
}

export const initialFiltersState = {
  active: {},
  temp: {}
}

export const initialUndoState = {
  openedTourRows: [],
  hasActionsToUndo: false
}

export const outputViewCommonStore = synapse.store(commonInitialState)
export const filtersStore = synapse.store(initialFiltersState)
export const undoStore = synapse.store(initialUndoState)

export default {
  outputViewCommon: outputViewCommonStore,
  filters: filtersStore,
  toursManagementUndo: undoStore
}
