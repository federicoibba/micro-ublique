export const scenarioEntity = {
  NAME: 'scenarioName',
  CODE: 'scenarioCode',
  ID: 'scenarioId',
  TOT_DISTANCE: 'scenarioTotalDistance',
  TOT_COST: 'scenarioTotalCost',
  AVG_SATURATION: 'scenarioAverageSaturation',
  TOT_TOURS: 'scenarioTotalTours',
  REUSE: 'scenarioReuse',
  TOT_UNIT_LOAD: 'scenarioTotalLoadUnitsEquivalents',
  TOT_VISITED_POS: 'scenarioTotalVisitedPos',
  TOT_VEHICLES: 'scenarioTotalVehicles',
  TOT_WORKED_ORDERS: 'scenarioTotalServicesWorked',
  TOT_UNWORKED_ORDERS: 'scenarioTotalServicesUnworked',
  STATUS: 'scenarioStatus'
}

export const planEntity = {
  ID: 'planId',
  NAME: 'planName',
  TOT_DISTANCE: 'planTotalDistance',
  TOT_COST: 'planTotalCost',
  TOT_UNIT_LOAD: 'planTotalLoadUnitsEquivalent',
  TOT_VISITED_POS: 'planTotalVisitedPos',
  AVG_COST: 'planAverageCost',
  MIN_COST: 'planMinCost',
  MAX_COST: 'planMaxCost',
  AVG_DISTANCE: 'planAverageDistance',
  MIN_DISTANCE: 'planMinDistance',
  MAX_DISTANCE: 'planMaxDistance',
  AVG_SATURATION: 'planAverageSaturation',
  MIN_SATURATION: 'planMinSaturation',
  MAX_SATURATION: 'planMaxSaturation',
  TOT_WORKED_ORDERS: 'planTotalServicesWorked',
  TOT_UNWORKED_ORDERS: 'planTotalServicesUnworked',
  TOT_TOURS: 'planTotalTours',
  TOT_VEHICLES: 'planTotalVehicles',
  TOT_PACKAGES: 'planTotalPackages',
  REUSE: 'planReuse',
  MAX_STOP_PER_TOUR: 'planMaxStopPerTour',
  AVG_STOP_PER_TOUR: 'planAverageStopPerTour',
  MIN_STOP_PER_TOUR: 'planMinStopPerTour',
  TOTAL_LOAD_UNIT_EQ: 'planTotalLoadUnitsEquivalent'
}

export const posEntity = {
  CITY: 'stopPosCity',
  DESCRIPTION: 'stopPosDescription',
  NAME: 'stopPosName',
  STATE: 'stopPosState',
  STREET_NAME: 'stopPosStreetName',
  STREET_NUMBER: 'stopPosStreetNumber',
  ZIP_CODE: 'stopPosZipCode'
}

export const tourEntity = {
  ID: 'tourId',
  NAME: 'tourName',
  PLAN_ID: 'tourPlanId',
  VEHICLE_CODE: 'tourVehicleCode',
  VEHICLE_CLASS: 'tourVehicleClass',
  CARRIER_NAME: 'tourCarrierName',
  TOTAL_COST: 'tourTotalCost',
  TOTAL_DISTANCE: 'tourTotalDistance',
  SATURATION: 'tourSaturation',
  TOTAL_TIME: 'tourTotalTime',
  TOTAL_DRIVING_TIME: 'tourTotalDrivingTime',
  TOTAL_RESTING_TIME: 'tourTotalRestingTime',
  TOTAL_SERVICE_TIME: 'tourTotalServiceTime',
  TOTAL_WAITING_TIME: 'tourTotalWaitingTime',
  DISTRIBUTION_CENTER: 'tourDistributionCenterName',
  HAS_VIOLATIONS: 'tourHaveViolations',
  CHECK_TOUR_RESULTS: 'tourCheckTourResults',
  NUM_STOPS: 'tourNumberOfStop',
  NUM_ORDERS: 'totalOrders',
  DRIVER_NAME: 'tourDriverName',
  TYPE: 'tourType',
  TIME_START: 'tourTimeStart',
  TIME_END: 'tourTimeEnd',
  POS_LIST: 'tourPointOfServiceList',
  VEHICLE_CAPACITY: 'tourVehicleCapacity',
  TOTAL_DOWNTIME: 'tourTotalDownTime'
}

export const stopEntity = {
  ID: 'stopId',
  TOUR_ID: 'stopTourId',
  STOP_NUMBER: 'stopStopNumber',
  POS: 'stopPointOfService',
  HAS_VIOLATIONS: 'stopHaveViolations',
  CHECK_TOUR_RESULTS: 'stopCheckTourResults',
  WT_START: 'stopDateTimeWindowsStart',
  WT_END: 'stopDateTimeWindowsEnd',
  ARRIVAL_TIME: 'stopArrivalTime',
  DEPARTURE_TIME: 'stopDepartureTime',
  SERVICE_TIME: 'stopServiceTime',
  WAITING_TIME: 'stopWaitingTime',
  DRIVING_TIME: 'stopDrivingTime',
  DISTANCE: 'stopDistance',
  LOAD_UNIT: 'stopLoadUnitUomValue',
  COST: 'stopCost',
  TIME: 'stopTime',
  QUANTITY: 'stopTotalQuantity',
  NUM_ORDERS: 'totalOrders',
  DOWNTIME: 'stopDownTime'
}

export const orderEntity = {
  ID: 'soId',
  STOP_ID: 'soStopId',
  TOUR_ID: 'soTourId',
  PLAN_ID: 'soPlanId',
  POINT_OF_SERVICE: 'soPointOfService',
  PACKAGES: 'soPackages',
  CODE: 'soServiceCode',
  NUMBER: 'soServiceOrderNumber',
  QUANTITY: 'soQuantity',
  CHECK_TOUR_RESULTS: 'soCheckTourResults',
  LOAD_UNIT: 'soLoadUnitUomValue',
  PRODUCT_TYPE: 'soProductTypeName',
  SERVICE_TYPE: 'soServiceType',
  TIME_END: 'soEndTime',
  TW_START: 'soDateTimeWindowsStart',
  TM_END: 'soDateTimeWindowsEnd',
  DISTRIBUTION_CENTER: 'soDistributionCenterName',
  DESCRIPTION: 'soPosDescription',
  CITY: 'soPosCity'
}

export const compartmentEntity = {
  ID: 'tcId'
}
