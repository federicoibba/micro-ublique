import { BASE_URL } from 'config'

export const getServiceOrdersUrl = () => `${BASE_URL}/serviceOrders`
export const getCreateTourUrl = () => `${BASE_URL}/tours`
