import { synapse } from 'services/state'
import { getServiceOrdersUrl, getCreateTourUrl } from './endpoints'

export const fetchGroundedOrders = async (planId) => {
  const url = getServiceOrdersUrl()
  const params = `linkType=Ground&planId=${planId}`
  const response = await synapse.fetch(`${url}?${params}`)
  return response.body
}

export const createNewTour = async (tourData) => {
  const url = getCreateTourUrl()
  const response = await synapse.fetch(url, { method: 'POST' }, tourData)
  return response.body
}
