import { synapse } from 'services/state'
import {
  getScenarioDetailUrl,
  getPlanDetailUrl,
  getVehicleClassSaturationUrl
} from './endpoints'

export const fetchScenarioKPIs = async (scenarioId, revision) => {
  const url = getScenarioDetailUrl(scenarioId, revision)
  const response = await synapse.fetch(url)
  return response.body
}

export const fetchPlanKPIs = async (planId) => {
  const url = getPlanDetailUrl(planId)
  const response = await synapse.fetch(url)
  return response.body
}

export const fetchVehicleClassSaturationData = async (planId) => {
  const url = getVehicleClassSaturationUrl(planId)
  const response = await synapse.fetch(url)
  return response.body
}
