import { BASE_URL } from 'config'

export const getScenarioDetailUrl = (scenarioId, revision = null) =>
  `${BASE_URL}/outputs/${scenarioId}/revisions/${revision}`

export const getScenarioPlansUrl = (scenarioId) =>
  `${BASE_URL}/outputs/${scenarioId}/plans`

export const getPlanDetailUrl = (planId) => `${BASE_URL}/plans/${planId}`

export const getVehicleClassSaturationUrl = (planId) =>
  `${BASE_URL}/plans/${planId}/vehicleClassSaturation`
