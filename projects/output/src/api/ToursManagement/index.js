import { synapse } from 'services/state'
import { BASE_URL } from 'config'
import { first } from 'lodash'
import * as urls from './endpoints'

export const fetchPlansData = async (scenarioId) => {
  const url = urls.getScenarioPlansUrl(scenarioId)
  const response = await synapse.fetch(url, { abortParallel: true })
  return response.body
}

export const fetchToursList = async (planId) => {
  const url = urls.getPlanToursUrl(planId)
  const response = await synapse.fetch(url, { abortParallel: true })
  return response.body
}

export const fetchTourStopsList = async (tourId) => {
  const url = urls.getTourStopsUrl(tourId)
  const response = await synapse.fetch(url, {
    abortParallel: true
  })
  return response.body
}

export const fetchStopOrdersList = async (stopId) => {
  const url = urls.getStopOrdersUrl(stopId)
  const response = await synapse.fetch(url, { abortParallel: true })
  return response.body
}

export const deleteTour = async (tourId) => {
  const url = urls.getTourDetailUrl(tourId)
  const response = await synapse.fetch(url, { method: 'DELETE' })
  return response.body
}

export const deleteToursList = async (toursToDelete) => {
  const url = urls.getToursListUrl()
  const response = await synapse.fetch(url, { method: 'DELETE' }, toursToDelete)
  return response.body
}

export const deleteStop = async (stopId) => {
  const url = urls.getStopDetailUrl(stopId)
  const response = await synapse.fetch(url, { method: 'DELETE' })
  return response.body
}

export const deleteStopsList = async (stopsToDelete) => {
  const url = urls.getStopsListUrl()
  const response = await synapse.fetch(url, { method: 'DELETE' }, stopsToDelete)
  return response.body
}

export const updateTour = async (tourId, tourData) => {
  const url = urls.getTourDetailUrl(tourId)
  const response = await synapse.fetch(url, { method: 'PUT' }, tourData)
  return response.body
}

export const updateStop = async (stopId, stopData) => {
  const url = urls.getStopDetailUrl(stopId)
  const response = await synapse.fetch(url, { method: 'PUT' }, stopData)
  return response.body
}

export const updateToursList = async (toursToUpdate) => {
  const url = urls.getToursListUrl()
  const response = await synapse.fetch(url, { method: 'PUT' }, toursToUpdate)
  return response.body
}

export const updateStopsList = async (stopsToUpdate) => {
  const url = urls.getStopsListUrl()
  const response = await synapse.fetch(url, { method: 'PUT' }, stopsToUpdate)
  return response.body
}

export const updateOrder = async (orderId, orderData) => {
  const url = urls.getOrderDetailUrl(orderId)
  const response = await synapse.fetch(url, { method: 'PUT' }, orderData)
  return response.body
}

export const updateOrdersList = async (ordersToUpdate) => {
  const url = urls.getOrdersListUrl()
  const response = await synapse.fetch(url, { method: 'PUT' }, ordersToUpdate)
  return response.body
}

export const getLastAction = async (scenarioId) => {
  const url = urls.getChangeEventUrl(scenarioId)
  const response = await synapse.fetch(url)
  return response.body
}

export const undoLastAction = async (scenarioId) => {
  const url = urls.getChangeEventUndoUrl(scenarioId)
  const response = await synapse.fetch(url, { method: 'DELETE' })
  return response.body
}

export const createStop = async (newStop) => {
  const url = urls.getStopsUrl()
  const response = await synapse.fetch(url, { method: 'POST' }, newStop)
  return response.body
}

export const fetchScenarioItineraries = async (scenarioCode) => {
  const url = urls.getItinerariesUrl(scenarioCode)
  const response = await synapse.fetch(url)
  return response.body
}

export const fetchTourCompartments = async (tourId) => {
  const url = urls.getTourCompartmentsUrl(tourId)
  const response = await synapse.fetch(url)
  return response.body
}

export const fetchTourCompartmentOrders = async (compartmentId) => {
  const url = urls.getTourCompartmentOrdersUrl(compartmentId)
  const response = await synapse.fetch(url)
  return response.body
}

export const fetchScenarioMapToolCenter = async (scenarioCode) => {
  const params = ['CON_MAP_TOOL_CENTER_LON', 'CON_MAP_TOOL_CENTER_LAT']

  const [lngRes, latRes] = await Promise.all(
    params.map((param) =>
      synapse.fetch(
        `${BASE_URL}/instances/${scenarioCode}/documents?type=adminparameter&title=${param}&adminParameterActive=YES`
      )
    )
  )

  return {
    longitude: first(lngRes.body)?.adminParameterValue,
    latitude: first(latRes.body)?.adminParameterValue
  }
}

export const fetchPosData = async (planId, posName) => {
  const url = urls.getPosUrl(planId, posName)
  const response = await synapse.fetch(url)
  return response.body
}

export const fetchPosGroundedSo = async (planId, posName) => {
  const url = urls.getPosGroundedSoUrl(planId, posName)
  const response = await synapse.fetch(url)
  return response.body
}

export const fetchTour = async (tourId) => {
  const url = urls.getTourDetailUrl(tourId)
  const response = await synapse.fetch(url)
  return response.body
}
