import { BASE_URL } from 'config'

export const getScenarioPlansUrl = (scenarioId) =>
  `${BASE_URL}/outputs/${scenarioId}/plans`

export const getPlanToursUrl = (planId) => `${BASE_URL}/plans/${planId}/tours`

export const getTourStopsUrl = (tourId) => `${BASE_URL}/tours/${tourId}/stops`

export const getStopOrdersUrl = (stopId) =>
  `${BASE_URL}/stops/${stopId}/serviceOrders`

export const getTourDetailUrl = (tourId) => `${BASE_URL}/tours/${tourId}`

export const getToursListUrl = () => `${BASE_URL}/tours/list`

export const getStopDetailUrl = (stopId) => `${BASE_URL}/stops/${stopId}`

export const getStopsListUrl = () => `${BASE_URL}/stops/list`

export const getOrderDetailUrl = (orderId) => `${BASE_URL}/serviceOrders/${orderId}`

export const getOrdersListUrl = () => `${BASE_URL}/serviceOrders/list`

export const getChangeEventUndoUrl = (scenarioId) =>
  `${BASE_URL}/changeEvents/undo/${scenarioId}`

export const getChangeEventUrl = (scenarioId) =>
  `${BASE_URL}/changeEvents/${scenarioId}`

export const getStopsUrl = () => `${BASE_URL}/stops`

export const getItinerariesUrl = (scenarioCode) =>
  `${BASE_URL}/outputs/${scenarioCode}/itineraries`

export const getTourCompartmentsUrl = (tourId) =>
  `${BASE_URL}/tours/${tourId}/tourCompartment`

export const getTourCompartmentOrdersUrl = (compartmentId) =>
  `${BASE_URL}/tourCompartments/${compartmentId}/tourCompartmentOrders`

export const getPosUrl = (planId, posName) =>
  `${BASE_URL}/outputs/itineraries/posInfo/planId/${planId}/posName/${posName}`

export const getPosGroundedSoUrl = (planId, posName) =>
  `${BASE_URL}/serviceOrders/planId/${planId}/posName/${posName}/onTheGround`
