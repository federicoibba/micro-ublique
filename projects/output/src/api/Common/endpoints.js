import { BASE_URL } from 'config'

export const getVehicleUrl = (scenarioCode) =>
  `${BASE_URL}/vehicle/ofscenario/${scenarioCode}`

export const getVehicleClassUrl = (scenarioCode) =>
  `${BASE_URL}/vehicleclass/ofscenario/${scenarioCode}`

export const getUnitOfMeasureUrl = (scenarioCode) =>
  `${BASE_URL}/unitofmeasure/ofscenario/${scenarioCode}`

export const getPointOfServiceUrl = (scenarioCode) =>
  `${BASE_URL}/pointofservice/ofscenario/${scenarioCode}`

export const getDriverUrl = (scenarioCode) =>
  `${BASE_URL}/driver/ofscenario/${scenarioCode}`

export const getExportUrl = () => `${BASE_URL}/reports/_/report/excel`
