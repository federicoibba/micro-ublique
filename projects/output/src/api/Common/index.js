import { synapse } from 'services/state'
import * as urls from './endpoints'

export const fetchVehiclesList = async (scenarioCode) => {
  const url = urls.getVehicleUrl(scenarioCode)
  const response = await synapse.fetch(url)
  return response.body
}

export const fetchVehicleClassesList = async (scenarioCode) => {
  const url = urls.getVehicleClassUrl(scenarioCode)
  const response = await synapse.fetch(url)
  return response.body
}

export const fetchDriversList = async (scenarioCode) => {
  const url = urls.getDriverUrl(scenarioCode)
  const response = await synapse.fetch(url)
  return response.body
}

export const fetchUnitsOfMeasureList = async (scenarioCode) => {
  const url = urls.getUnitOfMeasureUrl(scenarioCode)
  const response = await synapse.fetch(url)
  return response.body
}

export const fetchPointsOfServiceList = async (scenarioCode) => {
  const url = urls.getPointOfServiceUrl(scenarioCode)
  const params = 'posType=PointOfService'
  const response = await synapse.fetch(`${url}?${params}`)
  return response.body
}

export const fetchDistributionCentersList = async (scenarioCode) => {
  const url = urls.getPointOfServiceUrl(scenarioCode)
  const params = 'posType=DistributionCenter'
  const response = await synapse.fetch(`${url}?${params}`)
  return response.body
}

export const exportExcel = async (scenarioCode, planId, lang = 'en-GB') => {
  const response = await synapse.fetch(
    urls.getExportUrl(),
    { method: 'POST' },
    { zip: false, lang, scenarioCode, planId }
  )
  return { blob: response.body, filename: response.fileName }
}
