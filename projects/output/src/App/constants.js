export const isDevMode = process.env.NODE_ENV === 'development'

export const defaultThemes = [
  {
    id: 'light',
    label: 'Light'
  },
  {
    id: 'dark',
    label: 'Dark'
  }
]
