import React, { Suspense, lazy, useMemo } from 'react'
import {
  Router,
  Switch,
  Route,
  Redirect,
  useParams,
  matchPath
} from 'react-router-dom'
import { List32, Analytics32, Roadmap32, Box32 } from '@carbon/icons-react'
import SideNavPage from '@ublique/components/SideNavPage'
import { useTranslation } from 'react-i18next'
import initI18n from 'i18n'
import Loading from '@ublique/components/Loading'
import Theme from '@ublique/components/Theme'
import useStoreSelector from 'utils/hooks/useStoreSelector'

const KPIs = lazy(() => import('pages/KPIs'))
const ToursManagement = lazy(() => import('pages/ToursManagement'))
const GroundedOrders = lazy(() => import('pages/GroundedOrders'))
const GanttAndMap = lazy(() => import('pages/GanttAndMap'))

initI18n()

const routes = [
  {
    path: 'KPIOverview',
    component: KPIs,
    icon: Analytics32,
    title: 'SideNavigation.KPIOverview'
  },
  {
    path: 'ToursManagement',
    component: ToursManagement,
    icon: List32,
    title: 'SideNavigation.ToursManagement'
  },
  {
    path: 'GanttAndMap',
    component: GanttAndMap,
    icon: Roadmap32,
    title: 'SideNavigation.GanttAndMap'
  },
  {
    path: 'GroundedOrders',
    component: GroundedOrders,
    icon: Box32,
    title: 'SideNavigation.GroundedOrders'
  }
]

// Temporary...
const OutputRoute = ({ component: Component, basename }) => {
  const { t } = useTranslation('OutputView')
  const { scenarioId: scenarioCode } = useParams()

  const menuItems = useMemo(
    () =>
      routes.map(({ icon, title, path }) => ({
        icon,
        title: t(title),
        href: `${basename}/${scenarioCode}/${path}`
      })),
    [t]
  )

  return (
    <SideNavPage items={menuItems} title="Output" sidebar={false}>
      <Component basename={basename} />
    </SideNavPage>
  )
}

function App({ history, basename = '' }) {
  const theme = useStoreSelector('theme')
  const makePath = (name) => `${basename}/:scenarioId/${name}`

  const index = matchPath(history.location.pathname, {
    path: `${basename}/:scenarioId`,
    exact: true
  })

  return (
    <Suspense fallback={<Loading />}>
      <Theme name={theme}>
        <Router history={history}>
          <Switch>
            {routes.map(({ path, component }) => (
              <Route key={path} path={makePath(path)}>
                <OutputRoute basename={basename} component={component} />
              </Route>
            ))}
            {index && (
              <Route exact path={index.url}>
                <Redirect to={`${basename}/${index.params.scenarioId}/KPIOverview`} />
              </Route>
            )}
          </Switch>
        </Router>
      </Theme>
    </Suspense>
  )
}

export default App
