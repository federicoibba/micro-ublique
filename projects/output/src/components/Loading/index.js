import React from 'react'
import cn from 'classnames'
import Loading from '@ublique/components/Loading'
import './index.scss'

export const LoaderSmall = ({ className }) => {
  return (
    <div className={cn('loader-small', className)}>
      <Loading withOverlay={false} />
    </div>
  )
}
