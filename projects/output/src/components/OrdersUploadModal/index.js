import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { isEmpty } from 'lodash'

import Modal from '@ublique/components/Modal'
import UploadForm from './UploadForm'

import {
  importFile,
  getDateISO,
  sortMessages,
  fetchDataFromDocument,
  OrdersUploadTypes,
  autoImportFile
} from './helpers'

const UploadModal = ({ open, close, data }) => {
  const { t, i18n } = useTranslation(['Common', 'OrdersUpload'])

  const { MANUAL, AUTO } = OrdersUploadTypes

  const [isError, setIsError] = useState(false)
  const [overwrite, setOverwrite] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [uploadMode, setUploadMode] = useState(MANUAL)
  const [showCheckbox, setShowCheckbox] = useState(false)
  const [scenarioDocument, setScenarioDocument] = useState(null)
  const [response, setResponse] = useState({ file: null, messages: [] })

  useEffect(async () => {
    const [scenarioDocument] = await fetchDataFromDocument({
      params: `title=${data.name}`,
      instanceId: data.id
    })

    const serviceImportType = await fetchDataFromDocument({
      params: `title=CON_SERVICE_IMPORT_TYPE`,
      instanceId: data.id
    })

    const overWriteEnum = await fetchDataFromDocument({
      params: `title=CON_OVERWRITE_IMPORT_ORDER_ACTIVE`,
      instanceId: data.id
    })

    const overWriteDefault = await fetchDataFromDocument({
      params: `title=CON_SERVICE_OVERWRITE_DEFAULT`,
      instanceId: data.id
    })

    if (!isEmpty(overWriteEnum)) {
      const [element] = overWriteEnum
      const { adminParameterActive, adminParameterEnumValue } = element

      setShowCheckbox(
        adminParameterActive === 'YES' && adminParameterEnumValue === 'YES'
      )
    }

    if (!isEmpty(serviceImportType)) {
      const [element] = serviceImportType
      const importType = element.userParameterEnumValue
      const typeMap = {
        Automated: AUTO,
        Manual: MANUAL
      }

      setUploadMode(typeMap[importType])
    }

    if (!isEmpty(overWriteDefault)) {
      const [element] = overWriteDefault
      if (element.userParameterEnumValue === 'YES') setOverwrite(true)
    }

    setScenarioDocument(scenarioDocument)
  }, [])

  const onUpload = async (id, data, file = null) => {
    setIsLoading(true)
    setIsError(false)

    try {
      const {
        payload: { info }
      } =
        uploadMode === AUTO
          ? await autoImportFile(id, data)
          : await importFile(id, data, file)

      setResponse({
        file: info.file,
        messages: sortMessages(info.messages || [])
      })
      setIsLoading(false)
    } catch (error) {
      if (!isEmpty(error.responseBody) && !isEmpty(error.responseBody.payload)) {
        setResponse({
          messages: [{ message: error.responseBody.payload.info, type: 'error' }]
        })
      }

      setIsError(true)
      setIsLoading(false)
    }
  }

  const onAutoUpload = async () => {
    const postData = {
      data: {
        overwrite,
        mode: uploadMode,
        withSave: true,
        scenarioInstanceId: data.id,
        startDate: getDateISO(scenarioDocument.scenarioPlanningStartDate)
      },
      scenarioManager: { status: 'IMPORTING' }
    }

    await onUpload(data.id, postData)
  }

  const onManualUpload = async (e, files) => {
    const fileImported =
      (!isEmpty(files) && files.addedFiles && files.addedFiles[0]) ||
      e.target.files[0]

    const postData = {
      data: {
        overwrite,
        mode: uploadMode,
        withSave: true,
        lng: i18n.language,
        scenarioInstanceId: data.id
      },
      scenarioManager: { status: 'IMPORTING' }
    }

    await onUpload(data.id, postData, fileImported)
  }

  const onRequestSubmit = async () => {
    if (
      uploadMode === MANUAL ||
      (uploadMode === AUTO && !isEmpty(response.messages))
    ) {
      close()
      return
    }

    await onAutoUpload()
  }

  return (
    <Modal
      open={open}
      onRequestClose={close}
      modalHeading={t('OrdersUpload:MODAL_TITLE')}
      primaryButtonText={t(
        uploadMode === AUTO && isEmpty(response.messages)
          ? 'OrdersUpload:IMPORT'
          : 'Common:Close'
      )}
      secondaryButtonText={t('Common:Cancel')}
      onRequestSubmit={onRequestSubmit}
    >
      <UploadForm
        t={t}
        data={data}
        isError={isError}
        response={response}
        isLoading={isLoading}
        overwrite={overwrite}
        uploadMode={uploadMode}
        setOverwrite={setOverwrite}
        showCheckbox={showCheckbox}
        overwriteChecked={overwrite}
        setUploadMode={setUploadMode}
        onManualUpload={onManualUpload}
        scenarioDocument={scenarioDocument}
      />
    </Modal>
  )
}

export default UploadModal
