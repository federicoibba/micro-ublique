import React, { Fragment } from 'react'
import { isEmpty } from 'lodash'

import Loading from '@ublique/components/Loading'
import Checkbox from '@ublique/components/Checkbox'
import RadioButton from '@ublique/components/RadioButton'
import FileUploader from '@ublique/components/FileUploader'

import { OrdersUploadTypes } from './helpers'

import './upload.scss'

const UploadForm = ({
  t,
  isError,
  response,
  isLoading,
  overwrite,
  overwriteChecked,
  uploadMode,
  setOverwrite,
  showCheckbox,
  setUploadMode,
  onManualUpload,
  scenarioDocument
}) => {
  const { MANUAL, AUTO } = OrdersUploadTypes

  const radioOptions = [MANUAL, AUTO]
  const mimeAccepted = [
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  ]

  const getContainerLabel = (file) =>
    isEmpty(file) ? 'OrdersUpload:ACCEPTED_MIMES' : 'OrdersUpload:DELETE_BEFORE_ERROR'

  const displayCheckbox = () =>
    showCheckbox &&
    !isEmpty(scenarioDocument) &&
    scenarioDocument.scenarioOutputsImported === 'NOT'

  const renderMessages = () => {
    const { messages } = response

    return messages.map(({ message, type, value }, i) => {
      if (type === 'info' || type === 'warning') {
        return (
          <div key={`id-${i}`} className="upload-text">
            <span className="result-title">
              {t(`OrdersUpload:${message}`)}
              {value ? ': ' : ''}
            </span>
            <span className="result-value">{value}</span>
          </div>
        )
      }
      if (type === 'error') {
        // manage error messages with a string substitution due to
        // multiple values retrieved from BE
        const errorMessage = message
          .replace('MissingProductType', t('OrdersUpload:MissingProductType'))
          .replace(
            'MissingServicePosNameEnd',
            t('OrdersUpload:MissingServicePosNameEnd')
          )
          .replace(
            'MissingServicePosNameStart',
            t('OrdersUpload:MissingServicePosNameStart')
          )
          .replace('forTheService', t('OrdersUpload:forTheService'))

        return (
          <div key={`id-${i}`} className="upload-text">
            <span className="result-title">{errorMessage}</span>
          </div>
        )
      }
    })
  }

  const getUploader = () => {
    const { file, messages } = response
    return (
      <Fragment>
        {uploadMode === MANUAL && (
          <FileUploader.Container
            id="file-uploader"
            size="default"
            accept={mimeAccepted}
            disabled={!isEmpty(file)}
            onAddFiles={onManualUpload}
            labelText={t(getContainerLabel(file))}
          />
        )}

        {!isEmpty(messages) && (
          <Fragment>
            {uploadMode === MANUAL && !isEmpty(file) && (
              <FileUploader.Item status="edit" name={file.name} />
            )}

            <div className="upload-result">
              <div className="upload-header">
                <h5 className={isError ? 'error' : ''}>
                  {isError ? `${t('Common:Error')} - ` : ''}
                  {t('OrdersUpload:RESULT')}
                </h5>
                {/* {uploadMode === AUTO && !isUndefined(file) && (
                  <Download24 onClick={onDownloadFile(file)} />
                )} */}
              </div>

              <hr />
              {renderMessages()}
            </div>
          </Fragment>
        )}
      </Fragment>
    )
  }

  if (isEmpty(scenarioDocument)) {
    return <Loading />
  }

  return (
    <div className="upload-container">
      <p>{t(`OrdersUpload:TYPE`)}</p>
      <RadioButton.Form>
        <RadioButton.ButtonGroup
          name="upload-type"
          labelPosition="right"
          orientation="vertical"
          valueSelected={uploadMode}
          onChange={(mode) => setUploadMode(mode)}
        >
          {radioOptions.map((opt) => (
            <RadioButton
              id={opt}
              key={opt}
              value={opt}
              labelText={t(`OrdersUpload:${opt}`)}
            />
          ))}
        </RadioButton.ButtonGroup>
      </RadioButton.Form>

      {displayCheckbox() && (
        <div className="overwrite-choice">
          <legend className="bx--label">{t(`OrdersUpload:OVERWRITE_LABEL`)}</legend>
          <Checkbox
            id="overwrite-checkbox"
            labelText={t('OrdersUpload:OVERWRITE_VALUE')}
            value={overwrite}
            checked={overwriteChecked}
            onChange={() => setOverwrite((value) => !value)}
          />
        </div>
      )}

      {isError && (
        <span className="generic-error error">{t(`OrdersUpload:GENERIC_ERROR`)}</span>
      )}

      {isLoading ? <Loading active={isLoading} /> : getUploader()}
    </div>
  )
}

export default UploadForm
