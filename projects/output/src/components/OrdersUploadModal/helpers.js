import { synapse } from 'services/state'
import { BASE_URL } from 'config'

const typeWeigth = {
  info: 0,
  warning: 1,
  error: 2
}

export const OrdersUploadTypes = {
  MANUAL: 'MANUAL',
  AUTO: 'AUTO'
}

export const sortMessages = (messages) =>
  messages.sort((a, b) => typeWeigth[a.type] - typeWeigth[b.type])

export const getDateISO = (date, separator = '-') => {
  const event = new Date(date)
  const year = event.getFullYear()
  const month = `${event.getMonth() + 1}`.padStart(2, '0')
  const day = `${event.getDate()}`.padStart(2, '0')

  return [year, month, day].join(separator)
}

export const fetchDataFromDocument = async ({ params, instanceId }) => {
  const url = `${BASE_URL}/instances/${instanceId}/documents/?${params}`

  const { body } = await synapse.fetch(url)

  return body
}

export const importFile = async (id, data, file = null) => {
  const formData = new FormData()
  formData.append('data', JSON.stringify(data))

  if (file) {
    formData.append('file', file)
  }

  const { body } = await synapse.fetch(
    `${BASE_URL}/scenarios/${id}`,
    {
      method: 'POST'
    },
    formData
  )

  return body
}

export const autoImportFile = async (id, data) => {
  const { body } = await synapse.fetch(
    `${BASE_URL}/scenarios/${id}`,
    {
      method: 'PUT'
    },
    data
  )

  return body
}
