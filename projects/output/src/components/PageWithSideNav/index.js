import React from 'react'
import classNames from 'classnames'
import { Link } from 'react-router-dom'
import Breadcrumb from '@ublique/components/Breadcrumb'
import './index.scss'

const renderBreadcrumbs = (breadcrumbs) =>
  breadcrumbs === 'loading' ? (
    <Breadcrumb.Skeleton className="main-content-breadcrumbs" />
  ) : (
    <Breadcrumb noTrailingSlash className="main-content-breadcrumbs">
      {breadcrumbs.map(({ href, title, skeleton }, index) =>
        skeleton ? (
          <Breadcrumb.Skeleton />
        ) : (
          <Breadcrumb.Item
            key={`${href || 'breadcrumb'}-${index}`}
            isCurrentPage={index === breadcrumbs.length - 1}
          >
            {href ? <Link to={href}>{title}</Link> : <span>{title}</span>}
          </Breadcrumb.Item>
        )
      )}
    </Breadcrumb>
  )

const PageWithSideNav = ({ sideNav, noYScroll, children, breadcrumbs }) => {
  return (
    <div className="content-wrapper">
      {sideNav}
      <div
        className={classNames('main-content', {
          'overflow-y-hidden': noYScroll
        })}
      >
        {breadcrumbs && renderBreadcrumbs(breadcrumbs)}
        {children}
      </div>
    </div>
  )
}

export default PageWithSideNav
