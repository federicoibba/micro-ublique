import React from 'react'
import { map } from 'lodash'
import PropTypes from 'prop-types'
import './index.scss'

const StatsItem = ({ title, value }) => (
  <div className="stats-panel-container__item">
    <p className="item-title">{title}</p>
    <p className="item-value">{value}</p>
  </div>
)

const StatsPanel = ({ title, items = [] }) => (
  <div className="stats-panel-container">
    {title && (
      <header>
        <h4 className="stats-panel-container__title">{title}</h4>
      </header>
    )}
    <div className="stats-panel-container__items">
      {map(items, ({ id, title, value }, i) => (
        <StatsItem key={id || `${title}-${i}`} title={title} value={value} />
      ))}
    </div>
  </div>
)

StatsPanel.propTypes = {
  title: PropTypes.string,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      title: PropTypes.string,
      value: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
    })
  )
}

export default React.memo(StatsPanel)
