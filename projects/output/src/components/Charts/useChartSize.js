import { useEffect, useLayoutEffect, useState, useMemo } from 'react'
import { debounce } from 'lodash'

export const useChartSize = (ref, resizeDebounce = 200) => {
  const [size, setSize] = useState(null)

  const updateChartSize = () => {
    const width = ref.current.clientWidth
    const height = ref.current.clientHeight
    setSize({ width, height })
  }

  const debouncedUpdateChartSize = useMemo(
    () =>
      debounce(() => {
        if (!ref.current) return
        setSize(null) // Need to remove the chart to properly calculate client width and height
        updateChartSize()
      }, resizeDebounce),
    []
  )

  useEffect(() => {
    window.addEventListener('resize', debouncedUpdateChartSize)
    return () => {
      window.removeEventListener('resize', debouncedUpdateChartSize)
    }
  }, [])

  useLayoutEffect(() => {
    updateChartSize()
  }, [])

  return size
}
