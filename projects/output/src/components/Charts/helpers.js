import { map, get, reduce, isInteger } from 'lodash'
import { formatMinutes } from 'utils'

export const colorTheme = ['#AF144B', '#d13579', '#C3266B', '#991850']

// export const isPie = (options) => get(options, 'series[0].type') === 'pie'

export const getTotal = (data) => reduce(data, (acc, { value }) => acc + value, 0)

const getSeriesTitle = (theme, t, series, options) => {
  if (series.titleType === 'total') {
    const total = getTotal(options.data)
    return {
      text: `${t('Total')}: ${isInteger(total) ? total : total.toFixed(2)}`,
      color: theme.text01
    }
  }
  if (series.titleType === 'totalMinutes') {
    const total = getTotal(options.data)
    return {
      text: `${t('Total')} (hh:mm): ${formatMinutes(total)}`,
      color: theme.text01
    }
  }
  return series.title
}

const resolveThemeColor = (theme, prop) =>
  Object.assign({}, prop, { color: theme[prop.color] })

const formatAxes = (theme) => (axes) => {
  const { label, title } = axes
  if (label?.color || title?.color) {
    return Object.assign(
      {},
      axes,
      label && { label: resolveThemeColor(theme, label) },
      title && { title: resolveThemeColor(theme, title) }
    )
  }
  return axes
}

const formatSeries = (theme, t, options) => (series) =>
  Object.assign({}, series, {
    title: getSeriesTitle(theme, t, series, options),
    fills: colorTheme,
    strokeWidth: 0, // This should be at least 1 for line series
    highlightStyle: { fill: theme.highlight }
  })

export const getChartOptions = (t, theme, { dataConfig, ...options }, size) =>
  Object.assign({}, options, {
    autoSize: !size,
    width: get(size, 'width'),
    height: get(size, 'height'),
    background: { visible: false },
    legend: options.legend ? resolveThemeColor(theme, options.legend) : undefined,
    axes: map(options.axes, formatAxes(theme)),
    series: map(options.series, formatSeries(theme, t, options))
  })
