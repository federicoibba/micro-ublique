import React, { useRef } from 'react'
import { useTranslation } from 'react-i18next'
import Charts from '@ublique/components/Charts'
import { useTheme } from '@ublique/components/Theme'
import { useChartSize } from './useChartSize'
import { getChartOptions } from './helpers'
import './index.scss'

const ChartsComponentCalcSize = ({ t, theme, options }) => {
  const wrapperRef = useRef(null)
  const size = useChartSize(wrapperRef)
  return (
    <div ref={wrapperRef} className="chart-wrapper">
      {size && <Charts options={getChartOptions(t, theme, options, size)} />}
    </div>
  )
}

const ChartsComponentAutoSize = ({ t, theme, options }) => {
  return <Charts options={getChartOptions(t, theme, options)} />
}

const ChartsComponent = ({ options, calcSize }) => {
  const { t } = useTranslation('Common')
  const theme = useTheme()
  const Component = calcSize ? ChartsComponentCalcSize : ChartsComponentAutoSize
  return <Component options={options} t={t} theme={theme} />
}

export default React.memo(ChartsComponent)
