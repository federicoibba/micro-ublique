import { modalStore } from 'state'
import { last, reject, unset } from 'lodash'
import { initialState } from './state'

export const openModal = (modalId, data) => {
  modalStore.update((state) => ({
    activeModals: [...state.activeModals, modalId],
    modalsData: {
      ...state.modalsData,
      [modalId]: data
    }
  }))
}

export const closeModal = () => {
  modalStore.update((state) => {
    const lastModalId = last(state.activeModals)
    const newModalsData = { ...state.modalsData }
    unset(newModalsData, lastModalId)
    return {
      activeModals: reject(state.activeModals, (id) => id === lastModalId),
      modalsData: newModalsData
    }
  })
}

export const updateModalData = (data, modalId) => {
  modalStore.update((state) => {
    const modalIdToUpdate = modalId || last(state.activeModals)
    return {
      ...state,
      modalsData: {
        ...state.modalsData,
        [modalIdToUpdate]: { ...state.modalsData[modalIdToUpdate], ...data }
      }
    }
  })
}

export const closeAllModals = () => {
  modalStore.set(initialState)
}
