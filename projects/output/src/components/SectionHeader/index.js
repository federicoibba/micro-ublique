import React from 'react'
import './index.scss'

const SectionHeader = ({ title, rightComponent }) => {
  return (
    <div className="section-header">
      <h1>{title}</h1>
      {rightComponent && (
        <div className="section-header__right-component">{rightComponent}</div>
      )}
    </div>
  )
}

export default React.memo(SectionHeader)
