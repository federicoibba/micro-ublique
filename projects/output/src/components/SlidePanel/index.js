import React, { useEffect, useState, useCallback } from 'react'
import { ModalActions } from '@ublique/platform'
import classNames from 'classnames'
import { Close24 } from '@carbon/icons-react'
import './index.scss'

const { closeAllModals } = ModalActions

const SlidePanel = ({
  title,
  className,
  label,
  opened,
  onClose,
  style,
  children,
  closeText = 'Close'
}) => {
  const [showContent, setShowContent] = useState()

  const onEscapePressed = useCallback(({ key }) => {
    if (key === 'Escape') onCloseHandler()
  }, [])

  useEffect(() => {
    if (opened) {
      setShowContent(true)
      document.addEventListener('keydown', onEscapePressed)
    }
  }, [opened])

  const onCloseHandler = () => {
    onClose()
    closeAllModals()
    setTimeout(() => setShowContent(false), 200)
    document.removeEventListener('keydown', onEscapePressed)
  }

  const content = (
    <div className={className}>
      <div className="slide-panel__header">
        <button
          className="slide-panel__close-icon"
          type="button"
          title={closeText}
          aria-label={closeText}
          onClick={onCloseHandler}
        >
          <Close24 />
        </button>
        <div className="slide-panel__title">
          {label && <h4>{label}</h4>}
          {title && <h3>{title}</h3>}
        </div>
      </div>
      <div className="slide-panel__content">{children}</div>
    </div>
  )

  return (
    <div className="slide-panel-container">
      <div className={classNames('slide-panel', { opened })} style={style}>
        {showContent ? content : null}
      </div>
      <div
        className={classNames('slide-panel-backdrop', { opened })}
        onClick={onCloseHandler}
      />
    </div>
  )
}

export default SlidePanel
