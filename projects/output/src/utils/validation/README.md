# Validation overview

Each field may contain a `states` property, that lists every possibile state the field can be in. A state can be any arbitrary description that represents the field when certain conditions are met.\
Every state has a `when` property, containing the conditions that have to be satisfied to allow the field to transition to the state.\
All states are evaluated, and the field will get all the states that returned valid.\
It's also possible to nest states: this way, the nested state will be evaluated only if its parent state is valid; if the nested state is valid too, the field will use it as a state along with its parent, else will use only the valid parent state.

## Conditions

Inside `when`, conditions should be written as key-value pairs, where the key is the field id that should be evaluated, and the value are the conditions. When the key is `$`, it will be resolved to the field id.

Multiple pairs are evaluated as AND, i.e. **all must be true** to transition the field to that state. To evaluate conditions as OR, it's possibile to wrap them with a `$or` key.\
When writing conditions for a field id, all operators that appear inside the same object are evaluated as AND; if they are to be evaluated as OR, they should be placed inside an array referenced by the key `$or`.

When writing the value for an operator, it's possible to refer to the value of another field by using the symbol `%` in front of the field id to resolve. When validating a string length, `.length` needs to be appended to that string.

## Validation result

By default the validation function will return all the valid states and, **when a state is valid**, also an array with the conditions that evaluated to true, in this format:

```js
const result = [
  {
    state: 'invalid',
    validationResult: [
      {
        fieldId: 'customer',
        fieldProperties: 'personalData.age',
        operation: 'gt',
        target: 20
      },
      { fieldId: 'city', operation: 'in', target: ['city1', 'city2', 'city3'] }
    ]
  },
  {
    state: 'visible',
    validationResult: [{ fieldId: 'customerName', operation: 'eq', target: 'John' }]
  }
]
```

When the field is in dot notation, **fieldId** will be the root, **fieldProperties** the nested properties.\
They can be used for example to display why a certain field transitioned to an invalid state.

**Note:** by passing `{ showValidationResult: false }` the function will only return the state.

## Examples

```js
// Field is in "disabled" state when it's less than 20 AND "otherField" is null/undefined
{
  id: 'fieldId',
  states: {
    disabled: {
      when: {
        $: { lt: 20 },
        otherField: { ex: false }
      }
    }
  }
}

// Field is in "disabled" state when it's less than 20 OR "otherField" is equal to the value of "someOtherFieldId"
{
  id: 'fieldId',
  states: {
    disabled: {
      when: {
        $or: {
          $: { lt: 20 },
          otherField: { eq: '%someOtherFieldId' }
      }
    }
  }
}

// Field is in "disabled" state when "otherField1" is equal to "value" AND
// "fieldId"'s length is less than 20 OR "otherField2" is null/undefined (one of the previous two)
{
  id: 'fieldId',
  states: {
    disabled: {
      when: {
        otherField1: { eq: 'value' },
        $or: {
          '$.length': { lt: 20 },
          otherField2: { ex: false }
      }
    }
  }
}

// With specific default state when none of the states is valid
// If no default state is specified, it will be undefined
{
  id: 'fieldId',
  states: {
    defaultState: 'hidden',
    visible: {
      when: {
        otherField: { ex: true }
      }
    }
}

// Multiple states, the field will be in all those states that evaluate to true
{
  id: 'fieldId',
  states: {
    hidden: {
      when: {
        // "otherField" must be equal to 66 AND one of the conditions inside $or
        otherField: { eq: 66, $or: [{ gt: 20 }, { between: [100, 200] }] }
      }
    },
    disabled: {
      when: {
        $: { eq: 'forbidden' }
    }
  }
}

// Nested states: for disabled to be evaluated, visible has to be true
{
  id: 'fieldId',
  states: {
    visible: {
      when: {
        otherField: { ex: true }
      },
      states: {
        disabled: {
          '$.length': { gt: 200 }
        }
      }
    }
}

// Shorthand syntax for just one state, "invalid" by default
{
  id: 'fieldId',
  validation: {
    $: { eq: 'forbidden' },
    otherField: { ex: false }
  }
}
```

More examples can be found in `"__tests__/validation.test.js"`.
