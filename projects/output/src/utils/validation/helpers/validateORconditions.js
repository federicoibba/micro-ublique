import { toPairs } from 'lodash'
import { LOGICAL_OP, NESTED_OR_IN_OR_ERROR } from '../constants'
import validateRecursively from './validateRecursively'
import validateANDconditions from './validateANDconditions'
import { resolveSelfKey } from './misc'

export default function validateORconditions(
  { showValidationResult },
  rootFieldId,
  data,
  conditions
) {
  const conditionsPairs = toPairs(conditions)

  if (!conditionsPairs.length) return { isValid: true }

  for (const [fieldId, fieldConditions] of conditionsPairs) {
    if (fieldId === LOGICAL_OP.OR) {
      throw new Error(NESTED_OR_IN_OR_ERROR)
    }

    let result

    if (fieldId === LOGICAL_OP.AND) {
      result = validateANDconditions(
        { showValidationResult },
        rootFieldId,
        data,
        fieldConditions
      )
    } else {
      const resolvedFieldId = resolveSelfKey(rootFieldId, fieldId)
      result = validateRecursively(
        { showValidationResult },
        resolvedFieldId,
        data,
        fieldConditions
      )
    }

    const { isValid, validationResult } = result

    if (isValid) return { isValid, validationResult } // Short circuit at the first valid operation
  }

  return { isValid: false } // No valid operation, ALL conditions are invalid
}
