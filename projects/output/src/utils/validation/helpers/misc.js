import {
  get,
  concat,
  unset,
  isNil,
  isEmpty,
  trimStart,
  startsWith,
  split,
  isUndefined
} from 'lodash'
import {
  SHORT_FORM_DEFAULT_STATE,
  RESOLVE_VALUE_ID,
  SHORT_FORM_KEY,
  SELF_VALUE_ID
} from '../constants'

export const isNilOrEmptyString = (x) => isNil(x) || isEmpty(x)

export const joinArrays = (arr1, arr2) => {
  if (arr1 && arr2) return concat(arr1, arr2)
  return arr1 || arr2
}

export const convertToFullNotation = (shorthandObj) => {
  const newFieldObj = Object.assign({}, shorthandObj)
  newFieldObj.states = {
    [SHORT_FORM_DEFAULT_STATE]: {
      when: newFieldObj[SHORT_FORM_KEY]
    }
  }
  unset(newFieldObj, SHORT_FORM_KEY)
  return newFieldObj
}

export const isSelfKey = (fieldId) => startsWith(fieldId, SELF_VALUE_ID)

export const resolveSelfKey = (rootFieldId, currentFieldId) => {
  if (!isSelfKey(currentFieldId)) return currentFieldId

  const additionalPath = trimStart(currentFieldId, SELF_VALUE_ID)
  return `${rootFieldId}${additionalPath}`
}

export const resolveValue = (data, value) =>
  startsWith(value, RESOLVE_VALUE_ID)
    ? get(data, trimStart(value, RESOLVE_VALUE_ID))
    : value

export const getDefaultState = (defaultState) => [{ state: defaultState }]

export const getFieldProperties = (fieldId) => split(fieldId, /\.(.+)/, 2)

export const excludeUndefinedState = (validationData) =>
  isUndefined(get(validationData, '0.state')) ? [] : validationData
