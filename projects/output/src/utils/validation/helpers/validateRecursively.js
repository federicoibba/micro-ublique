import { isArray } from 'lodash'
import { LOGICAL_OP, EXPLICIT_AND_ERROR } from '../constants'
import areConditionsValid from './areConditionsValid'
import { joinArrays } from './misc'

export default function validateRecursively(
  { showValidationResult },
  fieldId,
  data,
  conditions,
  mode
) {
  if (mode === LOGICAL_OP.OR) {
    if (!isArray(conditions))
      throw new Error(
        `OR conditions that apply to the same fieldId have to be inside an array, e.g. "${LOGICAL_OP.OR}: [{...}, {...}]".`
      )

    for (const conditionsGroup of conditions) {
      const { isValid, validationResult } = validateRecursively(
        { showValidationResult },
        fieldId,
        data,
        conditionsGroup
      )
      if (isValid) return { isValid, validationResult } // Short circuit at the first valid operation
    }
    return { isValid: false } // No valid operation, ALL conditions are invalid
  }

  const {
    [LOGICAL_OP.OR]: ORconditions,
    [LOGICAL_OP.AND]: ANDconditionsExplicit,
    ...ANDconditions
  } = conditions

  if (ANDconditionsExplicit) {
    throw new Error(EXPLICIT_AND_ERROR)
  }

  // Base case
  if (!ORconditions) {
    return areConditionsValid({ showValidationResult }, fieldId, data, conditions)
  }

  const { isValid: ORvalid, validationResult: ORvalidationResult } =
    validateRecursively(
      { showValidationResult },
      fieldId,
      data,
      ORconditions,
      LOGICAL_OP.OR
    )
  const { isValid: ANDvalid, validationResult: ANDvalidationResult } =
    validateRecursively(
      { showValidationResult },
      fieldId,
      data,
      ANDconditions,
      LOGICAL_OP.AND
    )

  const validConditions = ORvalid && ANDvalid

  return {
    isValid: validConditions,
    validationResult:
      validConditions && showValidationResult
        ? joinArrays(ANDvalidationResult, ORvalidationResult)
        : undefined
  }
}
