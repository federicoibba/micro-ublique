import { toPairs, get } from 'lodash'
import { operators } from '../operators'
import { resolveValue, getFieldProperties } from './misc'

// Evaluate a single group, e.g. { gt: 1, lt: 20, neq: 17 }
// Inside a single group, all conditions are evaluated as AND
export default function areConditionsValid(
  { showValidationResult },
  fieldId,
  data,
  conditions
) {
  const conditionsPairs = toPairs(conditions)

  if (!conditionsPairs.length) return { isValid: true }

  const validationResultAcc = []

  for (const [op, value] of conditionsPairs) {
    const operation = operators[op]
    if (!operation)
      throw new Error(`"${op}" is not a valid operation for "${fieldId}".`)

    const resolvedValue = resolveValue(data, value)
    const isValid = operation(get(data, fieldId), resolvedValue)

    if (!isValid) return { isValid: false } // Short circuit at the first error

    if (showValidationResult) {
      const [field, fieldProperties] = getFieldProperties(fieldId)
      const validationResult = {
        operation: op,
        target: resolvedValue,
        fieldId: field
      }
      if (fieldProperties) {
        validationResult.fieldProperties = fieldProperties
      }
      validationResultAcc.push(validationResult)
    }
  }

  // No errors, ALL conditions are valid
  return {
    isValid: true,
    validationResult: showValidationResult ? validationResultAcc : undefined
  }
}
