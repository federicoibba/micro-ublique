import { isUndefined, last, negate } from 'lodash'
import { BASE_URL } from 'config'

// Field properties that will be available inside AG Grid custom cell renderers
const cellParamsKey = [
  'documentType',
  'filterField',
  'filterKey',
  'filter',
  'optionsKey',
  'valueKey',
  'labelKey',
  'options',
  'findBy',
  'operation',
  'operands',
  'excludeCurrentDocument'
]

const statesToReset = ['disabled', 'hidden']

export const HEADER_FIELD_SYMBOL = '^'

export const buildRowId = (dataId = {}) => {
  const { id, activityId } = dataId
  return `${id}-${activityId}`
}

// Keep backwards compatibility
export const adapters = {
  getId: (field) => field.id || field.field,
  getFields: (section) => section.fields || section.columnDefs,
  getLabel: (field) => field.labelText || field.headerName,
  getCellParams: (field) =>
    field.cellEditorParams ||
    cellParamsKey.reduce((acc, key) => ({ ...acc, [key]: field[key] }), {})
}

// Extract props from validation states, to pass to components
export const getPropsFromFieldState = (fieldState = []) =>
  fieldState.reduce((acc, { state, validationResult, ...rest }) => {
    return state ? { ...acc, [state]: true, ...rest } : acc
  }, {})

export const nullFieldsByState = (document, fieldStates) => {
  const newDocument = { ...document }

  for (const field in fieldStates) {
    const [{ state } = {}] = fieldStates[field]
    if (statesToReset.includes(state)) {
      newDocument[field] = null
    }
  }

  return newDocument
}

export const createQueryString = (filterField, filterValue, filter) => {
  if (!(filterField && (filterValue || filter))) return ''

  const isArrDep = Array.isArray(filterField) && Array.isArray(filterValue)

  const filterParams = new URLSearchParams()
  const filterValueArr = isArrDep ? filterValue : [filterValue]
  const allFilters = filter ? filterValueArr.concat(filter) : filterValueArr

  if (isArrDep && filterField.length !== allFilters.length) {
    alert('MODEL DEFINITION ERROR: filterfield lenght != filterKey + filter lenght')
    return ''
  }

  allFilters.forEach((filterVal, i) => {
    if (filterVal !== undefined) {
      // Filtering by uri does not work, it must be replaced by code
      const filterFieldItem = isArrDep ? filterField[i] : filterField
      if (filterFieldItem === 'uri') {
        filterParams.append('code', last(filterVal.split('/')))
      } else {
        filterParams.append(filterFieldItem, filterVal)
      }
    }
  })
  return filterParams.toString()
}

export const getOptionsRemoteUrl = (instanceId, documentType, query) =>
  `${BASE_URL}/instances/${instanceId}/documents?type=${documentType}${
    query ? `&${query}` : ''
  }`

export const getOptionsCacheKey = (instanceId, documentType, query) =>
  `${documentType}:${instanceId}${query ? `:${query}` : ''}`
