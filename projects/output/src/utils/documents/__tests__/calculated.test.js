import { calculateField } from '../calculated-fields'

const keyBy = (array, key) =>
  array.reduce((acc, item) => ({ ...acc, [item[key]]: item }), {})

const unitofmeasure = [
  {
    uri: 'ev://unitofmeasure/1',
    title: 'Doc 1',
    quantity: 40
  },
  {
    uri: 'ev://unitofmeasure/2',
    title: 'Doc 2',
    quantity: 102
  },
  {
    uri: 'ev://unitofmeasure/3',
    title: 'Doc 3',
    quantity: 66
  }
]

const vehicles = [
  {
    uri: 'ev://vehicle/1',
    title: 'Vec 1',
    cargo: 100,
    unitsItems: [
      {
        name: 'Soldier 1',
        power: 20
      },
      {
        name: 'Soldier 2',
        power: 14
      },
      {
        name: 'Soldier 3',
        power: 8
      }
    ]
  },
  {
    uri: 'ev://vehicle/2',
    title: 'Vec 2',
    cargo: 90,
    unitsItems: [
      {
        name: 'Soldier 4',
        power: 30
      },
      {
        name: 'Soldier 2',
        power: 14
      }
    ]
  }
]

jest.mock('utils/documents/RemoteOptions', () => ({
  linkedDocsCache: {
    getOptions: (key) =>
      ({
        'unitofmeasure:1': {
          list: unitofmeasure,
          map: keyBy(unitofmeasure, 'uri')
        },
        'vehicles:1': {
          list: vehicles,
          map: keyBy(vehicles, 'uri')
        }
      }[key])
  }
}))

const fieldsMap = {
  age: {
    id: 'age',
    type: 'number'
  },
  total: {
    id: 'total',
    type: 'number'
  },
  calculated1: {
    type: 'calculated',
    operation: 'multiplication',
    operands: ['age', 'total']
  },
  linkedDoc1: {
    documentType: 'unitofmeasure',
    valueKey: 'uri'
  },
  name: { type: 'text' },
  points: { type: 'number' },
  calculated2: {
    field: 'calculated2',
    type: 'calculated',
    cellEditorParams: {
      operation: 'concat',
      operands: ['name', 'points']
    }
  },
  linkedDoc2: {
    field: 'linkedDoc2',
    cellEditorParams: {
      documentType: 'unitofmeasure',
      valueKey: 'uri'
    }
  },
  'calculated3-link': {
    field: 'calculated3-link',
    type: 'calculated',
    cellEditorParams: {
      operation: 'multiplication',
      operands: ['linkedDoc2.quantity', 'points']
    }
  },
  linkedDetail: {
    field: 'linkedDetail',
    type: 'autocomplete',
    cellEditorParams: {
      documentType: 'vehicles',
      optionsKey: 'unitsItems',
      valueKey: 'name'
    }
  },
  'calculated4-link-detail': {
    field: 'calculated4-link-detail',
    type: 'calculated',
    cellEditorParams: {
      operation: 'multiplication',
      operands: [['linkedDetail.power', { isDetail: true }], 'points']
    }
  }
}

const instanceId = 1

describe('CALCULATED FIELDS', () => {
  describe('Calculations with operands in the same document', () => {
    const params = {
      instanceId,
      fieldsMap,
      document: { id: 'abc', age: 18, total: 10, points: 0 },
      operands: ['age', 'total']
    }

    it('Addition', async () => {
      const result = await calculateField({ operation: 'addition', ...params })
      expect(result).toEqual(28)
    })

    it('Subtraction', async () => {
      const result = await calculateField({
        operation: 'subtraction',
        ...params
      })
      expect(result).toEqual(8)
    })

    it('Multiplication', async () => {
      const result = await calculateField({
        operation: 'multiplication',
        ...params
      })
      expect(result).toEqual(180)
    })

    it('Division', async () => {
      const result = await calculateField({ operation: 'division', ...params })
      expect(result).toEqual(1.8)
    })

    it('Concatenation', async () => {
      const result = await calculateField({ operation: 'concat', ...params })
      expect(result).toEqual('18_10')
    })

    it('Concatenation with custom separator', async () => {
      const result = await calculateField({
        operation: ['concat', { separator: '--' }],
        ...params
      })
      expect(result).toEqual('18--10')
    })

    it('Correctly handles a zero (0) value', async () => {
      const result = await calculateField({
        operation: 'multiplication',
        ...params,
        operands: ['points', 'total']
      })
      expect(result).toEqual(0)
    })
  })

  describe('Calculations with operands in the same document accessing detail items', () => {
    const params = {
      instanceId,
      fieldsMap,
      document: {
        id: 'abc',
        age: 18,
        total: 10,
        peopleItems: [
          {
            name: 'Maria',
            points: 44
          },
          {
            name: 'Franco',
            points: 20
          },
          {
            name: 'Assunta',
            points: 66
          },
          {
            name: 'Luciano',
            points: 77
          }
        ]
      }
    }

    it('Concatenation of detail fields', async () => {
      const result = await calculateField({
        operation: 'concat',
        operands: ['peopleItems.name'],
        ...params
      })
      expect(result).toEqual('Maria_Franco_Assunta_Luciano')
    })

    it('Addition of detail fields', async () => {
      const result = await calculateField({
        operation: 'addition',
        operands: ['peopleItems.points'],
        ...params
      })
      expect(result).toEqual(207)
    })

    it('Addition of detail fields with fields from the root level', async () => {
      const result = await calculateField({
        operation: 'addition',
        operands: ['peopleItems.points', 'age', 'total'],
        ...params
      })
      expect(result).toEqual(235)
    })
  })

  describe('Calculations with operands in other documents [linked documents]', () => {
    const params = {
      instanceId,
      fieldsMap,
      document: {
        id: 'abc',
        age: 18,
        total: 10,
        linkedDoc1: 'ev://unitofmeasure/2',
        peopleItems: [
          {
            name: 'Maria',
            points: 44,
            linkedDoc2: 'ev://unitofmeasure/1',
            linkedDetail: 'Soldier 1'
          },
          {
            name: 'Franco',
            points: 20,
            linkedDoc2: 'ev://unitofmeasure/3',
            linkedDetail: 'Soldier 2'
          },
          {
            name: 'Assunta',
            points: 66,
            linkedDoc2: 'ev://unitofmeasure/3',
            linkedDetail: 'Soldier 1'
          },
          {
            name: 'Luciano',
            points: 77,
            linkedDoc2: 'ev://unitofmeasure/2',
            linkedDetail: 'Soldier 3'
          }
        ]
      }
    }

    it('Multiplication with a field in a linked document at the root level', async () => {
      const result = await calculateField({
        operation: 'multiplication',
        operands: ['total', 'linkedDoc1.quantity'],
        ...params
      })
      expect(result).toEqual(1020)
    })

    it('Concatenation of a field in a linked document in details', async () => {
      const result = await calculateField({
        operation: 'concat',
        operands: ['peopleItems.linkedDoc2.quantity'],
        ...params
      })
      expect(result).toEqual('40_66_66_102')
    })

    it('Addition with a detail field in a linked document', async () => {
      const result = await calculateField({
        operation: 'addition',
        operands: [['peopleItems.linkedDetail.power', { isDetail: true }]],
        ...params
      })
      expect(result).toEqual(62)
    })
  })

  describe('Calculations of fields depending on other calculated fields', () => {
    const params = {
      instanceId,
      fieldsMap,
      document: {
        id: 'abc',
        age: 18,
        total: 10,
        peopleItems: [
          {
            name: 'Maria',
            points: 44
          },
          {
            name: 'Franco',
            points: 20
          },
          {
            name: 'Assunta',
            points: 66
          },
          {
            name: 'Luciano',
            points: 77
          }
        ]
      }
    }

    it('Addition of a field and another calculated field at the root level', async () => {
      const result = await calculateField({
        operation: 'addition',
        operands: ['age', 'calculated1'],
        ...params
      })
      expect(result).toEqual(198)
    })

    it('Concatenation of a field and another calculated field in the details', async () => {
      const result = await calculateField({
        operation: ['concat', { separator: '/' }],
        operands: ['age', 'peopleItems.calculated2'],
        ...params
      })
      expect(result).toEqual('18/Maria_44/Franco_20/Assunta_66/Luciano_77')
    })
  })

  describe('Calculations of fields depending on other calculated fields [linked documents]', () => {
    const params = {
      instanceId,
      fieldsMap,
      document: {
        id: 'abc',
        age: 18,
        total: 10,
        linkedDoc1: 'ev://unitofmeasure/2',
        peopleItems: [
          {
            name: 'Maria',
            points: 44,
            linkedDoc2: 'ev://unitofmeasure/1', // 44 * 40 = 1760
            linkedDetail: 'Soldier 3' // 44 * 8 = 352
          },
          {
            name: 'Franco',
            points: 20,
            linkedDoc2: 'ev://unitofmeasure/3', // 20 * 66 = 1320
            linkedDetail: 'Soldier 2' // 20 * 14 = 280
          },
          {
            name: 'Assunta',
            points: 66,
            linkedDoc2: 'ev://unitofmeasure/3', // 66 * 66 = 4356
            linkedDetail: 'Soldier 1' // 66 * 20 = 1320
          },
          {
            name: 'Luciano',
            points: 77,
            linkedDoc2: 'ev://unitofmeasure/2', // 77 * 102 = 7854
            linkedDetail: 'Soldier 2' // 77 * 14 = 1078
          }
        ]
      }
    }

    it('Addition of a root field with a calculated field in details accessing a linked document', async () => {
      const result = await calculateField({
        operation: 'addition',
        operands: ['total', 'peopleItems.calculated3-link'],
        ...params
      })
      expect(result).toEqual(15300)
    })

    it('Concatenation of a root field with a calculated field in details accessing a linked document details item', async () => {
      const result = await calculateField({
        operation: 'concat',
        operands: ['age', 'peopleItems.calculated4-link-detail'],
        ...params
      })
      expect(result).toEqual('18_352_280_1320_1078')
    })
  })
})
