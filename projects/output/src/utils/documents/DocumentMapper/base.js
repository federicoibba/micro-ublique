import _compose from 'lodash/fp/compose'
import { addNotification } from 'components/Notifications/actions'

export default class BaseMapper {
  constructor(params) {
    this.params = params
  }

  transformDetails = (mapper) => (document) => {
    const { detailItemIds = [] } = this.params

    if (detailItemIds.length === 0) return document // A document might not have details

    const transformedDocument = { ...document }

    const detailMapper = mapper(document)

    detailItemIds.forEach((detailId) => {
      const details = document[detailId]
      if (!details) {
        showError(document, detailId) // Fast and dirty way to show errors
      } else {
        transformedDocument[detailId] = details.map(detailMapper)
      }
    })

    return transformedDocument
  }

  composeAsync(...fns) {
    return async (input) => {
      let acc = input
      for (const fn of fns.reverse()) {
        acc = await fn(acc)
      }
      return acc
    }
  }

  compose = _compose
}

const showError = (document, detailId) => {
  addNotification({
    kind: 'error',
    duration: 8000,
    title: 'Model mismatch',
    message: `"${detailId}" not found in "${document.title}"`
  })
}
