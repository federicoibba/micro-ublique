import { isEmpty } from 'lodash'
import BackendDocumentMapper from './backend'
import FrontentDocumentMapper from './frontend'

export const extraKeys = {
  DETAIL_ID_KEY: '_id',
  DOCUMENT_ID_KEY: '_documentId',
  ORDER_KEY: '_order',
  IS_BEING_EDITED: '_isBeingEdited'
}

// Fields of these types do not exist in the document, and must be resolved/calculated,
// then removed when sending the document back to server
const virtualFields = ['calculated', 'readonly']

class DocumentMapper {
  constructor(params) {
    const fieldsToResolve = this.getFieldsToResolveBySection(
      params.fieldsMap,
      virtualFields
    )
    const _params = {
      ...params,
      fieldsToResolve,
      hasFieldsToResolve: !isEmpty(fieldsToResolve)
    }
    this.frontendMapper = new FrontentDocumentMapper(_params)
    this.backendMapper = new BackendDocumentMapper(_params)
  }

  getFieldsToResolveBySection(fieldsMap, types) {
    return Object.values(fieldsMap).reduce((acc, field) => {
      return types.includes(field.type)
        ? { ...acc, [field.parent]: [...(acc[field.parent] || []), field] }
        : acc
    }, {})
  }

  get frontend() {
    return {
      transformDocument: (document) => {
        return this.frontendMapper.process(document)
      },
      transformDocuments: (documents) => {
        return Promise.all(documents.map((x) => this.frontendMapper.process(x)))
      },
      addVirtualFields: this.frontendMapper.addVirtualFields
    }
  }

  get backend() {
    return {
      transformDocument: (document) => {
        return this.backendMapper.process(document)
      }
    }
  }
}

export default DocumentMapper
