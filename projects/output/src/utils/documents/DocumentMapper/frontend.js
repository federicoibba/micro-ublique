import { v4 as uuidv4 } from 'uuid'
import { get } from 'lodash'
import { calculateField } from '../calculated-fields'
import { linkedDocsCache } from '../RemoteOptions'
import { adapters, buildRowId, getOptionsCacheKey, getOptionsRemoteUrl } from '..'
import BaseMapper from './base'
import { extraKeys } from '.'

export const generateUniqueId = (item) => ({
  [extraKeys.DETAIL_ID_KEY]: uuidv4(),
  ...item
})

const makeAddDocumentId = (id) => (item) => ({
  [extraKeys.DOCUMENT_ID_KEY]: buildRowId(id),
  ...item
})

export default class FrontentDocumentMapper extends BaseMapper {
  constructor(params) {
    super(params)
    this.params = params
    this.resolversMap = {
      calculated: this.calculatedFieldResolver,
      readonly: this.readonlyFieldResolver
    }
  }

  addDocumentId(document) {
    return makeAddDocumentId(document.id)(document)
  }

  reduceLinkedObjectsToUri(document) {
    return Object.entries(document).reduce((acc, [key, value]) => {
      return { ...acc, [key]: value?.uri || value }
    }, {})
  }

  calculatedFieldResolver = (document, field) => {
    const { fieldsMap, instanceId } = this.params
    const { operation, operands } = adapters.getCellParams(field)

    return calculateField({
      instanceId,
      operation,
      operands,
      fieldsMap,
      document
    })
  }

  getLegacyReadonlyParams(cellParams) {
    const { findBy, ...rest } = cellParams
    const [, sourceFieldKey] = findBy
    return { sourceFieldKey, ...rest }
  }

  getReadonlyParams({ src, labelKey }) {
    const { fieldsMap } = this.params
    const sourceField = fieldsMap[src]
    const { documentType, filterField, filterKey, filter } =
      adapters.getCellParams(sourceField)
    return {
      sourceFieldKey: src,
      documentType,
      labelKey,
      // The following are unused for now...
      filterField,
      filterKey,
      filter
    }
  }

  readonlyFieldResolver = async (document, field) => {
    const { instanceId } = this.params

    const cellParams = adapters.getCellParams(field)

    const { sourceFieldKey, documentType, labelKey } = cellParams.findBy
      ? this.getLegacyReadonlyParams(cellParams)
      : this.getReadonlyParams(cellParams)

    const uri = document[sourceFieldKey] // The document uri to read from

    const { map } = await linkedDocsCache.getOptions(
      getOptionsCacheKey(instanceId, documentType),
      getOptionsRemoteUrl(instanceId, documentType)
    )

    // Allow concatenation of multiple keys
    if (Array.isArray(labelKey)) {
      return labelKey.reduce(
        (acc, key) => `${acc}${get(map, `${uri}.${key}`, key)}`,
        ''
      )
    }

    return get(map, `${uri}.${labelKey}`)
  }

  addVirtualFields = async (document) => {
    const { fieldsToResolve, hasFieldsToResolve } = this.params

    if (!hasFieldsToResolve) return document
    // console.log('-- resolving calculated/readonly')
    const { main = [], ...details } = fieldsToResolve

    // Resolve fields at the root of the document
    const newDocument = await this.resolveFields(document, main)

    // Resolve fields nested in details
    for (const [detailKey, detailFields] of Object.entries(details)) {
      const newDetails = []

      for (const detailItem of newDocument[detailKey]) {
        const newDetail = await this.resolveFields(detailItem, detailFields)
        newDetails.push(newDetail)
      }

      newDocument[detailKey] = newDetails
    }

    return newDocument
  }

  resolveFields = async (document, fields) => {
    const documentCopy = Object.assign({}, document)
    for (const field of fields) {
      const fieldId = adapters.getId(field)
      const resolver = this.resolversMap[field.type]
      documentCopy[fieldId] = await resolver(documentCopy, field)
    }
    return documentCopy
  }

  makeDetaildMapper = (document) => {
    return this.compose(
      this.reduceLinkedObjectsToUri,
      generateUniqueId,
      makeAddDocumentId(document.id)
    )
  }

  process(document) {
    const composition = this.composeAsync(
      this.addVirtualFields,
      this.addDocumentId,
      this.reduceLinkedObjectsToUri,
      this.transformDetails(this.makeDetaildMapper)
    )
    return composition(document)
  }
}
