import { isBoolean } from 'lodash'
import compose from 'lodash/fp/compose'
import prop from 'lodash/fp/prop'
import {
  canRead,
  canCreate,
  canUpdate,
  canDelete,
  getPermissionsValue,
  PERMISSIONS
} from 'services/auth'

class PermissionsManager {
  constructor({
    permission,
    fieldPermission,
    modelPermissions, // Hardcoded permissions in model.json configuration, defined as { [permission_name]: boolean }
    readOnly = false,
    isNewDocument = false
  }) {
    this.documentPermission = permission
    this.fieldsPermission = fieldPermission ?? {}
    this.modelPermission = modelPermissions
      ? getModelPermissionAsInt(modelPermissions)
      : undefined
    this.readOnly = readOnly
    this.isNewDocument = isNewDocument
  }

  checkPermissionType(handler, id) {
    if (this.readOnly) {
      return handler === canRead
    }

    // When permissions are set at the model level, they must be valid before checking the others
    if (this.modelPermission !== undefined && !handler(this.modelPermission)) {
      return false
    }

    return handler(
      id
        ? this.fieldsPermission[id] ?? this.documentPermission
        : this.documentPermission
    )
  }

  canRead = (id) => {
    return this.checkPermissionType(canRead, id)
  }

  canCreate = (id) => {
    return this.checkPermissionType(canCreate, id)
  }

  canUpdate = (id) => {
    return this.checkPermissionType(canUpdate, id)
  }

  canDelete = (id) => {
    return this.checkPermissionType(canDelete, id)
  }

  canEdit = (id) => {
    return this.isNewDocument ? this.canCreate(id) : this.canUpdate(id)
  }

  isDocumentActionHidden = (action) => {
    const { readOnly, isNewDocument } = this

    switch (action.id) {
      case 'delete':
        return isNewDocument || readOnly || !this.canDelete()
      case 'save':
        return readOnly || !this.canEdit()
      case 'import':
        return isNewDocument || readOnly || !this.canCreate()
      case 'export':
      case 'refresh':
        return isNewDocument
      default:
        return false
    }
  }

  get rawPermissions() {
    return {
      permission: this.documentPermission,
      fieldPermission: this.fieldsPermission
    }
  }
}

const createPermissionsObject = (permissions) =>
  Object.values(PERMISSIONS).reduce((acc, pName) => {
    const pValue = permissions[pName]
    return {
      ...acc,
      [pName]: isBoolean(pValue) ? pValue : true
    }
  }, {})

const getModelPermissionAsInt = compose(
  prop('permission'),
  getPermissionsValue,
  createPermissionsObject
)

export default PermissionsManager
