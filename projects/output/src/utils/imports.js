const storesRequire = require.context('..', true, /.*\/state\.js$/)
const stores = storesRequire.keys().reduce((acc, statePath) => {
  return { ...acc, ...storesRequire(statePath).default }
}, {})

const modalsRequire = require.context('..', true, /.*\/modals\.js$/)
const modals = modalsRequire.keys().reduce((acc, modalsPath) => {
  return { ...acc, ...modalsRequire(modalsPath).default }
}, {})

const routesRequire = require.context('..', true, /routes?\.js$/)
const routes = routesRequire.keys().reduce((acc, page) => {
  const pageRoutes = routesRequire(page).default

  if (!pageRoutes) return acc

  if (Array.isArray(pageRoutes)) {
    return [...acc, ...pageRoutes]
  }

  return [...acc, pageRoutes]
}, [])

export { stores, modals, routes }
