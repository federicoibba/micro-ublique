import { useTranslation } from 'react-i18next'
import { useEffect, useMemo, useRef, useState } from 'react'

export default function useDocumentTranslation(documentType) {
  const { t, i18n } = useTranslation(['Models', 'Document', 'Options'])

  const modifyT = useMemo(() => {
    return (elementKey) => {
      return elementKey.includes(':')
        ? t(elementKey)
        : t(`DocumentModel-${documentType}.${elementKey}`)
    }
  }, [t])

  return { t: modifyT, i18n }
}

export function useAgGridTranslation() {
  const { t, i18n } = useTranslation('agGrid')
  const [destroyed, setDestroyed] = useState(false)
  const firstRenderRef = useRef(true)

  useEffect(() => {
    if (firstRenderRef.current) {
      firstRenderRef.current = false
      return
    }

    // Not very clean, but it's in the official docs...
    // https://blog.ag-grid.com/switching-the-localization-language-in-ag-grid/#destroying-recreating
    setDestroyed(true)
    setTimeout(() => setDestroyed(false))
  }, [i18n.language])

  return { t, destroyed }
}
