import { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { I18N_REMOTE } from 'config'

const defaultLanguages = {
  'it-IT': { full: 'Italiano', short: 'IT' },
  'en-GB': { full: 'English', short: 'EN' }
}

export const getI18nControls = (i18n, languages = defaultLanguages) => ({
  languages,
  selectedLanguage: i18n.language,
  onLanguageChange: (lang) =>
    // Avoid rerenders while language is being changed
    Promise.resolve().then(() => {
      if (lang !== i18n.language) {
        if (I18N_REMOTE) {
          localStorage.setItem('i18n-lang', lang)
        }

        return i18n.changeLanguage(lang)
      }
    })
})

const useI18nControls = (languages = defaultLanguages) => {
  const { i18n } = useTranslation()
  const controls = useMemo(
    () =>
      // Show controls only when there is more than 1 language
      Object.keys(languages).length > 1
        ? getI18nControls(i18n, languages)
        : undefined,
    [i18n.language]
  )
  return controls
}

export default useI18nControls
