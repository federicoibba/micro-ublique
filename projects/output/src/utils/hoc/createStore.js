/* eslint-disable no-prototype-builtins */
/**
 * Created by fgallucci on 27/04/2020.
 */

import React, { PureComponent } from 'react'
import StoreContext from 'context/StoreContext'
import StoreInstancesContext from 'context/StoreInstancesContext'

/**
 *
 * @param {function} WrappedComponent component to wrap
 * @param {Object} stores key/value synapse store
 * @param {Object=} options
 * @return {*}
 */
export default function createStore(WrappedComponent, stores, options = {}) {
  if (process.env.NODE_ENV !== 'production') {
    window.__PRINT_STORE__ = (storeKey) => {
      if (storeKey) {
        if (stores[storeKey]) {
          console.log(stores[storeKey].get())
        } else {
          console.warn(`No store with key ${storeKey}`)
        }
      } else if (!storeKey) {
        Object.keys(stores).forEach(window.__PRINT_STORE__)
      }
      return storeKey
    }
  }
  return class extends PureComponent {
    constructor(props) {
      super(props)
      this.state = {}
    }

    componentDidMount() {
      Object.entries(stores).forEach(([storeKey, store]) => {
        store.subscribe((value) => {
          this.setState({ [storeKey]: value })
        })
      })
    }

    render() {
      return (
        <StoreInstancesContext.Provider value={stores}>
          <StoreContext.Provider value={this.state}>
            <WrappedComponent {...this.props} />
          </StoreContext.Provider>
        </StoreInstancesContext.Provider>
      )
    }
  }
}
