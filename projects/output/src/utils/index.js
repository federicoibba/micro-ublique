import { padStart } from 'lodash'

export const delay = (ms) =>
  new Promise((resolve) => {
    setTimeout(resolve, ms)
  })

export const updateState = (state, newData) =>
  state.update((prevState) => ({ ...prevState, ...newData }))

export const formatMinutes = (minutes) => {
  const hours = Math.floor(minutes / 60)
  const remaningMinutes = Math.round(minutes % 60)
  return `${padStart(hours, 2, '0')}:${padStart(remaningMinutes, 2, '0')}`
}
