import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import ChainedBackend from 'i18next-chained-backend'
import Backend from 'i18next-http-backend'
import resourcesToBackend from 'i18next-resources-to-backend'
import { noop, merge } from 'lodash'
import { I18N_REMOTE, I18N_DEFAULT, I18N_SUPPORTED_LANGUAGES, BASE_URL } from 'config'

const debug = false // process.env.NODE_ENV === 'development'

const getPlatformLanguages = () => {
  const languagesRequire = require.context('./languages', true, /.*\.js$/)
  const languageFilesKeys = languagesRequire?.keys()
  const platformLanguages = languageFilesKeys?.reduce((acc, file) => {
    const [lang, fileName] = file.slice(2).split('/')
    const [langKey] = lang.split('-')
    const namespace = fileName.slice(0, -3)

    return {
      ...acc,
      [langKey]: {
        ...acc[langKey],
        [namespace]: languagesRequire(file).default
      }
    }
  }, {})
  return platformLanguages
}

let initI18n = noop
let language = localStorage.getItem('i18n-lang')

if (!language) {
  localStorage.setItem('i18n-lang', I18N_DEFAULT)
  language = I18N_DEFAULT
}

if (I18N_REMOTE) {
  const config = {
    debug,
    lng: language,
    fallbackLng: I18N_SUPPORTED_LANGUAGES.includes(language)
      ? language
      : I18N_DEFAULT,
    ns: 'Common',
    defaultNS: 'Common',
    missingKeyHandler: false,
    returnEmptyString: false,
    keySeparator: '.',
    load: 'currentOnly',
    interpolation: {
      escapeValue: false
    },
    backend: {
      backends: [
        Backend,
        resourcesToBackend((language, namespace, callback) => {
          import(`./languages/${language}/${namespace}.json`)
            .then((resources) => {
              callback(null, resources)
            })
            .catch((error) => {
              callback(error, null)
            })
        })
      ],
      backendOptions: [
        {
          loadPath: `${BASE_URL}/translations/{{lng}}/{{ns}}`,
          requestOptions: {
            mode: 'cors',
            credentials: 'same-origin',
            cache: 'default'
          }
        }
      ]
    }
  }

  const initCB = (err, t) => {
    if (err) return console.error(err)
    console.log('I18N LOADED')
  }

  initI18n = () => i18n.use(initReactI18next).use(ChainedBackend).init(config, initCB)
} else {
  const platformLanguages = getPlatformLanguages()
  initI18n = (additionalLanguages, initialLanguage = I18N_DEFAULT) => {
    let language = localStorage.getItem('i18n-lang')

    if (!language) {
      localStorage.setItem('i18n-lang', initialLanguage)
      language = initialLanguage
    }

    const resources = merge(platformLanguages, additionalLanguages)
    console.log('All languages', resources)
    const config = {
      resources,
      debug,
      lng: language,
      fallbackLng: initialLanguage,
      defaultNS: 'Common',
      missingKeyHandler: false,
      keySeparator: '.',
      interpolation: {
        escapeValue: false
      }
    }

    return i18n.use(initReactI18next).init(config)
  }
}

export const getAllLanguages = (additionalLanguages) => {
  const resources = merge(getPlatformLanguages(), additionalLanguages)
  return resources
}

export default initI18n
