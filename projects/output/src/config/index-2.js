import { lazy } from 'react'
import { List32, Analytics32, Roadmap32, Box32, Van32 } from '@carbon/icons-react'

/* const KPIs = lazy(() => import('../../pages/KPIs'))
const GanttAndMap = lazy(() => import('../../pages/GanttAndMap'))
const GroundedOrders = lazy(() => import('../../pages/GroundedOrders'))
const ToursManagement = lazy(() => import('../../pages/ToursManagement'))
const VehiclesAllocation = lazy(() =>
  import('../../pages/VehiclesAllocation')
)

const outputConfigComponents = {
  KPIOverview: {
    component: KPIs,
    icon: Analytics32
  },
  ToursManagement: {
    component: ToursManagement,
    icon: List32
  },
  GanttAndMap: {
    component: GanttAndMap,
    icon: Roadmap32
  },
  GroundedOrders: {
    component: GroundedOrders,
    icon: Box32
  },
  VehiclesAllocation: {
    component: VehiclesAllocation,
    icon: Van32
  }
} */

export default outputConfigComponents
