export const DEFAULT_THEME = 'dark'

export const BASE_URL =
  process.env.REACT_APP_BASE_URL ||
  `${window.location.origin}${window.location.pathname}api`

export const ENABLE_COMPARTMENTS = process.env.ENABLE_COMPARTMENTS === 'true'

export const EXTERNAL_SERVER_URL = process.env.EXTERNAL_SERVER_URL

export const MAPBOX_GL_TOKEN = process.env.MAPBOX_GL_TOKEN

export const MAPBOX_URL = process.env.MAPBOX_URL

export const INTERACTIVE_OUTPUT_MAP = process.env.INTERACTIVE_OUTPUT_MAP !== 'false'

export const I18N_REMOTE = true

export const I18N_DEFAULT = 'it-IT'

export const I18N_SUPPORTED_LANGUAGES = ['it-IT', 'en-GB', 'es-ES', 'ru-RU']
