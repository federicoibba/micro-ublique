module.exports = {
  printWidth: 86,
  semi: false,
  singleQuote: true,
  trailingComma: 'none',
  tabWidth: 2
}
