/* eslint-disable no-prototype-builtins */
/**
 * Created by fgallucci on 27/04/2020.
 */

import React, { PureComponent } from 'react'
import StoreContext from 'context/StoreContext'

export default function withStore(WrappedComponent, keys = []) {
  return class extends PureComponent {
    static displayName = `withStore(${
      WrappedComponent.displayName ||
      WrappedComponent.name ||
      (WrappedComponent.type && WrappedComponent.type.name) ||
      'Component'
    })`

    static WrappedComponent = WrappedComponent

    render() {
      return (
        <StoreContext.Consumer>
          {(stores) => {
            const ownStores = keys.reduce((acc, storeKey) => {
              if (!stores?.hasOwnProperty(storeKey)) {
                // console.warn(`${storeKey} is not a valid store key `)
                return acc
              }

              return { ...acc, [storeKey]: stores[storeKey] }
            }, {})

            return <WrappedComponent {...this.props} {...ownStores} />
          }}
        </StoreContext.Consumer>
      )
    }
  }
}

export function withStoreCurried(keys = []) {
  return function wrapComponent(WrappedComponent) {
    return withStore(WrappedComponent, keys)
  }
}
