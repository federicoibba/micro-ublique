import { toPairs, reduce } from 'lodash'
import {
  validateANDconditions,
  validateORconditions,
  joinArrays,
  convertToFullNotation,
  getDefaultState,
  excludeUndefinedState
} from './helpers'
import {
  LOGICAL_OP,
  SHORT_FORM_KEY,
  DEFAULT_STATE_KEY,
  EXPLICIT_AND_ERROR,
  IGNORED_STATE_PREFIX
} from './constants'

const defaultOptions = {
  showValidationResult: true
}

export const validateField = (data, field, options = defaultOptions) => {
  const { showValidationResult } = options
  const { states, id } = field[SHORT_FORM_KEY] ? convertToFullNotation(field) : field

  if (!states) return getDefaultState()

  const { [DEFAULT_STATE_KEY]: defaultState, ...statesToValidate } = states

  const validStates = reduce(
    toPairs(statesToValidate),
    (acc, [state, stateConfig]) => {
      if (state.startsWith(IGNORED_STATE_PREFIX)) return acc

      const {
        when: fieldsConditions = {},
        states: nestedStates,
        ...stateData
      } = stateConfig

      const {
        [LOGICAL_OP.OR]: ORconditions,
        [LOGICAL_OP.AND]: ANDconditionsExplicit,
        ...ANDconditions
      } = fieldsConditions

      if (ANDconditionsExplicit) {
        throw new Error(EXPLICIT_AND_ERROR)
      }

      const { isValid: ORvalid, validationResult: ORvalidationResult } =
        validateORconditions({ showValidationResult }, id, data, ORconditions)

      // If none of the OR conditions is valid, skip AND's, as the state is already invalid
      const { isValid: ANDvalid, validationResult: ANDvalidationResult } = ORvalid
        ? validateANDconditions({ showValidationResult }, id, data, ANDconditions)
        : { isValid: false }

      // The field can transition to the state and evaluate nested states
      if (ORvalid && ANDvalid) {
        const validNestedStates = nestedStates
          ? excludeUndefinedState(validateField(data, { states: nestedStates, id }))
          : []

        if (
          !validNestedStates.length &&
          !Object.keys(ORconditions || {}).length &&
          !Object.keys(ANDconditions || {}).length
        ) {
          return acc
        }

        return [
          ...acc,
          ...validNestedStates,
          {
            state,
            ...stateData,
            validationResult: joinArrays(ANDvalidationResult, ORvalidationResult)
          }
        ]
      }

      return acc
    },
    []
  )

  // Default state when no other state is valid
  return validStates.length ? validStates : getDefaultState(defaultState)
}

export const validateFields = (data = {}, fields = {}, options = defaultOptions) =>
  Object.entries(fields).reduce((acc, [id, field]) => {
    const { $each } = field
    return {
      ...acc,
      [id]: $each
        ? validateEachValue(data[id], $each, options)
        : validateField(data, { ...field, id }, options)
    }
  }, {})

export const validateEachValue = (data = [], fields, options = defaultOptions) =>
  data.map((row) => validateFields(row, fields, options))
