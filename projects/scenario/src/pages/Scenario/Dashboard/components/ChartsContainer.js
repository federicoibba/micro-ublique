import React, { Fragment, useMemo } from 'react'
import { Row, Column } from '@ublique/components/Grid'
import { useTranslation } from 'react-i18next'

import Queue from './Queue'
import Types from './Types'
import Total from './Total'
import Status from './Status'
import Errors from './Errors'
import Progress from './Progress'

import {
  getGroupedData,
  getProgressData,
  getStatusSize,
  getTotalData
} from 'pages/Scenario/utils/charts'

import { ERROR, SCENARIO_TYPE, WAIT, STATUS } from 'pages/Scenario/constants'

const DashboardCharts = ({ scenarioInfo }) => {
  const { t } = useTranslation('Scenario')

  const [
    queueChartData,
    errorChartData,
    totalChartData,
    statusChartData,
    progressChartData,
    typesChartData
  ] = useMemo(
    () => [
      getStatusSize(scenarioInfo, WAIT),
      getStatusSize(scenarioInfo, ERROR),
      getTotalData(scenarioInfo, t),
      getGroupedData(scenarioInfo, STATUS, t),
      getProgressData(scenarioInfo, t),
      getGroupedData(scenarioInfo, SCENARIO_TYPE, t)
    ],
    [scenarioInfo, t]
  )

  return (
    <Fragment>
      <Row>
        <Column className="quarter-chart">
          <Total data={totalChartData} />
        </Column>
        <Column className="quarter-chart">
          <Progress data={progressChartData} />
        </Column>
        <Column className="quarter-chart">
          <Errors numErrors={errorChartData} />
        </Column>
        <Column className="quarter-chart">
          <Queue numQueue={queueChartData} />
        </Column>
      </Row>
      <Row>
        <Status data={statusChartData} />
        <Types data={typesChartData} />
      </Row>
    </Fragment>
  )
}

export default React.memo(DashboardCharts)
