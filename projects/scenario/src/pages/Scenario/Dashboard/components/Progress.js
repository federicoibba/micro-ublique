import React, { Fragment, useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { map } from 'lodash'

import { useThemeVariable } from '@ublique/components/Theme'

import Charts from '@ublique/components/Charts'
import { CHARTS_COLOR_THEME } from 'pages/Scenario/constants'
import { formatLabelToInt } from 'pages/Scenario/utils/charts'

const getOptions = (config) => ({
  data: config.data,
  background: {
    visible: false
  },
  series: [
    {
      type: 'bar',
      xKey: 'type',
      yKeys: ['value'],
      fills: CHARTS_COLOR_THEME,
      strokes: 0,
      highlightStyle: {
        fill: useThemeVariable('highlight'),
        stroke: useThemeVariable('highlight')
      }
    }
  ],
  axes: [
    {
      type: 'category',
      position: 'left',
      tick: {
        width: 0
      },
      label: {
        color: useThemeVariable('text01')
      }
    },
    {
      type: 'number',
      position: 'bottom',
      tick: {
        width: 0
      },
      label: {
        color: useThemeVariable('text01'),
        formatter: formatLabelToInt
      }
    }
  ],
  legend: {
    enabled: false
  }
})

const Progress = ({ data }) => {
  const { t } = useTranslation('Scenario')

  const areDataMissing = useMemo(
    () =>
      map(data.data, 'value').filter((el) => el !== 0 && typeof el !== 'undefined')
        .length === 0,
    [data]
  )

  return (
    <Fragment>
      <h4>{t('Dashboard.ProgressChartTitle')}</h4>
      {areDataMissing ? (
        <p>{t('Dashboard.MissingData')}</p>
      ) : (
        <Charts options={getOptions(data)} />
      )}
    </Fragment>
  )
}

export default React.memo(Progress)
