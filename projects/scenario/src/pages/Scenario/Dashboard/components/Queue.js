import React from 'react'
import moment from 'moment'
import { useTranslation } from 'react-i18next'
import { ISO_DATE_FORMAT } from 'pages/Scenario/constants'

const Queue = ({ numQueue = 0 }) => {
  const { t } = useTranslation('Scenario')

  return (
    <div className="card-number">
      <h4>{t('Dashboard.QueueChartTitle')}</h4>
      <p>{`${t('Dashboard.LastUpdate')}: ${moment().format(ISO_DATE_FORMAT)}`}</p>
      <span>{numQueue}</span>
    </div>
  )
}

export default React.memo(Queue)
