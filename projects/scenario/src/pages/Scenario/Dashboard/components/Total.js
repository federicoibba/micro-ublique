import React, { Fragment } from 'react'
import Charts from '@ublique/components/Charts'
import { useTranslation } from 'react-i18next'
import { useThemeVariable } from '@ublique/components/Theme'

import { CHARTS_COLOR_THEME } from 'pages/Scenario/constants'

const getOptions = (config, t) => ({
  data: config.data,
  background: {
    visible: false
  },
  series: [
    {
      type: 'pie',
      labelKey: 'type',
      angleKey: 'value',
      label: {
        enabled: false
      },
      title: {
        color: useThemeVariable('text01'),
        fontWeight: 400,
        text: `${t('Dashboard.ScenarioTotal')}: ${config.total}`
      },
      fills: CHARTS_COLOR_THEME,
      strokes: 0,
      // strokes: ['#d13579'],
      highlightStyle: {
        fill: useThemeVariable('highlight'),
        stroke: useThemeVariable('highlight')
      },
      innerRadiusOffset: -20
    }
  ],
  legend: {
    position: 'bottom',
    color: useThemeVariable('text01')
  }
})

const Total = ({ data }) => {
  const { t } = useTranslation('Scenario')

  return (
    <Fragment>
      <h4>{t('Dashboard.TotalChartTitle')}</h4>
      <Charts options={getOptions(data, t)} />
    </Fragment>
  )
}

export default React.memo(Total)
