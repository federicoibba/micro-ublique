import React from 'react'
import moment from 'moment'
import { useTranslation } from 'react-i18next'
import { ISO_DATE_FORMAT } from 'pages/Scenario/constants'

const Errors = ({ numErrors = 0 }) => {
  const { t } = useTranslation('Scenario')

  return (
    <div className="card-number">
      <h4>{t('Dashboard.ErrorChartTitle')}</h4>
      <p>{`${t('Dashboard.LastUpdate')}: ${moment().format(ISO_DATE_FORMAT)}`}</p>
      <span>{numErrors}</span>
    </div>
  )
}

export default React.memo(Errors)
