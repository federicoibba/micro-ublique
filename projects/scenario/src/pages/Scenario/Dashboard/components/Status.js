import React from 'react'
import { useTranslation } from 'react-i18next'
import { useThemeVariable } from '@ublique/components/Theme'

import Charts from '@ublique/components/Charts'
import { formatLabelToInt } from 'pages/Scenario/utils/charts'

import { CHARTS_COLOR_THEME } from 'pages/Scenario/constants'

const getOptions = (config, t) => ({
  data: config.data,
  background: {
    visible: false
  },
  series: [
    {
      type: 'column',
      xKey: 'type',
      yKeys: ['value'],
      label: {
        enabled: false
      },
      fills: CHARTS_COLOR_THEME,
      strokes: 0,
      // strokes: ['#d13579'],
      highlightStyle: {
        fill: useThemeVariable('highlight'),
        stroke: useThemeVariable('highlight')
      }
    }
  ],
  axes: [
    {
      type: 'category',
      position: 'bottom',
      title: {
        text: t('Dashboard.StatusChartAxisX'),
        color: useThemeVariable('text01')
      },
      tick: {
        width: 0
      },
      label: {
        color: useThemeVariable('text01')
      }
    },
    {
      type: 'number',
      position: 'left',
      title: {
        text: t('Dashboard.StatusChartAxisY'),
        color: useThemeVariable('text01')
      },
      tick: {
        width: 0
      },
      label: {
        color: useThemeVariable('text01'),
        formatter: formatLabelToInt
      }
    }
  ],
  legend: {
    enabled: false
  }
})

const Status = ({ data }) => {
  const { t } = useTranslation('Scenario')

  return (
    <div className="long-chart">
      <h4>{t('Dashboard.StatusChartTitle')}</h4>
      <Charts options={getOptions(data, t)} />
    </div>
  )
}

export default React.memo(Status)
