import React, { Fragment, useState, useMemo, useCallback } from 'react'
import { useTranslation } from 'react-i18next'
import { isEmpty } from 'lodash'
import moment from 'moment'

import { Grid } from '@ublique/components/Grid'
import Dropdown from '@ublique/components/Dropdown'
import ChartsContainer from './components/ChartsContainer'
import useStoreSelector from 'utils/hooks/useStoreSelector'

import {
  MY_SCENARIOS,
  filterItems,
  ISO_DATE_TIME_FORMAT
} from 'pages/Scenario/constants'

import './index.scss'

const Dashboard = () => {
  const [filterKey, setFilterKey] = useState(MY_SCENARIOS)

  const { t } = useTranslation('Scenario')
  const { originalData } = useStoreSelector('scenario')

  const lastUpdate = moment().format(ISO_DATE_TIME_FORMAT)

  const onFilterChange = useCallback(
    ({ selectedItem: { value } }) => setFilterKey(value),
    [setFilterKey]
  )

  const filteredScenarios = useMemo(() => {
    if (filterKey === MY_SCENARIOS) {
      return originalData.filter(({ owner }) => owner)
    }

    return originalData
  }, [filterKey])

  const renderContent = useMemo(() => {
    if (!isEmpty(filteredScenarios)) {
      return (
        <Fragment>
          <div className="dashboard-subtitle">
            <h2>{t('Dashboard.Subtitle')}</h2>
            <Dropdown
              id="select-scenarios"
              items={filterItems(t)}
              value={filterKey}
              label={t(`Dashboard.${filterKey}`)}
              onChange={onFilterChange}
            />
          </div>
          <Grid className="dashboard-charts">
            <ChartsContainer scenarioInfo={filteredScenarios} />
          </Grid>
        </Fragment>
      )
    }

    return (
      <Fragment>
        <div className="dashboard-subtitle">
          <h2>{t('Dashboard.Subtitle')}</h2>
        </div>
        <Grid className="dashboard-charts">
          <h3>{t('NoScenariosAvailable')}</h3>
        </Grid>
      </Fragment>
    )
  }, [t, filteredScenarios, filterKey, setFilterKey])

  return (
    <div className="scenario-dashboard">
      <div className="dashboard-heading">
        <h1>{t('Dashboard.Heading')}</h1>
        {!isEmpty(filteredScenarios) && (
          <span>{`${t('Dashboard.LastUpdate')}: ${lastUpdate}`}</span>
        )}
      </div>
      {renderContent}
    </div>
  )
}

export default React.memo(Dashboard)
