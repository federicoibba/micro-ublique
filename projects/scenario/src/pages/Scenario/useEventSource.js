import { synapse } from 'services/state'
import { useState, useEffect } from 'react'
import { debounce, groupBy, keys, last } from 'lodash'

const useEventSource = () => {
  const buf = {}
  const [buffer, setBuffer] = useState({})

  const update = debounce(
    (buf) => {
      setBuffer({ ...buf })
    },
    250,
    { maxWait: 1000 }
  )

  useEffect(() => {
    const unsubscribe = synapse.subscribe('scenario', (e) => {
      const data = JSON.parse(e.data).map((event) => JSON.parse(event.data))
      const grouped = groupBy(data, (event) => event.scenarioId)

      for (const key of keys(grouped)) {
        buf[key] = last(grouped[key])
      }

      update(buf)
    })

    return unsubscribe
  }, [])

  return { buffer }
}

export default useEventSource
