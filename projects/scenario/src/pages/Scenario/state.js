export const initialState = {
  buffer: {},
  originalData: [],
  notifications: [],
  tableActions: {},
  permissions: {}
}
