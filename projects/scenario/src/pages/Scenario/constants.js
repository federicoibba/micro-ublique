export const SCENARIO_MANAGER = 'SCENARIO_MANAGER'
export const SCENARIO_DASHBOARD = 'SCENARIO_DASHBOARD'

export const MY_SCENARIOS = 'MyScenarios'
export const ALL_SCENARIOS = 'AllScenarios'

export const PAGINATION_SIZE = 10

export const STATUS = 'status'
export const SCENARIO_TYPE = 'typeValue'

export const CHARTS_COLOR_THEME = ['#AF144B', '#d13579', '#C3266B', '#991850']

export const WAIT = 'WAIT'
export const DONE = 'DONE'
export const READY = 'READY'
export const ERROR = 'ERROR'
export const RESET = 'RESET'
export const CREATED = 'CREATED'
export const RUNNING = 'RUNNING'
export const DELETED = 'DELETED'
export const DELETING = 'DELETING'

export const DATE_FORMAT = 'DD/MM/YYYY HH:mm:ss'
export const ISO_DATE_FORMAT = 'YYYY/MM/DD'
export const ISO_DATE_TIME_FORMAT = 'YYYY/MM/DD HH:mm:ss'

export const filterObjects = [
  {
    key: 'name',
    title: 'Filter.Name'
  },
  {
    key: 'username',
    title: 'Filter.Users'
  },
  {
    key: 'typeValue',
    title: 'Filter.Type'
  },
  {
    key: 'status',
    title: 'Filter.Status'
  }
]

export const filterItems = (t) => [
  {
    label: t(`Dashboard.${MY_SCENARIOS}`),
    value: MY_SCENARIOS
  },
  {
    label: t(`Dashboard.${ALL_SCENARIOS}`),
    value: ALL_SCENARIOS
  }
]

export const ScenarioProgressColumn = {
  ERROR: 'error',
  OUTPUT: 'output',
  PLAY: 'play',
  PROGRESS: 'progress',
  LOADING: 'loading',
  TIME_PROGRESS: 'timeProgress'
}

export const ScenarioPlatformActions = {
  DELETE_SCENARIO: 'deleteScenario',
  RESET_SCENARIO: 'resetScenario'
}

export const ScenarioPermissions = {
  CREATE: 'create',
  READ: 'read',
  UPDATE: 'update',
  DELETE: 'delete',
  EXECUTE: 'execute'
}
