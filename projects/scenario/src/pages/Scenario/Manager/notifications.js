import { uniqueId, isEmpty } from 'lodash'
import { addNotification } from 'components/Notifications/actions'

const notifications = ({ t, name, status, username }) => ({
  success: {
    create: {
      title: !isEmpty(username)
        ? `${t('SCENARIO_ADDED_TITLE_USERNAME')} ${username}`
        : t('SCENARIO_ADDED_TITLE'),
      message: `${name} ${t('SCENARIO_ADDED_CAPTION')}`
    }
  },
  warning: {
    delete: {
      title: `${t('SCENARIO_DELETED_TITLE')}`,
      message: `${name} ${t('SCENARIO_DELETED_CAPTION')}`
    }
  },
  error: {
    deleteStatus: {
      title: `${t('SCENARIO_DELETED_ERROR_TITLE')} ${name}`,
      caption: `${t('SCENARIO_DELETED_STATUS_ERROR_CAPTION')} ${status}`
    }
  }
})

export const sendNotification = ({ t, kind, action, ...other }) => {
  const notificationMap = notifications({ t, ...other })
  const notification = {
    ...notificationMap[kind][action],
    id: uniqueId('notif-'),
    type: kind === 'error' ? 'toast' : 'inline',
    kind
  }

  addNotification(notification)
}
