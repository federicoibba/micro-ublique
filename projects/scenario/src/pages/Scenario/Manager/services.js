import { isEmpty, isString } from 'lodash'
import { removeScenario } from '../actions'
import { ScenarioPlatformActions, ScenarioPermissions } from '../constants'
import { sendNotification } from './notifications'

const { DELETE } = ScenarioPermissions

export const onDeleteItems =
  ({ t, gridApi, tableActions, setSelectedRows }) =>
  async (items) => {
    setSelectedRows([])
    gridApi.deselectAll()

    for (const data of items) {
      const { name, status, scenarioPermission: rowPermissions } = data
      const menuActions = tableActions[status] ? tableActions[status].menuActions : []

      const isDeleteAllowed = menuActions.find(
        ({ action }) =>
          isString(action) && action === ScenarioPlatformActions.DELETE_SCENARIO
      )

      if (!isEmpty(isDeleteAllowed) && rowPermissions[DELETE]) {
        removeScenario({ data })
      } else {
        sendNotification({
          t,
          name,
          status: t(status).toLowerCase(),
          kind: 'error',
          action: 'deleteStatus'
        })
      }
    }
  }
