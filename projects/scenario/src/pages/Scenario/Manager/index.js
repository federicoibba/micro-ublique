import React, { useCallback, useEffect, useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import { isEmpty, keyBy } from 'lodash'

import withStore from 'hoc/withStore'
// import { FiltersActions, getInstancesList } from 'App/actions'
import { scenarioStore, filterStore } from 'state'
import { updateState } from 'pages/Scenario/utils/state'
import { columnDefs, tableParser, columnFrameworks } from 'pages/Scenario/utils/table'
import {
  // filterDataGrid,
  getActiveFiltersData
  // getFilterSections
} from 'pages/Scenario/utils/filter'

import DataTable from '@ublique/components/DataTable'
import ActiveFilters from '@ublique/components/ActiveFilters'

import WizardContainer from 'components/Wizard'
import TableHeader from './components/TableHeader'

import { onDeleteItems } from 'pages/Scenario/Manager/services'
import { createScenario, runScenario } from 'pages/Scenario/actions'
import {
  checkUpdates,
  updateTableData,
  handleTableColumns
} from 'pages/Scenario/Manager/helpers'

import useEventSource from '../useEventSource'
import {
  ScenarioPermissions,
  PAGINATION_SIZE,
  CREATED,
  READY
} from 'pages/Scenario/constants'

import './index.scss'

const { DELETE, EXECUTE } = ScenarioPermissions

const ScenarioManager = ({
  cloningStatusAllowed,
  getAllScenarios,
  scenario: { originalData, tableActions, permissions }
}) => {
  const { t } = useTranslation('Scenario')
  const { t: commonT } = useTranslation('Common')
  const showFilters = false

  const [open, setOpen] = useState(false)
  const [filters, setFilters] = useState({})
  const [gridApi, setGridApi] = useState(null)
  // const [showFilters, setShowFilters] = useState(false)
  const [selectedRows, setSelectedRows] = useState([])

  const { buffer } = useEventSource()

  useEffect(() => {
    if (!isEmpty(gridApi) && !isEmpty(originalData)) {
      handleTableColumns({ t, gridApi, rows: originalData })

      // FiltersActions.update(() => ({
      //   t: commonT,
      //   sections: getFilterSections(t, originalData),
      //   onReset: onReset,
      //   onChange: onFilterChange,
      //   isActiveFiltersOpened: showFilters,
      //   onToggleFilters: () => setShowFilters((show) => !show)
      // }))

      // return FiltersActions.reset
    }
  }, [t, commonT, gridApi, originalData])

  useEffect(() => {
    return function cleanFilterStore() {
      filterStore.set({})
    }
  }, [])

  useEffect(() => {
    filterStore.update((prevState) => ({
      ...prevState,
      isActiveFiltersOpened: showFilters
    }))
  }, [showFilters])

  useEffect(async () => {
    if (!isEmpty(buffer)) {
      updateState(scenarioStore, { buffer })
      await checkUpdates({ t, buffer, gridApi })
    }
  }, [buffer])

  // const onFilterChange = useCallback(
  //   (filter) => {
  //     setFilters(filter)
  //     filterDataGrid({ filter, gridApi, originalData })
  //   },
  //   [gridApi, originalData]
  // )

  // const onReset = useCallback(() => {
  //   setFilters({})
  //   gridApi.setRowData(
  //     tableParser({
  //       rowData: originalData
  //     })
  //   )
  // }, [gridApi, originalData])

  const onSave = useCallback(async (data) => {
    try {
      await createScenario(data)
      // await getInstancesList()
    } catch (error) {
      console.log('Error on create', error)
      setOpen(false)
    }
  }, [])

  const onRunItems = useCallback(
    (items) => {
      const filtered = items.filter((scenario) =>
        [CREATED, READY].includes(scenario.status)
      )

      for (const scenario of filtered) {
        runScenario(scenario)
      }

      setSelectedRows([])
      gridApi.deselectAll()
    },
    [gridApi]
  )

  const onUpdateRows = useCallback(async () => {
    gridApi.showLoadingOverlay()
    gridApi.setRowData([])
    setFilters({})

    updateState(scenarioStore, { buffer: {} })
    await getAllScenarios()

    gridApi.applyTransaction({
      add: tableParser({
        rowData: originalData
      })
    })
    gridApi.hideOverlay()
  }, [gridApi, originalData])

  const onGridReady = useCallback(
    ({ api }) => {
      if (!isEmpty(api)) {
        setGridApi(api)
        updateTableData({ t, gridApi: api, originalData })
      }
    },
    [t, originalData]
  )

  const onGridSizeChanged = useCallback(({ api }) => {
    api.sizeColumnsToFit()
  }, [])

  const onSelectionChanged = useCallback(
    (rows) => {
      const originalMap = keyBy(originalData, ({ id }) => id)
      const originalRows = rows.map(({ id }) => originalMap[id])
      setSelectedRows(originalRows)
    },
    [originalData]
  )

  const isRowSelectable = useCallback(
    ({ data: { scenarioPermission } }) =>
      scenarioPermission[DELETE] || scenarioPermission[EXECUTE],
    []
  )

  const renderWizard = useMemo(() => {
    const clones = originalData.filter((scenario) =>
      cloningStatusAllowed.includes(scenario.status)
    )

    if (!open) {
      return null
    }

    return (
      <WizardContainer
        open={open}
        onSave={onSave}
        clones={clones}
        setOpen={setOpen}
        headingLabel="Heading"
      />
    )
  }, [originalData, open, onSave])

  return (
    <div className="scenario-manager">
      <h1 className="manager-heading">{t('MainHeading')}</h1>

      {showFilters && <ActiveFilters filters={getActiveFiltersData(filters)} />}

      <DataTable
        id="scenario-table"
        filter
        pagination
        animateRows
        rowHeight={80}
        floatingFilter
        tooltipShowDelay={0}
        enableBrowserTooltips
        rowSelection="multiple"
        onGridReady={onGridReady}
        suppressRowClickSelection
        columnDefs={columnDefs(t)}
        suppressColumnVirtualisation
        getRowNodeId={({ id }) => id}
        isRowSelectable={isRowSelectable}
        onGridSizeChanged={onGridSizeChanged}
        onSelectionChanged={onSelectionChanged}
        paginationPageSize={PAGINATION_SIZE}
        frameworkComponents={columnFrameworks}
        loadingOverlayComponent="customLoadingOverlay"
        extraHeaderComponent={
          <TableHeader
            onRunItems={onRunItems}
            selectedRows={selectedRows}
            onUpdateRows={onUpdateRows}
            showAdd={permissions?.owner}
            onAddRow={() => setOpen(true)}
            onDeleteItems={onDeleteItems({
              t,
              gridApi,
              setSelectedRows,
              tableActions
            })}
          />
        }
      />

      {renderWizard}
    </div>
  )
}

ScenarioManager.propTypes = {
  cloningStatusAllowed: PropTypes.arrayOf(PropTypes.string),
  getAllScenarios: PropTypes.func.isRequired,
  scenario: PropTypes.shape({
    originalData: PropTypes.arrayOf(PropTypes.object).isRequired
  })
}

ScenarioManager.defaultProps = {
  cloningStatusAllowed: []
}

export default React.memo(withStore(ScenarioManager, ['user', 'scenario']))
