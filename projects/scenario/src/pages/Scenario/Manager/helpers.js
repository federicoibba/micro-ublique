import {
  map,
  keys,
  pickBy,
  filter,
  isEmpty,
  mapValues,
  difference,
  first
} from 'lodash'
import { sendNotification } from './notifications'
import {
  columnDefs,
  tableParser,
  getScenarioColumns
} from 'pages/Scenario/utils/table'
import { addRows, deleteRows } from 'pages/Scenario/utils/state'
import { getScenarioListByIds } from 'pages/Scenario/actions'
import { scenarioStore } from 'services/state'
import { DELETED } from 'pages/Scenario/constants'

export const updateTableData = ({ t, gridApi, originalData }) => {
  if (!isEmpty(originalData)) {
    const scenariosInTable = []
    const filteredIds = map(originalData, 'id')

    gridApi.forEachNode(({ id }) => {
      scenariosInTable.push(id)
    })

    const dataAdded = difference(filteredIds, scenariosInTable)
    const dataRemoved = difference(scenariosInTable, filteredIds)

    const rowsToAdd = originalData.filter(({ id }) => dataAdded.includes(id))

    handleTableColumns({ t, gridApi, rows: originalData })

    gridApi.applyTransaction({
      remove: dataRemoved.map((id) => ({ id }))
    })

    gridApi.applyTransaction({
      add: tableParser({
        rowData: rowsToAdd
      })
    })

    gridApi.setSortModel([
      {
        colId: 'id',
        sort: 'desc'
      }
    ])
    gridApi.onSortChanged()
  } else {
    gridApi.applyTransaction({
      add: []
    })
    gridApi.hideOverlay()
  }
}

export const checkUpdates = async ({ t, buffer, gridApi }) => {
  const scenariosInTable = map(
    scenarioStore.get(({ originalData }) => originalData),
    (scenario) => `${scenario.id}`
  )

  const bufferCodes = keys(
    pickBy(
      mapValues(buffer, ({ status }) => status !== DELETED),
      (el) => !!el
    )
  )
  const newScenarios = difference(bufferCodes, scenariosInTable)
  const toDelete = filter(
    keys(
      pickBy(
        mapValues(buffer, ({ status }) => status === DELETED),
        (id) => !!id
      )
    ),
    (id) => scenariosInTable.includes(id)
  )

  if (!isEmpty(newScenarios)) {
    await insertNewScenarios({ t, newScenarios, gridApi })
  }

  if (!isEmpty(toDelete)) {
    deleteScenarios({ t, toDelete, gridApi })
  }
}

const insertNewScenarios = async ({ t, gridApi, newScenarios }) => {
  const {
    body: { items }
  } = await getScenarioListByIds(newScenarios)

  for (const item of items) {
    item.typeValue = item.type?.value

    sendNotification({
      t,
      kind: 'success',
      action: 'create',
      name: item.name,
      username: item.username
    })
  }

  addRows(items)

  gridApi.applyTransaction({
    add: tableParser({
      rowData: items
    })
  })

  gridApi.setSortModel([
    {
      colId: 'id',
      sort: 'desc'
    }
  ])
  gridApi.onSortChanged()
}

const deleteScenarios = ({ t, toDelete, gridApi }) => {
  for (const id of toDelete) {
    const rowNode = gridApi.getRowNode(id)

    if (rowNode) {
      sendNotification({
        t,
        kind: 'warning',
        action: 'delete',
        name: rowNode.data.name
      })
    }
  }

  gridApi.applyTransaction({
    remove: toDelete.map((id) => ({ id }))
  })

  deleteRows((scenario) => !toDelete.includes(`${scenario.id}`))
}

export const handleTableColumns = ({ t, gridApi, rows }) => {
  const cols = getScenarioColumns(t)
  const columnDefinitions = columnDefs(t)
  const {
    scenarioPermission: { fields }
  } = first(rows)

  for (const name in cols) {
    if (fields[name]) {
      columnDefinitions.push(cols[name])
    }
  }

  gridApi.setColumnDefs(columnDefinitions)
  gridApi.sizeColumnsToFit()
}
