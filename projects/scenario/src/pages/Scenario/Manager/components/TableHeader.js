import React, { useMemo } from 'react'
import { isEmpty } from 'lodash'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

import Button from '@ublique/components/Button'
import { Play20, TrashCan20 } from '@carbon/icons-react'

const TableHeader = ({
  showAdd,
  onAddRow,
  onRunItems,
  onUpdateRows,
  onDeleteItems,
  selectedRows
}) => {
  const { t } = useTranslation('Scenario')

  const selectedLabel = useMemo(() => {
    const plural = selectedRows.length > 1 ? 's' : ''
    const label = `Table.SelectedItem${plural}`

    return `${selectedRows.length} ${t(label)}`
  }, [selectedRows])

  if (isEmpty(selectedRows)) {
    if (showAdd) {
      return (
        <div className="table-header">
          <div className="action-buttons">
            <Button onClick={onUpdateRows} kind="secondary">
              {t('Refresh')}
            </Button>
            <Button onClick={onAddRow}>{t('AddButton')}</Button>
          </div>
        </div>
      )
    }
    return null
  }

  return (
    <div className="table-header selected">
      <div className="label-container">
        <span>{selectedLabel}</span>
      </div>
      <div className="action-buttons">
        <Button onClick={() => onRunItems(selectedRows)} kind="secondary">
          {t('Table.PlayItems')} <Play20 />
        </Button>
        <Button onClick={() => onDeleteItems(selectedRows)} kind="danger">
          {t('Table.DeleteItems')} <TrashCan20 />
        </Button>
      </div>
    </div>
  )
}

TableHeader.propTypes = {
  showAdd: PropTypes.bool,
  onAddRow: PropTypes.func,
  onRunItems: PropTypes.func,
  onUpdateRows: PropTypes.func,
  onDeleteItems: PropTypes.func,
  selectedRows: PropTypes.array
}

TableHeader.defaultProps = {
  showAdd: false,
  onAddRow: () => {},
  onRunItems: () => {},
  onUpdateRows: () => {},
  onReleteItems: () => {},
  selectedRows: []
}

export default React.memo(TableHeader)
