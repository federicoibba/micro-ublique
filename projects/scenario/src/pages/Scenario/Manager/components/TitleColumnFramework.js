import React, { useMemo } from 'react'
import { isUndefined } from 'lodash'
import { Link } from 'react-router-dom'

const TitleColumnFramework = ({ value, data: { id } }) => {
  const MAX_CHARS = 30

  const title = useMemo(
    () =>
      !isUndefined(value) && value.length > MAX_CHARS
        ? `${value.substring(0, MAX_CHARS)}...`
        : value,
    [value]
  )

  if (isUndefined(id)) {
    return value
  }

  return (
    <div className="title-column">
      <Link to={`/instances/${id}/workingtables`}>{title}</Link>
    </div>
  )
}

export default TitleColumnFramework
