import React from 'react'
import moment from 'moment'
import { DATE_FORMAT, ISO_DATE_TIME_FORMAT } from 'pages/Scenario/constants'

const DateColumnFramework = ({ value }) => {
  if (!value) return null

  const [date, time] = moment(value, DATE_FORMAT)
    .utc(true)
    .local()
    .format(ISO_DATE_TIME_FORMAT)
    .split(' ')

  return (
    <div className="date-column">
      <span>{date}</span>
      <span>{time}</span>
    </div>
  )
}

export default DateColumnFramework
