import React, { useState, useEffect } from 'react'
import { isUndefined } from 'lodash'
import { useTranslation } from 'react-i18next'
import { scenarioStore } from 'services/state'
import useStoreSelector from 'utils/hooks/useStoreSelector'

const StatusColumnFramework = ({ value, data: { id } }) => {
  const [scenarioStatus, setScenarioStatus] = useState(value)

  const { t } = useTranslation('Scenario')
  const buffer = useStoreSelector('scenario', (store) => store.buffer)

  useEffect(() => {
    if (!isUndefined(buffer[id])) {
      const { status: newStatus } = buffer[id]

      setScenarioStatus(newStatus)

      scenarioStore.update((prevState) => {
        const { originalData } = prevState
        const index = originalData.findIndex((el) => el.id === id)

        if (index >= 0) {
          originalData[index].status = newStatus

          return {
            ...prevState,
            originalData
          }
        }

        return prevState
      })
    }
  }, [buffer[id]])

  useEffect(() => {
    if (!isUndefined(value) && value !== scenarioStatus && isUndefined(buffer[id])) {
      setScenarioStatus(value)
    }
  }, [value])

  return <span>{t(scenarioStatus)}</span>
}

export default StatusColumnFramework
