import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'

import { isEmpty, isPlainObject, isUndefined } from 'lodash'
import { buildMenu } from 'pages/Scenario/utils/table'
import { updateStatusRow } from 'pages/Scenario/utils/state'
import { runScenario, downloadError } from 'pages/Scenario/actions'

import Loading from '@ublique/components/Loading'
import { PlayFilled32 } from '@carbon/icons-react'

import ErrorLink from './progress/ErrorLink'
import OutputLink from './progress/OutputLink'
import RunningProgress from './progress/RunningProgress'
import ProgressWithLoading from './progress/ProgressWithLoading'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import {
  ScenarioPermissions,
  ScenarioProgressColumn,
  ScenarioPlatformActions
} from 'pages/Scenario/constants'

const { DELETE_SCENARIO, RESET_SCENARIO } = ScenarioPlatformActions
const { PROGRESS, OUTPUT, ERROR, LOADING, PLAY } = ScenarioProgressColumn
const { EXECUTE } = ScenarioPermissions

const tableActions = {
  CREATED: {
    column: PLAY,
    menuActions: [
      {
        title: 'deleteScenario',
        action: DELETE_SCENARIO
      }
    ]
  },
  WAIT: {
    column: LOADING,
    menuActions: []
  },
  IMPORTING: {
    column: LOADING,
    menuActions: []
  },
  BUILDING: {
    column: PROGRESS,
    menuActions: []
  },
  CLONING: {
    column: PROGRESS,
    menuActions: []
  },
  SAVING: {
    column: PROGRESS,
    menuActions: []
  },
  DELETING: {
    column: PROGRESS,
    menuActions: []
  },
  DONE: {
    column: OUTPUT,
    menuActions: []
  },
  CREATE_ERROR: {
    column: ERROR,
    menuActions: [
      {
        title: 'deleteScenario',
        action: DELETE_SCENARIO
      }
    ]
  },
  ERROR: {
    column: ERROR,
    menuActions: [
      {
        title: 'resetScenario',
        action: RESET_SCENARIO
      },
      {
        title: 'deleteScenario',
        action: DELETE_SCENARIO
      }
    ]
  }
}

const columnContent = {
  error: ErrorLink,
  loading: Loading.Inline,
  output: OutputLink,
  play: PlayFilled32,
  progress: ProgressWithLoading,
  timeProgress: RunningProgress
}

const ProgressColumnFramework = ({ api, data, value: { status } }) => {
  const { t } = useTranslation('Scenario')
  const history = useHistory()
  const { buffer /* tableActions */ } = useStoreSelector('scenario')
  const [progressData, setProgressData] = useState(null)
  const [scenarioStatus, setScenarioStatus] = useState(null)

  const { id, scenarioPermission: rowPermissions } = data
  const columnConfig = tableActions[scenarioStatus] || {}

  useEffect(() => {
    if (!isUndefined(buffer[id])) {
      const bufferData = buffer[id]
      setProgressData(bufferData)
      setScenarioStatus(bufferData.status)
    }
  }, [buffer[id]])

  useEffect(() => {
    if (
      !isUndefined(status) &&
      isUndefined(buffer[id]) &&
      status !== scenarioStatus
    ) {
      setScenarioStatus(status)
      updateStatusRow(id, status)
    }
  }, [status])

  /**
   * Use the 'getProps' function to get props from the outside (can be sync or async; here it's always converted into a Promise)
   * 'getProps' is remapped to 'getExternalPropsAsync', that is passed down to the component
   * Internally the component should always call 'getExternalPropsAsync' as a Promise, and wait for it
   */
  const contentProps = {
    progress: {
      max: progressData?.total,
      value: progressData?.progress,
      label: progressData?.step ?? null
    },
    play: {
      className: rowPermissions[EXECUTE] ? 'play-btn' : 'play-btn-disabled',
      onClick: () => {
        if (rowPermissions[EXECUTE]) runScenario(data, t)
      }
    },
    error: {
      labelText: t('Table.ProgressError'),
      downloadErrorFile: () => downloadError(data.id, data.name)
    },
    timeProgress: {
      getExternalPropsAsync: () =>
        Promise.resolve(columnConfig.column?.getProps?.(data))
    },
    output: {
      id,
      labelText: t('Table.ProgressOutput')
    }
  }

  const renderContentByStatus = () => {
    const { column } = columnConfig
    const columnName = isPlainObject(column) ? column.name : column

    const Element = columnContent[columnName]
    const props = contentProps[columnName] || {}

    if (Element) {
      return <Element {...props} />
    }

    return null
  }

  const renderOverflowMenu = () => {
    if (scenarioStatus) {
      return buildMenu({
        data,
        gridApi: api,
        history,
        config: columnConfig?.menuActions
      })
    }
    return null
  }

  return (
    <div className="progress-column">
      <div className="flex-wrap">
        {scenarioStatus && !isEmpty(columnConfig) && renderContentByStatus()}
      </div>

      {!isEmpty(columnConfig) && renderOverflowMenu()}
    </div>
  )
}

export default ProgressColumnFramework
