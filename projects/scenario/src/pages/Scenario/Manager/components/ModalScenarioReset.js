import React from 'react'
import { useTranslation } from 'react-i18next'

import Modal from '@ublique/components/Modal'
import { downloadFile } from 'pages/Scenario/actions'

const ModalScenarioReset = ({ open, close, data: { file, update } }) => {
  const { t } = useTranslation(['Common', 'Scenario'])

  const onSubmit = async () => {
    await downloadFile(file)
    update()
    close()
  }

  const onRequestClose = () => {
    update()
    close()
  }

  return (
    <Modal
      open={open}
      size="sm"
      iconDescription={t('close')}
      modalHeading={t('MODAL_DOWNLOAD_ERROR_TITLE')}
      onRequestClose={onRequestClose}
      onRequestSubmit={onSubmit}
      onSecondarySubmit={onRequestClose}
      primaryButtonText={t('Download')}
      secondaryButtonText={t('Cancel')}
    >
      {t('MODAL_BODY_DOWNLOAD_REQUEST')}
    </Modal>
  )
}

export default React.memo(ModalScenarioReset)
