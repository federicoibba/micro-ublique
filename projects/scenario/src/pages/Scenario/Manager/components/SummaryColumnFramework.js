import React from 'react'
import moment from 'moment'
import { isEmpty } from 'lodash'
import { useTranslation } from 'react-i18next'
import { ISO_DATE_FORMAT } from 'pages/Scenario/constants'

const SummaryColumnFramework = ({ value }) => {
  const { t } = useTranslation('Wizard')

  const renderSummaryItem = ({ key, value }) => {
    const tKey = `Table.${key}`
    let formattedValue = value

    if (value.includes('-') && moment(value).isValid()) {
      formattedValue = moment(value).format(ISO_DATE_FORMAT)
    }

    formattedValue = formattedValue
      .split(' ')
      .map((el) => t(el))
      .join(' ')

    return (
      <div key={key}>
        <span>{t(tKey)}: </span>
        <span>{formattedValue}</span>
      </div>
    )
  }

  const renderSummary = () => value.map((item) => renderSummaryItem(item))

  if (isEmpty(value)) {
    return null
  }

  return <div className="summary-column">{renderSummary()}</div>
}

export default SummaryColumnFramework
