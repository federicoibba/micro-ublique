import React, { useState, useEffect } from 'react'
import { isEmpty } from 'lodash'
import moment from 'moment'

import Loading from '@ublique/components/Loading'
import ProgressBar from '@ublique/components/ProgressBar'

const calculateRunningProgress = ({ secondDuration, endDateTime }) => {
  const now = moment.utc()
  const timeRemaining = moment.duration(endDateTime.diff(now)).asSeconds()
  const value = Math.floor(((secondDuration - timeRemaining) / secondDuration) * 100)

  if (timeRemaining < 0) {
    return 100
  }

  if (value < 0 || isNaN(timeRemaining)) {
    return 0
  }

  return value
}

const RunningProgress = ({ getExternalPropsAsync }) => {
  const [progressValue, setProgressValue] = useState(0)
  const [values, setValues] = useState(null)

  useEffect(() => {
    getExternalPropsAsync().then(setValues) // TODO: cancel promise when the component is unmounted
  }, [])

  useEffect(() => {
    if (!isEmpty(values)) {
      const interval = setInterval(() => {
        const progr = calculateRunningProgress(values)

        setProgressValue(progr)

        if (progr === 100) {
          clearInterval(interval)
        }
      }, 1000)
    }
  }, [values])

  if (!values?.endDateTime.isValid()) {
    return <Loading.Inline />
  }

  return <ProgressBar value={progressValue} />
}

export default RunningProgress
