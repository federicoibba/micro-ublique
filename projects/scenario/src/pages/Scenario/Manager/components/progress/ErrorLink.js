import React from 'react'
import { Download32 } from '@carbon/icons-react'

const ErrorLink = ({ labelText, downloadErrorFile }) => {
  return (
    <div className="error-link" onClick={downloadErrorFile}>
      <div className="vertical-items">
        <Download32 className="progress-icon" />
        <span>{labelText}</span>
      </div>
    </div>
  )
}

export default ErrorLink
