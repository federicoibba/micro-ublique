import React from 'react'
import Loading from '@ublique/components/Loading'
import ProgressBar from '@ublique/components/ProgressBar'

const ProgressWithLoading = ({ value, max, label }) => {
  if (!value || !max) {
    return <Loading.Inline />
  }

  return <ProgressBar value={value} max={max} label={label} />
}

ProgressBar.defaultProps = {
  value: 0,
  max: 100,
  label: null
}

export default ProgressWithLoading
