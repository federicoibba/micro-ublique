import React from 'react'
import { Link } from 'react-router-dom'
import { Ordinal32 } from '@carbon/icons-react'

const OutputLink = ({ labelText, id }) => {
  return (
    <Link className="output-link" to={`/output/${id}`}>
      <div className="vertical-items">
        <Ordinal32 className="progress-icon" />
        <span>{labelText}</span>
      </div>
    </Link>
  )
}

export default OutputLink
