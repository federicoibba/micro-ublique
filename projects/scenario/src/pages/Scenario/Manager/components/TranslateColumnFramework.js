import { useTranslation } from 'react-i18next'

// This column fixes a bug with translation
// Ag Grid does not trigger the valueFormatter when t changes
const TranslateColumnFramework = ({ value }) => {
  const { t } = useTranslation('Wizard')
  return t(value)
}

export default TranslateColumnFramework
