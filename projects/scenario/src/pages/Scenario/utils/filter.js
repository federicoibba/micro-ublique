import moment from 'moment'
import { isEmpty, min, max, map, cloneDeep, difference } from 'lodash'

import { tableParser } from './table'
import { filterObjects, DATE_FORMAT, ISO_DATE_FORMAT } from 'pages/Scenario/constants'

export const getActiveFiltersData = (filter) =>
  Object.keys(filter).reduce(
    (acc, key) => ({
      ...acc,
      [key]: Object.keys(filter[key])
    }),
    {}
  )

export const filterDataGrid = ({ filter, gridApi, originalData }) => {
  if (gridApi) {
    const filteredIds = map(filterData(originalData, filter), 'id')
    const scenariosInTable = []

    gridApi.forEachNode(({ id }) => {
      scenariosInTable.push(id)
    })

    const dataAdded = difference(filteredIds, scenariosInTable)
    const dataRemoved = difference(scenariosInTable, filteredIds)

    const rowsToAdd = originalData.filter(({ id }) => dataAdded.includes(id))

    gridApi.applyTransaction({
      remove: dataRemoved.map((id) => ({ id }))
    })

    gridApi.applyTransaction({
      add: tableParser({
        rowData: rowsToAdd
      })
    })
  }
}

const getUniqueListBy = (arr, key) => {
  return !isEmpty(arr)
    ? [...new Map(arr.map((item) => [item[key], item])).values()]
    : []
}

const getElementsByKey = (arr, key, t) =>
  getUniqueListBy(arr, key).map((row) => ({
    label: t(row[key]),
    val: row[key]
  }))

export const getFilterSections = (t, data) => {
  const checkboxProps = {
    type: 'checkbox',
    props: { withSelectAll: true, search: true }
  }
  const result = []

  for (const filter of filterObjects) {
    const elements = getElementsByKey(data, filter.key, t)

    if (!(elements.length === 1 && isEmpty(elements[0].val))) {
      result.push({
        ...checkboxProps,
        key: filter.key,
        title: t(filter.title),
        elements: getElementsByKey(data, filter.key, t)
      })
    }
  }

  const epochDates = map(data, 'creationDateTime')
    .map((date) => moment(date, DATE_FORMAT).valueOf())
    .filter((num) => !isNaN(num))

  if (!isEmpty(epochDates)) {
    const minDate = new Date(min(epochDates))
    const maxDate = new Date(max(epochDates))

    result.push({
      key: 'creationDateTime',
      type: 'date',
      title: t('Filter.Period'),
      elements: [
        {
          val: minDate,
          key: 'fromDate',
          label: t('Filter.FromDate')
        },
        {
          val: maxDate,
          key: 'toDate',
          label: t('Filter.ToDate')
        }
      ]
    })
  }

  return result
}

export const filterData = (originalData, filter) => {
  let dataFiltered = cloneDeep(originalData)

  for (const column of Object.keys(filter)) {
    if (column === 'creationDateTime') {
      if (filter[column].fromDate) {
        dataFiltered = dataFiltered.filter((row) => {
          const rowData = moment(row[column], DATE_FORMAT)
            .utc(true)
            .local()
            .endOf('day')
            .unix()
          const filterData = moment(filter[column].fromDate, ISO_DATE_FORMAT)
            .startOf('day')
            .unix()

          return rowData >= filterData
        })
      }
      if (filter[column].toDate) {
        dataFiltered = dataFiltered.filter((row) => {
          const rowData = moment(row[column], DATE_FORMAT)
            .utc(true)
            .local()
            .startOf('day')
            .unix()
          const filterData = moment(filter[column].toDate, ISO_DATE_FORMAT)
            .endOf('day')
            .unix()

          return rowData <= filterData
        })
      }
    } else {
      const conditions = Object.entries(filter[column])
        .filter(([_, value]) => value)
        .map(([key]) => key)

      dataFiltered = dataFiltered.filter((row) => conditions.includes(row[column]))
    }
  }

  return dataFiltered
}
