import { mapValues, groupBy, isUndefined } from 'lodash'
import { DONE, RUNNING } from 'pages/Scenario/constants'

export const getStatusSize = (data, statusName) =>
  data.filter((scenario) => scenario.status === statusName).length

export const getGroupedData = (data, groupWith, t) => {
  const types = mapValues(groupBy(data, groupWith), (el) => el.length)
  const result = []

  for (const key in types) {
    let type = isUndefined(t) ? key : t(key)

    if (key === 'undefined') {
      type = t('Dashboard.UnknownType')
    }

    result.push({
      type,
      value: types[key]
    })
  }

  return { data: result }
}

export const getTotalData = (data, t) => {
  const newScenarios = data.filter((scenario) => !scenario.cloned).length
  const cloneScenarios = data.filter((scenario) => scenario.cloned).length

  return {
    total: data.length,
    data: [
      {
        type: t('Dashboard.NewScenarios'),
        value: newScenarios
      },
      {
        type: t('Dashboard.CloneScenario'),
        value: cloneScenarios
      }
    ]
  }
}

export const getProgressData = (data, t) => {
  const progress = data.filter((scenario) => scenario.status === RUNNING).length
  const done = data.filter((scenario) => scenario.status === DONE).length

  return {
    total: data.length,
    data: [
      {
        type: t(RUNNING),
        value: progress
      },
      {
        type: t(DONE),
        value: done
      }
    ]
  }
}

export const formatLabelToInt = ({ value }) => {
  const rounded = Math.floor(value)
  return value - rounded > 0 ? '' : rounded
}
