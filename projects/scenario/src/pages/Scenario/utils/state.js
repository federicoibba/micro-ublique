import { scenarioStore } from 'services/state'

export const updateState = (state, newData) =>
  state.update((prevState) => ({
    ...prevState,
    ...newData
  }))

export const updateBuffer = (newBuffer) => {
  scenarioStore.update((prevState) => ({
    ...prevState,
    buffer: {
      ...prevState.buffer,
      ...newBuffer
    }
  }))
}

export const addRows = (newRows = []) => {
  scenarioStore.update((prevState) => ({
    ...prevState,
    originalData: [...prevState.originalData, ...newRows]
  }))
}

export const deleteRows = (onDelete) => {
  scenarioStore.update((prevState) => {
    return {
      ...prevState,
      originalData: prevState.originalData.filter(onDelete)
    }
  })
}

export const updateStatusRow = (id, newStatus) => {
  scenarioStore.update((prevState) => {
    const { originalData } = prevState
    const rowIndex = originalData.findIndex((row) => row.id === id)

    originalData[rowIndex].status = newStatus

    return {
      ...prevState,
      originalData
    }
  })
}
