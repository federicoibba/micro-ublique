import React from 'react'
import { isNull, isEmpty, isFunction, identity, isUndefined } from 'lodash'

import { Loading } from '@ublique/components/Loading'
import OverflowMenu from '@ublique/components/OverflowMenu'

import CustomDateComponent from 'components/AgGridCustomFilters/CustomDateComponent'
import {
  scenarioDateComparator,
  scenarioDateFilterParams,
  scenarioSummaryFilterParams
} from 'components/AgGridCustomFilters/helpers'

import { DATE_FORMAT, ScenarioPermissions } from 'pages/Scenario/constants'
import { removeScenario, resetScenario } from 'pages/Scenario/actions'

import DateColumnFramework from 'pages/Scenario/Manager/components/DateColumnFramework'
import TitleColumnFramework from 'pages/Scenario/Manager/components/TitleColumnFramework'
import StatusColumnFramework from 'pages/Scenario/Manager/components/StatusColumnFramework'
import SummaryColumnFramework from 'pages/Scenario/Manager/components/SummaryColumnFramework'
import ProgressColumnFramework from 'pages/Scenario/Manager/components/ProgressColumnFramework'
import TranslateColumnFramework from 'pages/Scenario/Manager/components/TranslateColumnFramework'

const ScenarioTableMenuActions = {
  resetScenario: {
    permission: ScenarioPermissions.UPDATE,
    menuAction: resetScenario
  },
  deleteScenario: {
    permission: ScenarioPermissions.DELETE,
    menuAction: removeScenario
  }
}

const commonColumnProps = {
  sortable: true,
  resizable: true,
  suppressMovable: true,
  filter: 'agTextColumnFilter'
}

const getFilterParams = (t) => ({
  textFormatter: (value) => t(value).toLowerCase()
})

export const getScenarioColumns = (t) => ({
  masterdataName: {
    ...commonColumnProps,
    headerName: t('Table.MasterDataName'),
    field: 'masterdataName',
    colId: 'masterdataName',
    minWidth: 180,
    filterParams: getFilterParams(t)
  },
  parentName: {
    ...commonColumnProps,
    headerName: t('Table.ParentName'),
    field: 'parentName',
    colId: 'parentName',
    minWidth: 180,
    filterParams: getFilterParams(t),
    valueFormatter: ({ data: { masterdataName, parentName }, value }) => {
      return masterdataName === parentName ? '' : value
    }
  },
  username: {
    ...commonColumnProps,
    headerName: t('Table.User'),
    field: 'username',
    colId: 'username',
    minWidth: 130
  },
  type: {
    ...commonColumnProps,
    headerName: t('Table.Type'),
    field: 'type',
    colId: 'type',
    wrapText: true,
    minWidth: 200,
    cellRenderer: 'translateRenderer'
  },
  summary: {
    ...commonColumnProps,
    headerName: t('Table.Summary'),
    field: 'summary',
    colId: 'summary',
    cellRenderer: 'summaryRenderer',
    minWidth: 250,
    filterParams: scenarioSummaryFilterParams
  },
  creationDateTime: {
    ...commonColumnProps,
    headerName: t('Table.CreationData'),
    headerTooltip: t('Table.CreationData'),
    field: 'creationDateTime',
    colId: 'creationDateTime',
    minWidth: 200,
    filter: 'agDateColumnFilter',
    cellRenderer: 'dateRenderer',
    comparator: scenarioDateComparator(DATE_FORMAT),
    filterParams: scenarioDateFilterParams(DATE_FORMAT)
  },
  startDateTime: {
    ...commonColumnProps,
    headerName: t('Table.StartData'),
    headerTooltip: t('Table.StartData'),
    field: 'startDateTime',
    colId: 'startDateTime',
    minWidth: 190,
    filter: 'agDateColumnFilter',
    cellRenderer: 'dateRenderer',
    comparator: scenarioDateComparator(DATE_FORMAT),
    filterParams: scenarioDateFilterParams(DATE_FORMAT)
  },
  endDateTime: {
    ...commonColumnProps,
    headerName: t('Table.EndData'),
    headerTooltip: t('Table.EndData'),
    field: 'endDateTime',
    colId: 'endDateTime',
    minWidth: 180,
    filter: 'agDateColumnFilter',
    cellRenderer: 'dateRenderer',
    comparator: scenarioDateComparator(DATE_FORMAT),
    filterParams: scenarioDateFilterParams(DATE_FORMAT)
  },
  consolidated: {
    ...commonColumnProps,
    headerName: t('Table.ConsolidationDate'),
    headerTooltip: t('Table.ConsolidationDate'),
    field: 'consolidated',
    colId: 'consolidated',
    minWidth: 190,
    filter: 'agDateColumnFilter',
    cellRenderer: 'dateRenderer',
    comparator: scenarioDateComparator(DATE_FORMAT),
    filterParams: scenarioDateFilterParams(DATE_FORMAT)
  }
})

export const columnDefs = (t) => [
  {
    ...commonColumnProps,
    headerName: 'id',
    field: 'id',
    colId: 'id',
    hide: true,
    lockVisible: true
  },
  {
    ...commonColumnProps,
    headerName: t('Table.Name'),
    field: 'name',
    colId: 'name',
    minWidth: 300,
    pinned: 'left',
    tooltipField: 'name',
    cellRenderer: 'nameRenderer',
    checkboxSelection: true,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    lockVisible: true
  },
  {
    ...commonColumnProps,
    headerName: t('Table.Status'),
    field: 'status',
    colId: 'status',
    pinned: 'right',
    minWidth: 180,
    cellRenderer: 'statusRenderer',
    filterParams: getFilterParams(t),
    lockVisible: true
  },
  {
    suppressMovable: true,
    headerName: t('Table.Progress'),
    field: 'progress',
    colId: 'progress',
    pinned: 'right',
    minWidth: 200,
    cellRenderer: 'progressRenderer',
    floatingFilter: false,
    lockVisible: true
  }
]

export const columnFrameworks = {
  // Custom loading
  customLoadingOverlay: () => <Loading />,

  // Custom columns
  dateRenderer: DateColumnFramework,
  nameRenderer: TitleColumnFramework,
  statusRenderer: StatusColumnFramework,
  summaryRenderer: SummaryColumnFramework,
  progressRenderer: ProgressColumnFramework,
  translateRenderer: TranslateColumnFramework,

  // Date custom
  agDateInput: CustomDateComponent
}

export const tableParser = ({ rowData }) => rowData.map((item) => rowParser({ item }))

export const rowParser = ({ item }) => ({
  ...item,
  type: item.typeValue,
  progress: {
    status: item.status
  }
})

export const buildMenu = ({ config, data, gridApi, history }) => {
  const { id, scenarioPermission: rowPermissions } = data

  const getMenuAction = ({ action, ...props }) => {
    // If the function came from the config
    if (isFunction(action)) {
      return action(props)
    }

    // If the action chosen came from platfrom
    const { permission, menuAction } = ScenarioTableMenuActions[action] ?? {}

    if (menuAction && rowPermissions[permission]) {
      return menuAction(props)
    }

    // Default case: action not found
    return identity(props)
  }

  const checkDisabled = (permission, action) => {
    if (isUndefined(permission)) {
      const { permission: platPermission } = ScenarioTableMenuActions[action] ?? {}

      return !rowPermissions[platPermission]
    }

    return !rowPermissions[permission]
  }

  return (
    <OverflowMenu ariaLabel="Menu" direction="top" flipped>
      {!isEmpty(config) &&
        config.map(({ title, action, permission }) => (
          <OverflowMenu.Item
            key={`${id}-${title}`}
            itemText={title}
            disabled={checkDisabled(permission, action)}
            onClick={() => getMenuAction({ action, data, gridApi, history })}
          />
        ))}
    </OverflowMenu>
  )
}

const padTime = (time) => `${time}`.padStart(2, '0')

export const calculateTimeLeft = (futureEpoch) => {
  let timeLeft = {}
  const difference = futureEpoch - Date.now()

  if (difference > 0) {
    timeLeft = {
      days: Math.floor(difference / (1000 * 60 * 60 * 24)),
      hours: padTime(Math.floor((difference / (1000 * 60 * 60)) % 24)),
      minutes: padTime(Math.floor((difference / 1000 / 60) % 60)),
      seconds: padTime(Math.floor((difference / 1000) % 60))
    }
  }

  return { difference, timeLeft }
}

export const formatTimeLeft = ({ timeLeft, LABEL_DAYS }) => {
  let stringDate = isNull(timeLeft) ? '' : '00:00:00'

  if (!isEmpty(timeLeft)) {
    const { days, hours, minutes, seconds } = timeLeft

    stringDate = days > 0 ? `${days} ${LABEL_DAYS} ` : ''
    stringDate += `${hours}:${minutes}:${seconds}`
  }

  return stringDate
}
