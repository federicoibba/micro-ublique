import { isEmpty } from 'lodash'
import { saveAs } from 'file-saver'
import { getInstances } from 'api/instances'
import {
  put,
  getAll,
  remove,
  insert,
  getFiles,
  getFromDocument,
  downloadFileById,
  fetchMyPermissions
} from 'api/scenarios'
import { fetchConfig } from 'api/config'
// import { openModal } from 'components/Modal/actions'
import { appConfigStore, instancesStore, scenarioStore } from 'services/state'

import { RESET, WAIT, DELETING } from './constants'
import { updateBuffer } from 'pages/Scenario/utils/state'

/* ------------------------- Config REST APIs ------------------------- */
export const fetchWizardConfig = async () => {
  const config = await fetchConfig('scenario')

  appConfigStore.update((prevState) => ({
    ...prevState,
    scenarioWizardModel: config
  }))
}

export const getMyScenarioPermissions = async () => {
  const permissions = await fetchMyPermissions()

  scenarioStore.update((prev) => ({ ...prev, permissions }))
}

export async function getInstancesList() {
  const instances = await getInstances()

  if (!instances || !instances.body) {
    instancesStore.set([])
    return
  }

  const {
    body: { items }
  } = instances

  instancesStore.set(items)
}

/* ------------------------- Instances and Document section ------------------------- */
export const fetchDataFromDocument = async ({ params, instanceId }) => {
  return await getFromDocument({ params, instanceId })
}

/* ------------------------- Scenario section ------------------------- */

export const getScenarioList = async () => {
  const {
    body: { items }
  } = await getAll()

  return items
}

export const getScenarioListByIds = async (idList) => {
  const params = new URLSearchParams()

  for (const id of idList) {
    params.append('id', id)
  }

  return await getAll(params)
}

export const createScenario = async (data) => {
  return await insert(data)
}

export const removeScenario = async ({ data }) => {
  const { id, status } = data

  try {
    updateBuffer({
      [id]: {
        status: DELETING
      }
    })
    await remove(id)
  } catch (err) {
    updateBuffer({
      [id]: {
        status
      }
    })
  }
}

export const runScenario = async (scenario) => {
  const { id } = scenario

  updateBuffer({
    [id]: {
      status: WAIT
    }
  })

  const body = {
    data: {},
    scenarioManager: { status: WAIT }
  }

  await put(id, body)
}

export const resetScenario = async ({ data }) => {
  const { id, name } = data

  const body = {
    data: {},
    scenarioManager: { status: RESET }
  }

  try {
    // Check if a log file is present
    // If there is, show a modal to ask if they want to download the file
    const {
      body: [scenarioDocument]
    } = await fetchDataFromDocument({ instanceId: id, params: `title=${name}` })

    const file = await getFileByScenarioId(scenarioDocument.id)

    if (!isEmpty(file)) {
      // openModal('scenarioReset', { file, update: () => put(id, body) })
    } else {
      await put(id, body)
    }
  } catch (error) {
    console.log('error on reset')
  }
}

/* ------------------------- File section ------------------------- */

export const downloadFile = async ({ id, name }) => {
  const { body } = await downloadFileById(id)

  saveAs(body, name)
}

export const getFileInfo = async (id) => {
  const files = await getFiles()

  return files.find((file) => file.id === id)
}

export const getFileByScenarioId = async ({
  id: scenarioEventId,
  activityId: scenarioActivityId
}) => {
  const { body: files } = await getFiles()

  return files.find(
    ({ name, eventId, activityId }) =>
      eventId === scenarioEventId &&
      activityId === scenarioActivityId &&
      name.endsWith('Error.txt')
  )
}

export const downloadError = async (instanceId, name) => {
  const {
    body: [scenarioDocument]
  } = await fetchDataFromDocument({ instanceId, params: `title=${name}` })

  const file = await getFileByScenarioId(scenarioDocument.id)

  await downloadFile(file)
}
