import React, {
  useState,
  useEffect,
  createContext,
  useCallback,
  useMemo
} from 'react'
import { useTranslation } from 'react-i18next'
// import { isEmpty } from 'lodash'
import cn from 'classnames'
import { useLocation } from 'react-router-dom'

// import useStoreSelector from 'utils/hooks/useStoreSelector'
import { filterStore, scenarioStore, sidebarStore } from 'services/state'

import Loading from '@ublique/components/Loading'
// import Forbidden from '@ublique/components/PageForbidden'
import SideNavPage from '@ublique/components/SideNavPage'

import { Analytics32, QueryQueue32 } from '@carbon/icons-react'

// import { setInstances } from 'App/actions'
import {
  fetchWizardConfig,
  getInstancesList,
  getMyScenarioPermissions,
  getScenarioList
} from './actions'
import { SCENARIO_MANAGER, SCENARIO_DASHBOARD } from 'pages/Scenario/constants'

import Dashboard from './Dashboard'

import './index.scss'

const Manager = React.lazy(() => import('./Manager'))

// Context for custom wizard components coming from the product
export const ScenarioWizardComponentsContext = createContext()

const ScenarioLoading = () => (
  <div className="scenario-center">
    <Loading active />
  </div>
)

const Scenario = ({
  basename,
  // scenarioActionsConfig: tableActions,
  scenarioAllowedStatusForCloning: cloningStatusAllowed,
  scenarioWizardComponents: externalWizardComponents
}) => {
  const { pathname } = useLocation()
  const [isError, setIsError] = useState(false)
  const [isLoading, setIsLoading] = useState(true)
  const [page, setPage] = useState(SCENARIO_DASHBOARD)

  const { t } = useTranslation(['Scenario', 'Wizard'])
  // const { t: commonT } = useTranslation('Common')

  // const {
  //   scenarioWizardModel: { sidebar }
  // } = useStoreSelector('appConfig')
  const sidebar = false
  // const { permissions } = useStoreSelector('scenario')

  useEffect(() => {
    if (pathname.includes('dashboard')) {
      filterStore.update(() => ({
        sections: []
      }))
      setPage(SCENARIO_DASHBOARD)
    }
    if (pathname.includes('management')) {
      setPage(SCENARIO_MANAGER)
    }
  }, [pathname])

  const getAllScenarios = useCallback(async () => {
    setIsLoading(true)

    try {
      const items = await getScenarioList()
      // const instances = await getInstancesList()

      for (const item of items) {
        item.typeValue = item?.type?.value
      }

      scenarioStore.update((oldState) => ({
        ...oldState,
        originalData: items
      }))

      // setInstances(instances)
      setIsLoading(false)
    } catch (err) {
      console.error(err)
      setIsError(true)
      setIsLoading(false)
    }
  }, [])

  useEffect(() => {
    getInstancesList()
    getAllScenarios()
    fetchWizardConfig()
    getMyScenarioPermissions()
  }, [])

  const items = useMemo(
    () => [
      {
        title: t('Sidebar.Scenario'),
        href: basename + '/management',
        icon: QueryQueue32
      },
      {
        title: t('Sidebar.Dashboard'),
        href: basename + '/dashboard',
        icon: Analytics32
      }
    ],
    [t]
  )

  const renderContent = useMemo(() => {
    if (isLoading) {
      return <ScenarioLoading />
    }

    if (isError) {
      return (
        <div className="scenario-center">
          <h1>{t('ErrorFetching')}</h1>
        </div>
      )
    }

    return (
      <div className={cn('scenario-main', { 'scenario-padding': sidebar })}>
        {page === SCENARIO_MANAGER && (
          <Manager
            getAllScenarios={getAllScenarios}
            cloningStatusAllowed={cloningStatusAllowed}
          />
        )}
        {page === SCENARIO_DASHBOARD && <Dashboard />}
      </div>
    )
  }, [t, isLoading, isError, page, sidebar])

  // if (!isEmpty(permissions) && !permissions?.owner && !permissions?.others) {
  //   return (
  //     <Forbidden text={commonT('PageForbidden')} goBackText={commonT('GoBack')} />
  //   )
  // }

  return (
    <div id="scenario">
      <React.Suspense fallback={<ScenarioLoading />}>
        <ScenarioWizardComponentsContext.Provider value={externalWizardComponents}>
          <SideNavPage
            items={items}
            title="Scenario"
            sidebar={sidebar}
            sidebarStore={sidebarStore}
          >
            {renderContent}
          </SideNavPage>
        </ScenarioWizardComponentsContext.Provider>
      </React.Suspense>
    </div>
  )
}

export default React.memo(Scenario)
