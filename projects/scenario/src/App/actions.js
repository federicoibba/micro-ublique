import { synapse, userStore } from 'services/state'

export const fetchUser = async () => {
  const user = await synapse.user()
  userStore.set(user)
}
