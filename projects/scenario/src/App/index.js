import React, { Suspense, useEffect } from 'react'
import { Router, Switch, Route, Redirect } from 'react-router-dom'
import initI18n from 'i18n'
import Loading from '@ublique/components/Loading'
import Theme from '@ublique/components/Theme'

import useStoreSelector from 'utils/hooks/useStoreSelector'

import Scenario from 'pages/Scenario'
import { fetchUser } from './actions'
import { synapse } from 'services/state'
import { BASE_URL } from 'config'

initI18n()

function App({ history, basename }) {
  const theme = useStoreSelector('theme')

  useEffect(() => {
    fetchUser()

    const closeStream = synapse.connect(`${BASE_URL}/stream`, (e) => {
      if (e.data === 'heartbeat' || e.data === 'connected') {
        return
      }
      synapse.publish(e.lastEventId, e)
    })

    // "Uncaught TypeError: Illegal invocation" all'unmont
    //return closeStream
  }, [])

  return (
    <Suspense fallback={<Loading />}>
      <Theme name={theme}>
        <Router history={history}>
          <Switch>
            <Route path={basename + '/management'}>
              <Scenario basename={basename} />
            </Route>
            <Route path={basename + '/dashboard'}>
              <Scenario basename={basename} />
            </Route>
            <Route exact path={basename || '/'}>
              <Redirect to={basename + '/management'} />
            </Route>
          </Switch>
        </Router>
      </Theme>
    </Suspense>
  )
}

export default App
