import { AUTH_ACCESS } from 'config'
import { canReadResource } from 'services/auth'

const SUPE_USER_ROUTES = ['admin', 'translations']
export const SUPER_USER_ROLE = 'UbliqueSuperUser'

export function isUserAuthorized(user, route) {
  if (route.public) {
    return AUTH_ACCESS.GRANTED
  }

  if (!user) {
    return AUTH_ACCESS.UNAUTHORIZED
  }

  const resource = route.id
  const isAuthorized = canReadResource(resource)

  if (
    isAuthorized &&
    SUPE_USER_ROUTES.includes(resource) &&
    !user.roles.includes(SUPER_USER_ROLE)
  ) {
    return AUTH_ACCESS.FORBIDDEN
  }

  if (isAuthorized) {
    return AUTH_ACCESS.GRANTED
  }

  return AUTH_ACCESS.FORBIDDEN
}

export default isUserAuthorized
