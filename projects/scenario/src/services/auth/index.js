import { permissionsStore, userStore } from 'state'
import { get } from 'lodash'

export const PERMISSIONS = {
  READ: 'read',
  CREATE: 'create',
  EXECUTE: 'execute',
  DELETE: 'delete',
  UPDATE: 'update'
}

const OFFSETS = {
  [PERMISSIONS.READ]: 0,
  [PERMISSIONS.CREATE]: 1,
  [PERMISSIONS.UPDATE]: 2,
  [PERMISSIONS.DELETE]: 3,
  [PERMISSIONS.EXECUTE]: 4
}

const allPermissions = Object.values(OFFSETS).reduce(
  (acc, offset) => acc + (1 << offset),
  0
)

export const getPermissionsValue = (permissions) => {
  const labels = []
  Object.keys(permissions).forEach((permission, idx) => {
    if (permission) {
      labels.push(Object.keys(permissions)[idx])
    }
  })
  const permission = Object.values(PERMISSIONS).reduce((acc, inc) => {
    const offset = OFFSETS[inc] || 0
    return acc | (Number(permissions[inc]) << offset)
  }, 0)

  return { labels, permission }
}

export const can = (n, type) => {
  if (!type) {
    return Object.values(PERMISSIONS).reduce(
      (acc, inc) => ({
        ...acc,
        [inc]: can(n, inc)
      }),
      {}
    )
  }

  const offset = OFFSETS[type] || 0
  const mask = 1 << offset
  return Boolean((n & mask) >> offset)
}

export const canRead = (n) => can(n, PERMISSIONS.READ)
export const canCreate = (n) => can(n, PERMISSIONS.CREATE)
export const canExecute = (n) => can(n, PERMISSIONS.EXECUTE)
export const canDelete = (n) => can(n, PERMISSIONS.DELETE)
export const canUpdate = (n) => can(n, PERMISSIONS.UPDATE)

export const canResource = (resource, type) => {
  const user = userStore.get()
  if (!user) return false

  const permission = get(user, ['resources', resource]) ?? allPermissions
  return can(permission, type)
}

export const canReadResource = (resource) => canResource(resource, PERMISSIONS.READ)
export const canCreateResource = (resource) =>
  canResource(resource, PERMISSIONS.CREATE)
export const canExecuteResource = (resource) =>
  canResource(resource, PERMISSIONS.EXECUTE)

export const canPermission = (keys, type) => {
  const permissions = permissionsStore.get()
  if (!permissions) return false

  const permission = get(permissions, keys) ?? 0
  return can(permission, type)
}

export const canReadPermission = (keys) => canPermission(keys, PERMISSIONS.READ)
export const canCreatePermission = (keys) => canPermission(keys, PERMISSIONS.CREATE)
export const canExecutePermission = (keys) => canPermission(keys, PERMISSIONS.EXECUTE)
