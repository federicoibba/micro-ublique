import { synapse } from 'state'
import { BASE_URL } from 'config'

export const getAll = async (params) => {
  try {
    let url = `${BASE_URL}/scenarios`

    if (params) url += `?${params}`

    const scenarios = await synapse.fetch(url)
    return scenarios
  } catch (error) {
    console.error(error)
    return null
  }
}

export const getFromDocument = async ({ params, instanceId }) => {
  const url = `${BASE_URL}/instances/${instanceId}/documents/?${params}`

  try {
    return await synapse.fetch(url)
  } catch (error) {
    console.error(error)
    return { body: [] }
  }
}

export const insert = async (body) => {
  return await synapse.fetch(
    `${BASE_URL}/scenarios`,
    {
      method: 'POST'
    },
    body
  )
}

export const put = async (id, body) => {
  return await synapse.fetch(
    `${BASE_URL}/scenarios/${id}`,
    {
      method: 'PUT'
    },
    body
  )
}

export const remove = async (id) => {
  try {
    const deletion = await synapse.fetch(`${BASE_URL}/scenarios/${id}`, {
      method: 'DELETE'
    })
    return deletion
  } catch (error) {
    console.error(error)
    return null
  }
}

export const run = async (id) => {
  const result = await synapse.fetch(
    `${BASE_URL}/scenario/actions/run`,
    {
      method: 'POST'
    },
    id
  )
  return result
}

export const consolidate = async (id) => {
  try {
    const result = await synapse.fetch(`${BASE_URL}/scenarios/${id}`, {
      method: 'PUT'
    })
    return result
  } catch (error) {
    console.error(error)
    return null
  }
}

export const getFiles = async (params) => {
  let path = `${BASE_URL}/files`

  if (params) path += `?${params}`

  try {
    const result = await synapse.fetch(path)
    return result
  } catch (error) {
    console.error(error)
    return null
  }
}

export const downloadFileById = async (id) => {
  try {
    const result = await synapse.fetch(`${BASE_URL}/files/${id}`, {
      method: 'GET'
    })
    return result
  } catch (error) {
    console.error(error)
    return null
  }
}

export const fetchMyPermissions = async () => {
  try {
    const { body } = await synapse.fetch(`${BASE_URL}/scenarios/myPermissions`)

    return body
  } catch (error) {
    console.error(error)
    return {}
  }
}
