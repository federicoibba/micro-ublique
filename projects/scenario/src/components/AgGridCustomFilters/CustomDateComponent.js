import React, { Component, createRef } from 'react'
import { CloseOutline16 } from '@carbon/icons-react'
import './index.scss'

export default class CustomDateComponent extends Component {
  constructor(props) {
    super(props)

    this.state = {
      date: null
    }
    this.eInput = createRef()
    this.flatpickr = createRef()
  }

  render() {
    return (
      <div
        className="ag-wrapper ag-input-wrapper ag-text-field-input-wrapper"
        role="presentation"
        ref={this.flatpickr}
      >
        <input
          type="date"
          ref={this.eInput}
          data-input
          style={{ width: '100%' }}
          className="ag-input-field-input ag-text-field-input"
        />
        <a className="input-customdate-button" title="clear" data-clear>
          <CloseOutline16 />
        </a>
      </div>
    )
  }

  componentDidMount() {
    // Not 100% safe but flatpicker is installed by carbon-components-react
    // eslint-disable-next-line no-undef
    this.picker = flatpickr(this.flatpickr.current, {
      onChange: this.onDateChanged.bind(this),
      dateFormat: 'Y/m/d',
      wrap: true
    })

    this.picker?.calendarContainer?.classList.add('ag-custom-component-popup')
  }

  getDate() {
    return this.state.date
  }

  setDate(date) {
    this.setState({ date })
    this.picker.setDate(date)
  }

  setInputPlaceholder(placeholder) {
    this.eInput.current?.setAttribute('placeholder', placeholder)
  }

  setInputAriaLabel(label) {
    this.eInput.current?.setAttribute('aria-label', label)
  }

  updateAndNotifyAgGrid(date) {
    this.setState({ date }, this.props.onDateChanged)
  }

  onDateChanged = (selectedDates) => {
    this.setState({ date: selectedDates[0] })
    this.updateAndNotifyAgGrid(selectedDates[0])
  }
}
