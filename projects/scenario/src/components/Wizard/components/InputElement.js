import React, { useState, useContext } from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import useStoreSelector from 'utils/hooks/useStoreSelector'

import { typeElement } from 'components/Wizard/constants'
import { getInvalidText } from 'components/Wizard/helpers/input-element'

import { ScenarioWizardComponentsContext } from 'pages/Scenario'

const InputElement = (props) => {
  const inputProps = { ...props }

  const externalComponents = useContext(ScenarioWizardComponentsContext)

  const [touched, setTouched] = useState(false)
  const { conditions, invalid, submitted, type, element, hidden, formHidden } =
    inputProps

  // Translations Hooks
  const { t } = useTranslation('Wizard')

  // Wizard store
  const wizard = useStoreSelector('wizard')

  // Format of common props
  const commonProps = {
    touched,
    hidden: hidden || formHidden,
    onBlur: () => setTouched(true),
    invalid: invalid && (touched || submitted),
    invalidText: getInvalidText({ conditions, invalid, tInvalid: t })
  }

  // It searches either for the element in the map list or a custom element provided
  const Input = typeElement[type] || externalComponents?.[element]

  // If the element is not supported
  if (!Input) {
    console.error(`${type} not supported`)
    return null
  }

  // This control avoid the propagation of the React elements to the input element itself
  if (element) {
    delete inputProps.element
    delete inputProps.recapElement
  }

  return <Input {...inputProps} {...commonProps} t={t} wizard={wizard} />
}

InputElement.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.string,
  submitted: PropTypes.bool,
  invalid: PropTypes.bool,
  invalidText: PropTypes.string,
  disabled: PropTypes.bool,
  hidden: PropTypes.bool,
  readOnlyWhenClone: PropTypes.bool,
  conditions: PropTypes.object,
  conditionInfo: PropTypes.object
}

InputElement.defaultProps = {
  label: '',
  submitted: false,
  invalid: false,
  disabled: false,
  hidden: false,
  readOnlyWhenClone: false,
  conditions: {},
  conditionInfo: {}
}

export default React.memo(InputElement)
