import React, { useState, useEffect, useCallback, useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { isEmpty, map } from 'lodash'
import PropTypes from 'prop-types'

import Button from '@ublique/components/Button'
import RadioButton from '@ublique/components/RadioButton'
import Autocomplete from '@ublique/components/Autocomplete'
import { Grid, Row, Column } from '@ublique/components/Grid'
import TextInput, { TextArea } from '@ublique/components/TextInput'

import { wizardStore } from 'services/state'
import { updateState } from 'pages/Scenario/utils/state'
import useStoreSelector from 'utils/hooks/useStoreSelector'

import { fetchDataFromDocument } from 'pages/Scenario/actions'
import { NEW_SCENARIO, CLONE_SCENARIO } from 'components/Wizard/constants'

const Creation = ({ clones, cloning, onSubmit, changeOpen, enumScenarioType }) => {
  const { t } = useTranslation('Wizard')

  const { creationMode } = useStoreSelector('wizard')
  const masterInstances = useStoreSelector('instances').filter(
    ({ config }) => config !== 'scenario'
  )

  const { title, optionsKey, valueKey } = enumScenarioType

  const [name, setName] = useState('')
  const [notes, setNotes] = useState('')
  const [submitted, setSubmitted] = useState(false)
  const [masterData, setMasterData] = useState(null)
  const [scenarioType, setScenarioType] = useState('')
  const [scenarioToCopy, setScenarioToCopy] = useState(null)
  const [scenarioTypesList, setScenarioTypesList] = useState([])

  useEffect(() => {
    if (masterInstances.length === 1) {
      setMasterData(masterInstances[0])
    }
  }, [])

  useEffect(async () => {
    if (!isEmpty(masterData)) {
      const { id: instanceId } = masterData
      const params = `title=${title}`

      const { body } = await fetchDataFromDocument({ params, instanceId })
      const typeList = body.reduce(
        (acc, item) => [
          ...acc,
          ...item[optionsKey].map((el) => ({
            label: t(el[valueKey]),
            value: el[valueKey]
          }))
        ],
        []
      )

      setScenarioTypesList(typeList)

      if (typeList.length === 1) {
        setScenarioType(typeList[0].value)
      }
    }
  }, [masterData])

  useEffect(() => {
    setSubmitted(false)
    if (creationMode === NEW_SCENARIO) {
      setName('')
      setScenarioToCopy(null)
    } else {
      setScenarioType('')
      if (clones.length === 1) {
        setScenarioToCopy(clones[0])
      }
    }
  }, [creationMode])

  useEffect(() => {
    if (creationMode === CLONE_SCENARIO && !isEmpty(scenarioToCopy)) {
      setName(`${t('Copy')} ${scenarioToCopy.name}`)
      setScenarioType(scenarioToCopy.type.value)
    }
  }, [scenarioToCopy])

  const setMasterInstance = (id) => {
    if (id) {
      const instance = masterInstances.find((instance) => instance.id === id)

      setMasterData(instance)
    }
  }

  const setScenarioToClone = useCallback(
    (name) => {
      if (name) {
        const clone = clones.find((el) => el.name === name)

        setScenarioToCopy(clone)
        setScenarioType(clone.type.value)
      }
    },
    [clones]
  )

  const [isNameInvalid, invalidNameError] = useMemo(() => {
    const used = map(clones, 'name').find(
      (item) => item.toLowerCase() === name.toLowerCase()
    )

    if (!name || name.trim() === '') return [true, 'ScenarioNameRequired']
    if (!isEmpty(used)) return [true, 'ScenarioNameAlreadyUsed']
    if (name.length > 30) return [true, 'ScenarioNameTooLong']

    return [false, '']
  }, [name])

  const formSubmit = async (e) => {
    e.preventDefault()
    setSubmitted(true)

    const isCloneValid =
      creationMode === CLONE_SCENARIO ? !isEmpty(scenarioToCopy) : true

    if (!isNameInvalid && !isEmpty(scenarioType) && isCloneValid) {
      let values = {
        name: name.trim(),
        notes,
        clone: {},
        creationMode,
        type: scenarioType,
        masterdataInstance: masterData,
        instanceId: masterData?.id ?? null
      }

      if (creationMode === CLONE_SCENARIO) {
        const params = `title=${scenarioToCopy.name}`
        const instanceId = scenarioToCopy.id

        const {
          body: [scenarioDocument]
        } = await fetchDataFromDocument({
          instanceId,
          params
        })

        scenarioDocument.scenarioStatus = scenarioToCopy.status

        values = {
          ...values,
          instanceId,
          clone: { document: scenarioDocument, scenario: scenarioToCopy },
          type: scenarioToCopy.type.value
        }
      }

      onSubmit(values)
    }
  }

  const renderScenarioTypeAutocomplete = useMemo(() => {
    const defaultValue = scenarioTypesList.length === 1 ? scenarioTypesList[0] : null

    if (
      creationMode === CLONE_SCENARIO ||
      (isEmpty(scenarioTypesList) && !defaultValue)
    ) {
      return (
        <TextInput
          disabled
          id="scenario-type"
          value={scenarioType ? t(scenarioType) : ''}
          labelText={t('ScenarioType')}
          placeholder={t('Placeholder')}
        />
      )
    }

    if (creationMode === NEW_SCENARIO) {
      return (
        <Autocomplete
          id="scenario-type"
          valueKey="value"
          labelKey="label"
          searchKey="label"
          items={scenarioTypesList}
          labelText={t('ScenarioType')}
          placeholder={t('Placeholder')}
          invalidText={t('ScenarioTypeRequired')}
          onChange={(type) => setScenarioType(isEmpty(type) ? null : type)}
          invalid={isEmpty(scenarioType) && submitted}
          defaultValue={defaultValue}
          disabled={isEmpty(scenarioTypesList) || creationMode === CLONE_SCENARIO}
        />
      )
    }

    return null
  }, [masterData, scenarioType, scenarioTypesList, creationMode, submitted])

  return (
    <Grid>
      <form onSubmit={formSubmit} autoComplete="off">
        <Row id="scenario-creation-heading">
          <Column md={8}>
            <p>{t('CreationInfo')}</p>
          </Column>
        </Row>
        <Row>
          <h4>{t('CreationMode')}</h4>
          <hr />
          <Column>
            <RadioButton.ButtonGroup
              legend="Creation"
              labelPosition="right"
              orientation="vertical"
              name="radio-button-group"
              onChange={(e) => {
                updateState(wizardStore, { creationMode: e })
              }}
              valueSelected={creationMode}
            >
              <RadioButton id="radio-1" value={NEW_SCENARIO} labelText={t('New')} />
              <RadioButton
                id="radio-2"
                value={CLONE_SCENARIO}
                labelText={t('Clone')}
                disabled={clones.length === 0 || !cloning}
              />
            </RadioButton.ButtonGroup>
          </Column>
        </Row>
        <Row>
          <h4>{t('ScenarioDetails')}</h4>
          <hr />
          {creationMode === CLONE_SCENARIO && (
            <Column md={4}>
              <Autocomplete
                id="scenario-copy"
                items={clones}
                valueKey="name"
                labelKey="name"
                onChange={setScenarioToClone}
                placeholder={t('Placeholder')}
                labelText={t('ScenarioToClone')}
                invalidText={t('ScenarioToCloneRequired')}
                invalid={isEmpty(scenarioToCopy) && submitted}
                defaultValue={clones.length === 1 ? clones[0] : null}
              />
            </Column>
          )}
          {creationMode === NEW_SCENARIO && (
            <Column md={4}>
              <Autocomplete
                id="scenario-master"
                items={masterInstances}
                valueKey="id"
                labelKey="title"
                onChange={setMasterInstance}
                placeholder={t('Placeholder')}
                labelText={t('MasterInstance')}
                invalidText={t('MasterInstanceRequired')}
                invalid={isEmpty(masterData) && submitted}
                defaultValue={
                  masterInstances.length === 1 ? masterInstances[0] : null
                }
              />
            </Column>
          )}
          <Column md={4}>{renderScenarioTypeAutocomplete}</Column>
          <Column md={4}>
            <TextInput
              id="scenario-name"
              value={name}
              invalid={isNameInvalid && submitted}
              labelText={t('ScenarioName')}
              invalidText={t(invalidNameError)}
              onChange={(e) => setName(e.target.value)}
              placeholder={t('ScenarioNamePlaceholder')}
            />
          </Column>
        </Row>
        <Row>
          <Column md={8}>
            <TextArea
              value={notes}
              id="scenario-notes"
              labelText={t('Notes')}
              placeholder={t('NotesPlaceholder')}
              onChange={(e) => setNotes(e.target.value)}
            />
          </Column>
        </Row>
        <Row className="buttons-footer">
          <a onClick={() => changeOpen(false)}>{t('SecondaryButtonLabel')}</a>
          <Button type="submit">{t('PrimaryButtonLabel')}</Button>
        </Row>
      </form>
    </Grid>
  )
}

Creation.propTypes = {
  clones: PropTypes.arrayOf(PropTypes.object),
  onSubmit: PropTypes.func,
  changeOpen: PropTypes.func
}

Creation.defaultProps = {
  clones: [],
  onSubmit: () => {},
  changeOpen: () => {}
}

export default React.memo(Creation)
