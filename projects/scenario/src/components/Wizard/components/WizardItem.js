import React, { Fragment, useState, useEffect, useCallback, useMemo } from 'react'
import { pick, find, isEmpty, mapValues, isUndefined } from 'lodash'
import { useTranslation } from 'react-i18next'

import InputElement from './InputElement'
import Button from '@ublique/components/Button'
import { Row, Column } from '@ublique/components/Grid'

import { wizardStore } from 'state'
import withStore from 'hoc/withStore'
import { validateFields } from 'utils/validation'
import {
  chunks,
  getValueState,
  isFormValid
} from 'components/Wizard/helpers/wizard-item'
import { CLONE_SCENARIO, FORM_STATUS } from 'components/Wizard/constants'

const WizardItem = ({
  wizard,
  values,
  setData,
  endpoints,
  conditions,
  stepNumber,
  stepConfig,
  changeOpen,
  progressSubmit,
  progressBarStep,
  setProgressBarStep
}) => {
  const [loading, setLoading] = useState(true)
  const [formData, setFormData] = useState({})
  const [submitted, setSubmitted] = useState(false)
  const [stepSubmit, setStepSubmit] = useState(false)
  const [conditionStates, setConditionStates] = useState({})

  const { t } = useTranslation('Wizard')
  const { clone, creationMode, inputElements, scenarioType } = wizard

  const info = ['inputs', 'configurations']

  const getInitialState = (dataInit) => {
    const state = {}
    const elements = {}

    for (const input of dataInit) {
      state[input.id] = getValueState({ values, input, clone })
      elements[input.id] = input
    }

    return { state, elements }
  }

  const validateWizardItem = (validationData) => {
    if (!isEmpty(validationData) && !isEmpty(inputElements)) {
      let toValidate = mapValues(validationData, (value, key) => {
        if (['date', 'datetime'].includes(inputElements[key].type)) {
          return new Date(value).getTime()
        }

        if (['select', 'radio'].includes(inputElements[key].type)) {
          return value?.value
        }

        if (['checkbox'].includes(inputElements[key].type)) {
          const entries = Object.entries(value)
            .filter(([_, pred]) => pred)
            .map(([key]) => key)

          return entries.length > 0
        }

        return value
      })

      toValidate = {
        ...toValidate,
        creationMode,
        scenarioType,
        scenarioStatus: !isEmpty(clone) ? clone.scenarioStatus : ''
      }

      const value = validateFields(
        toValidate,
        pick(conditions, Object.keys(validationData))
      )
      wizardStore.update((prevState) => ({
        ...prevState,
        status: { ...prevState.status, ...value }
      }))
      setConditionStates(value)
    }
  }

  useEffect(() => {
    validateWizardItem(formData)
  }, [formData])

  useEffect(() => {
    let formDataState = {}
    let inputElements = {}

    setLoading(true)

    if (stepConfig && (stepConfig.inputs || stepConfig.configuration)) {
      for (const key of info) {
        if (stepConfig[key]) {
          const { state, elements } = getInitialState(stepConfig[key])

          formDataState = { ...formDataState, ...state }
          inputElements = { ...inputElements, ...elements }
        }
      }
    }

    if (!isEmpty(stepConfig.sections)) {
      for (const section of stepConfig.sections) {
        const { state, elements } = getInitialState(section.inputs)
        formDataState = { ...formDataState, ...state }
        inputElements = { ...inputElements, ...elements }
      }
    }

    wizardStore.update((oldState) => ({
      ...oldState,
      inputElements: {
        ...oldState.inputElements,
        ...inputElements
      }
    }))

    setFormData(formDataState)
    setLoading(false)
  }, [stepConfig])

  useEffect(() => {
    if (Number.isInteger(progressBarStep)) {
      const isWizardFormValid = isFormValid(conditionStates)
      setStepSubmit(true)

      if (isWizardFormValid) {
        setData(formData)
      } else {
        setProgressBarStep(null)
      }

      progressSubmit(isWizardFormValid)
    }
  }, [progressBarStep])

  const setValue = (id, value) => {
    setFormData((prevData) => ({
      ...prevData,
      [id]: value
    }))
  }

  const onSubmit = useCallback(
    (e) => {
      e.preventDefault()
      setSubmitted(true)

      if (isFormValid(conditionStates)) {
        setData(formData)
        setSubmitted(false)
      }
    },
    [conditionStates, formData, setData]
  )

  const getLabel = ({ label, id }) => {
    return t(!isUndefined(label) ? label : id)
  }

  const checkState = (elementConditions, state) =>
    !isEmpty(find(elementConditions, (el) => el.state === state))

  const getInputElement = (element) => {
    const className = element.type === 'custom' ? 'full-width' : ''
    let elementStates = {}

    for (const state of FORM_STATUS) {
      elementStates = {
        ...elementStates,
        [state]: checkState(conditionStates[element.id], state)
      }
    }

    elementStates.disabled =
      elementStates.disabled ||
      (creationMode === CLONE_SCENARIO && element.readOnlyWhenClone)

    return (
      <Column key={`column-${element.id}`} className={className}>
        <InputElement
          {...element}
          {...elementStates}
          key={element.id}
          setValue={setValue}
          formValues={formData}
          endpoints={endpoints}
          label={getLabel(element)}
          value={formData[element.id]}
          submitted={submitted || stepSubmit}
          conditionInfo={conditions[element.id]}
          conditions={conditionStates[element.id]}
        />
      </Column>
    )
  }

  const renderButtons = useMemo(
    () => (
      <Row className="buttons-footer">
        <a onClick={() => changeOpen(false)}>{t('SecondaryButtonLabel')}</a>

        {stepNumber > 0 && (
          <Button kind="secondary" onClick={() => setProgressBarStep(stepNumber - 1)}>
            {t('Back')}
          </Button>
        )}

        <Button type="submit">{t('PrimaryButtonLabel')}</Button>
      </Row>
    ),
    [t, changeOpen, stepNumber]
  )

  return (
    !loading && (
      <Row>
        <form onSubmit={onSubmit}>
          <div className="overflow-box">
            {stepConfig &&
              info.map(
                (key) =>
                  stepConfig[key] &&
                  stepConfig[key].length > 0 &&
                  chunks(stepConfig[key]).map((inputs) => (
                    <Row key={`row-${inputs[0].id}`}>
                      {inputs.map((couple) => {
                        return getInputElement(couple)
                      })}
                    </Row>
                  ))
              )}
            {stepConfig &&
              stepConfig.sections &&
              stepConfig.sections.map((section) => (
                <Fragment key={`section-${section.title}`}>
                  <h5 className="section-title">
                    {getLabel({ label: section.title, id: section.title })}
                  </h5>
                  <hr className="section-line" />

                  {chunks(section.inputs).map((inputs) => (
                    <Row key={`row-${inputs[0].id}`}>
                      {inputs.map((couple) => {
                        return getInputElement(couple)
                      })}
                    </Row>
                  ))}
                </Fragment>
              ))}
          </div>
          {renderButtons}
        </form>
      </Row>
    )
  )
}

export default withStore(React.memo(WizardItem), ['wizard'])
