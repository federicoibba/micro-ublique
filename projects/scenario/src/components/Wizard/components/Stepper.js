import React, { Fragment, useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import withStore from 'hoc/withStore'
import PropTypes from 'prop-types'

import {
  ProgressIndicator,
  ProgressStep
} from '@ublique/components/ProgressIndicator'
import { Grid } from '@ublique/components/Grid'

import Recap from './Recap'
import WizardItem from './WizardItem'
import { wizardStore } from 'services/state'
import { formatValues } from 'components/Wizard/helpers/stepper'

const Stepper = ({ steps, onSave, wizard, endpoints, conditions, changeOpen }) => {
  const [loading, setLoading] = useState(false)
  const [stepValid, setStepValid] = useState({})
  const [stepNumber, setStepNumber] = useState(0)
  const [progressBarStep, setProgressBarStep] = useState(null)

  const { t } = useTranslation('Wizard')
  const { values } = wizard

  const setValues = (values) => {
    wizardStore.update((oldState) => ({
      ...oldState,
      values: {
        ...oldState.values,
        ...values
      }
    }))
  }

  // It formats the values as follow:
  // values -> { numberStep: { idInput: value }}
  // stepValid -> { numberStep: false }
  useEffect(() => {
    const el = { ...values }
    for (const i in steps) {
      el[i] = {}
      stepValid[i] = false
    }

    setValues(el)
    setLoading(false)
  }, [steps])

  // It updates the data of the given step with the given data
  const setData = (id, formData) => {
    const el = { ...values }
    el[id] = formData

    wizardStore.update((prevState) => ({
      ...prevState,
      formData: {
        ...prevState.formData,
        ...formData
      },
      values: {
        ...prevState.values,
        ...el
      }
    }))
  }

  const onSubmit = (wizardData) => {
    setData(stepNumber, wizardData)
    setStepValid((steps) => ({
      ...steps,
      [stepNumber]: true
    }))

    if (stepNumber < steps.length) {
      setStepNumber((stepNumber) => stepNumber + 1)
    }
  }

  // It handles the change between the steps using the upper tabs
  const progressSubmit = (isStepExchangeable) => {
    if (isStepExchangeable) {
      setStepNumber(progressBarStep)
      setProgressBarStep(null)
    }
  }

  const onChangeStep = (step) => {
    if (stepNumber !== steps.length) {
      setProgressBarStep(step)
    } else {
      setStepNumber(step)
    }
  }

  const saveScenario = () => {
    const finalValues = Object.keys(values).reduce(
      (accValues, stepKey) => ({
        ...accValues,
        ...values[stepKey]
      }),
      {}
    )

    onSave(formatValues(finalValues))
  }

  return (
    !loading && (
      <Fragment>
        <ProgressIndicator
          spaceEqually
          vertical={false}
          onChange={onChangeStep}
          currentIndex={stepNumber}
        >
          {steps.map((el, index) => (
            <ProgressStep
              label={t(el.title)}
              key={`step-${index + 1}`}
              disabled={index > 0 && !stepValid[index - 1]}
            />
          ))}
          <ProgressStep label="Recap" disabled={!stepValid[steps.length - 1]} />
        </ProgressIndicator>
        <Grid>
          {stepNumber < steps.length && (
            <WizardItem
              setData={onSubmit}
              endpoints={endpoints}
              conditions={conditions}
              stepNumber={stepNumber}
              changeOpen={changeOpen}
              values={values[stepNumber]}
              stepConfig={steps[stepNumber]}
              progressSubmit={progressSubmit}
              progressBarStep={progressBarStep}
              setProgressBarStep={setProgressBarStep}
              lastStep={stepNumber + 1 === steps.length}
            />
          )}

          {stepNumber === steps.length && (
            <Recap
              data={values}
              steps={steps}
              setStep={setStepNumber}
              changeOpen={changeOpen}
              saveScenario={saveScenario}
            />
          )}
        </Grid>
      </Fragment>
    )
  )
}

Stepper.propTypes = {
  steps: PropTypes.array.isRequired,
  conditions: PropTypes.object.isRequired,
  endpoints: PropTypes.object,
  wizard: PropTypes.shape({
    mode: PropTypes.string,
    values: PropTypes.object
  }),
  changeOpen: PropTypes.func,
  onSave: PropTypes.func
}

Stepper.defaultProps = {
  endPoints: {},
  changeOpen: () => {},
  onSave: () => {}
}

export default withStore(React.memo(Stepper), ['wizard'])
