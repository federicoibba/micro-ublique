import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

import { isEmpty } from 'lodash'
import { useTranslation } from 'react-i18next'

const Summary = ({ creationMode, type, name, notes }) => {
  const { t } = useTranslation('Wizard')

  const sections = [
    {
      title: 'CreationMode',
      detail: t(creationMode)
    },
    {
      title: 'ScenarioType',
      detail: t(type)
    },
    {
      title: 'ScenarioName',
      detail: name
    },
    {
      title: 'NotesSummary',
      detail: notes
    }
  ]

  const renderSections = () =>
    sections.map(
      ({ title, detail }) =>
        !isEmpty(detail) && (
          <div key={`key-${title}`}>
            <h5>{t(title)}</h5>
            <p>{detail}</p>
          </div>
        )
    )

  return (
    <Fragment>
      <header>
        <h5>{t('RecapHeading')}</h5>
      </header>
      <section>{renderSections()}</section>
    </Fragment>
  )
}

Summary.propTypes = {
  creationMode: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string,
  notes: PropTypes.string
}

Summary.defaultProps = {
  creationMode: '',
  type: '',
  name: '',
  notes: ''
}

export default Summary
