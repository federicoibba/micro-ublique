import React from 'react'
import PropTypes from 'prop-types'
import { Grid, Row } from '@ublique/components/Grid'

import Summary from './Summary'
import Stepper from './Stepper'

const FormStep = ({ creationData, config, changeOpen, onSave }) => (
  <Grid id="wizard">
    <Row>
      <div id="summary">
        <Summary {...creationData} />
      </div>
      <div id="stepper">
        <Stepper {...config} onSave={onSave} changeOpen={changeOpen} />
      </div>
    </Row>
  </Grid>
)

FormStep.propTypes = {
  creationData: PropTypes.shape({
    creationMode: PropTypes.string,
    type: PropTypes.string,
    name: PropTypes.string,
    notes: PropTypes.string
  }),
  config: PropTypes.shape({
    steps: PropTypes.array,
    conditions: PropTypes.object,
    endpoints: PropTypes.object
  }).isRequired,
  changeOpen: PropTypes.func,
  onSave: PropTypes.func
}

FormStep.defaultProps = {
  creationData: {},
  changeOpen: () => {},
  onSave: () => {}
}

export default React.memo(FormStep)
