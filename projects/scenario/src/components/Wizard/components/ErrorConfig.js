import React from 'react'
import { useTranslation } from 'react-i18next'

import Button from '@ublique/components/Button'
import { Row } from '@ublique/components/Grid'

const ErrorConfig = ({ onClose }) => {
  const { t } = useTranslation('Wizard')

  return (
    <div id="error-config">
      <Row>
        <h4>{t('CreationWizardError')}</h4>
        <p>{t('NoScenarioConfigError')}</p>
      </Row>

      <div className="buttons-footer">
        <Button onClick={onClose}>{t('Close')}</Button>
      </div>
    </div>
  )
}

export default ErrorConfig
