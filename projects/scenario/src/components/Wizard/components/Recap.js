import React, { Fragment, useContext } from 'react'
import { useTranslation } from 'react-i18next'
import { isEmpty, isUndefined } from 'lodash'
import PropTypes from 'prop-types'

import { Edit20 } from '@carbon/icons-react'
import { Row } from '@ublique/components/Grid'
import Button from '@ublique/components/Button'

import { HIDDEN } from '../constants'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { getDataString } from 'components/Wizard/helpers/recap'
import { ScenarioWizardComponentsContext } from 'pages/Scenario'

const InputRowHeader = ({ value, onEdit }) => (
  <Fragment>
    <div className="recap-input-row-header">
      <h5>{value}</h5>
      <Edit20 onClick={onEdit} />
    </div>
    <hr />
  </Fragment>
)

const InputRowBody = ({ t, data, status, inputElements }) => {
  const externalComponents = useContext(ScenarioWizardComponentsContext)

  return (
    <div className="recap-input-row-values-container">
      {Object.keys(data).map((elementId) => {
        const inputProps = inputElements[elementId]
        const value = data[elementId]
        const isHidden = status[elementId]?.some(({ state }) => state === HIDDEN)

        if (isUndefined(value) || value === '' || isHidden) {
          return null
        }

        return (
          <div key={elementId} className="recap-input-row-values">
            <h6>{t(inputElements[elementId].label)}</h6>
            <span>
              {getDataString({
                ...inputProps,
                recapElement: externalComponents?.[inputProps.recapElement],
                elementId,
                value,
                t
              })}
            </span>
          </div>
        )
      })}
    </div>
  )
}

const RecapFooter = ({ t, onCancelClick, onSecondaryClick, onSubmit }) => (
  <Row className="buttons-footer">
    <a onClick={onCancelClick}>{t('SecondaryButtonLabel')}</a>
    <Button kind="secondary" onClick={onSecondaryClick}>
      {t('Back')}
    </Button>
    <Button onClick={onSubmit}>{t('SaveButtonLabel')}</Button>
  </Row>
)

const Recap = ({ steps, setStep, changeOpen, saveScenario }) => {
  const { t } = useTranslation('Wizard')
  const { inputElements, values, status } = useStoreSelector('wizard')

  if (isEmpty(values)) {
    return null
  }

  return (
    <div id="recap">
      <div className="overflow-box">
        {Object.keys(values).map((stepIndex) => (
          <Row key={stepIndex} className="recap-input-row">
            <InputRowHeader
              value={t(steps[stepIndex].title)}
              onEdit={() => setStep(parseInt(stepIndex))}
            />

            <InputRowBody
              t={t}
              status={status}
              data={values[stepIndex]}
              inputElements={inputElements}
            />
          </Row>
        ))}
      </div>

      <RecapFooter
        t={t}
        onSubmit={saveScenario}
        onCancelClick={() => changeOpen(false)}
        onSecondaryClick={() => setStep((step) => step - 1)}
      />
    </div>
  )
}

Recap.propTypes = {
  steps: PropTypes.array.isRequired,
  setSteps: PropTypes.func,
  changeOpen: PropTypes.func,
  saveScenario: PropTypes.func
}

Recap.defaultProps = {
  setSteps: () => {},
  changeOpen: () => {},
  saveScenario: () => {}
}

export default React.memo(Recap)
