import Slider from 'components/Wizard/elements/Slider'
import Toggle from 'components/Wizard/elements/Toggle'
import TextInput from 'components/Wizard/elements/Text'
import Dropdown from 'components/Wizard/elements/Select'
import Checkbox from 'components/Wizard/elements/Checkbox'
import DatePicker from 'components/Wizard/elements/DatePicker'
import RadioGroup from 'components/Wizard/elements/RadioGroup'
import TimePicker from 'components/Wizard/elements/TimePicker'
import MultiSelect from 'components/Wizard/elements/MultiSelect'
import Autocomplete from 'components/Wizard/elements/Autocomplete'
import DateTimePicker from 'components/Wizard/elements/DateTimePicker'

export const typeElement = {
  slider: Slider,
  toggle: Toggle,
  text: TextInput,
  time: TimePicker,
  date: DatePicker,
  select: Dropdown,
  number: TextInput,
  radio: RadioGroup,
  checkbox: Checkbox,
  datetime: DateTimePicker,
  multiselect: MultiSelect,
  autocomplete: Autocomplete
}

export const STEPS_STAGE = 'STEPS_STAGE'
export const CREATION_STAGE = 'CREATION_STAGE'

export const NEW_SCENARIO = 'NEW_SCENARIO'
export const CLONE_SCENARIO = 'CLONE_SCENARIO'
export const ENUM_SCENARIO_TYPE = 'ENUM_SCENARIO_TYPE'

export const YES = 'YES'
export const NO = 'NOT'

export const HIDDEN = 'hidden'
export const DISABLED = 'disabled'
export const INVALID = 'invalid'
export const FORM_HIDDEN = 'formHidden'
export const FORM_STATUS = [HIDDEN, DISABLED, INVALID, FORM_HIDDEN]
