import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { isEmpty, isFunction } from 'lodash'
import PropTypes from 'prop-types'

import Wizard from '@ublique/components/Wizard'
import Loading from '@ublique/components/Loading'
import Creation from 'components/Wizard/components/Creation'
import FormStep from 'components/Wizard/components/FormStep'
import ErrorConfig from 'components/Wizard/components/ErrorConfig'

import { wizardStore } from 'state'
import { updateState } from 'pages/Scenario/utils/state'
import useStoreSelector from 'utils/hooks/useStoreSelector'
import { initialState as wizardInitialState } from 'components/Wizard/state'
import { generateCreationObject } from 'components/Wizard/helpers/container'

import { STEPS_STAGE, CREATION_STAGE } from 'components/Wizard/constants'
import './index.scss'

const selectScenarioWizardModel = (state) => state.scenarioWizardModel

const WizardContainer = ({ open, clones, onSave, setOpen, headingLabel }) => {
  const scenarioWizardModel = useStoreSelector('appConfig', selectScenarioWizardModel)
  const [stage, setStage] = useState(CREATION_STAGE)
  const [cloneInfo, setCloneInfo] = useState({})
  const [isLoading, setIsLoading] = useState(false)
  const [creationValues, setCreationValues] = useState(null)

  const { t } = useTranslation('Wizard')
  const { sub: username } = useStoreSelector('user')
  const {
    status,
    inputElements,
    formData: flattenValues
  } = useStoreSelector('wizard')

  const { enumScenarioType, config, cloning = true } = scenarioWizardModel

  const changeOpen = (value) => {
    if (!value) {
      setStage(CREATION_STAGE)
      setCreationValues(null)
      updateState(wizardStore, wizardInitialState)
    }
    setOpen(value)
  }

  const onCreationSubmit = ({
    name,
    type,
    clone: { document, scenario },
    notes,
    masterdataInstance,
    instanceId,
    creationMode
  }) => {
    const newState = { instanceId, scenarioType: type }

    if (!isEmpty(document) && !isEmpty(scenario)) {
      newState.clone = document

      setCloneInfo({
        scenarioToCopyCode: document.code,
        scenarioToCopyTitle: document.title,
        masterdata: scenario.masterdata,
        masterdataName: scenario.masterdataName,
        parent: scenario.id,
        parentName: scenario.name
      })
    } else {
      const { id, title } = masterdataInstance

      setCloneInfo({
        masterdata: id,
        masterdataName: title,
        parent: id,
        parentName: title
      })
    }

    updateState(wizardStore, newState)

    setStage(STEPS_STAGE)
    setCreationValues({ name, creationMode, type, notes, instanceId })
  }

  const onFormSave = async (formData) => {
    if (!isFunction(onSave)) {
      changeOpen(false)
      return
    }

    setIsLoading(true)

    const { type } = creationValues
    const {
      summary,
      source: { documentFilters }
    } = config[type]

    const body = generateCreationObject({
      t,
      status,
      username,
      formData,
      cloneInfo,
      flattenValues,
      inputElements,
      creationValues,
      documentFilters,
      scenarioSummary: summary
    })

    try {
      await onSave(body)
      setIsLoading(false)
      changeOpen(false)
    } catch (error) {
      console.error(error)
    }
  }

  if (!open) {
    return null
  }

  const renderFormStep = () => {
    const formConfig = config[creationValues.type]

    if (!formConfig) {
      return <ErrorConfig onClose={() => setOpen(false)} />
    }

    return (
      <FormStep
        onSave={onFormSave}
        changeOpen={changeOpen}
        creationData={creationValues}
        config={formConfig}
      />
    )
  }

  return (
    <div
      id="wizard-container"
      className={stage === CREATION_STAGE ? 'creation-step' : 'form-step'}
    >
      <Wizard open={open} setOpen={changeOpen} headingLabel={t(headingLabel)}>
        {isLoading && <Loading />}
        {stage === CREATION_STAGE && (
          <Creation
            clones={clones}
            cloning={cloning}
            changeOpen={changeOpen}
            onSubmit={onCreationSubmit}
            enumScenarioType={enumScenarioType}
          />
        )}
        {stage === STEPS_STAGE && creationValues && renderFormStep()}
      </Wizard>
    </div>
  )
}

WizardContainer.propTypes = {
  open: PropTypes.bool,
  setOpen: PropTypes.func,
  onSave: PropTypes.func,
  clones: PropTypes.array,
  extConfig: PropTypes.object,
  headingLabel: PropTypes.string
}

WizardContainer.defaultProps = {
  setOpen: () => {}
}

export default React.memo(WizardContainer)
