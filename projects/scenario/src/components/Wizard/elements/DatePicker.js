import React, { useEffect } from 'react'
import { isUndefined } from 'lodash'
import { DatePicker as DatePickerComponent } from '@ublique/components/DatePicker'

const DatePicker = ({
  id,
  label,
  value,
  invalid,
  disabled,
  hidden,
  setValue,
  invalidText,
  formValues,
  equals
}) => {
  const equality = equals ? formValues[equals.slice(1)] : null

  useEffect(() => {
    if (isUndefined(value)) setValue(id, '')
  }, [])

  useEffect(() => {
    if (equals) {
      setValue(id, equality)
    }
  }, [equals, equality])

  const onChange = (eventOrDates) => {
    const value = eventOrDates.target ? eventOrDates.target.value : eventOrDates[0]
    const newValue = isUndefined(value) ? '' : value.toUTCString()

    setValue(id, newValue)
  }

  if (hidden) {
    return null
  }

  return (
    <DatePickerComponent
      dateFormat="Y/m/d"
      onChange={onChange}
      datePickerType="single"
      value={value}
    >
      <DatePickerComponent.Input
        id={id}
        labelText={label}
        invalid={invalid}
        disabled={disabled}
        placeholder="yyyy/mm/dd"
        invalidText={invalidText}
        pattern="\d{4}\/\d{1,2}\/\d{1,2}"
      />
    </DatePickerComponent>
  )
}

export default DatePicker
