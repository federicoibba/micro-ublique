import React, { Fragment } from 'react'
import { get, set } from 'lodash'

import UbliqueCheckbox from '@ublique/components/Checkbox'

const Checkbox = ({
  t,
  id,
  label,
  value,
  option,
  hidden,
  invalid,
  disabled,
  setValue,
  invalidText
}) => {
  const onChange = (valueKey) => (checked) => {
    const valueCloned = { ...value }
    set(valueCloned, valueKey, checked)
    setValue(id, valueCloned)
  }

  if (hidden) {
    return null
  }

  return (
    <Fragment>
      <fieldset className="bx--fieldset">
        <legend className="bx--label">{t(label)}</legend>
        {option.map(({ label, value: valueKey }) => {
          const key = `${id}-${label}`

          return (
            <UbliqueCheckbox
              id={key}
              key={key}
              labelText={t(label)}
              disabled={disabled}
              onChange={onChange(valueKey)}
              checked={get(value, valueKey)}
            />
          )
        })}
      </fieldset>
      {invalid && <span className="invalid-text">{invalidText}</span>}
    </Fragment>
  )
}

export default React.memo(Checkbox)
