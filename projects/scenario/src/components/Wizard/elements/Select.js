import React, { useCallback, useEffect, useRef } from 'react'
import { isEmpty, isObject, isString } from 'lodash'
import PropTypes from 'prop-types'

import withDocument from 'components/Wizard/elements/withDocument'
import CustomSelect from 'components/Select'

const Select = ({ t, id, name, option, value, setValue, invalidText, ...props }) => {
  const { hidden, invalid, disabled, label, onBlur, autoSelect } = props
  const ref = useRef(null)

  useEffect(() => {
    if (autoSelect && isEmpty(value) && option.length > 0) {
      const { value: optionValue, label } = option[0]

      setValue(id, {
        label,
        value: name
          ? {
              [`${id}Name`]: name,
              [`${id}Value`]: optionValue
            }
          : optionValue
      })
    }
  }, [option])

  const itemToString = () => {
    if (isString(value) && value !== '') return value
    if (isObject(value) && value.label) return value.label

    return null
  }

  const setSelectedValue = useCallback(
    (e) => {
      if (!isEmpty(e)) {
        const { label, value: optionValue } = e
        setValue(id, {
          label,
          value: name
            ? {
                [`${id}Name`]: name,
                [`${id}Value`]: optionValue
              }
            : optionValue
        })
      }
    },
    [setValue]
  )

  if (hidden) return null

  return (
    <CustomSelect
      id={id}
      ref={ref}
      getLabel={t}
      getValueFromLabel
      value={value}
      invalid={invalid}
      invalidText={t(invalidText)}
      disabled={disabled}
      labelText={label}
      items={option}
      onBlur={onBlur}
      placeholderLabel={t('Select')}
      label={label}
      currentSelection={itemToString(value)}
      onChangeValue={setSelectedValue}
    />
  )
}

Select.PropTypes = {
  t: PropTypes.func,
  id: PropTypes.string.isRequired,
  value: PropTypes.object,
  label: PropTypes.string,
  submitted: PropTypes.bool,
  invalid: PropTypes.bool,
  invalidText: PropTypes.string,
  disabled: PropTypes.bool,
  hidden: PropTypes.bool,
  onBlur: PropTypes.func,
  readOnlyWhenClone: PropTypes.bool,
  autoSelect: PropTypes.bool
}

Select.defaultProps = {
  autoSelect: true
}

export default withDocument(React.memo(Select))
