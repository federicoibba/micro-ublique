import React from 'react'
import TextInput from '@ublique/components/TextInput'

const Text = ({
  id,
  label,
  type,
  value,
  setValue,
  onBlur,
  invalid,
  invalidText,
  t
}) => {
  const onChange = (e) => setValue(id, e.target.value)
  const placeholder = t(type === 'text' ? 'Placeholder' : 'PlaceholderNumber')

  return (
    <TextInput
      id={id}
      type={type}
      value={value}
      onBlur={onBlur}
      labelText={label}
      invalid={invalid}
      onChange={onChange}
      invalidText={invalidText}
      placeholder={placeholder}
    />
  )
}

export default Text
