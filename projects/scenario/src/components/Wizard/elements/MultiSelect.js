import React, { useState, useEffect } from 'react'
import { size } from 'lodash'

import { MultiSelectWithSelectAll } from '@ublique/components/MultiSelect'
import withDocument from 'components/Wizard/elements/withDocument'

const MultiSelect = ({
  t,
  id,
  option,
  label,
  value,
  setValue,
  valueKey,
  ...props
}) => {
  const { hidden, onBlur, invalid, disabled, invalidText } = props
  const items = option.map(({ label, value }) => ({
    label: t(label),
    value: { [valueKey]: value }
  }))

  const onMultiChange = ({ selectedItems }) => {
    const formatted = selectedItems.map(({ label, value }) => ({
      label,
      value: value
    }))
    setValue(id, formatted)
  }

  const multiselectProps = {
    id,
    label,
    hidden,
    invalid,
    disabled,
    invalidText,
    light: true,
    onBlur: onBlur,
    titleText: label,
    onChange: onMultiChange,
    btnLabels: {
      select: t('SELECT_ALL'),
      deselect: t('DESELECT_ALL')
    },
    selectOptions: true,
    useTitleInItem: false,
    initialSelectedItems: [],
    itemToString: (item) => item.label
  }
  const [initialSelected, setInitialSelected] = useState(multiselectProps)
  const [key, setKey] = useState(0)

  useEffect(() => {
    setKey((prevState) => ++prevState)
  }, [])

  useEffect(() => {
    async function updateProps() {
      await setInitialSelected((prevState) => {
        let initialSelectedItems = []
        let keyCount

        if (size(items) === 1) {
          onMultiChange({ selectedItems: items })
          initialSelectedItems = items
          keyCount = key
        }

        if (size(items) > 1 && size(value) > 0) {
          initialSelectedItems = value
          keyCount = 0
        }

        return {
          ...prevState,
          invalid,
          invalidText,
          initialSelectedItems,
          key: keyCount
        }
      })
    }
    updateProps()
  }, [option, invalid, disabled, invalidText])

  if (hidden) {
    return null
  }

  return <MultiSelectWithSelectAll {...initialSelected} items={items} />
}

export default withDocument(MultiSelect)
