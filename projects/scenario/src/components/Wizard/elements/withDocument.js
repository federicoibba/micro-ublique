import React, { useEffect, useState } from 'react'
import { uniq, uniqBy, isEmpty, isString, isEqual } from 'lodash'
import withStore from 'hoc/withStore'

import { fetchDataFromDocument } from 'pages/Scenario/actions'

const withDocument = (WrappedComponent) => {
  const wrappedDocument = ({
    option,
    filterBy,
    instances,
    formValues,
    documentType,
    wizard: { instanceId },
    ...props
  }) => {
    const [options, setOptions] = useState(typeof option === 'string' ? [] : option)
    const [queryValues, setQueryValues] = useState(null)
    const [prevQueryString, setPrevQueryString] = useState(null)

    const { type, disabled } = props

    const getQueryString = (obj) => {
      if (isEmpty(obj)) {
        return null
      }

      return new URLSearchParams(obj).toString()
    }

    useEffect(() => {
      if (!isEmpty(documentType)) {
        let params = { type: documentType.toLowerCase() }

        if (filterBy && formValues) {
          for (const key of Object.keys(filterBy)) {
            // BEGIN part to delete after fix
            let value

            if (!filterBy[key].includes('%')) {
              value = filterBy[key]
            }

            // END part to delete

            // TODO Fix with new system
            // const value = filterBy[key].includes('%')
            // ? formValues[filterBy[key].slice(1)]
            // : filterBy[key]

            // If a dependent value (the one with %) is null, the fetch is not performed
            if (value) {
              params = {
                ...params,
                [key]: value
              }
            }
          }
        }

        if (!isEqual(params, queryValues)) {
          setQueryValues(params)
        }
      }
    }, [formValues])

    useEffect(() => {
      if (!disabled && !isEmpty(queryValues) && isString(documentType)) {
        const queryString = getQueryString(queryValues)

        // If there is a filterBy condition and the string is not changed, it doesn't perform a new fetch
        if (filterBy && (!queryString || queryString === prevQueryString)) return

        setOptions([])

        fetchDataFromDocument({
          instanceId,
          params: queryString
        })
          .then((result) => result.body)
          .then((result) => {
            let resultPick = []

            if (type === 'autocomplete') {
              resultPick = uniq(result.map((el) => ({ [option]: el[option] })))
            } else {
              if (props.optionsKey) {
                for (const item of result) {
                  const el = item[props.optionsKey].map((el) => ({
                    label: props.labelKey ? item[props.labelKey] : el[option],
                    value: el[option]
                  }))
                  resultPick.push(...el)
                }
              } else {
                resultPick = uniqBy(
                  result.map(
                    (el) => ({
                      label: props.labelKey ? el[props.labelKey] : el[option],
                      value: el[option]
                    }),
                    'value'
                  )
                )
              }
            }

            setOptions(resultPick)
            setPrevQueryString(queryString)
          })
          .catch((error) => console.log(error))
      }
    }, [disabled, queryValues])

    return <WrappedComponent {...props} option={options} />
  }

  return withStore(React.memo(wrappedDocument), ['wizard', 'instances'])
}

export default withDocument
