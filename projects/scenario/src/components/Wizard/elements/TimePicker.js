import React, { useEffect } from 'react'
import UbliqueTimePicker from '@ublique/components/TimePicker'
import { isUndefined } from 'lodash'

const TimePicker = ({ id, value, label, type, setValue, ...props }) => {
  useEffect(() => {
    if (isUndefined(value) || value === '') {
      setValue(id, '00:00')
    }
  }, [])

  const onChange = (e) => {
    setValue(id, e.target.value)
  }

  return (
    <UbliqueTimePicker
      {...props}
      id={id}
      type={type}
      value={value}
      labelText={label}
      onChange={onChange}
    />
  )
}

export default TimePicker
