import React, { useCallback } from 'react'
import UbliqueToggle from '@ublique/components/Toggle'

const Toggle = ({ t, id, label, value, disabled, hidden, setValue }) => {
  const onToggle = useCallback(
    (eventValue) => {
      setValue(id, eventValue)
    },
    [setValue]
  )

  if (hidden) {
    return null
  }

  return (
    <UbliqueToggle
      id={id}
      labelA={t('NOT')}
      labelB={t('YES')}
      labelText={label}
      onToggle={onToggle}
      disabled={disabled}
      toggled={Boolean(value)}
    />
  )
}

export default Toggle
