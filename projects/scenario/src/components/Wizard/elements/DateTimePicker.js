import React from 'react'
import { isUndefined } from 'lodash'
import DatePicker from '@ublique/components/DatePicker'
import TimePicker from '@ublique/components/TimePicker'

const DateTimePicker = ({
  id,
  value,
  label,
  onBlur,
  invalid,
  disabled,
  setValue,
  invalidText
}) => {
  const onChange = (newValue) => {
    setValue(id, newValue)
  }

  const onChangeDate = (eventOrDates) => {
    const newValue = eventOrDates.target ? eventOrDates.target.value : eventOrDates[0]

    if (isUndefined(newValue)) {
      return
    }

    const newValueDate = new Date(newValue)
    const newDate = new Date(value)

    newDate.setDate(newValueDate.getDate())
    newDate.setMonth(newValueDate.getMonth())
    newDate.setFullYear(newValueDate.getFullYear())

    onChange(newDate.toUTCString())
  }
  const onChangeTime = (e) => {
    const newDate = new Date(value)
    const [hours, minutes] = e.target.value.split(':')

    newDate.setHours(hours)
    newDate.setMinutes(minutes)

    onChange(newDate.toUTCString())
  }

  const getTimeValue = () => {
    if (isUndefined(value)) return '00:00'

    const date = new Date(value)
    const hours = `${date.getHours()}`.padStart(2, '0')
    const minutes = `${date.getMinutes()}`.padStart(2, '0')

    return `${hours}:${minutes}`
  }

  return (
    <div className="datetime-picker" onBlur={onBlur}>
      <span className="bx--label">{label}</span>
      <div className="datetime-picker-selectors">
        <DatePicker
          dateFormat="Y/m/d"
          onChange={onChangeDate}
          datePickerType="single"
          value={value}
        >
          <DatePicker.Input
            id={`date-${id}`}
            invalid={invalid}
            disabled={disabled}
            placeholder="yyyy/mm/dd"
            invalidText=""
            pattern="\d{4}\/\d{1,2}\/\d{1,2}"
          />
        </DatePicker>
        <TimePicker
          id={`time-${id}`}
          type="time"
          invalid={invalid}
          invalidText=""
          disabled={disabled}
          value={getTimeValue()}
          onChange={onChangeTime}
        />
      </div>
      <span className="invalid-text">{invalidText}</span>
    </div>
  )
}

export default DateTimePicker
