import React from 'react'
import withDocument from 'components/Wizard/elements/withDocument'
import UbliqueAutocomplete from '@ublique/components/Autocomplete'

const AutoComplete = ({ t, id, option, label, setValue, ...props }) => {
  const onChange = (value) => setValue(id, value)
  const { hidden, disabled, onBlur, invalid, invalidText } = props

  return (
    <UbliqueAutocomplete
      id={id}
      hidden={hidden}
      onBlur={onBlur}
      invalid={invalid}
      valueKey={option}
      labelKey={option}
      labelText={label}
      disabled={disabled}
      onChange={onChange}
      invalidText={invalidText}
      placeholder={t('Placeholder')}
    />
  )
}

export default withDocument(AutoComplete)
