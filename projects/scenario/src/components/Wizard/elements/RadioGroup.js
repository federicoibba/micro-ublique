import React from 'react'
import RadioButton from '@ublique/components/RadioButton'

const RadioGroup = ({
  t,
  id,
  label,
  value,
  option,
  hidden,
  invalid,
  disabled,
  setValue,
  invalidText
}) => {
  const getId = (prefix, opt) =>
    `${prefix}-${label.replaceAll(' ', '-')}-${opt}`.toLowerCase()

  if (hidden) {
    return null
  }

  return (
    <RadioButton.Form legendText={label}>
      <RadioButton.ButtonGroup
        name={id}
        legend={t(label)}
        valueSelected={value}
        labelPosition="right"
        orientation="vertical"
        onChange={(e) => setValue(id, e)}
      >
        {option.map(({ label, value }) => (
          <RadioButton
            value={value}
            labelText={t(label)}
            key={('key', value)}
            disabled={disabled}
            id={getId('id', value)}
          />
        ))}
      </RadioButton.ButtonGroup>
      {invalid && <span className="bx--form-requirement">{invalidText}</span>}
    </RadioButton.Form>
  )
}

export default RadioGroup
