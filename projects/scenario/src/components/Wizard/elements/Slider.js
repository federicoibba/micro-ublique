import React, { Fragment, useEffect } from 'react'
import { isEmpty } from 'lodash'

import UbliqueSlider from '@ublique/components/Slider'
import { getBoundFromCondition } from 'components/Wizard/helpers/slider'

const Slider = ({ id, value, setValue, invalidText, conditionInfo, ...props }) => {
  const { min, max } = getBoundFromCondition(conditionInfo)
  const { onBlur, hidden, invalid, label, disabled } = props

  const onChange = (e) => setValue(id, e.value)

  useEffect(() => {
    if (isNaN(value) && isEmpty(value)) {
      setValue(id, min)
    }
  }, [])

  const sliderProps = {
    id,
    min,
    max,
    onChange,
    value,
    onBlur,
    hidden,
    disabled,
    labelText: label,
    invalid: invalid || !isEmpty(invalidText)
  }

  if (hidden) {
    return null
  }

  return (
    <Fragment>
      <UbliqueSlider {...sliderProps} inputType="number" />
      {sliderProps.invalid && <span className="invalid-text">{invalidText}</span>}
    </Fragment>
  )
}

export default Slider
