import {
  NO,
  YES,
  NEW_SCENARIO,
  CLONE_SCENARIO,
  ENUM_SCENARIO_TYPE,
  HIDDEN
} from 'components/Wizard/constants'
import { isEmpty, isNull, isString, isUndefined } from 'lodash'
import { getDateTimeISO } from './date'
import { getDataString } from './recap'

const formatDates = (items, inputElements, status) => {
  return Object.keys(items).reduce((acc, key) => {
    const isHidden = status[key]?.some(({ state }) => state === HIDDEN)

    if (isHidden) return acc

    if (['date', 'datetime'].includes(inputElements[key]?.type)) {
      return { ...acc, [key]: getDateTimeISO(items[key]) }
    }

    return { ...acc, [key]: items[key] }
  }, {})
}

export const generateCreationObject = ({
  t,
  status,
  username,
  formData,
  cloneInfo,
  flattenValues,
  inputElements,
  creationValues,
  documentFilters,
  scenarioSummary
}) => {
  const { name, type, creationMode, notes, instanceId } = creationValues

  const filter = {
    documentFilters,
    instance: instanceId
  }

  const summary = scenarioSummary
    .map(({ label, element }) => {
      if (isNull(inputElements[element]) || isUndefined(inputElements[element])) {
        return null
      }

      return {
        key: label,
        value: isString(element)
          ? getDataString({
              t,
              summary: true,
              value: flattenValues[element],
              type: inputElements[element].type
            })
          : element
              .map((item) =>
                getDataString({
                  t,
                  summary: true,
                  value: flattenValues[item],
                  type: inputElements[item].type
                })
              )
              .join(' ')
      }
    })
    .filter((el) => !isEmpty(el))

  const formattedData = formatDates(formData, inputElements, status)

  const data = {
    ...formattedData,
    title: name,
    scenarioName: name,
    scenarioDescription: notes,
    scenarioTypeEnumName: ENUM_SCENARIO_TYPE,
    scenarioTypeEnumValue: type,
    scenarioIsNew: creationMode === NEW_SCENARIO ? YES : NO
  }

  let scenarioManager = {
    name,
    summary,
    username,
    type: {
      name: ENUM_SCENARIO_TYPE,
      value: type
    },
    cloned: creationMode === CLONE_SCENARIO
  }

  if (!isEmpty(cloneInfo)) {
    scenarioManager = {
      ...scenarioManager,
      ...cloneInfo
    }
  }

  const body = {
    filter,
    scenarioManager,
    data
  }

  return body
}
