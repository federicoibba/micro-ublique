import { filter, isEmpty, isUndefined } from 'lodash'

export const getValueState = ({ values, input, clone }) => {
  const { id, type, defaultValue, option } = input
  const value = !isUndefined(defaultValue) ? defaultValue : ''

  if (!isEmpty(clone)) {
    return getValueByClone({ input, clone })
  }

  if (values && !isUndefined(values[id])) {
    return values[id]
  }

  if (type === 'toggle' && value === '') {
    return false
  }

  if ((type === 'date' || type === 'datetime') && value === '') {
    return new Date().toUTCString()
  }

  if (type === 'time' && value === '') {
    return '00:00'
  }

  if (type === 'checkbox' && value === '') {
    return option.reduce(
      (acc, { value }) => ({
        ...acc,
        [value]: false
      }),
      {}
    )
  }

  return value
}

const getValueByClone = ({
  input: { id, type, name, documentType, valueKey, labelKey, option },
  clone
}) => {
  let value = clone[id]

  if (documentType) {
    // This is the case of an enum with [id].Name, [id].Value
    if (name) {
      value = {
        label: clone[`${id}Value`],
        value: {
          [`${id}Name`]: clone[`${id}Name`],
          [`${id}Value`]: clone[`${id}Value`]
        }
      }
    } else {
      // In this case, there is a corrispondency between id in config and id in scenarioBody
      const objectToClone = clone[id]

      if (objectToClone) {
        if (type === 'multiselect') {
          value = []

          for (const item of objectToClone) {
            const objItem = item[valueKey]

            value.push({
              label: labelKey ? objItem[labelKey] : objItem[option],
              value: { [valueKey]: objItem[option] }
            })
          }
        } else {
          value = {
            label: labelKey ? objectToClone[labelKey] : objectToClone[option],
            value: objectToClone[option]
          }
        }
      }
    }
  } else {
    // TODO add checkbox
    if (type === 'select') {
      value = option.find((item) => item.value === value)
    }
    if (type === 'date' || (type === 'datetime' && typeof value === 'number')) {
      value = new Date(value).toUTCString()
    }
  }

  return value
}

// It searches for at least an element in conditionStates that it's invalid
export const isFormValid = (conditionStates) =>
  Object.keys(conditionStates).reduce(
    (flag, elementId) =>
      flag &&
      isEmpty(
        filter(conditionStates[elementId], (element) => element.state === 'invalid')
      ),
    true
  )

export const chunks = (arr, size = 2) => {
  const arrayWithEmpties = []

  // It creates empty spaces for new row
  for (let i = 0; i < arr.length; i++) {
    arrayWithEmpties.push(arr[i])

    if (arr[i + 1] && arr[i + 1].newRow) {
      arrayWithEmpties.push({ id: 'empty' })
    }
  }

  // Creates the couples and remove the empty columns
  return arrayWithEmpties
    .map((x, i) => i % size === 0 && arrayWithEmpties.slice(i, i + size))
    .filter((x) => x)
    .map((x) => x && x.filter((el) => el.id !== 'empty'))
}
