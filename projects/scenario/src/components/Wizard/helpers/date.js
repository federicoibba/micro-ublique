export const getDateISO = (date, separator = '-') => {
  const event = new Date(date)
  const year = event.getFullYear()
  const month = `${event.getMonth() + 1}`.padStart(2, '0')
  const day = `${event.getDate()}`.padStart(2, '0')

  return [year, month, day].join(separator)
}

export const getDateTimeISO = (date) => {
  const event = new Date(date)
  const minutes = `${event.getMinutes()}`.padStart(2, '0')
  const hours = `${event.getHours()}`.padStart(2, '0')
  const seconds = `${event.getSeconds()}`.padStart(2, '0')

  return `${getDateISO(date)} ${hours}:${minutes}:${seconds}`
}
