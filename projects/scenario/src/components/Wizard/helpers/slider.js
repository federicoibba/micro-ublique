import { isUndefined } from 'lodash'

export const getBoundFromCondition = (condition) => {
  const { minInvalid, maxInvalid } = condition.states.invalid.states

  const min = isUndefined(minInvalid.when.$.lt)
    ? minInvalid.when.$.lte
    : minInvalid.when.$.lt
  const max = isUndefined(maxInvalid.when.$.gt)
    ? maxInvalid.when.$.gte
    : maxInvalid.when.$.gt

  return { min, max }
}
