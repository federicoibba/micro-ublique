import { isUndefined } from 'lodash'

export const getInvalidText = ({ conditions, invalid, tInvalid }) => {
  if (!isUndefined(conditions) && invalid) {
    const { invalidText } = conditions.find((rule) => !isUndefined(rule.invalidText))
    return tInvalid(invalidText)
  }

  return ''
}
