import React from 'react'
import { getDateISO, getDateTimeISO } from './date'
import { map, isNaN, isUndefined, pickBy, keys } from 'lodash'

const typeToString = {
  radio: ({ t, value }) => t(value.label),
  toggle: ({ t, value }) => (value ? t('YES') : t('NOT')),
  select: ({ t, value }) => t(value.label) || value.toString(),
  multiselect: ({ value }) => map(value, 'label').join(', '),
  checkbox: ({ value }) => keys(pickBy(value, (el) => el)).join(', '),
  datetime: ({ value }) =>
    !isNaN(Date.parse(value)) ? getDateTimeISO(value.toString()) : '',
  date: ({ value }) => (!isNaN(Date.parse(value)) ? getDateISO(value.toString()) : '')
}

export const getDataString = ({
  t,
  type,
  value,
  elementId,
  recapElement,
  summary = false
}) => {
  if (type === 'custom' && !summary && recapElement) {
    const Element = recapElement

    return <Element id={elementId} value={value} />
  }

  const stringify = typeToString[type]

  if (isUndefined(stringify)) {
    return value
  }

  return stringify({ t, value })
}
