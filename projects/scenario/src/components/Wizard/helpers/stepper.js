import { isEmpty, isObject, isArray, pickBy } from 'lodash'

export const formatValues = (values) => {
  let formatted = { ...values }

  for (const element of Object.keys(formatted)) {
    if (!isEmpty(formatted[element]) && isObject(formatted[element])) {
      if (isArray(formatted[element])) {
        formatted[element] = formatted[element].map((el) => el.value)
      }

      if (!isEmpty(formatted[element].value)) {
        if (isArray(formatted[element].value)) {
          formatted[element] = formatted[element].value
        } else {
          if (isObject(formatted[element].value)) {
            formatted = {
              ...formatted,
              ...formatted[element].value
            }
            delete formatted[element]
          } else {
            formatted[element] = formatted[element].value
          }
        }
      }
    }
  }

  return pickBy(formatted, (item) => item !== '')
}
