import { NEW_SCENARIO } from 'components/Wizard/constants'

export const initialState = {
  // Wizard mode info
  scenarioType: '',
  creationMode: NEW_SCENARIO,

  // Instance info
  clone: {},
  instanceId: null,

  // Step info
  inputElements: {}, // config for each input element
  values: {},
  formData: {},
  status: {}
}
