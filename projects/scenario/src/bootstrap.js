import React from 'react'
import ReactDOM from 'react-dom'
import 'ubliqueComponentsStyle'
import App from './App'

import { createMemoryHistory, createBrowserHistory } from 'history'
import createStore from 'utils/hoc/createStore'
import { globalStore } from './services/state'

const UbliqueContainer = createStore(App, {
  ...globalStore
})

export const mount = (el, { onNavigate, defaultHistory, initialPath }) => {
  const history =
    defaultHistory ||
    createMemoryHistory({
      initialEntries: [initialPath]
    })

  if (onNavigate) {
    history.listen(onNavigate)
  }

  ReactDOM.render(
    <React.StrictMode>
      <UbliqueContainer
        history={history}
        basename={!defaultHistory && initialPath ? '/scenario' : ''}
      />
    </React.StrictMode>,
    el
  )

  return {
    onParentNavigate({ pathname: nextPathname }) {
      const { pathname } = history.location

      if (pathname !== nextPathname) {
        history.push(nextPathname)
      }
    },
    unmount() {
      ReactDOM.unmountComponentAtNode(el)
    }
  }
}

if (process.env.REACT_APP_RUNNING_MODE === 'isolation') {
  mount(document.getElementById('ublique_micro_scenario'), {
    defaultHistory: createBrowserHistory({ basename: '/scenario' })
  })
}
/*
if (process.env.NODE_ENV === 'development') {
  const devRoot = document.querySelector('#ublique_micro_scenario');

  if (devRoot) {
    mount(devRoot, { defaultHistory: createBrowserHistory() });
  }
}
 */
