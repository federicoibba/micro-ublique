const webpack = require('webpack')
const path = require('path')
const InterpolateHtmlPlugin = require('interpolate-html-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const paths = require('./paths')
const getClientEnvironment = require('./env')

const env = getClientEnvironment(paths.publicUrlOrPath.slice(0, -1))

module.exports = {
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-react',
              ['@babel/preset-env', { targets: 'defaults' }]
            ],
            plugins: [
              '@babel/plugin-transform-runtime',
              [
                'babel-plugin-named-asset-import',
                {
                  loaderMap: {
                    svg: {
                      ReactComponent: '@svgr/webpack?-svgo,+titleProp,+ref![path]'
                    }
                  }
                }
              ]
            ]
          }
        }
      },
      {
        test: /\.m?js/,
        resolve: {
          fullySpecified: false
        }
      },
      {
        test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
        type: 'asset/resource'
      },
      {
        test: /\.svg/,
        type: 'asset/inline'
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin(env.stringified),
    new InterpolateHtmlPlugin(HtmlWebpackPlugin, env.raw),
    new HtmlWebpackPlugin({
      template: './public/index.html'
      // favicon: './public/favicon.ico',
      // filename: './index.html'
    })
  ],
  resolve: {
    alias: {
      App: path.resolve(__dirname, '../src/App'),
      ext: path.resolve(__dirname, '../src/ext'),
      assets: path.resolve(__dirname, '../src/assets'),
      utils: path.resolve(__dirname, '../src/utils'),
      hoc: path.resolve(__dirname, '../src/utils/hoc'),
      hooks: path.resolve(__dirname, '../src/utils/hooks'),
      context: path.resolve(__dirname, '../src/utils/context'),
      pages: path.resolve(__dirname, '../src/pages'),
      components: path.resolve(__dirname, '../src/components'),
      services: path.resolve(__dirname, '../src/services'),
      i18n: path.resolve(__dirname, '../src/i18n'),
      config: path.resolve(__dirname, '../src/config'),
      auth: path.resolve(__dirname, '../src/services/auth'),
      state: path.resolve(__dirname, '../src/services/state'),
      api: path.resolve(__dirname, '../src/services/api'),
      // Don't import ublique css when the app is consumed by a container
      ubliqueComponentsStyle: path.resolve(
        __dirname,
        `../src/index.${process.env.REACT_APP_RUNNING_MODE || 'microfe'}.scss`
      )
    }
  }
}
