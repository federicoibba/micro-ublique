const { WebpackManifestPlugin } = require('webpack-manifest-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const ProgressPlugin = require('progress-webpack-plugin')
const { merge } = require('webpack-merge')

const commonConfig = require('./webpack.common')
const paths = require('./paths')

const MiniCssExtractLoader = {
  loader: MiniCssExtractPlugin.loader,
  options: paths.publicUrlOrPath.startsWith('.') ? { publicPath: '../../' } : {}
}

const prodConfig = {
  mode: 'production',
  output: {
    path: paths.appBuild,
    filename: 'static/js/[name].[contenthash:8].js',
    chunkFilename: 'static/js/[name].[contenthash:8].chunk.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [MiniCssExtractLoader, 'css-loader']
      },
      {
        test: /\.(scss|sass)$/,
        use: [MiniCssExtractLoader, 'css-loader', 'sass-loader']
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'static/css/[name].[contenthash:8].css',
      chunkFilename: 'static/css/[name].[contenthash:8].chunk.css'
    }),
    new ProgressPlugin(),
    new WebpackManifestPlugin({
      // fileName: 'asset-manifest.json',
      publicPath: paths.publicUrlOrPath
    })
  ]
}

module.exports = merge(commonConfig, prodConfig)
